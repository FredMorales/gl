// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Runtime.InteropServices;

namespace gl
{
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glAccum(uint op, float value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glAlphaFunc(uint func, float r);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glAreTexturesResident(int n, IntPtr textures, IntPtr residences);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glArrayElement(int i);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBegin(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindTexture(uint target, uint texture);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBitmap(int width, int height, float xorig, float yorig, float xmove, float ymove, IntPtr bitmap);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendFunc(uint sfactor, uint dfactor);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCallList(uint list);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCallLists(int n, uint type, IntPtr lists);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClear(uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearAccum(float red, float green, float blue, float alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearColor(float red, float green, float blue, float alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearDepth(double depth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearIndex(float c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearStencil(int s);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClipPlane(uint plane, IntPtr equation);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3b(sbyte red, sbyte green, sbyte blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3bv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3d(double red, double green, double blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3f(float red, float green, float blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3i(int red, int green, int blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3s(short red, short green, short blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3ub(byte red, byte green, byte blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3ubv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3ui(uint red, uint green, uint blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3uiv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3us(ushort red, ushort green, ushort blue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor3usv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4b(sbyte red, sbyte green, sbyte blue, sbyte alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4bv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4d(double red, double green, double blue, double alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4f(float red, float green, float blue, float alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4i(int red, int green, int blue, int alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4s(short red, short green, short blue, short alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4ub(byte red, byte green, byte blue, byte alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4ubv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4ui(uint red, uint green, uint blue, uint alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4uiv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4us(ushort red, ushort green, ushort blue, ushort alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColor4usv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColorMask(byte red, byte green, byte blue, byte alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColorMaterial(uint face, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColorPointer(int size, uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyPixels(int x, int y, int width, int height, uint type);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTexImage1D(uint target, int level, uint internalFormat, int x, int y, int width, int border);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTexImage2D(uint target, int level, uint internalFormat, int x, int y, int width, int height, int border);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTexSubImage1D(uint target, int level, int xoffset, int x, int y, int width);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTexSubImage2D(uint target, int level, int xoffset, int yoffset, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCullFace(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteLists(uint list, int range);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteTextures(int n, IntPtr textures);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthFunc(uint func);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthMask(byte flag);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthRange(double zNear, double zFar);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDisable(uint cap);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDisableClientState(uint array);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawArrays(uint mode, int first, int count);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawBuffer(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElements(uint mode, int count, uint type, IntPtr indices);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawPixels(int width, int height, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEdgeFlag(byte flag);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEdgeFlagPointer(int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEdgeFlagv(IntPtr flag);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnable(uint cap);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnableClientState(uint array);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnd();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEndList();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord1d(double u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord1dv(IntPtr u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord1f(float u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord1fv(IntPtr u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord2d(double u, double v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord2dv(IntPtr u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord2f(float u, float v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalCoord2fv(IntPtr u);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalMesh1(uint mode, int i1, int i2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalMesh2(uint mode, int i1, int i2, int j1, int j2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalPoint1(int i);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEvalPoint2(int i, int j);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFeedbackBuffer(int size, uint type, IntPtr buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFinish();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFlush();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFogf(uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFogfv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFogi(uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFogiv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFrontFace(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFrustum(double left, double right, double bottom, double top, double zNear, double zFar);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGenLists(int range);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenTextures(int n, IntPtr textures);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBooleanv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetClipPlane(uint plane, IntPtr equation);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetDoublev(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetError();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetFloatv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetIntegerv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetLightfv(uint light, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetLightiv(uint light, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMapdv(uint target, uint query, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMapfv(uint target, uint query, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMapiv(uint target, uint query, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMaterialfv(uint face, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMaterialiv(uint face, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetPixelMapfv(uint map, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetPixelMapuiv(uint map, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetPixelMapusv(uint map, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetPointerv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetPolygonStipple(IntPtr mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glGetString(uint name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexEnvfv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexEnviv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexGendv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexGenfv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexGeniv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexImage(uint target, int level, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexLevelParameterfv(uint target, int level, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexLevelParameteriv(uint target, int level, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexParameterfv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexParameteriv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glHint(uint target, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexMask(uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexPointer(uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexd(double c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexdv(IntPtr c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexf(float c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexfv(IntPtr c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexi(int c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexiv(IntPtr c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexs(short c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexsv(IntPtr c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexub(byte c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glIndexubv(IntPtr c);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInitNames();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInterleavedArrays(uint format, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsEnabled(uint cap);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsList(uint list);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsTexture(uint texture);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightModelf(uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightModelfv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightModeli(uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightModeliv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightf(uint light, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightfv(uint light, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLighti(uint light, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLightiv(uint light, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLineStipple(int factor, ushort pattern);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLineWidth(float width);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glListBase(uint b);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLoadIdentity();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLoadMatrixd(IntPtr m);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLoadMatrixf(IntPtr m);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLoadName(uint name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLogicOp(uint opcode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMap1d(uint target, double u1, double u2, int stride, int order, IntPtr points);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMap1f(uint target, float u1, float u2, int stride, int order, IntPtr points);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMap2d(uint target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, IntPtr points);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMap2f(uint target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, IntPtr points);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMapGrid1d(int un, double u1, double u2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMapGrid1f(int un, float u1, float u2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMapGrid2d(int un, double u1, double u2, int vn, double v1, double v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMapGrid2f(int un, float u1, float u2, int vn, float v1, float v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMaterialf(uint face, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMaterialfv(uint face, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMateriali(uint face, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMaterialiv(uint face, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMatrixMode(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultMatrixd(IntPtr m);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultMatrixf(IntPtr m);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNewList(uint list, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3b(sbyte nx, sbyte ny, sbyte nz);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3bv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3d(double nx, double ny, double nz);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3f(float nx, float ny, float nz);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3i(int nx, int ny, int nz);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3s(short nx, short ny, short nz);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormal3sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNormalPointer(uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glOrtho(double left, double right, double bottom, double top, double zNear, double zFar);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPassThrough(float token);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelMapfv(uint map, int mapsize, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelMapuiv(uint map, int mapsize, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelMapusv(uint map, int mapsize, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelStoref(uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelStorei(uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelTransferf(uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelTransferi(uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPixelZoom(float xfactor, float yfactor);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPointSize(float size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPolygonMode(uint face, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPolygonOffset(float factor, float units);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPolygonStipple(IntPtr mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPopAttrib();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPopClientAttrib();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPopMatrix();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPopName();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPrioritizeTextures(int n, IntPtr textures, IntPtr priorities);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPushAttrib(uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPushClientAttrib(uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPushMatrix();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPushName(uint name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2d(double x, double y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2f(float x, float y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2i(int x, int y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2s(short x, short y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos2sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3d(double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3f(float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3i(int x, int y, int z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3s(short x, short y, short z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos3sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4d(double x, double y, double z, double w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4f(float x, float y, float z, float w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4i(int x, int y, int z, int w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4s(short x, short y, short z, short w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRasterPos4sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glReadBuffer(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glReadPixels(int x, int y, int width, int height, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectd(double x1, double y1, double x2, double y2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectdv(IntPtr v1, IntPtr v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectf(float x1, float y1, float x2, float y2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectfv(IntPtr v1, IntPtr v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRecti(int x1, int y1, int x2, int y2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectiv(IntPtr v1, IntPtr v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRects(short x1, short y1, short x2, short y2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRectsv(IntPtr v1, IntPtr v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glRenderMode(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRotated(double angle, double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRotatef(float angle, float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScaled(double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScalef(float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScissor(int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSelectBuffer(int size, IntPtr buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glShadeModel(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilFunc(uint func, int r, uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilMask(uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilOp(uint fail, uint zfail, uint zpass);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1d(double s);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1f(float s);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1i(int s);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1s(short s);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord1sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2d(double s, double t);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2f(float s, float t);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2i(int s, int t);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2s(short s, short t);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord2sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3d(double s, double t, double r);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3f(float s, float t, float r);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3i(int s, int t, int r);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3s(short s, short t, short r);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord3sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4d(double s, double t, double r, double q);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4f(float s, float t, float r, float q);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4i(int s, int t, int r, int q);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4s(short s, short t, short r, short q);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoord4sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexCoordPointer(int size, uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexEnvf(uint target, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexEnvfv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexEnvi(uint target, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexEnviv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGend(uint coord, uint pname, double param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGendv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGenf(uint coord, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGenfv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGeni(uint coord, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexGeniv(uint coord, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexImage1D(uint target, int level, int internalformat, int width, int border, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexImage2D(uint target, int level, int internalformat, int width, int height, int border, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameterf(uint target, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameterfv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameteri(uint target, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameteriv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexSubImage1D(uint target, int level, int xoffset, int width, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexSubImage2D(uint target, int level, int xoffset, int yoffset, int width, int height, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTranslated(double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTranslatef(float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2d(double x, double y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2f(float x, float y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2i(int x, int y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2s(short x, short y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex2sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3d(double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3f(float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3i(int x, int y, int z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3s(short x, short y, short z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex3sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4d(double x, double y, double z, double w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4dv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4f(float x, float y, float z, float w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4fv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4i(int x, int y, int z, int w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4iv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4s(short x, short y, short z, short w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertex4sv(IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexPointer(int size, uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glViewport(int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawRangeElements(uint mode, uint start, uint end, int count, uint type, IntPtr indices);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexImage3D(uint target, int level, int internalformat, int width, int height, int depth, int border, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexSubImage3D(uint target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTexSubImage3D(uint target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glActiveTexture(uint texture);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSampleCoverage(float value, byte invert);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexImage3D(uint target, int level, uint internalformat, int width, int height, int depth, int border, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexImage2D(uint target, int level, uint internalformat, int width, int height, int border, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexImage1D(uint target, int level, uint internalformat, int width, int border, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexSubImage3D(uint target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexSubImage2D(uint target, int level, int xoffset, int yoffset, int width, int height, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTexSubImage1D(uint target, int level, int xoffset, int width, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetCompressedTexImage(uint target, int level, IntPtr img);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendFuncSeparate(uint sfactorRGB, uint dfactorRGB, uint sfactorAlpha, uint dfactorAlpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawArrays(uint mode, IntPtr first, IntPtr count, int drawcount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawElements(uint mode, IntPtr count, uint type, IntPtr indices, int drawcount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPointParameterf(uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPointParameterfv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPointParameteri(uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPointParameteriv(uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendColor(float red, float green, float blue, float alpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendEquation(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenQueries(int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteQueries(int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsQuery(uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBeginQuery(uint target, uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEndQuery(uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryiv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryObjectiv(uint id, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryObjectuiv(uint id, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindBuffer(uint target, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteBuffers(int n, IntPtr buffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenBuffers(int n, IntPtr buffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsBuffer(uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBufferData(uint target, IntPtr size, IntPtr data, uint usage);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBufferSubData(uint target, IntPtr offset, IntPtr size, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBufferSubData(uint target, IntPtr offset, IntPtr size, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glMapBuffer(uint target, uint access);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glUnmapBuffer(uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBufferParameteriv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBufferPointerv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendEquationSeparate(uint modeRGB, uint modeAlpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawBuffers(int n, IntPtr bufs);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilOpSeparate(uint face, uint sfail, uint dpfail, uint dppass);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilFuncSeparate(uint face, uint func, int r, uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glStencilMaskSeparate(uint face, uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glAttachShader(uint program, uint shader);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindAttribLocation(uint program, uint index, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompileShader(uint shader);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glCreateProgram();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glCreateShader(uint type);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteProgram(uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteShader(uint shader);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDetachShader(uint program, uint shader);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDisableVertexAttribArray(uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnableVertexAttribArray(uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveAttrib(uint program, uint index, int bufSize, IntPtr length, IntPtr size, IntPtr type, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveUniform(uint program, uint index, int bufSize, IntPtr length, IntPtr size, IntPtr type, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetAttachedShaders(uint program, int maxCount, IntPtr count, IntPtr shaders);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetAttribLocation(uint program, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramiv(uint program, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramInfoLog(uint program, int bufSize, IntPtr length, IntPtr infoLog);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetShaderiv(uint shader, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetShaderInfoLog(uint shader, int bufSize, IntPtr length, IntPtr infoLog);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetShaderSource(uint shader, int bufSize, IntPtr length, IntPtr source);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetUniformLocation(uint program, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformfv(uint program, int location, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformiv(uint program, int location, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribdv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribfv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribiv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribPointerv(uint index, uint pname, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsProgram(uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsShader(uint shader);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glLinkProgram(uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glShaderSource(uint shader, int count, IntPtr str, IntPtr length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUseProgram(uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1f(int location, float v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2f(int location, float v0, float v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3f(int location, float v0, float v1, float v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4f(int location, float v0, float v1, float v2, float v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1i(int location, int v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2i(int location, int v0, int v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3i(int location, int v0, int v1, int v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4i(int location, int v0, int v1, int v2, int v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1fv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2fv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3fv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4fv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1iv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2iv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3iv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4iv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glValidateProgram(uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1d(uint index, double x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1f(uint index, float x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1fv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1s(uint index, short x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib1sv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2d(uint index, double x, double y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2f(uint index, float x, float y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2fv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2s(uint index, short x, short y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib2sv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3d(uint index, double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3f(uint index, float x, float y, float z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3fv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3s(uint index, short x, short y, short z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib3sv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nbv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Niv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nsv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nub(uint index, byte x, byte y, byte z, byte w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nubv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nuiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4Nusv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4bv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4d(uint index, double x, double y, double z, double w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4f(uint index, float x, float y, float z, float w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4fv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4iv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4s(uint index, short x, short y, short z, short w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4sv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4ubv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4uiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttrib4usv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribPointer(uint index, int size, uint type, byte normalized, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2x3fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3x2fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2x4fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4x2fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3x4fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4x3fv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glColorMaski(uint index, byte r, byte g, byte b, byte a);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBooleani_v(uint target, uint index, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetIntegeri_v(uint target, uint index, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnablei(uint target, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDisablei(uint target, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsEnabledi(uint target, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBeginTransformFeedback(uint primitiveMode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEndTransformFeedback();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindBufferRange(uint target, uint index, uint buffer, IntPtr offset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindBufferBase(uint target, uint index, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTransformFeedbackVaryings(uint program, int count, IntPtr varyings, uint bufferMode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTransformFeedbackVarying(uint program, uint index, int bufSize, IntPtr length, IntPtr size, IntPtr type, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClampColor(uint target, uint clamp);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBeginConditionalRender(uint id, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEndConditionalRender();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribIPointer(uint index, int size, uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribIiv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribIuiv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI1i(uint index, int x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI2i(uint index, int x, int y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI3i(uint index, int x, int y, int z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4i(uint index, int x, int y, int z, int w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI1ui(uint index, uint x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI2ui(uint index, uint x, uint y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI3ui(uint index, uint x, uint y, uint z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4ui(uint index, uint x, uint y, uint z, uint w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI1iv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI2iv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI3iv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4iv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI1uiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI2uiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI3uiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4uiv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4bv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4sv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4ubv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribI4usv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformuiv(uint program, int location, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindFragDataLocation(uint program, uint color, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetFragDataLocation(uint program, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1ui(int location, uint v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2ui(int location, uint v0, uint v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3ui(int location, uint v0, uint v1, uint v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4ui(int location, uint v0, uint v1, uint v2, uint v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1uiv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2uiv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3uiv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4uiv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameterIiv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexParameterIuiv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexParameterIiv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTexParameterIuiv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferiv(uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferuiv(uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferfv(uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferfi(uint buffer, int drawbuffer, float depth, int stencil);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glGetStringi(uint name, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsRenderbuffer(uint renderbuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindRenderbuffer(uint target, uint renderbuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteRenderbuffers(int n, IntPtr renderbuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenRenderbuffers(int n, IntPtr renderbuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRenderbufferStorage(uint target, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetRenderbufferParameteriv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsFramebuffer(uint framebuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindFramebuffer(uint target, uint framebuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteFramebuffers(int n, IntPtr framebuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenFramebuffers(int n, IntPtr framebuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glCheckFramebufferStatus(uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferTexture1D(uint target, uint attachment, uint textarget, uint texture, int level);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferTexture2D(uint target, uint attachment, uint textarget, uint texture, int level);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferTexture3D(uint target, uint attachment, uint textarget, uint texture, int level, int zoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferRenderbuffer(uint target, uint attachment, uint renderbuffertarget, uint renderbuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetFramebufferAttachmentParameteriv(uint target, uint attachment, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenerateMipmap(uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, uint mask, uint filter);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glRenderbufferStorageMultisample(uint target, int samples, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferTextureLayer(uint target, uint attachment, uint texture, int level, int layer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glMapBufferRange(uint target, IntPtr offset, IntPtr length, uint access);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFlushMappedBufferRange(uint target, IntPtr offset, IntPtr length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindVertexArray(uint array);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteVertexArrays(int n, IntPtr arrays);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenVertexArrays(int n, IntPtr arrays);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsVertexArray(uint array);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawArraysInstanced(uint mode, int first, int count, int instancecount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsInstanced(uint mode, int count, uint type, IntPtr indices, int instancecount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexBuffer(uint target, uint internalformat, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPrimitiveRestartIndex(uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyBufferSubData(uint readTarget, uint writeTarget, IntPtr readOffset, IntPtr writeOffset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformIndices(uint program, int uniformCount, IntPtr uniformNames, IntPtr uniformIndices);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveUniformsiv(uint program, int uniformCount, IntPtr uniformIndices, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveUniformName(uint program, uint uniformIndex, int bufSize, IntPtr length, IntPtr uniformName);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetUniformBlockIndex(uint program, IntPtr uniformBlockName);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveUniformBlockiv(uint program, uint uniformBlockIndex, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveUniformBlockName(uint program, uint uniformBlockIndex, int bufSize, IntPtr length, IntPtr uniformBlockName);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformBlockBinding(uint program, uint uniformBlockIndex, uint uniformBlockBinding);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsBaseVertex(uint mode, int count, uint type, IntPtr indices, int basevertex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawRangeElementsBaseVertex(uint mode, uint start, uint end, int count, uint type, IntPtr indices, int basevertex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsInstancedBaseVertex(uint mode, int count, uint type, IntPtr indices, int instancecount, int basevertex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawElementsBaseVertex(uint mode, IntPtr count, uint type, IntPtr indices, int drawcount, IntPtr basevertex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProvokingVertex(uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glFenceSync(uint condition, uint flags);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsSync(IntPtr sync);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteSync(IntPtr sync);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glClientWaitSync(IntPtr sync, uint flags, ulong timeout);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glWaitSync(IntPtr sync, uint flags, ulong timeout);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetInteger64v(uint pname, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetSynciv(IntPtr sync, uint pname, int bufSize, IntPtr length, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetInteger64i_v(uint target, uint index, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetBufferParameteri64v(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferTexture(uint target, uint attachment, uint texture, int level);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexImage2DMultisample(uint target, int samples, uint internalformat, int width, int height, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexImage3DMultisample(uint target, int samples, uint internalformat, int width, int height, int depth, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetMultisamplefv(uint pname, uint index, IntPtr val);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSampleMaski(uint maskNumber, uint mask);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindFragDataLocationIndexed(uint program, uint colorNumber, uint index, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetFragDataIndex(uint program, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenSamplers(int count, IntPtr samplers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteSamplers(int count, IntPtr samplers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsSampler(uint sampler);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindSampler(uint unit, uint sampler);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameteri(uint sampler, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameteriv(uint sampler, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameterf(uint sampler, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameterfv(uint sampler, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameterIiv(uint sampler, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSamplerParameterIuiv(uint sampler, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetSamplerParameteriv(uint sampler, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetSamplerParameterIiv(uint sampler, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetSamplerParameterfv(uint sampler, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetSamplerParameterIuiv(uint sampler, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glQueryCounter(uint id, uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryObjecti64v(uint id, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryObjectui64v(uint id, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribDivisor(uint index, uint divisor);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP1ui(uint index, uint type, byte normalized, uint value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP1uiv(uint index, uint type, byte normalized, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP2ui(uint index, uint type, byte normalized, uint value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP2uiv(uint index, uint type, byte normalized, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP3ui(uint index, uint type, byte normalized, uint value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP3uiv(uint index, uint type, byte normalized, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP4ui(uint index, uint type, byte normalized, uint value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribP4uiv(uint index, uint type, byte normalized, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMinSampleShading(float value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendEquationi(uint buf, uint mode);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendEquationSeparatei(uint buf, uint modeRGB, uint modeAlpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendFunci(uint buf, uint src, uint dst);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlendFuncSeparatei(uint buf, uint srcRGB, uint dstRGB, uint srcAlpha, uint dstAlpha);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawArraysIndirect(uint mode, IntPtr indirect);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsIndirect(uint mode, uint type, IntPtr indirect);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1d(int location, double x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2d(int location, double x, double y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3d(int location, double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4d(int location, double x, double y, double z, double w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform1dv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform2dv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform3dv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniform4dv(int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2x3dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix2x4dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3x2dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix3x4dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4x2dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformMatrix4x3dv(int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformdv(uint program, int location, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetSubroutineUniformLocation(uint program, uint shadertype, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetSubroutineIndex(uint program, uint shadertype, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveSubroutineUniformiv(uint program, uint shadertype, uint index, uint pname, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveSubroutineUniformName(uint program, uint shadertype, uint index, int bufsize, IntPtr length, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveSubroutineName(uint program, uint shadertype, uint index, int bufsize, IntPtr length, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUniformSubroutinesuiv(uint shadertype, int count, IntPtr indices);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetUniformSubroutineuiv(uint shadertype, int location, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramStageiv(uint program, uint shadertype, uint pname, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPatchParameteri(uint pname, int value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPatchParameterfv(uint pname, IntPtr values);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindTransformFeedback(uint target, uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteTransformFeedbacks(int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenTransformFeedbacks(int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsTransformFeedback(uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPauseTransformFeedback();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glResumeTransformFeedback();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawTransformFeedback(uint mode, uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawTransformFeedbackStream(uint mode, uint id, uint stream);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBeginQueryIndexed(uint target, uint index, uint id);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEndQueryIndexed(uint target, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryIndexediv(uint target, uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glReleaseShaderCompiler();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glShaderBinary(int count, IntPtr shaders, uint binaryformat, IntPtr binary, int length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetShaderPrecisionFormat(uint shadertype, uint precisiontype, IntPtr range, IntPtr precision);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthRangef(float n, float f);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearDepthf(float d);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramBinary(uint program, int bufSize, IntPtr length, IntPtr binaryFormat, IntPtr binary);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramBinary(uint program, uint binaryFormat, IntPtr binary, int length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramParameteri(uint program, uint pname, int value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glUseProgramStages(uint pipeline, uint stages, uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glActiveShaderProgram(uint pipeline, uint program);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glCreateShaderProgramv(uint type, int count, IntPtr strings);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindProgramPipeline(uint pipeline);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDeleteProgramPipelines(int n, IntPtr pipelines);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenProgramPipelines(int n, IntPtr pipelines);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glIsProgramPipeline(uint pipeline);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramPipelineiv(uint pipeline, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1i(uint program, int location, int v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1iv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1f(uint program, int location, float v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1fv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1d(uint program, int location, double v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1dv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1ui(uint program, int location, uint v0);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform1uiv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2i(uint program, int location, int v0, int v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2iv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2f(uint program, int location, float v0, float v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2fv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2d(uint program, int location, double v0, double v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2dv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2ui(uint program, int location, uint v0, uint v1);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform2uiv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3i(uint program, int location, int v0, int v1, int v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3iv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3f(uint program, int location, float v0, float v1, float v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3fv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3d(uint program, int location, double v0, double v1, double v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3dv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3ui(uint program, int location, uint v0, uint v1, uint v2);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform3uiv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4i(uint program, int location, int v0, int v1, int v2, int v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4iv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4f(uint program, int location, float v0, float v1, float v2, float v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4fv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4d(uint program, int location, double v0, double v1, double v2, double v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4dv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4ui(uint program, int location, uint v0, uint v1, uint v2, uint v3);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniform4uiv(uint program, int location, int count, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2x3fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3x2fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2x4fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4x2fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3x4fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4x3fv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2x3dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3x2dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix2x4dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4x2dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix3x4dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glProgramUniformMatrix4x3dv(uint program, int location, int count, byte transpose, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glValidateProgramPipeline(uint pipeline);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramPipelineInfoLog(uint pipeline, int bufSize, IntPtr length, IntPtr infoLog);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL1d(uint index, double x);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL2d(uint index, double x, double y);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL3d(uint index, double x, double y, double z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL4d(uint index, double x, double y, double z, double w);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL1dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL2dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL3dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribL4dv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribLPointer(uint index, int size, uint type, int stride, IntPtr pointer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexAttribLdv(uint index, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glViewportArrayv(uint first, int count, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glViewportIndexedf(uint index, float x, float y, float w, float h);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glViewportIndexedfv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScissorArrayv(uint first, int count, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScissorIndexed(uint index, int left, int bottom, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glScissorIndexedv(uint index, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthRangeArrayv(uint first, int count, IntPtr v);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDepthRangeIndexed(uint index, double n, double f);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetFloati_v(uint target, uint index, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetDoublei_v(uint target, uint index, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawArraysInstancedBaseInstance(uint mode, int first, int count, int instancecount, uint baseinstance);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsInstancedBaseInstance(uint mode, int count, uint type, IntPtr indices, int instancecount, uint baseinstance);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawElementsInstancedBaseVertexBaseInstance(uint mode, int count, uint type, IntPtr indices, int instancecount, int basevertex, uint baseinstance);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetInternalformativ(uint target, uint internalformat, uint pname, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetActiveAtomicCounterBufferiv(uint program, uint bufferIndex, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindImageTexture(uint unit, uint texture, int level, byte layered, int layer, uint access, uint format);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMemoryBarrier(uint barriers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexStorage1D(uint target, int levels, uint internalformat, int width);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexStorage2D(uint target, int levels, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexStorage3D(uint target, int levels, uint internalformat, int width, int height, int depth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawTransformFeedbackInstanced(uint mode, uint id, int instancecount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDrawTransformFeedbackStreamInstanced(uint mode, uint id, uint stream, int instancecount);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferData(uint target, uint internalformat, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearBufferSubData(uint target, uint internalformat, IntPtr offset, IntPtr size, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDispatchCompute(uint num_groups_x, uint num_groups_y, uint num_groups_z);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDispatchComputeIndirect(IntPtr indirect);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyImageSubData(uint srcName, uint srcTarget, int srcLevel, int srcX, int srcY, int srcZ, uint dstName, uint dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFramebufferParameteri(uint target, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetFramebufferParameteriv(uint target, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetInternalformati64v(uint target, uint internalformat, uint pname, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateTexSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateTexImage(uint texture, int level);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateBufferSubData(uint buffer, IntPtr offset, IntPtr length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateBufferData(uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateFramebuffer(uint target, int numAttachments, IntPtr attachments);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateSubFramebuffer(uint target, int numAttachments, IntPtr attachments, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawArraysIndirect(uint mode, IntPtr indirect, int drawcount, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawElementsIndirect(uint mode, uint type, IntPtr indirect, int drawcount, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramInterfaceiv(uint program, uint programInterface, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetProgramResourceIndex(uint program, uint programInterface, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramResourceName(uint program, uint programInterface, uint index, int bufSize, IntPtr length, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetProgramResourceiv(uint program, uint programInterface, uint index, int propCount, IntPtr props, int bufSize, IntPtr length, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetProgramResourceLocation(uint program, uint programInterface, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate int Call_glGetProgramResourceLocationIndex(uint program, uint programInterface, IntPtr name);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glShaderStorageBlockBinding(uint program, uint storageBlockIndex, uint storageBlockBinding);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexBufferRange(uint target, uint internalformat, uint buffer, IntPtr offset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexStorage2DMultisample(uint target, int samples, uint internalformat, int width, int height, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTexStorage3DMultisample(uint target, int samples, uint internalformat, int width, int height, int depth, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureView(uint texture, uint target, uint origtexture, uint internalformat, uint minlevel, uint numlevels, uint minlayer, uint numlayers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindVertexBuffer(uint bindingindex, uint buffer, IntPtr offset, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribFormat(uint attribindex, int size, uint type, byte normalized, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribIFormat(uint attribindex, int size, uint type, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribLFormat(uint attribindex, int size, uint type, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexAttribBinding(uint attribindex, uint bindingindex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexBindingDivisor(uint bindingindex, uint divisor);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDebugMessageControl(uint source, uint type, uint severity, int count, IntPtr ids, byte enabled);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDebugMessageInsert(uint source, uint type, uint id, uint severity, int length, IntPtr buf);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDebugMessageCallback(IntPtr callback, IntPtr userParam);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetDebugMessageLog(uint count, int bufSize, IntPtr sources, IntPtr types, IntPtr ids, IntPtr severities, IntPtr lengths, IntPtr messageLog);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPushDebugGroup(uint source, uint id, int length, IntPtr message);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPopDebugGroup();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glObjectLabel(uint identifier, uint name, int length, IntPtr label);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetObjectLabel(uint identifier, uint name, int bufSize, IntPtr length, IntPtr label);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glObjectPtrLabel(IntPtr ptr, int length, IntPtr label);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetObjectPtrLabel(IntPtr ptr, int bufSize, IntPtr length, IntPtr label);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBufferStorage(uint target, IntPtr size, IntPtr data, uint flags);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearTexImage(uint texture, int level, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearTexSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindBuffersBase(uint target, uint first, int count, IntPtr buffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindBuffersRange(uint target, uint first, int count, IntPtr buffers, IntPtr offsets, IntPtr sizes);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindTextures(uint first, int count, IntPtr textures);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindSamplers(uint first, int count, IntPtr samplers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindImageTextures(uint first, int count, IntPtr textures);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindVertexBuffers(uint first, int count, IntPtr buffers, IntPtr offsets, IntPtr strides);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClipControl(uint origin, uint depth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateTransformFeedbacks(int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTransformFeedbackBufferBase(uint xfb, uint index, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTransformFeedbackBufferRange(uint xfb, uint index, uint buffer, IntPtr offset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTransformFeedbackiv(uint xfb, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTransformFeedbacki_v(uint xfb, uint pname, uint index, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTransformFeedbacki64_v(uint xfb, uint pname, uint index, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateBuffers(int n, IntPtr buffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedBufferStorage(uint buffer, IntPtr size, IntPtr data, uint flags);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedBufferData(uint buffer, IntPtr size, IntPtr data, uint usage);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedBufferSubData(uint buffer, IntPtr offset, IntPtr size, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyNamedBufferSubData(uint readBuffer, uint writeBuffer, IntPtr readOffset, IntPtr writeOffset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedBufferData(uint buffer, uint internalformat, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedBufferSubData(uint buffer, uint internalformat, IntPtr offset, IntPtr size, uint format, uint type, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glMapNamedBuffer(uint buffer, uint access);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate IntPtr Call_glMapNamedBufferRange(uint buffer, IntPtr offset, IntPtr length, uint access);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate byte Call_glUnmapNamedBuffer(uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glFlushMappedNamedBufferRange(uint buffer, IntPtr offset, IntPtr length);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedBufferParameteriv(uint buffer, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedBufferParameteri64v(uint buffer, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedBufferPointerv(uint buffer, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedBufferSubData(uint buffer, IntPtr offset, IntPtr size, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateFramebuffers(int n, IntPtr framebuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferRenderbuffer(uint framebuffer, uint attachment, uint renderbuffertarget, uint renderbuffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferParameteri(uint framebuffer, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferTexture(uint framebuffer, uint attachment, uint texture, int level);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferTextureLayer(uint framebuffer, uint attachment, uint texture, int level, int layer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferDrawBuffer(uint framebuffer, uint buf);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferDrawBuffers(uint framebuffer, int n, IntPtr bufs);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedFramebufferReadBuffer(uint framebuffer, uint src);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateNamedFramebufferData(uint framebuffer, int numAttachments, IntPtr attachments);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glInvalidateNamedFramebufferSubData(uint framebuffer, int numAttachments, IntPtr attachments, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedFramebufferiv(uint framebuffer, uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedFramebufferuiv(uint framebuffer, uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedFramebufferfv(uint framebuffer, uint buffer, int drawbuffer, IntPtr value);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glClearNamedFramebufferfi(uint framebuffer, uint buffer, int drawbuffer, float depth, int stencil);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBlitNamedFramebuffer(uint readFramebuffer, uint drawFramebuffer, int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, uint mask, uint filter);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glCheckNamedFramebufferStatus(uint framebuffer, uint target);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedFramebufferParameteriv(uint framebuffer, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedFramebufferAttachmentParameteriv(uint framebuffer, uint attachment, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateRenderbuffers(int n, IntPtr renderbuffers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedRenderbufferStorage(uint renderbuffer, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glNamedRenderbufferStorageMultisample(uint renderbuffer, int samples, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetNamedRenderbufferParameteriv(uint renderbuffer, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateTextures(uint target, int n, IntPtr textures);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureBuffer(uint texture, uint internalformat, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureBufferRange(uint texture, uint internalformat, uint buffer, IntPtr offset, IntPtr size);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureStorage1D(uint texture, int levels, uint internalformat, int width);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureStorage2D(uint texture, int levels, uint internalformat, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureStorage3D(uint texture, int levels, uint internalformat, int width, int height, int depth);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureStorage2DMultisample(uint texture, int samples, uint internalformat, int width, int height, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureStorage3DMultisample(uint texture, int samples, uint internalformat, int width, int height, int depth, byte fixedsamplelocations);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureSubImage1D(uint texture, int level, int xoffset, int width, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int width, int height, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, uint type, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTextureSubImage1D(uint texture, int level, int xoffset, int width, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int width, int height, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCompressedTextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, int imageSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTextureSubImage1D(uint texture, int level, int xoffset, int x, int y, int width);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCopyTextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameterf(uint texture, uint pname, float param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameterfv(uint texture, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameteri(uint texture, uint pname, int param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameterIiv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameterIuiv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureParameteriv(uint texture, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGenerateTextureMipmap(uint texture);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glBindTextureUnit(uint unit, uint texture);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureImage(uint texture, int level, uint format, uint type, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetCompressedTextureImage(uint texture, int level, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureLevelParameterfv(uint texture, int level, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureLevelParameteriv(uint texture, int level, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureParameterfv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureParameterIiv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureParameterIuiv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureParameteriv(uint texture, uint pname, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateVertexArrays(int n, IntPtr arrays);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glDisableVertexArrayAttrib(uint vaobj, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glEnableVertexArrayAttrib(uint vaobj, uint index);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayElementBuffer(uint vaobj, uint buffer);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayVertexBuffer(uint vaobj, uint bindingindex, uint buffer, IntPtr offset, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayVertexBuffers(uint vaobj, uint first, int count, IntPtr buffers, IntPtr offsets, IntPtr strides);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayAttribBinding(uint vaobj, uint attribindex, uint bindingindex);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayAttribFormat(uint vaobj, uint attribindex, int size, uint type, byte normalized, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayAttribIFormat(uint vaobj, uint attribindex, int size, uint type, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayAttribLFormat(uint vaobj, uint attribindex, int size, uint type, uint relativeoffset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glVertexArrayBindingDivisor(uint vaobj, uint bindingindex, uint divisor);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexArrayiv(uint vaobj, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexArrayIndexediv(uint vaobj, uint index, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetVertexArrayIndexed64iv(uint vaobj, uint index, uint pname, IntPtr param);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateSamplers(int n, IntPtr samplers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateProgramPipelines(int n, IntPtr pipelines);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glCreateQueries(uint target, int n, IntPtr ids);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryBufferObjecti64v(uint id, uint buffer, uint pname, IntPtr offset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryBufferObjectiv(uint id, uint buffer, uint pname, IntPtr offset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryBufferObjectui64v(uint id, uint buffer, uint pname, IntPtr offset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetQueryBufferObjectuiv(uint id, uint buffer, uint pname, IntPtr offset);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMemoryBarrierByRegion(uint barriers);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetTextureSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, uint format, uint type, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetCompressedTextureSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate uint Call_glGetGraphicsResetStatus();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnCompressedTexImage(uint target, int lod, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnTexImage(uint target, int level, uint format, uint type, int bufSize, IntPtr pixels);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnUniformdv(uint program, int location, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnUniformfv(uint program, int location, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnUniformiv(uint program, int location, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glGetnUniformuiv(uint program, int location, int bufSize, IntPtr p);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glReadnPixels(int x, int y, int width, int height, uint format, uint type, int bufSize, IntPtr data);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glTextureBarrier();
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glSpecializeShader(uint shader, IntPtr pEntryPoint, uint numSpecializationConstants, IntPtr pConstantIndex, IntPtr pConstantValue);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawArraysIndirectCount(uint mode, IntPtr indirect, IntPtr drawcount, int maxdrawcount, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glMultiDrawElementsIndirectCount(uint mode, uint type, IntPtr indirect, IntPtr drawcount, int maxdrawcount, int stride);
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void Call_glPolygonOffsetClamp(float factor, float units, float clamp);

	public static class gl
	{
		public static uint GL_DEPTH_BUFFER_BIT = 0x00000100;
		public static uint GL_STENCIL_BUFFER_BIT = 0x00000400;
		public static uint GL_COLOR_BUFFER_BIT = 0x00004000;
		public static uint GL_FALSE = 0;
		public static uint GL_TRUE = 1;
		public static uint GL_POINTS = 0x0000;
		public static uint GL_LINES = 0x0001;
		public static uint GL_LINE_LOOP = 0x0002;
		public static uint GL_LINE_STRIP = 0x0003;
		public static uint GL_TRIANGLES = 0x0004;
		public static uint GL_TRIANGLE_STRIP = 0x0005;
		public static uint GL_TRIANGLE_FAN = 0x0006;
		public static uint GL_QUADS = 0x0007;
		public static uint GL_NEVER = 0x0200;
		public static uint GL_LESS = 0x0201;
		public static uint GL_EQUAL = 0x0202;
		public static uint GL_LEQUAL = 0x0203;
		public static uint GL_GREATER = 0x0204;
		public static uint GL_NOTEQUAL = 0x0205;
		public static uint GL_GEQUAL = 0x0206;
		public static uint GL_ALWAYS = 0x0207;
		public static uint GL_ZERO = 0;
		public static uint GL_ONE = 1;
		public static uint GL_SRC_COLOR = 0x0300;
		public static uint GL_ONE_MINUS_SRC_COLOR = 0x0301;
		public static uint GL_SRC_ALPHA = 0x0302;
		public static uint GL_ONE_MINUS_SRC_ALPHA = 0x0303;
		public static uint GL_DST_ALPHA = 0x0304;
		public static uint GL_ONE_MINUS_DST_ALPHA = 0x0305;
		public static uint GL_DST_COLOR = 0x0306;
		public static uint GL_ONE_MINUS_DST_COLOR = 0x0307;
		public static uint GL_SRC_ALPHA_SATURATE = 0x0308;
		public static uint GL_NONE = 0;
		public static uint GL_FRONT_LEFT = 0x0400;
		public static uint GL_FRONT_RIGHT = 0x0401;
		public static uint GL_BACK_LEFT = 0x0402;
		public static uint GL_BACK_RIGHT = 0x0403;
		public static uint GL_FRONT = 0x0404;
		public static uint GL_BACK = 0x0405;
		public static uint GL_LEFT = 0x0406;
		public static uint GL_RIGHT = 0x0407;
		public static uint GL_FRONT_AND_BACK = 0x0408;
		public static uint GL_NO_ERROR = 0;
		public static uint GL_INVALID_ENUM = 0x0500;
		public static uint GL_INVALID_VALUE = 0x0501;
		public static uint GL_INVALID_OPERATION = 0x0502;
		public static uint GL_OUT_OF_MEMORY = 0x0505;
		public static uint GL_CW = 0x0900;
		public static uint GL_CCW = 0x0901;
		public static uint GL_POINT_SIZE = 0x0B11;
		public static uint GL_POINT_SIZE_RANGE = 0x0B12;
		public static uint GL_POINT_SIZE_GRANULARITY = 0x0B13;
		public static uint GL_LINE_SMOOTH = 0x0B20;
		public static uint GL_LINE_WIDTH = 0x0B21;
		public static uint GL_LINE_WIDTH_RANGE = 0x0B22;
		public static uint GL_LINE_WIDTH_GRANULARITY = 0x0B23;
		public static uint GL_POLYGON_MODE = 0x0B40;
		public static uint GL_POLYGON_SMOOTH = 0x0B41;
		public static uint GL_CULL_FACE = 0x0B44;
		public static uint GL_CULL_FACE_MODE = 0x0B45;
		public static uint GL_FRONT_FACE = 0x0B46;
		public static uint GL_DEPTH_RANGE = 0x0B70;
		public static uint GL_DEPTH_TEST = 0x0B71;
		public static uint GL_DEPTH_WRITEMASK = 0x0B72;
		public static uint GL_DEPTH_CLEAR_VALUE = 0x0B73;
		public static uint GL_DEPTH_FUNC = 0x0B74;
		public static uint GL_STENCIL_TEST = 0x0B90;
		public static uint GL_STENCIL_CLEAR_VALUE = 0x0B91;
		public static uint GL_STENCIL_FUNC = 0x0B92;
		public static uint GL_STENCIL_VALUE_MASK = 0x0B93;
		public static uint GL_STENCIL_FAIL = 0x0B94;
		public static uint GL_STENCIL_PASS_DEPTH_FAIL = 0x0B95;
		public static uint GL_STENCIL_PASS_DEPTH_PASS = 0x0B96;
		public static uint GL_STENCIL_REF = 0x0B97;
		public static uint GL_STENCIL_WRITEMASK = 0x0B98;
		public static uint GL_VIEWPORT = 0x0BA2;
		public static uint GL_DITHER = 0x0BD0;
		public static uint GL_BLEND_DST = 0x0BE0;
		public static uint GL_BLEND_SRC = 0x0BE1;
		public static uint GL_BLEND = 0x0BE2;
		public static uint GL_LOGIC_OP_MODE = 0x0BF0;
		public static uint GL_DRAW_BUFFER = 0x0C01;
		public static uint GL_READ_BUFFER = 0x0C02;
		public static uint GL_SCISSOR_BOX = 0x0C10;
		public static uint GL_SCISSOR_TEST = 0x0C11;
		public static uint GL_COLOR_CLEAR_VALUE = 0x0C22;
		public static uint GL_COLOR_WRITEMASK = 0x0C23;
		public static uint GL_DOUBLEBUFFER = 0x0C32;
		public static uint GL_STEREO = 0x0C33;
		public static uint GL_LINE_SMOOTH_HINT = 0x0C52;
		public static uint GL_POLYGON_SMOOTH_HINT = 0x0C53;
		public static uint GL_UNPACK_SWAP_BYTES = 0x0CF0;
		public static uint GL_UNPACK_LSB_FIRST = 0x0CF1;
		public static uint GL_UNPACK_ROW_LENGTH = 0x0CF2;
		public static uint GL_UNPACK_SKIP_ROWS = 0x0CF3;
		public static uint GL_UNPACK_SKIP_PIXELS = 0x0CF4;
		public static uint GL_UNPACK_ALIGNMENT = 0x0CF5;
		public static uint GL_PACK_SWAP_BYTES = 0x0D00;
		public static uint GL_PACK_LSB_FIRST = 0x0D01;
		public static uint GL_PACK_ROW_LENGTH = 0x0D02;
		public static uint GL_PACK_SKIP_ROWS = 0x0D03;
		public static uint GL_PACK_SKIP_PIXELS = 0x0D04;
		public static uint GL_PACK_ALIGNMENT = 0x0D05;
		public static uint GL_MAX_TEXTURE_SIZE = 0x0D33;
		public static uint GL_MAX_VIEWPORT_DIMS = 0x0D3A;
		public static uint GL_SUBPIXEL_BITS = 0x0D50;
		public static uint GL_TEXTURE_1D = 0x0DE0;
		public static uint GL_TEXTURE_2D = 0x0DE1;
		public static uint GL_TEXTURE_WIDTH = 0x1000;
		public static uint GL_TEXTURE_HEIGHT = 0x1001;
		public static uint GL_TEXTURE_BORDER_COLOR = 0x1004;
		public static uint GL_DONT_CARE = 0x1100;
		public static uint GL_FASTEST = 0x1101;
		public static uint GL_NICEST = 0x1102;
		public static uint GL_BYTE = 0x1400;
		public static uint GL_UNSIGNED_BYTE = 0x1401;
		public static uint GL_SHORT = 0x1402;
		public static uint GL_UNSIGNED_SHORT = 0x1403;
		public static uint GL_INT = 0x1404;
		public static uint GL_UNSIGNED_INT = 0x1405;
		public static uint GL_FLOAT = 0x1406;
		public static uint GL_STACK_OVERFLOW = 0x0503;
		public static uint GL_STACK_UNDERFLOW = 0x0504;
		public static uint GL_CLEAR = 0x1500;
		public static uint GL_AND = 0x1501;
		public static uint GL_AND_REVERSE = 0x1502;
		public static uint GL_COPY = 0x1503;
		public static uint GL_AND_INVERTED = 0x1504;
		public static uint GL_NOOP = 0x1505;
		public static uint GL_XOR = 0x1506;
		public static uint GL_OR = 0x1507;
		public static uint GL_NOR = 0x1508;
		public static uint GL_EQUIV = 0x1509;
		public static uint GL_INVERT = 0x150A;
		public static uint GL_OR_REVERSE = 0x150B;
		public static uint GL_COPY_INVERTED = 0x150C;
		public static uint GL_OR_INVERTED = 0x150D;
		public static uint GL_NAND = 0x150E;
		public static uint GL_SET = 0x150F;
		public static uint GL_TEXTURE = 0x1702;
		public static uint GL_COLOR = 0x1800;
		public static uint GL_DEPTH = 0x1801;
		public static uint GL_STENCIL = 0x1802;
		public static uint GL_STENCIL_INDEX = 0x1901;
		public static uint GL_DEPTH_COMPONENT = 0x1902;
		public static uint GL_RED = 0x1903;
		public static uint GL_GREEN = 0x1904;
		public static uint GL_BLUE = 0x1905;
		public static uint GL_ALPHA = 0x1906;
		public static uint GL_RGB = 0x1907;
		public static uint GL_RGBA = 0x1908;
		public static uint GL_POINT = 0x1B00;
		public static uint GL_LINE = 0x1B01;
		public static uint GL_FILL = 0x1B02;
		public static uint GL_KEEP = 0x1E00;
		public static uint GL_REPLACE = 0x1E01;
		public static uint GL_INCR = 0x1E02;
		public static uint GL_DECR = 0x1E03;
		public static uint GL_VENDOR = 0x1F00;
		public static uint GL_RENDERER = 0x1F01;
		public static uint GL_VERSION = 0x1F02;
		public static uint GL_EXTENSIONS = 0x1F03;
		public static uint GL_NEAREST = 0x2600;
		public static uint GL_LINEAR = 0x2601;
		public static uint GL_NEAREST_MIPMAP_NEAREST = 0x2700;
		public static uint GL_LINEAR_MIPMAP_NEAREST = 0x2701;
		public static uint GL_NEAREST_MIPMAP_LINEAR = 0x2702;
		public static uint GL_LINEAR_MIPMAP_LINEAR = 0x2703;
		public static uint GL_TEXTURE_MAG_FILTER = 0x2800;
		public static uint GL_TEXTURE_MIN_FILTER = 0x2801;
		public static uint GL_TEXTURE_WRAP_S = 0x2802;
		public static uint GL_TEXTURE_WRAP_T = 0x2803;
		public static uint GL_REPEAT = 0x2901;
		public static uint GL_ACCUM = 0x0100;
		public static uint GL_LOAD = 0x0101;
		public static uint GL_RETURN = 0x0102;
		public static uint GL_MULT = 0x0103;
		public static uint GL_ADD = 0x0104;
		public static uint GL_CURRENT_BIT = 0x00000001;
		public static uint GL_POINT_BIT = 0x00000002;
		public static uint GL_LINE_BIT = 0x00000004;
		public static uint GL_POLYGON_BIT = 0x00000008;
		public static uint GL_POLYGON_STIPPLE_BIT = 0x00000010;
		public static uint GL_PIXEL_MODE_BIT = 0x00000020;
		public static uint GL_LIGHTING_BIT = 0x00000040;
		public static uint GL_FOG_BIT = 0x00000080;
		public static uint GL_ACCUM_BUFFER_BIT = 0x00000200;
		public static uint GL_VIEWPORT_BIT = 0x00000800;
		public static uint GL_TRANSFORM_BIT = 0x00001000;
		public static uint GL_ENABLE_BIT = 0x00002000;
		public static uint GL_HINT_BIT = 0x00008000;
		public static uint GL_EVAL_BIT = 0x00010000;
		public static uint GL_LIST_BIT = 0x00020000;
		public static uint GL_TEXTURE_BIT = 0x00040000;
		public static uint GL_SCISSOR_BIT = 0x00080000;
		public static uint GL_ALL_ATTRIB_BITS = 0x000fffff;
		public static uint GL_QUAD_STRIP = 0x0008;
		public static uint GL_POLYGON = 0x0009;
		public static uint GL_CLIP_PLANE0 = 0x3000;
		public static uint GL_CLIP_PLANE1 = 0x3001;
		public static uint GL_CLIP_PLANE2 = 0x3002;
		public static uint GL_CLIP_PLANE3 = 0x3003;
		public static uint GL_CLIP_PLANE4 = 0x3004;
		public static uint GL_CLIP_PLANE5 = 0x3005;
		public static uint GL_2_BYTES = 0x1407;
		public static uint GL_3_BYTES = 0x1408;
		public static uint GL_4_BYTES = 0x1409;
		public static uint GL_DOUBLE = 0x140A;
		public static uint GL_AUX0 = 0x0409;
		public static uint GL_AUX1 = 0x040A;
		public static uint GL_AUX2 = 0x040B;
		public static uint GL_AUX3 = 0x040C;
		public static uint GL_2D = 0x0600;
		public static uint GL_3D = 0x0601;
		public static uint GL_3D_COLOR = 0x0602;
		public static uint GL_3D_COLOR_TEXTURE = 0x0603;
		public static uint GL_4D_COLOR_TEXTURE = 0x0604;
		public static uint GL_PASS_THROUGH_TOKEN = 0x0700;
		public static uint GL_POINT_TOKEN = 0x0701;
		public static uint GL_LINE_TOKEN = 0x0702;
		public static uint GL_POLYGON_TOKEN = 0x0703;
		public static uint GL_BITMAP_TOKEN = 0x0704;
		public static uint GL_DRAW_PIXEL_TOKEN = 0x0705;
		public static uint GL_COPY_PIXEL_TOKEN = 0x0706;
		public static uint GL_LINE_RESET_TOKEN = 0x0707;
		public static uint GL_EXP = 0x0800;
		public static uint GL_EXP2 = 0x0801;
		public static uint GL_COEFF = 0x0A00;
		public static uint GL_ORDER = 0x0A01;
		public static uint GL_DOMAIN = 0x0A02;
		public static uint GL_CURRENT_COLOR = 0x0B00;
		public static uint GL_CURRENT_INDEX = 0x0B01;
		public static uint GL_CURRENT_NORMAL = 0x0B02;
		public static uint GL_CURRENT_TEXTURE_COORDS = 0x0B03;
		public static uint GL_CURRENT_RASTER_COLOR = 0x0B04;
		public static uint GL_CURRENT_RASTER_INDEX = 0x0B05;
		public static uint GL_CURRENT_RASTER_TEXTURE_COORDS = 0x0B06;
		public static uint GL_CURRENT_RASTER_POSITION = 0x0B07;
		public static uint GL_CURRENT_RASTER_POSITION_VALID = 0x0B08;
		public static uint GL_CURRENT_RASTER_DISTANCE = 0x0B09;
		public static uint GL_POINT_SMOOTH = 0x0B10;
		public static uint GL_LINE_STIPPLE = 0x0B24;
		public static uint GL_LINE_STIPPLE_PATTERN = 0x0B25;
		public static uint GL_LINE_STIPPLE_REPEAT = 0x0B26;
		public static uint GL_LIST_MODE = 0x0B30;
		public static uint GL_MAX_LIST_NESTING = 0x0B31;
		public static uint GL_LIST_BASE = 0x0B32;
		public static uint GL_LIST_INDEX = 0x0B33;
		public static uint GL_POLYGON_STIPPLE = 0x0B42;
		public static uint GL_EDGE_FLAG = 0x0B43;
		public static uint GL_LIGHTING = 0x0B50;
		public static uint GL_LIGHT_MODEL_LOCAL_VIEWER = 0x0B51;
		public static uint GL_LIGHT_MODEL_TWO_SIDE = 0x0B52;
		public static uint GL_LIGHT_MODEL_AMBIENT = 0x0B53;
		public static uint GL_SHADE_MODEL = 0x0B54;
		public static uint GL_COLOR_MATERIAL_FACE = 0x0B55;
		public static uint GL_COLOR_MATERIAL_PARAMETER = 0x0B56;
		public static uint GL_COLOR_MATERIAL = 0x0B57;
		public static uint GL_FOG = 0x0B60;
		public static uint GL_FOG_INDEX = 0x0B61;
		public static uint GL_FOG_DENSITY = 0x0B62;
		public static uint GL_FOG_START = 0x0B63;
		public static uint GL_FOG_END = 0x0B64;
		public static uint GL_FOG_MODE = 0x0B65;
		public static uint GL_FOG_COLOR = 0x0B66;
		public static uint GL_ACCUM_CLEAR_VALUE = 0x0B80;
		public static uint GL_MATRIX_MODE = 0x0BA0;
		public static uint GL_NORMALIZE = 0x0BA1;
		public static uint GL_MODELVIEW_STACK_DEPTH = 0x0BA3;
		public static uint GL_PROJECTION_STACK_DEPTH = 0x0BA4;
		public static uint GL_TEXTURE_STACK_DEPTH = 0x0BA5;
		public static uint GL_MODELVIEW_MATRIX = 0x0BA6;
		public static uint GL_PROJECTION_MATRIX = 0x0BA7;
		public static uint GL_TEXTURE_MATRIX = 0x0BA8;
		public static uint GL_ATTRIB_STACK_DEPTH = 0x0BB0;
		public static uint GL_CLIENT_ATTRIB_STACK_DEPTH = 0x0BB1;
		public static uint GL_ALPHA_TEST = 0x0BC0;
		public static uint GL_ALPHA_TEST_FUNC = 0x0BC1;
		public static uint GL_ALPHA_TEST_REF = 0x0BC2;
		public static uint GL_INDEX_LOGIC_OP = 0x0BF1;
		public static uint GL_COLOR_LOGIC_OP = 0x0BF2;
		public static uint GL_AUX_BUFFERS = 0x0C00;
		public static uint GL_INDEX_CLEAR_VALUE = 0x0C20;
		public static uint GL_INDEX_WRITEMASK = 0x0C21;
		public static uint GL_INDEX_MODE = 0x0C30;
		public static uint GL_RGBA_MODE = 0x0C31;
		public static uint GL_RENDER_MODE = 0x0C40;
		public static uint GL_PERSPECTIVE_CORRECTION_HINT = 0x0C50;
		public static uint GL_POINT_SMOOTH_HINT = 0x0C51;
		public static uint GL_FOG_HINT = 0x0C54;
		public static uint GL_TEXTURE_GEN_S = 0x0C60;
		public static uint GL_TEXTURE_GEN_T = 0x0C61;
		public static uint GL_TEXTURE_GEN_R = 0x0C62;
		public static uint GL_TEXTURE_GEN_Q = 0x0C63;
		public static uint GL_PIXEL_MAP_I_TO_I = 0x0C70;
		public static uint GL_PIXEL_MAP_S_TO_S = 0x0C71;
		public static uint GL_PIXEL_MAP_I_TO_R = 0x0C72;
		public static uint GL_PIXEL_MAP_I_TO_G = 0x0C73;
		public static uint GL_PIXEL_MAP_I_TO_B = 0x0C74;
		public static uint GL_PIXEL_MAP_I_TO_A = 0x0C75;
		public static uint GL_PIXEL_MAP_R_TO_R = 0x0C76;
		public static uint GL_PIXEL_MAP_G_TO_G = 0x0C77;
		public static uint GL_PIXEL_MAP_B_TO_B = 0x0C78;
		public static uint GL_PIXEL_MAP_A_TO_A = 0x0C79;
		public static uint GL_PIXEL_MAP_I_TO_I_SIZE = 0x0CB0;
		public static uint GL_PIXEL_MAP_S_TO_S_SIZE = 0x0CB1;
		public static uint GL_PIXEL_MAP_I_TO_R_SIZE = 0x0CB2;
		public static uint GL_PIXEL_MAP_I_TO_G_SIZE = 0x0CB3;
		public static uint GL_PIXEL_MAP_I_TO_B_SIZE = 0x0CB4;
		public static uint GL_PIXEL_MAP_I_TO_A_SIZE = 0x0CB5;
		public static uint GL_PIXEL_MAP_R_TO_R_SIZE = 0x0CB6;
		public static uint GL_PIXEL_MAP_G_TO_G_SIZE = 0x0CB7;
		public static uint GL_PIXEL_MAP_B_TO_B_SIZE = 0x0CB8;
		public static uint GL_PIXEL_MAP_A_TO_A_SIZE = 0x0CB9;
		public static uint GL_MAP_COLOR = 0x0D10;
		public static uint GL_MAP_STENCIL = 0x0D11;
		public static uint GL_INDEX_SHIFT = 0x0D12;
		public static uint GL_INDEX_OFFSET = 0x0D13;
		public static uint GL_RED_SCALE = 0x0D14;
		public static uint GL_RED_BIAS = 0x0D15;
		public static uint GL_ZOOM_X = 0x0D16;
		public static uint GL_ZOOM_Y = 0x0D17;
		public static uint GL_GREEN_SCALE = 0x0D18;
		public static uint GL_GREEN_BIAS = 0x0D19;
		public static uint GL_BLUE_SCALE = 0x0D1A;
		public static uint GL_BLUE_BIAS = 0x0D1B;
		public static uint GL_ALPHA_SCALE = 0x0D1C;
		public static uint GL_ALPHA_BIAS = 0x0D1D;
		public static uint GL_DEPTH_SCALE = 0x0D1E;
		public static uint GL_DEPTH_BIAS = 0x0D1F;
		public static uint GL_MAX_EVAL_ORDER = 0x0D30;
		public static uint GL_MAX_LIGHTS = 0x0D31;
		public static uint GL_MAX_CLIP_PLANES = 0x0D32;
		public static uint GL_MAX_PIXEL_MAP_TABLE = 0x0D34;
		public static uint GL_MAX_ATTRIB_STACK_DEPTH = 0x0D35;
		public static uint GL_MAX_MODELVIEW_STACK_DEPTH = 0x0D36;
		public static uint GL_MAX_NAME_STACK_DEPTH = 0x0D37;
		public static uint GL_MAX_PROJECTION_STACK_DEPTH = 0x0D38;
		public static uint GL_MAX_TEXTURE_STACK_DEPTH = 0x0D39;
		public static uint GL_MAX_CLIENT_ATTRIB_STACK_DEPTH = 0x0D3B;
		public static uint GL_INDEX_BITS = 0x0D51;
		public static uint GL_RED_BITS = 0x0D52;
		public static uint GL_GREEN_BITS = 0x0D53;
		public static uint GL_BLUE_BITS = 0x0D54;
		public static uint GL_ALPHA_BITS = 0x0D55;
		public static uint GL_DEPTH_BITS = 0x0D56;
		public static uint GL_STENCIL_BITS = 0x0D57;
		public static uint GL_ACCUM_RED_BITS = 0x0D58;
		public static uint GL_ACCUM_GREEN_BITS = 0x0D59;
		public static uint GL_ACCUM_BLUE_BITS = 0x0D5A;
		public static uint GL_ACCUM_ALPHA_BITS = 0x0D5B;
		public static uint GL_NAME_STACK_DEPTH = 0x0D70;
		public static uint GL_AUTO_NORMAL = 0x0D80;
		public static uint GL_MAP1_COLOR_4 = 0x0D90;
		public static uint GL_MAP1_INDEX = 0x0D91;
		public static uint GL_MAP1_NORMAL = 0x0D92;
		public static uint GL_MAP1_TEXTURE_COORD_1 = 0x0D93;
		public static uint GL_MAP1_TEXTURE_COORD_2 = 0x0D94;
		public static uint GL_MAP1_TEXTURE_COORD_3 = 0x0D95;
		public static uint GL_MAP1_TEXTURE_COORD_4 = 0x0D96;
		public static uint GL_MAP1_VERTEX_3 = 0x0D97;
		public static uint GL_MAP1_VERTEX_4 = 0x0D98;
		public static uint GL_MAP2_COLOR_4 = 0x0DB0;
		public static uint GL_MAP2_INDEX = 0x0DB1;
		public static uint GL_MAP2_NORMAL = 0x0DB2;
		public static uint GL_MAP2_TEXTURE_COORD_1 = 0x0DB3;
		public static uint GL_MAP2_TEXTURE_COORD_2 = 0x0DB4;
		public static uint GL_MAP2_TEXTURE_COORD_3 = 0x0DB5;
		public static uint GL_MAP2_TEXTURE_COORD_4 = 0x0DB6;
		public static uint GL_MAP2_VERTEX_3 = 0x0DB7;
		public static uint GL_MAP2_VERTEX_4 = 0x0DB8;
		public static uint GL_MAP1_GRID_DOMAIN = 0x0DD0;
		public static uint GL_MAP1_GRID_SEGMENTS = 0x0DD1;
		public static uint GL_MAP2_GRID_DOMAIN = 0x0DD2;
		public static uint GL_MAP2_GRID_SEGMENTS = 0x0DD3;
		public static uint GL_FEEDBACK_BUFFER_POINTER = 0x0DF0;
		public static uint GL_FEEDBACK_BUFFER_SIZE = 0x0DF1;
		public static uint GL_FEEDBACK_BUFFER_TYPE = 0x0DF2;
		public static uint GL_SELECTION_BUFFER_POINTER = 0x0DF3;
		public static uint GL_SELECTION_BUFFER_SIZE = 0x0DF4;
		public static uint GL_TEXTURE_INTERNAL_FORMAT = 0x1003;
		public static uint GL_TEXTURE_BORDER = 0x1005;
		public static uint GL_LIGHT0 = 0x4000;
		public static uint GL_LIGHT1 = 0x4001;
		public static uint GL_LIGHT2 = 0x4002;
		public static uint GL_LIGHT3 = 0x4003;
		public static uint GL_LIGHT4 = 0x4004;
		public static uint GL_LIGHT5 = 0x4005;
		public static uint GL_LIGHT6 = 0x4006;
		public static uint GL_LIGHT7 = 0x4007;
		public static uint GL_AMBIENT = 0x1200;
		public static uint GL_DIFFUSE = 0x1201;
		public static uint GL_SPECULAR = 0x1202;
		public static uint GL_POSITION = 0x1203;
		public static uint GL_SPOT_DIRECTION = 0x1204;
		public static uint GL_SPOT_EXPONENT = 0x1205;
		public static uint GL_SPOT_CUTOFF = 0x1206;
		public static uint GL_CONSTANT_ATTENUATION = 0x1207;
		public static uint GL_LINEAR_ATTENUATION = 0x1208;
		public static uint GL_QUADRATIC_ATTENUATION = 0x1209;
		public static uint GL_COMPILE = 0x1300;
		public static uint GL_COMPILE_AND_EXECUTE = 0x1301;
		public static uint GL_EMISSION = 0x1600;
		public static uint GL_SHININESS = 0x1601;
		public static uint GL_AMBIENT_AND_DIFFUSE = 0x1602;
		public static uint GL_COLOR_INDEXES = 0x1603;
		public static uint GL_MODELVIEW = 0x1700;
		public static uint GL_PROJECTION = 0x1701;
		public static uint GL_COLOR_INDEX = 0x1900;
		public static uint GL_LUMINANCE = 0x1909;
		public static uint GL_LUMINANCE_ALPHA = 0x190A;
		public static uint GL_BITMAP = 0x1A00;
		public static uint GL_RENDER = 0x1C00;
		public static uint GL_FEEDBACK = 0x1C01;
		public static uint GL_SELECT = 0x1C02;
		public static uint GL_FLAT = 0x1D00;
		public static uint GL_SMOOTH = 0x1D01;
		public static uint GL_S = 0x2000;
		public static uint GL_T = 0x2001;
		public static uint GL_R = 0x2002;
		public static uint GL_Q = 0x2003;
		public static uint GL_MODULATE = 0x2100;
		public static uint GL_DECAL = 0x2101;
		public static uint GL_TEXTURE_ENV_MODE = 0x2200;
		public static uint GL_TEXTURE_ENV_COLOR = 0x2201;
		public static uint GL_TEXTURE_ENV = 0x2300;
		public static uint GL_EYE_LINEAR = 0x2400;
		public static uint GL_OBJECT_LINEAR = 0x2401;
		public static uint GL_SPHERE_MAP = 0x2402;
		public static uint GL_TEXTURE_GEN_MODE = 0x2500;
		public static uint GL_OBJECT_PLANE = 0x2501;
		public static uint GL_EYE_PLANE = 0x2502;
		public static uint GL_CLAMP = 0x2900;
		public static uint GL_CLIENT_PIXEL_STORE_BIT = 0x00000001;
		public static uint GL_CLIENT_VERTEX_ARRAY_BIT = 0x00000002;
		public static uint GL_CLIENT_ALL_ATTRIB_BITS = 0xffffffff;
		public static uint GL_POLYGON_OFFSET_FACTOR = 0x8038;
		public static uint GL_POLYGON_OFFSET_UNITS = 0x2A00;
		public static uint GL_POLYGON_OFFSET_POINT = 0x2A01;
		public static uint GL_POLYGON_OFFSET_LINE = 0x2A02;
		public static uint GL_POLYGON_OFFSET_FILL = 0x8037;
		public static uint GL_ALPHA4 = 0x803B;
		public static uint GL_ALPHA8 = 0x803C;
		public static uint GL_ALPHA12 = 0x803D;
		public static uint GL_ALPHA16 = 0x803E;
		public static uint GL_LUMINANCE4 = 0x803F;
		public static uint GL_LUMINANCE8 = 0x8040;
		public static uint GL_LUMINANCE12 = 0x8041;
		public static uint GL_LUMINANCE16 = 0x8042;
		public static uint GL_LUMINANCE4_ALPHA4 = 0x8043;
		public static uint GL_LUMINANCE6_ALPHA2 = 0x8044;
		public static uint GL_LUMINANCE8_ALPHA8 = 0x8045;
		public static uint GL_LUMINANCE12_ALPHA4 = 0x8046;
		public static uint GL_LUMINANCE12_ALPHA12 = 0x8047;
		public static uint GL_LUMINANCE16_ALPHA16 = 0x8048;
		public static uint GL_INTENSITY = 0x8049;
		public static uint GL_INTENSITY4 = 0x804A;
		public static uint GL_INTENSITY8 = 0x804B;
		public static uint GL_INTENSITY12 = 0x804C;
		public static uint GL_INTENSITY16 = 0x804D;
		public static uint GL_R3_G3_B2 = 0x2A10;
		public static uint GL_RGB4 = 0x804F;
		public static uint GL_RGB5 = 0x8050;
		public static uint GL_RGB8 = 0x8051;
		public static uint GL_RGB10 = 0x8052;
		public static uint GL_RGB12 = 0x8053;
		public static uint GL_RGB16 = 0x8054;
		public static uint GL_RGBA2 = 0x8055;
		public static uint GL_RGBA4 = 0x8056;
		public static uint GL_RGB5_A1 = 0x8057;
		public static uint GL_RGBA8 = 0x8058;
		public static uint GL_RGB10_A2 = 0x8059;
		public static uint GL_RGBA12 = 0x805A;
		public static uint GL_RGBA16 = 0x805B;
		public static uint GL_TEXTURE_RED_SIZE = 0x805C;
		public static uint GL_TEXTURE_GREEN_SIZE = 0x805D;
		public static uint GL_TEXTURE_BLUE_SIZE = 0x805E;
		public static uint GL_TEXTURE_ALPHA_SIZE = 0x805F;
		public static uint GL_TEXTURE_LUMINANCE_SIZE = 0x8060;
		public static uint GL_TEXTURE_INTENSITY_SIZE = 0x8061;
		public static uint GL_PROXY_TEXTURE_1D = 0x8063;
		public static uint GL_PROXY_TEXTURE_2D = 0x8064;
		public static uint GL_TEXTURE_PRIORITY = 0x8066;
		public static uint GL_TEXTURE_RESIDENT = 0x8067;
		public static uint GL_TEXTURE_BINDING_1D = 0x8068;
		public static uint GL_TEXTURE_BINDING_2D = 0x8069;
		public static uint GL_VERTEX_ARRAY = 0x8074;
		public static uint GL_NORMAL_ARRAY = 0x8075;
		public static uint GL_COLOR_ARRAY = 0x8076;
		public static uint GL_INDEX_ARRAY = 0x8077;
		public static uint GL_TEXTURE_COORD_ARRAY = 0x8078;
		public static uint GL_EDGE_FLAG_ARRAY = 0x8079;
		public static uint GL_VERTEX_ARRAY_SIZE = 0x807A;
		public static uint GL_VERTEX_ARRAY_TYPE = 0x807B;
		public static uint GL_VERTEX_ARRAY_STRIDE = 0x807C;
		public static uint GL_NORMAL_ARRAY_TYPE = 0x807E;
		public static uint GL_NORMAL_ARRAY_STRIDE = 0x807F;
		public static uint GL_COLOR_ARRAY_SIZE = 0x8081;
		public static uint GL_COLOR_ARRAY_TYPE = 0x8082;
		public static uint GL_COLOR_ARRAY_STRIDE = 0x8083;
		public static uint GL_INDEX_ARRAY_TYPE = 0x8085;
		public static uint GL_INDEX_ARRAY_STRIDE = 0x8086;
		public static uint GL_TEXTURE_COORD_ARRAY_SIZE = 0x8088;
		public static uint GL_TEXTURE_COORD_ARRAY_TYPE = 0x8089;
		public static uint GL_TEXTURE_COORD_ARRAY_STRIDE = 0x808A;
		public static uint GL_EDGE_FLAG_ARRAY_STRIDE = 0x808C;
		public static uint GL_VERTEX_ARRAY_POINTER = 0x808E;
		public static uint GL_NORMAL_ARRAY_POINTER = 0x808F;
		public static uint GL_COLOR_ARRAY_POINTER = 0x8090;
		public static uint GL_INDEX_ARRAY_POINTER = 0x8091;
		public static uint GL_TEXTURE_COORD_ARRAY_POINTER = 0x8092;
		public static uint GL_EDGE_FLAG_ARRAY_POINTER = 0x8093;
		public static uint GL_V2F = 0x2A20;
		public static uint GL_V3F = 0x2A21;
		public static uint GL_C4UB_V2F = 0x2A22;
		public static uint GL_C4UB_V3F = 0x2A23;
		public static uint GL_C3F_V3F = 0x2A24;
		public static uint GL_N3F_V3F = 0x2A25;
		public static uint GL_C4F_N3F_V3F = 0x2A26;
		public static uint GL_T2F_V3F = 0x2A27;
		public static uint GL_T4F_V4F = 0x2A28;
		public static uint GL_T2F_C4UB_V3F = 0x2A29;
		public static uint GL_T2F_C3F_V3F = 0x2A2A;
		public static uint GL_T2F_N3F_V3F = 0x2A2B;
		public static uint GL_T2F_C4F_N3F_V3F = 0x2A2C;
		public static uint GL_T4F_C4F_N3F_V4F = 0x2A2D;
		public static uint GL_EXT_vertex_array = 1;
		public static uint GL_EXT_bgra = 1;
		public static uint GL_EXT_paletted_texture = 1;
		public static uint GL_WIN_swap_hint = 1;
		public static uint GL_WIN_draw_range_elements = 1;
		public static uint GL_VERTEX_ARRAY_EXT = 0x8074;
		public static uint GL_NORMAL_ARRAY_EXT = 0x8075;
		public static uint GL_COLOR_ARRAY_EXT = 0x8076;
		public static uint GL_INDEX_ARRAY_EXT = 0x8077;
		public static uint GL_TEXTURE_COORD_ARRAY_EXT = 0x8078;
		public static uint GL_EDGE_FLAG_ARRAY_EXT = 0x8079;
		public static uint GL_VERTEX_ARRAY_SIZE_EXT = 0x807A;
		public static uint GL_VERTEX_ARRAY_TYPE_EXT = 0x807B;
		public static uint GL_VERTEX_ARRAY_STRIDE_EXT = 0x807C;
		public static uint GL_VERTEX_ARRAY_COUNT_EXT = 0x807D;
		public static uint GL_NORMAL_ARRAY_TYPE_EXT = 0x807E;
		public static uint GL_NORMAL_ARRAY_STRIDE_EXT = 0x807F;
		public static uint GL_NORMAL_ARRAY_COUNT_EXT = 0x8080;
		public static uint GL_COLOR_ARRAY_SIZE_EXT = 0x8081;
		public static uint GL_COLOR_ARRAY_TYPE_EXT = 0x8082;
		public static uint GL_COLOR_ARRAY_STRIDE_EXT = 0x8083;
		public static uint GL_COLOR_ARRAY_COUNT_EXT = 0x8084;
		public static uint GL_INDEX_ARRAY_TYPE_EXT = 0x8085;
		public static uint GL_INDEX_ARRAY_STRIDE_EXT = 0x8086;
		public static uint GL_INDEX_ARRAY_COUNT_EXT = 0x8087;
		public static uint GL_TEXTURE_COORD_ARRAY_SIZE_EXT = 0x8088;
		public static uint GL_TEXTURE_COORD_ARRAY_TYPE_EXT = 0x8089;
		public static uint GL_TEXTURE_COORD_ARRAY_STRIDE_EXT = 0x808A;
		public static uint GL_TEXTURE_COORD_ARRAY_COUNT_EXT = 0x808B;
		public static uint GL_EDGE_FLAG_ARRAY_STRIDE_EXT = 0x808C;
		public static uint GL_EDGE_FLAG_ARRAY_COUNT_EXT = 0x808D;
		public static uint GL_VERTEX_ARRAY_POINTER_EXT = 0x808E;
		public static uint GL_NORMAL_ARRAY_POINTER_EXT = 0x808F;
		public static uint GL_COLOR_ARRAY_POINTER_EXT = 0x8090;
		public static uint GL_INDEX_ARRAY_POINTER_EXT = 0x8091;
		public static uint GL_TEXTURE_COORD_ARRAY_POINTER_EXT = 0x8092;
		public static uint GL_EDGE_FLAG_ARRAY_POINTER_EXT = 0x8093;
		public static uint GL_DOUBLE_EXT = GL_DOUBLE;
		public static uint GL_BGR_EXT = 0x80E0;
		public static uint GL_BGRA_EXT = 0x80E1;
		public static uint GL_COLOR_TABLE_FORMAT_EXT = 0x80D8;
		public static uint GL_COLOR_TABLE_WIDTH_EXT = 0x80D9;
		public static uint GL_COLOR_TABLE_RED_SIZE_EXT = 0x80DA;
		public static uint GL_COLOR_TABLE_GREEN_SIZE_EXT = 0x80DB;
		public static uint GL_COLOR_TABLE_BLUE_SIZE_EXT = 0x80DC;
		public static uint GL_COLOR_TABLE_ALPHA_SIZE_EXT = 0x80DD;
		public static uint GL_COLOR_TABLE_LUMINANCE_SIZE_EXT = 0x80DE;
		public static uint GL_COLOR_TABLE_INTENSITY_SIZE_EXT = 0x80DF;
		public static uint GL_COLOR_INDEX1_EXT = 0x80E2;
		public static uint GL_COLOR_INDEX2_EXT = 0x80E3;
		public static uint GL_COLOR_INDEX4_EXT = 0x80E4;
		public static uint GL_COLOR_INDEX8_EXT = 0x80E5;
		public static uint GL_COLOR_INDEX12_EXT = 0x80E6;
		public static uint GL_COLOR_INDEX16_EXT = 0x80E7;
		public static uint GL_MAX_ELEMENTS_VERTICES_WIN = 0x80E8;
		public static uint GL_MAX_ELEMENTS_INDICES_WIN = 0x80E9;
		public static uint GL_PHONG_WIN = 0x80EA;
		public static uint GL_PHONG_HINT_WIN = 0x80EB;
		public static uint GL_FOG_SPECULAR_TEXTURE_WIN = 0x80EC;
		public static uint GL_LOGIC_OP = GL_INDEX_LOGIC_OP;
		public static uint GL_TEXTURE_COMPONENTS = GL_TEXTURE_INTERNAL_FORMAT;

		public static uint GL_UNSIGNED_BYTE_3_3_2 = 0x8032;
		public static uint GL_UNSIGNED_SHORT_4_4_4_4 = 0x8033;
		public static uint GL_UNSIGNED_SHORT_5_5_5_1 = 0x8034;
		public static uint GL_UNSIGNED_INT_8_8_8_8 = 0x8035;
		public static uint GL_UNSIGNED_INT_10_10_10_2 = 0x8036;
		public static uint GL_TEXTURE_BINDING_3D = 0x806A;
		public static uint GL_PACK_SKIP_IMAGES = 0x806B;
		public static uint GL_PACK_IMAGE_HEIGHT = 0x806C;
		public static uint GL_UNPACK_SKIP_IMAGES = 0x806D;
		public static uint GL_UNPACK_IMAGE_HEIGHT = 0x806E;
		public static uint GL_TEXTURE_3D = 0x806F;
		public static uint GL_PROXY_TEXTURE_3D = 0x8070;
		public static uint GL_TEXTURE_DEPTH = 0x8071;
		public static uint GL_TEXTURE_WRAP_R = 0x8072;
		public static uint GL_MAX_3D_TEXTURE_SIZE = 0x8073;
		public static uint GL_UNSIGNED_BYTE_2_3_3_REV = 0x8362;
		public static uint GL_UNSIGNED_SHORT_5_6_5 = 0x8363;
		public static uint GL_UNSIGNED_SHORT_5_6_5_REV = 0x8364;
		public static uint GL_UNSIGNED_SHORT_4_4_4_4_REV = 0x8365;
		public static uint GL_UNSIGNED_SHORT_1_5_5_5_REV = 0x8366;
		public static uint GL_UNSIGNED_INT_8_8_8_8_REV = 0x8367;
		public static uint GL_UNSIGNED_INT_2_10_10_10_REV = 0x8368;
		public static uint GL_BGR = 0x80E0;
		public static uint GL_BGRA = 0x80E1;
		public static uint GL_MAX_ELEMENTS_VERTICES = 0x80E8;
		public static uint GL_MAX_ELEMENTS_INDICES = 0x80E9;
		public static uint GL_CLAMP_TO_EDGE = 0x812F;
		public static uint GL_TEXTURE_MIN_LOD = 0x813A;
		public static uint GL_TEXTURE_MAX_LOD = 0x813B;
		public static uint GL_TEXTURE_BASE_LEVEL = 0x813C;
		public static uint GL_TEXTURE_MAX_LEVEL = 0x813D;
		public static uint GL_SMOOTH_POINT_SIZE_RANGE = 0x0B12;
		public static uint GL_SMOOTH_POINT_SIZE_GRANULARITY = 0x0B13;
		public static uint GL_SMOOTH_LINE_WIDTH_RANGE = 0x0B22;
		public static uint GL_SMOOTH_LINE_WIDTH_GRANULARITY = 0x0B23;
		public static uint GL_ALIASED_LINE_WIDTH_RANGE = 0x846E;

		public static uint GL_TEXTURE0 = 0x84C0;
		public static uint GL_TEXTURE1 = 0x84C1;
		public static uint GL_TEXTURE2 = 0x84C2;
		public static uint GL_TEXTURE3 = 0x84C3;
		public static uint GL_TEXTURE4 = 0x84C4;
		public static uint GL_TEXTURE5 = 0x84C5;
		public static uint GL_TEXTURE6 = 0x84C6;
		public static uint GL_TEXTURE7 = 0x84C7;
		public static uint GL_TEXTURE8 = 0x84C8;
		public static uint GL_TEXTURE9 = 0x84C9;
		public static uint GL_TEXTURE10 = 0x84CA;
		public static uint GL_TEXTURE11 = 0x84CB;
		public static uint GL_TEXTURE12 = 0x84CC;
		public static uint GL_TEXTURE13 = 0x84CD;
		public static uint GL_TEXTURE14 = 0x84CE;
		public static uint GL_TEXTURE15 = 0x84CF;
		public static uint GL_TEXTURE16 = 0x84D0;
		public static uint GL_TEXTURE17 = 0x84D1;
		public static uint GL_TEXTURE18 = 0x84D2;
		public static uint GL_TEXTURE19 = 0x84D3;
		public static uint GL_TEXTURE20 = 0x84D4;
		public static uint GL_TEXTURE21 = 0x84D5;
		public static uint GL_TEXTURE22 = 0x84D6;
		public static uint GL_TEXTURE23 = 0x84D7;
		public static uint GL_TEXTURE24 = 0x84D8;
		public static uint GL_TEXTURE25 = 0x84D9;
		public static uint GL_TEXTURE26 = 0x84DA;
		public static uint GL_TEXTURE27 = 0x84DB;
		public static uint GL_TEXTURE28 = 0x84DC;
		public static uint GL_TEXTURE29 = 0x84DD;
		public static uint GL_TEXTURE30 = 0x84DE;
		public static uint GL_TEXTURE31 = 0x84DF;
		public static uint GL_ACTIVE_TEXTURE = 0x84E0;
		public static uint GL_MULTISAMPLE = 0x809D;
		public static uint GL_SAMPLE_ALPHA_TO_COVERAGE = 0x809E;
		public static uint GL_SAMPLE_ALPHA_TO_ONE = 0x809F;
		public static uint GL_SAMPLE_COVERAGE = 0x80A0;
		public static uint GL_SAMPLE_BUFFERS = 0x80A8;
		public static uint GL_SAMPLES = 0x80A9;
		public static uint GL_SAMPLE_COVERAGE_VALUE = 0x80AA;
		public static uint GL_SAMPLE_COVERAGE_INVERT = 0x80AB;
		public static uint GL_TEXTURE_CUBE_MAP = 0x8513;
		public static uint GL_TEXTURE_BINDING_CUBE_MAP = 0x8514;
		public static uint GL_TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515;
		public static uint GL_TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516;
		public static uint GL_TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517;
		public static uint GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518;
		public static uint GL_TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519;
		public static uint GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A;
		public static uint GL_PROXY_TEXTURE_CUBE_MAP = 0x851B;
		public static uint GL_MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C;
		public static uint GL_COMPRESSED_RGB = 0x84ED;
		public static uint GL_COMPRESSED_RGBA = 0x84EE;
		public static uint GL_TEXTURE_COMPRESSION_HINT = 0x84EF;
		public static uint GL_TEXTURE_COMPRESSED_IMAGE_SIZE = 0x86A0;
		public static uint GL_TEXTURE_COMPRESSED = 0x86A1;
		public static uint GL_NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2;
		public static uint GL_COMPRESSED_TEXTURE_FORMATS = 0x86A3;
		public static uint GL_CLAMP_TO_BORDER = 0x812D;

		public static uint GL_BLEND_DST_RGB = 0x80C8;
		public static uint GL_BLEND_SRC_RGB = 0x80C9;
		public static uint GL_BLEND_DST_ALPHA = 0x80CA;
		public static uint GL_BLEND_SRC_ALPHA = 0x80CB;
		public static uint GL_POINT_FADE_THRESHOLD_SIZE = 0x8128;
		public static uint GL_DEPTH_COMPONENT16 = 0x81A5;
		public static uint GL_DEPTH_COMPONENT24 = 0x81A6;
		public static uint GL_DEPTH_COMPONENT32 = 0x81A7;
		public static uint GL_MIRRORED_REPEAT = 0x8370;
		public static uint GL_MAX_TEXTURE_LOD_BIAS = 0x84FD;
		public static uint GL_TEXTURE_LOD_BIAS = 0x8501;
		public static uint GL_INCR_WRAP = 0x8507;
		public static uint GL_DECR_WRAP = 0x8508;
		public static uint GL_TEXTURE_DEPTH_SIZE = 0x884A;
		public static uint GL_TEXTURE_COMPARE_MODE = 0x884C;
		public static uint GL_TEXTURE_COMPARE_FUNC = 0x884D;
		public static uint GL_BLEND_COLOR = 0x8005;
		public static uint GL_BLEND_EQUATION = 0x8009;
		public static uint GL_CONSTANT_COLOR = 0x8001;
		public static uint GL_ONE_MINUS_CONSTANT_COLOR = 0x8002;
		public static uint GL_CONSTANT_ALPHA = 0x8003;
		public static uint GL_ONE_MINUS_CONSTANT_ALPHA = 0x8004;
		public static uint GL_FUNC_ADD = 0x8006;
		public static uint GL_FUNC_REVERSE_SUBTRACT = 0x800B;
		public static uint GL_FUNC_SUBTRACT = 0x800A;
		public static uint GL_MIN = 0x8007;
		public static uint GL_MAX = 0x8008;

		public static uint GL_BUFFER_SIZE = 0x8764;
		public static uint GL_BUFFER_USAGE = 0x8765;
		public static uint GL_QUERY_COUNTER_BITS = 0x8864;
		public static uint GL_CURRENT_QUERY = 0x8865;
		public static uint GL_QUERY_RESULT = 0x8866;
		public static uint GL_QUERY_RESULT_AVAILABLE = 0x8867;
		public static uint GL_ARRAY_BUFFER = 0x8892;
		public static uint GL_ELEMENT_ARRAY_BUFFER = 0x8893;
		public static uint GL_ARRAY_BUFFER_BINDING = 0x8894;
		public static uint GL_ELEMENT_ARRAY_BUFFER_BINDING = 0x8895;
		public static uint GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 0x889F;
		public static uint GL_READ_ONLY = 0x88B8;
		public static uint GL_WRITE_ONLY = 0x88B9;
		public static uint GL_READ_WRITE = 0x88BA;
		public static uint GL_BUFFER_ACCESS = 0x88BB;
		public static uint GL_BUFFER_MAPPED = 0x88BC;
		public static uint GL_BUFFER_MAP_POINTER = 0x88BD;
		public static uint GL_STREAM_DRAW = 0x88E0;
		public static uint GL_STREAM_READ = 0x88E1;
		public static uint GL_STREAM_COPY = 0x88E2;
		public static uint GL_STATIC_DRAW = 0x88E4;
		public static uint GL_STATIC_READ = 0x88E5;
		public static uint GL_STATIC_COPY = 0x88E6;
		public static uint GL_DYNAMIC_DRAW = 0x88E8;
		public static uint GL_DYNAMIC_READ = 0x88E9;
		public static uint GL_DYNAMIC_COPY = 0x88EA;
		public static uint GL_SAMPLES_PASSED = 0x8914;
		public static uint GL_SRC1_ALPHA = 0x8589;

		public static uint GL_BLEND_EQUATION_RGB = 0x8009;
		public static uint GL_VERTEX_ATTRIB_ARRAY_ENABLED = 0x8622;
		public static uint GL_VERTEX_ATTRIB_ARRAY_SIZE = 0x8623;
		public static uint GL_VERTEX_ATTRIB_ARRAY_STRIDE = 0x8624;
		public static uint GL_VERTEX_ATTRIB_ARRAY_TYPE = 0x8625;
		public static uint GL_CURRENT_VERTEX_ATTRIB = 0x8626;
		public static uint GL_VERTEX_PROGRAM_POINT_SIZE = 0x8642;
		public static uint GL_VERTEX_ATTRIB_ARRAY_POINTER = 0x8645;
		public static uint GL_STENCIL_BACK_FUNC = 0x8800;
		public static uint GL_STENCIL_BACK_FAIL = 0x8801;
		public static uint GL_STENCIL_BACK_PASS_DEPTH_FAIL = 0x8802;
		public static uint GL_STENCIL_BACK_PASS_DEPTH_PASS = 0x8803;
		public static uint GL_MAX_DRAW_BUFFERS = 0x8824;
		public static uint GL_DRAW_BUFFER0 = 0x8825;
		public static uint GL_DRAW_BUFFER1 = 0x8826;
		public static uint GL_DRAW_BUFFER2 = 0x8827;
		public static uint GL_DRAW_BUFFER3 = 0x8828;
		public static uint GL_DRAW_BUFFER4 = 0x8829;
		public static uint GL_DRAW_BUFFER5 = 0x882A;
		public static uint GL_DRAW_BUFFER6 = 0x882B;
		public static uint GL_DRAW_BUFFER7 = 0x882C;
		public static uint GL_DRAW_BUFFER8 = 0x882D;
		public static uint GL_DRAW_BUFFER9 = 0x882E;
		public static uint GL_DRAW_BUFFER10 = 0x882F;
		public static uint GL_DRAW_BUFFER11 = 0x8830;
		public static uint GL_DRAW_BUFFER12 = 0x8831;
		public static uint GL_DRAW_BUFFER13 = 0x8832;
		public static uint GL_DRAW_BUFFER14 = 0x8833;
		public static uint GL_DRAW_BUFFER15 = 0x8834;
		public static uint GL_BLEND_EQUATION_ALPHA = 0x883D;
		public static uint GL_MAX_VERTEX_ATTRIBS = 0x8869;
		public static uint GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A;
		public static uint GL_MAX_TEXTURE_IMAGE_UNITS = 0x8872;
		public static uint GL_FRAGMENT_SHADER = 0x8B30;
		public static uint GL_VERTEX_SHADER = 0x8B31;
		public static uint GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49;
		public static uint GL_MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A;
		public static uint GL_MAX_VARYING_FLOATS = 0x8B4B;
		public static uint GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0x8B4C;
		public static uint GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0x8B4D;
		public static uint GL_SHADER_TYPE = 0x8B4F;
		public static uint GL_FLOAT_VEC2 = 0x8B50;
		public static uint GL_FLOAT_VEC3 = 0x8B51;
		public static uint GL_FLOAT_VEC4 = 0x8B52;
		public static uint GL_INT_VEC2 = 0x8B53;
		public static uint GL_INT_VEC3 = 0x8B54;
		public static uint GL_INT_VEC4 = 0x8B55;
		public static uint GL_BOOL = 0x8B56;
		public static uint GL_BOOL_VEC2 = 0x8B57;
		public static uint GL_BOOL_VEC3 = 0x8B58;
		public static uint GL_BOOL_VEC4 = 0x8B59;
		public static uint GL_FLOAT_MAT2 = 0x8B5A;
		public static uint GL_FLOAT_MAT3 = 0x8B5B;
		public static uint GL_FLOAT_MAT4 = 0x8B5C;
		public static uint GL_SAMPLER_1D = 0x8B5D;
		public static uint GL_SAMPLER_2D = 0x8B5E;
		public static uint GL_SAMPLER_3D = 0x8B5F;
		public static uint GL_SAMPLER_CUBE = 0x8B60;
		public static uint GL_SAMPLER_1D_SHADOW = 0x8B61;
		public static uint GL_SAMPLER_2D_SHADOW = 0x8B62;
		public static uint GL_DELETE_STATUS = 0x8B80;
		public static uint GL_COMPILE_STATUS = 0x8B81;
		public static uint GL_LINK_STATUS = 0x8B82;
		public static uint GL_VALIDATE_STATUS = 0x8B83;
		public static uint GL_INFO_LOG_LENGTH = 0x8B84;
		public static uint GL_ATTACHED_SHADERS = 0x8B85;
		public static uint GL_ACTIVE_UNIFORMS = 0x8B86;
		public static uint GL_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
		public static uint GL_SHADER_SOURCE_LENGTH = 0x8B88;
		public static uint GL_ACTIVE_ATTRIBUTES = 0x8B89;
		public static uint GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A;
		public static uint GL_FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B;
		public static uint GL_SHADING_LANGUAGE_VERSION = 0x8B8C;
		public static uint GL_CURRENT_PROGRAM = 0x8B8D;
		public static uint GL_POINT_SPRITE_COORD_ORIGIN = 0x8CA0;
		public static uint GL_LOWER_LEFT = 0x8CA1;
		public static uint GL_UPPER_LEFT = 0x8CA2;
		public static uint GL_STENCIL_BACK_REF = 0x8CA3;
		public static uint GL_STENCIL_BACK_VALUE_MASK = 0x8CA4;
		public static uint GL_STENCIL_BACK_WRITEMASK = 0x8CA5;

		public static uint GL_PIXEL_PACK_BUFFER = 0x88EB;
		public static uint GL_PIXEL_UNPACK_BUFFER = 0x88EC;
		public static uint GL_PIXEL_PACK_BUFFER_BINDING = 0x88ED;
		public static uint GL_PIXEL_UNPACK_BUFFER_BINDING = 0x88EF;
		public static uint GL_FLOAT_MAT2x3 = 0x8B65;
		public static uint GL_FLOAT_MAT2x4 = 0x8B66;
		public static uint GL_FLOAT_MAT3x2 = 0x8B67;
		public static uint GL_FLOAT_MAT3x4 = 0x8B68;
		public static uint GL_FLOAT_MAT4x2 = 0x8B69;
		public static uint GL_FLOAT_MAT4x3 = 0x8B6A;
		public static uint GL_SRGB = 0x8C40;
		public static uint GL_SRGB8 = 0x8C41;
		public static uint GL_SRGB_ALPHA = 0x8C42;
		public static uint GL_SRGB8_ALPHA8 = 0x8C43;
		public static uint GL_COMPRESSED_SRGB = 0x8C48;
		public static uint GL_COMPRESSED_SRGB_ALPHA = 0x8C49;

		public static uint GL_COMPARE_REF_TO_TEXTURE = 0x884E;
		public static uint GL_CLIP_DISTANCE0 = 0x3000;
		public static uint GL_CLIP_DISTANCE1 = 0x3001;
		public static uint GL_CLIP_DISTANCE2 = 0x3002;
		public static uint GL_CLIP_DISTANCE3 = 0x3003;
		public static uint GL_CLIP_DISTANCE4 = 0x3004;
		public static uint GL_CLIP_DISTANCE5 = 0x3005;
		public static uint GL_CLIP_DISTANCE6 = 0x3006;
		public static uint GL_CLIP_DISTANCE7 = 0x3007;
		public static uint GL_MAX_CLIP_DISTANCES = 0x0D32;
		public static uint GL_MAJOR_VERSION = 0x821B;
		public static uint GL_MINOR_VERSION = 0x821C;
		public static uint GL_NUM_EXTENSIONS = 0x821D;
		public static uint GL_CONTEXT_FLAGS = 0x821E;
		public static uint GL_COMPRESSED_RED = 0x8225;
		public static uint GL_COMPRESSED_RG = 0x8226;
		public static uint GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT = 0x00000001;
		public static uint GL_RGBA32F = 0x8814;
		public static uint GL_RGB32F = 0x8815;
		public static uint GL_RGBA16F = 0x881A;
		public static uint GL_RGB16F = 0x881B;
		public static uint GL_VERTEX_ATTRIB_ARRAY_INTEGER = 0x88FD;
		public static uint GL_MAX_ARRAY_TEXTURE_LAYERS = 0x88FF;
		public static uint GL_MIN_PROGRAM_TEXEL_OFFSET = 0x8904;
		public static uint GL_MAX_PROGRAM_TEXEL_OFFSET = 0x8905;
		public static uint GL_CLAMP_READ_COLOR = 0x891C;
		public static uint GL_FIXED_ONLY = 0x891D;
		public static uint GL_MAX_VARYING_COMPONENTS = 0x8B4B;
		public static uint GL_TEXTURE_1D_ARRAY = 0x8C18;
		public static uint GL_PROXY_TEXTURE_1D_ARRAY = 0x8C19;
		public static uint GL_TEXTURE_2D_ARRAY = 0x8C1A;
		public static uint GL_PROXY_TEXTURE_2D_ARRAY = 0x8C1B;
		public static uint GL_TEXTURE_BINDING_1D_ARRAY = 0x8C1C;
		public static uint GL_TEXTURE_BINDING_2D_ARRAY = 0x8C1D;
		public static uint GL_R11F_G11F_B10F = 0x8C3A;
		public static uint GL_UNSIGNED_INT_10F_11F_11F_REV = 0x8C3B;
		public static uint GL_RGB9_E5 = 0x8C3D;
		public static uint GL_UNSIGNED_INT_5_9_9_9_REV = 0x8C3E;
		public static uint GL_TEXTURE_SHARED_SIZE = 0x8C3F;
		public static uint GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH = 0x8C76;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_MODE = 0x8C7F;
		public static uint GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0x8C80;
		public static uint GL_TRANSFORM_FEEDBACK_VARYINGS = 0x8C83;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_START = 0x8C84;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_SIZE = 0x8C85;
		public static uint GL_PRIMITIVES_GENERATED = 0x8C87;
		public static uint GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = 0x8C88;
		public static uint GL_RASTERIZER_DISCARD = 0x8C89;
		public static uint GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0x8C8A;
		public static uint GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0x8C8B;
		public static uint GL_INTERLEAVED_ATTRIBS = 0x8C8C;
		public static uint GL_SEPARATE_ATTRIBS = 0x8C8D;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER = 0x8C8E;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_BINDING = 0x8C8F;
		public static uint GL_RGBA32UI = 0x8D70;
		public static uint GL_RGB32UI = 0x8D71;
		public static uint GL_RGBA16UI = 0x8D76;
		public static uint GL_RGB16UI = 0x8D77;
		public static uint GL_RGBA8UI = 0x8D7C;
		public static uint GL_RGB8UI = 0x8D7D;
		public static uint GL_RGBA32I = 0x8D82;
		public static uint GL_RGB32I = 0x8D83;
		public static uint GL_RGBA16I = 0x8D88;
		public static uint GL_RGB16I = 0x8D89;
		public static uint GL_RGBA8I = 0x8D8E;
		public static uint GL_RGB8I = 0x8D8F;
		public static uint GL_RED_INTEGER = 0x8D94;
		public static uint GL_GREEN_INTEGER = 0x8D95;
		public static uint GL_BLUE_INTEGER = 0x8D96;
		public static uint GL_RGB_INTEGER = 0x8D98;
		public static uint GL_RGBA_INTEGER = 0x8D99;
		public static uint GL_BGR_INTEGER = 0x8D9A;
		public static uint GL_BGRA_INTEGER = 0x8D9B;
		public static uint GL_SAMPLER_1D_ARRAY = 0x8DC0;
		public static uint GL_SAMPLER_2D_ARRAY = 0x8DC1;
		public static uint GL_SAMPLER_1D_ARRAY_SHADOW = 0x8DC3;
		public static uint GL_SAMPLER_2D_ARRAY_SHADOW = 0x8DC4;
		public static uint GL_SAMPLER_CUBE_SHADOW = 0x8DC5;
		public static uint GL_UNSIGNED_INT_VEC2 = 0x8DC6;
		public static uint GL_UNSIGNED_INT_VEC3 = 0x8DC7;
		public static uint GL_UNSIGNED_INT_VEC4 = 0x8DC8;
		public static uint GL_INT_SAMPLER_1D = 0x8DC9;
		public static uint GL_INT_SAMPLER_2D = 0x8DCA;
		public static uint GL_INT_SAMPLER_3D = 0x8DCB;
		public static uint GL_INT_SAMPLER_CUBE = 0x8DCC;
		public static uint GL_INT_SAMPLER_1D_ARRAY = 0x8DCE;
		public static uint GL_INT_SAMPLER_2D_ARRAY = 0x8DCF;
		public static uint GL_UNSIGNED_INT_SAMPLER_1D = 0x8DD1;
		public static uint GL_UNSIGNED_INT_SAMPLER_2D = 0x8DD2;
		public static uint GL_UNSIGNED_INT_SAMPLER_3D = 0x8DD3;
		public static uint GL_UNSIGNED_INT_SAMPLER_CUBE = 0x8DD4;
		public static uint GL_UNSIGNED_INT_SAMPLER_1D_ARRAY = 0x8DD6;
		public static uint GL_UNSIGNED_INT_SAMPLER_2D_ARRAY = 0x8DD7;
		public static uint GL_QUERY_WAIT = 0x8E13;
		public static uint GL_QUERY_NO_WAIT = 0x8E14;
		public static uint GL_QUERY_BY_REGION_WAIT = 0x8E15;
		public static uint GL_QUERY_BY_REGION_NO_WAIT = 0x8E16;
		public static uint GL_BUFFER_ACCESS_FLAGS = 0x911F;
		public static uint GL_BUFFER_MAP_LENGTH = 0x9120;
		public static uint GL_BUFFER_MAP_OFFSET = 0x9121;
		public static uint GL_DEPTH_COMPONENT32F = 0x8CAC;
		public static uint GL_DEPTH32F_STENCIL8 = 0x8CAD;
		public static uint GL_FLOAT_32_UNSIGNED_INT_24_8_REV = 0x8DAD;
		public static uint GL_INVALID_FRAMEBUFFER_OPERATION = 0x0506;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = 0x8210;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = 0x8211;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = 0x8212;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = 0x8213;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = 0x8214;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = 0x8215;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = 0x8216;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = 0x8217;
		public static uint GL_FRAMEBUFFER_DEFAULT = 0x8218;
		public static uint GL_FRAMEBUFFER_UNDEFINED = 0x8219;
		public static uint GL_DEPTH_STENCIL_ATTACHMENT = 0x821A;
		public static uint GL_MAX_RENDERBUFFER_SIZE = 0x84E8;
		public static uint GL_DEPTH_STENCIL = 0x84F9;
		public static uint GL_UNSIGNED_INT_24_8 = 0x84FA;
		public static uint GL_DEPTH24_STENCIL8 = 0x88F0;
		public static uint GL_TEXTURE_STENCIL_SIZE = 0x88F1;
		public static uint GL_TEXTURE_RED_TYPE = 0x8C10;
		public static uint GL_TEXTURE_GREEN_TYPE = 0x8C11;
		public static uint GL_TEXTURE_BLUE_TYPE = 0x8C12;
		public static uint GL_TEXTURE_ALPHA_TYPE = 0x8C13;
		public static uint GL_TEXTURE_DEPTH_TYPE = 0x8C16;
		public static uint GL_UNSIGNED_NORMALIZED = 0x8C17;
		public static uint GL_FRAMEBUFFER_BINDING = 0x8CA6;
		public static uint GL_DRAW_FRAMEBUFFER_BINDING = 0x8CA6;
		public static uint GL_RENDERBUFFER_BINDING = 0x8CA7;
		public static uint GL_READ_FRAMEBUFFER = 0x8CA8;
		public static uint GL_DRAW_FRAMEBUFFER = 0x8CA9;
		public static uint GL_READ_FRAMEBUFFER_BINDING = 0x8CAA;
		public static uint GL_RENDERBUFFER_SAMPLES = 0x8CAB;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 0x8CD0;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 0x8CD1;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 0x8CD2;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 0x8CD3;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = 0x8CD4;
		public static uint GL_FRAMEBUFFER_COMPLETE = 0x8CD5;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 0x8CD6;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 0x8CD7;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = 0x8CDB;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = 0x8CDC;
		public static uint GL_FRAMEBUFFER_UNSUPPORTED = 0x8CDD;
		public static uint GL_MAX_COLOR_ATTACHMENTS = 0x8CDF;
		public static uint GL_COLOR_ATTACHMENT0 = 0x8CE0;
		public static uint GL_COLOR_ATTACHMENT1 = 0x8CE1;
		public static uint GL_COLOR_ATTACHMENT2 = 0x8CE2;
		public static uint GL_COLOR_ATTACHMENT3 = 0x8CE3;
		public static uint GL_COLOR_ATTACHMENT4 = 0x8CE4;
		public static uint GL_COLOR_ATTACHMENT5 = 0x8CE5;
		public static uint GL_COLOR_ATTACHMENT6 = 0x8CE6;
		public static uint GL_COLOR_ATTACHMENT7 = 0x8CE7;
		public static uint GL_COLOR_ATTACHMENT8 = 0x8CE8;
		public static uint GL_COLOR_ATTACHMENT9 = 0x8CE9;
		public static uint GL_COLOR_ATTACHMENT10 = 0x8CEA;
		public static uint GL_COLOR_ATTACHMENT11 = 0x8CEB;
		public static uint GL_COLOR_ATTACHMENT12 = 0x8CEC;
		public static uint GL_COLOR_ATTACHMENT13 = 0x8CED;
		public static uint GL_COLOR_ATTACHMENT14 = 0x8CEE;
		public static uint GL_COLOR_ATTACHMENT15 = 0x8CEF;
		public static uint GL_COLOR_ATTACHMENT16 = 0x8CF0;
		public static uint GL_COLOR_ATTACHMENT17 = 0x8CF1;
		public static uint GL_COLOR_ATTACHMENT18 = 0x8CF2;
		public static uint GL_COLOR_ATTACHMENT19 = 0x8CF3;
		public static uint GL_COLOR_ATTACHMENT20 = 0x8CF4;
		public static uint GL_COLOR_ATTACHMENT21 = 0x8CF5;
		public static uint GL_COLOR_ATTACHMENT22 = 0x8CF6;
		public static uint GL_COLOR_ATTACHMENT23 = 0x8CF7;
		public static uint GL_COLOR_ATTACHMENT24 = 0x8CF8;
		public static uint GL_COLOR_ATTACHMENT25 = 0x8CF9;
		public static uint GL_COLOR_ATTACHMENT26 = 0x8CFA;
		public static uint GL_COLOR_ATTACHMENT27 = 0x8CFB;
		public static uint GL_COLOR_ATTACHMENT28 = 0x8CFC;
		public static uint GL_COLOR_ATTACHMENT29 = 0x8CFD;
		public static uint GL_COLOR_ATTACHMENT30 = 0x8CFE;
		public static uint GL_COLOR_ATTACHMENT31 = 0x8CFF;
		public static uint GL_DEPTH_ATTACHMENT = 0x8D00;
		public static uint GL_STENCIL_ATTACHMENT = 0x8D20;
		public static uint GL_FRAMEBUFFER = 0x8D40;
		public static uint GL_RENDERBUFFER = 0x8D41;
		public static uint GL_RENDERBUFFER_WIDTH = 0x8D42;
		public static uint GL_RENDERBUFFER_HEIGHT = 0x8D43;
		public static uint GL_RENDERBUFFER_INTERNAL_FORMAT = 0x8D44;
		public static uint GL_STENCIL_INDEX1 = 0x8D46;
		public static uint GL_STENCIL_INDEX4 = 0x8D47;
		public static uint GL_STENCIL_INDEX8 = 0x8D48;
		public static uint GL_STENCIL_INDEX16 = 0x8D49;
		public static uint GL_RENDERBUFFER_RED_SIZE = 0x8D50;
		public static uint GL_RENDERBUFFER_GREEN_SIZE = 0x8D51;
		public static uint GL_RENDERBUFFER_BLUE_SIZE = 0x8D52;
		public static uint GL_RENDERBUFFER_ALPHA_SIZE = 0x8D53;
		public static uint GL_RENDERBUFFER_DEPTH_SIZE = 0x8D54;
		public static uint GL_RENDERBUFFER_STENCIL_SIZE = 0x8D55;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = 0x8D56;
		public static uint GL_MAX_SAMPLES = 0x8D57;
		public static uint GL_FRAMEBUFFER_SRGB = 0x8DB9;
		public static uint GL_HALF_FLOAT = 0x140B;
		public static uint GL_MAP_READ_BIT = 0x0001;
		public static uint GL_MAP_WRITE_BIT = 0x0002;
		public static uint GL_MAP_INVALIDATE_RANGE_BIT = 0x0004;
		public static uint GL_MAP_INVALIDATE_BUFFER_BIT = 0x0008;
		public static uint GL_MAP_FLUSH_EXPLICIT_BIT = 0x0010;
		public static uint GL_MAP_UNSYNCHRONIZED_BIT = 0x0020;
		public static uint GL_COMPRESSED_RED_RGTC1 = 0x8DBB;
		public static uint GL_COMPRESSED_SIGNED_RED_RGTC1 = 0x8DBC;
		public static uint GL_COMPRESSED_RG_RGTC2 = 0x8DBD;
		public static uint GL_COMPRESSED_SIGNED_RG_RGTC2 = 0x8DBE;
		public static uint GL_RG = 0x8227;
		public static uint GL_RG_INTEGER = 0x8228;
		public static uint GL_R8 = 0x8229;
		public static uint GL_R16 = 0x822A;
		public static uint GL_RG8 = 0x822B;
		public static uint GL_RG16 = 0x822C;
		public static uint GL_R16F = 0x822D;
		public static uint GL_R32F = 0x822E;
		public static uint GL_RG16F = 0x822F;
		public static uint GL_RG32F = 0x8230;
		public static uint GL_R8I = 0x8231;
		public static uint GL_R8UI = 0x8232;
		public static uint GL_R16I = 0x8233;
		public static uint GL_R16UI = 0x8234;
		public static uint GL_R32I = 0x8235;
		public static uint GL_R32UI = 0x8236;
		public static uint GL_RG8I = 0x8237;
		public static uint GL_RG8UI = 0x8238;
		public static uint GL_RG16I = 0x8239;
		public static uint GL_RG16UI = 0x823A;
		public static uint GL_RG32I = 0x823B;
		public static uint GL_RG32UI = 0x823C;
		public static uint GL_VERTEX_ARRAY_BINDING = 0x85B5;

		public static uint GL_SAMPLER_2D_RECT = 0x8B63;
		public static uint GL_SAMPLER_2D_RECT_SHADOW = 0x8B64;
		public static uint GL_SAMPLER_BUFFER = 0x8DC2;
		public static uint GL_INT_SAMPLER_2D_RECT = 0x8DCD;
		public static uint GL_INT_SAMPLER_BUFFER = 0x8DD0;
		public static uint GL_UNSIGNED_INT_SAMPLER_2D_RECT = 0x8DD5;
		public static uint GL_UNSIGNED_INT_SAMPLER_BUFFER = 0x8DD8;
		public static uint GL_TEXTURE_BUFFER = 0x8C2A;
		public static uint GL_MAX_TEXTURE_BUFFER_SIZE = 0x8C2B;
		public static uint GL_TEXTURE_BINDING_BUFFER = 0x8C2C;
		public static uint GL_TEXTURE_BUFFER_DATA_STORE_BINDING = 0x8C2D;
		public static uint GL_TEXTURE_RECTANGLE = 0x84F5;
		public static uint GL_TEXTURE_BINDING_RECTANGLE = 0x84F6;
		public static uint GL_PROXY_TEXTURE_RECTANGLE = 0x84F7;
		public static uint GL_MAX_RECTANGLE_TEXTURE_SIZE = 0x84F8;
		public static uint GL_R8_SNORM = 0x8F94;
		public static uint GL_RG8_SNORM = 0x8F95;
		public static uint GL_RGB8_SNORM = 0x8F96;
		public static uint GL_RGBA8_SNORM = 0x8F97;
		public static uint GL_R16_SNORM = 0x8F98;
		public static uint GL_RG16_SNORM = 0x8F99;
		public static uint GL_RGB16_SNORM = 0x8F9A;
		public static uint GL_RGBA16_SNORM = 0x8F9B;
		public static uint GL_SIGNED_NORMALIZED = 0x8F9C;
		public static uint GL_PRIMITIVE_RESTART = 0x8F9D;
		public static uint GL_PRIMITIVE_RESTART_INDEX = 0x8F9E;
		public static uint GL_COPY_READ_BUFFER = 0x8F36;
		public static uint GL_COPY_WRITE_BUFFER = 0x8F37;
		public static uint GL_UNIFORM_BUFFER = 0x8A11;
		public static uint GL_UNIFORM_BUFFER_BINDING = 0x8A28;
		public static uint GL_UNIFORM_BUFFER_START = 0x8A29;
		public static uint GL_UNIFORM_BUFFER_SIZE = 0x8A2A;
		public static uint GL_MAX_VERTEX_UNIFORM_BLOCKS = 0x8A2B;
		public static uint GL_MAX_GEOMETRY_UNIFORM_BLOCKS = 0x8A2C;
		public static uint GL_MAX_FRAGMENT_UNIFORM_BLOCKS = 0x8A2D;
		public static uint GL_MAX_COMBINED_UNIFORM_BLOCKS = 0x8A2E;
		public static uint GL_MAX_UNIFORM_BUFFER_BINDINGS = 0x8A2F;
		public static uint GL_MAX_UNIFORM_BLOCK_SIZE = 0x8A30;
		public static uint GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0x8A31;
		public static uint GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0x8A32;
		public static uint GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0x8A33;
		public static uint GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0x8A34;
		public static uint GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = 0x8A35;
		public static uint GL_ACTIVE_UNIFORM_BLOCKS = 0x8A36;
		public static uint GL_UNIFORM_TYPE = 0x8A37;
		public static uint GL_UNIFORM_SIZE = 0x8A38;
		public static uint GL_UNIFORM_NAME_LENGTH = 0x8A39;
		public static uint GL_UNIFORM_BLOCK_INDEX = 0x8A3A;
		public static uint GL_UNIFORM_OFFSET = 0x8A3B;
		public static uint GL_UNIFORM_ARRAY_STRIDE = 0x8A3C;
		public static uint GL_UNIFORM_MATRIX_STRIDE = 0x8A3D;
		public static uint GL_UNIFORM_IS_ROW_MAJOR = 0x8A3E;
		public static uint GL_UNIFORM_BLOCK_BINDING = 0x8A3F;
		public static uint GL_UNIFORM_BLOCK_DATA_SIZE = 0x8A40;
		public static uint GL_UNIFORM_BLOCK_NAME_LENGTH = 0x8A41;
		public static uint GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS = 0x8A42;
		public static uint GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = 0x8A43;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = 0x8A44;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER = 0x8A45;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = 0x8A46;
		public static uint GL_INVALID_INDEX = 0xFFFFFFFFu;

		public static uint GL_CONTEXT_CORE_PROFILE_BIT = 0x00000001;
		public static uint GL_CONTEXT_COMPATIBILITY_PROFILE_BIT = 0x00000002;
		public static uint GL_LINES_ADJACENCY = 0x000A;
		public static uint GL_LINE_STRIP_ADJACENCY = 0x000B;
		public static uint GL_TRIANGLES_ADJACENCY = 0x000C;
		public static uint GL_TRIANGLE_STRIP_ADJACENCY = 0x000D;
		public static uint GL_PROGRAM_POINT_SIZE = 0x8642;
		public static uint GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = 0x8C29;
		public static uint GL_FRAMEBUFFER_ATTACHMENT_LAYERED = 0x8DA7;
		public static uint GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = 0x8DA8;
		public static uint GL_GEOMETRY_SHADER = 0x8DD9;
		public static uint GL_GEOMETRY_VERTICES_OUT = 0x8916;
		public static uint GL_GEOMETRY_INPUT_TYPE = 0x8917;
		public static uint GL_GEOMETRY_OUTPUT_TYPE = 0x8918;
		public static uint GL_MAX_GEOMETRY_UNIFORM_COMPONENTS = 0x8DDF;
		public static uint GL_MAX_GEOMETRY_OUTPUT_VERTICES = 0x8DE0;
		public static uint GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0x8DE1;
		public static uint GL_MAX_VERTEX_OUTPUT_COMPONENTS = 0x9122;
		public static uint GL_MAX_GEOMETRY_INPUT_COMPONENTS = 0x9123;
		public static uint GL_MAX_GEOMETRY_OUTPUT_COMPONENTS = 0x9124;
		public static uint GL_MAX_FRAGMENT_INPUT_COMPONENTS = 0x9125;
		public static uint GL_CONTEXT_PROFILE_MASK = 0x9126;
		public static uint GL_DEPTH_CLAMP = 0x864F;
		public static uint GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION = 0x8E4C;
		public static uint GL_FIRST_VERTEX_CONVENTION = 0x8E4D;
		public static uint GL_LAST_VERTEX_CONVENTION = 0x8E4E;
		public static uint GL_PROVOKING_VERTEX = 0x8E4F;
		public static uint GL_TEXTURE_CUBE_MAP_SEAMLESS = 0x884F;
		public static uint GL_MAX_SERVER_WAIT_TIMEOUT = 0x9111;
		public static uint GL_OBJECT_TYPE = 0x9112;
		public static uint GL_SYNC_CONDITION = 0x9113;
		public static uint GL_SYNC_STATUS = 0x9114;
		public static uint GL_SYNC_FLAGS = 0x9115;
		public static uint GL_SYNC_FENCE = 0x9116;
		public static uint GL_SYNC_GPU_COMMANDS_COMPLETE = 0x9117;
		public static uint GL_UNSIGNALED = 0x9118;
		public static uint GL_SIGNALED = 0x9119;
		public static uint GL_ALREADY_SIGNALED = 0x911A;
		public static uint GL_TIMEOUT_EXPIRED = 0x911B;
		public static uint GL_CONDITION_SATISFIED = 0x911C;
		public static uint GL_WAIT_FAILED = 0x911D;
		public static ulong GL_TIMEOUT_IGNORED = 0xFFFFFFFFFFFFFFFF;
		public static uint GL_SYNC_FLUSH_COMMANDS_BIT = 0x00000001;
		public static uint GL_SAMPLE_POSITION = 0x8E50;
		public static uint GL_SAMPLE_MASK = 0x8E51;
		public static uint GL_SAMPLE_MASK_VALUE = 0x8E52;
		public static uint GL_MAX_SAMPLE_MASK_WORDS = 0x8E59;
		public static uint GL_TEXTURE_2D_MULTISAMPLE = 0x9100;
		public static uint GL_PROXY_TEXTURE_2D_MULTISAMPLE = 0x9101;
		public static uint GL_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9102;
		public static uint GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY = 0x9103;
		public static uint GL_TEXTURE_BINDING_2D_MULTISAMPLE = 0x9104;
		public static uint GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = 0x9105;
		public static uint GL_TEXTURE_SAMPLES = 0x9106;
		public static uint GL_TEXTURE_FIXED_SAMPLE_LOCATIONS = 0x9107;
		public static uint GL_SAMPLER_2D_MULTISAMPLE = 0x9108;
		public static uint GL_INT_SAMPLER_2D_MULTISAMPLE = 0x9109;
		public static uint GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = 0x910A;
		public static uint GL_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910B;
		public static uint GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910C;
		public static uint GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = 0x910D;
		public static uint GL_MAX_COLOR_TEXTURE_SAMPLES = 0x910E;
		public static uint GL_MAX_DEPTH_TEXTURE_SAMPLES = 0x910F;
		public static uint GL_MAX_INTEGER_SAMPLES = 0x9110;

		public static uint GL_VERTEX_ATTRIB_ARRAY_DIVISOR = 0x88FE;
		public static uint GL_SRC1_COLOR = 0x88F9;
		public static uint GL_ONE_MINUS_SRC1_COLOR = 0x88FA;
		public static uint GL_ONE_MINUS_SRC1_ALPHA = 0x88FB;
		public static uint GL_MAX_DUAL_SOURCE_DRAW_BUFFERS = 0x88FC;
		public static uint GL_ANY_SAMPLES_PASSED = 0x8C2F;
		public static uint GL_SAMPLER_BINDING = 0x8919;
		public static uint GL_RGB10_A2UI = 0x906F;
		public static uint GL_TEXTURE_SWIZZLE_R = 0x8E42;
		public static uint GL_TEXTURE_SWIZZLE_G = 0x8E43;
		public static uint GL_TEXTURE_SWIZZLE_B = 0x8E44;
		public static uint GL_TEXTURE_SWIZZLE_A = 0x8E45;
		public static uint GL_TEXTURE_SWIZZLE_RGBA = 0x8E46;
		public static uint GL_TIME_ELAPSED = 0x88BF;
		public static uint GL_TIMESTAMP = 0x8E28;
		public static uint GL_INT_2_10_10_10_REV = 0x8D9F;

		public static uint GL_SAMPLE_SHADING = 0x8C36;
		public static uint GL_MIN_SAMPLE_SHADING_VALUE = 0x8C37;
		public static uint GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5E;
		public static uint GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET = 0x8E5F;
		public static uint GL_TEXTURE_CUBE_MAP_ARRAY = 0x9009;
		public static uint GL_TEXTURE_BINDING_CUBE_MAP_ARRAY = 0x900A;
		public static uint GL_PROXY_TEXTURE_CUBE_MAP_ARRAY = 0x900B;
		public static uint GL_SAMPLER_CUBE_MAP_ARRAY = 0x900C;
		public static uint GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW = 0x900D;
		public static uint GL_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900E;
		public static uint GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY = 0x900F;
		public static uint GL_DRAW_INDIRECT_BUFFER = 0x8F3F;
		public static uint GL_DRAW_INDIRECT_BUFFER_BINDING = 0x8F43;
		public static uint GL_GEOMETRY_SHADER_INVOCATIONS = 0x887F;
		public static uint GL_MAX_GEOMETRY_SHADER_INVOCATIONS = 0x8E5A;
		public static uint GL_MIN_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5B;
		public static uint GL_MAX_FRAGMENT_INTERPOLATION_OFFSET = 0x8E5C;
		public static uint GL_FRAGMENT_INTERPOLATION_OFFSET_BITS = 0x8E5D;
		public static uint GL_MAX_VERTEX_STREAMS = 0x8E71;
		public static uint GL_DOUBLE_VEC2 = 0x8FFC;
		public static uint GL_DOUBLE_VEC3 = 0x8FFD;
		public static uint GL_DOUBLE_VEC4 = 0x8FFE;
		public static uint GL_DOUBLE_MAT2 = 0x8F46;
		public static uint GL_DOUBLE_MAT3 = 0x8F47;
		public static uint GL_DOUBLE_MAT4 = 0x8F48;
		public static uint GL_DOUBLE_MAT2x3 = 0x8F49;
		public static uint GL_DOUBLE_MAT2x4 = 0x8F4A;
		public static uint GL_DOUBLE_MAT3x2 = 0x8F4B;
		public static uint GL_DOUBLE_MAT3x4 = 0x8F4C;
		public static uint GL_DOUBLE_MAT4x2 = 0x8F4D;
		public static uint GL_DOUBLE_MAT4x3 = 0x8F4E;
		public static uint GL_ACTIVE_SUBROUTINES = 0x8DE5;
		public static uint GL_ACTIVE_SUBROUTINE_UNIFORMS = 0x8DE6;
		public static uint GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS = 0x8E47;
		public static uint GL_ACTIVE_SUBROUTINE_MAX_LENGTH = 0x8E48;
		public static uint GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH = 0x8E49;
		public static uint GL_MAX_SUBROUTINES = 0x8DE7;
		public static uint GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS = 0x8DE8;
		public static uint GL_NUM_COMPATIBLE_SUBROUTINES = 0x8E4A;
		public static uint GL_COMPATIBLE_SUBROUTINES = 0x8E4B;
		public static uint GL_PATCHES = 0x000E;
		public static uint GL_PATCH_VERTICES = 0x8E72;
		public static uint GL_PATCH_DEFAULT_INNER_LEVEL = 0x8E73;
		public static uint GL_PATCH_DEFAULT_OUTER_LEVEL = 0x8E74;
		public static uint GL_TESS_CONTROL_OUTPUT_VERTICES = 0x8E75;
		public static uint GL_TESS_GEN_MODE = 0x8E76;
		public static uint GL_TESS_GEN_SPACING = 0x8E77;
		public static uint GL_TESS_GEN_VERTEX_ORDER = 0x8E78;
		public static uint GL_TESS_GEN_POINT_MODE = 0x8E79;
		public static uint GL_ISOLINES = 0x8E7A;
		public static uint GL_FRACTIONAL_ODD = 0x8E7B;
		public static uint GL_FRACTIONAL_EVEN = 0x8E7C;
		public static uint GL_MAX_PATCH_VERTICES = 0x8E7D;
		public static uint GL_MAX_TESS_GEN_LEVEL = 0x8E7E;
		public static uint GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E7F;
		public static uint GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E80;
		public static uint GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0x8E81;
		public static uint GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0x8E82;
		public static uint GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0x8E83;
		public static uint GL_MAX_TESS_PATCH_COMPONENTS = 0x8E84;
		public static uint GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0x8E85;
		public static uint GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0x8E86;
		public static uint GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0x8E89;
		public static uint GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0x8E8A;
		public static uint GL_MAX_TESS_CONTROL_INPUT_COMPONENTS = 0x886C;
		public static uint GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0x886D;
		public static uint GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0x8E1E;
		public static uint GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0x8E1F;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER = 0x84F0;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x84F1;
		public static uint GL_TESS_EVALUATION_SHADER = 0x8E87;
		public static uint GL_TESS_CONTROL_SHADER = 0x8E88;
		public static uint GL_TRANSFORM_FEEDBACK = 0x8E22;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED = 0x8E23;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE = 0x8E24;
		public static uint GL_TRANSFORM_FEEDBACK_BINDING = 0x8E25;
		public static uint GL_MAX_TRANSFORM_FEEDBACK_BUFFERS = 0x8E70;

		public static uint GL_FIXED = 0x140C;
		public static uint GL_IMPLEMENTATION_COLOR_READ_TYPE = 0x8B9A;
		public static uint GL_IMPLEMENTATION_COLOR_READ_FORMAT = 0x8B9B;
		public static uint GL_LOW_FLOAT = 0x8DF0;
		public static uint GL_MEDIUM_FLOAT = 0x8DF1;
		public static uint GL_HIGH_FLOAT = 0x8DF2;
		public static uint GL_LOW_INT = 0x8DF3;
		public static uint GL_MEDIUM_INT = 0x8DF4;
		public static uint GL_HIGH_INT = 0x8DF5;
		public static uint GL_SHADER_COMPILER = 0x8DFA;
		public static uint GL_SHADER_BINARY_FORMATS = 0x8DF8;
		public static uint GL_NUM_SHADER_BINARY_FORMATS = 0x8DF9;
		public static uint GL_MAX_VERTEX_UNIFORM_VECTORS = 0x8DFB;
		public static uint GL_MAX_VARYING_VECTORS = 0x8DFC;
		public static uint GL_MAX_FRAGMENT_UNIFORM_VECTORS = 0x8DFD;
		public static uint GL_RGB565 = 0x8D62;
		public static uint GL_PROGRAM_BINARY_RETRIEVABLE_HINT = 0x8257;
		public static uint GL_PROGRAM_BINARY_LENGTH = 0x8741;
		public static uint GL_NUM_PROGRAM_BINARY_FORMATS = 0x87FE;
		public static uint GL_PROGRAM_BINARY_FORMATS = 0x87FF;
		public static uint GL_VERTEX_SHADER_BIT = 0x00000001;
		public static uint GL_FRAGMENT_SHADER_BIT = 0x00000002;
		public static uint GL_GEOMETRY_SHADER_BIT = 0x00000004;
		public static uint GL_TESS_CONTROL_SHADER_BIT = 0x00000008;
		public static uint GL_TESS_EVALUATION_SHADER_BIT = 0x00000010;
		public static uint GL_ALL_SHADER_BITS = 0xFFFFFFFF;
		public static uint GL_PROGRAM_SEPARABLE = 0x8258;
		public static uint GL_ACTIVE_PROGRAM = 0x8259;
		public static uint GL_PROGRAM_PIPELINE_BINDING = 0x825A;
		public static uint GL_MAX_VIEWPORTS = 0x825B;
		public static uint GL_VIEWPORT_SUBPIXEL_BITS = 0x825C;
		public static uint GL_VIEWPORT_BOUNDS_RANGE = 0x825D;
		public static uint GL_LAYER_PROVOKING_VERTEX = 0x825E;
		public static uint GL_VIEWPORT_INDEX_PROVOKING_VERTEX = 0x825F;
		public static uint GL_UNDEFINED_VERTEX = 0x8260;

		public static uint GL_COPY_READ_BUFFER_BINDING = 0x8F36;
		public static uint GL_COPY_WRITE_BUFFER_BINDING = 0x8F37;
		public static uint GL_TRANSFORM_FEEDBACK_ACTIVE = 0x8E24;
		public static uint GL_TRANSFORM_FEEDBACK_PAUSED = 0x8E23;
		public static uint GL_UNPACK_COMPRESSED_BLOCK_WIDTH = 0x9127;
		public static uint GL_UNPACK_COMPRESSED_BLOCK_HEIGHT = 0x9128;
		public static uint GL_UNPACK_COMPRESSED_BLOCK_DEPTH = 0x9129;
		public static uint GL_UNPACK_COMPRESSED_BLOCK_SIZE = 0x912A;
		public static uint GL_PACK_COMPRESSED_BLOCK_WIDTH = 0x912B;
		public static uint GL_PACK_COMPRESSED_BLOCK_HEIGHT = 0x912C;
		public static uint GL_PACK_COMPRESSED_BLOCK_DEPTH = 0x912D;
		public static uint GL_PACK_COMPRESSED_BLOCK_SIZE = 0x912E;
		public static uint GL_NUM_SAMPLE_COUNTS = 0x9380;
		public static uint GL_MIN_MAP_BUFFER_ALIGNMENT = 0x90BC;
		public static uint GL_ATOMIC_COUNTER_BUFFER = 0x92C0;
		public static uint GL_ATOMIC_COUNTER_BUFFER_BINDING = 0x92C1;
		public static uint GL_ATOMIC_COUNTER_BUFFER_START = 0x92C2;
		public static uint GL_ATOMIC_COUNTER_BUFFER_SIZE = 0x92C3;
		public static uint GL_ATOMIC_COUNTER_BUFFER_DATA_SIZE = 0x92C4;
		public static uint GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTERS = 0x92C5;
		public static uint GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTER_INDICES = 0x92C6;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_VERTEX_SHADER = 0x92C7;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_CONTROL_SHADER = 0x92C8;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x92C9;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_GEOMETRY_SHADER = 0x92CA;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_FRAGMENT_SHADER = 0x92CB;
		public static uint GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0x92CC;
		public static uint GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0x92CD;
		public static uint GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0x92CE;
		public static uint GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0x92CF;
		public static uint GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0x92D0;
		public static uint GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = 0x92D1;
		public static uint GL_MAX_VERTEX_ATOMIC_COUNTERS = 0x92D2;
		public static uint GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0x92D3;
		public static uint GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0x92D4;
		public static uint GL_MAX_GEOMETRY_ATOMIC_COUNTERS = 0x92D5;
		public static uint GL_MAX_FRAGMENT_ATOMIC_COUNTERS = 0x92D6;
		public static uint GL_MAX_COMBINED_ATOMIC_COUNTERS = 0x92D7;
		public static uint GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0x92D8;
		public static uint GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0x92DC;
		public static uint GL_ACTIVE_ATOMIC_COUNTER_BUFFERS = 0x92D9;
		public static uint GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX = 0x92DA;
		public static uint GL_UNSIGNED_INT_ATOMIC_COUNTER = 0x92DB;
		public static uint GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT = 0x00000001;
		public static uint GL_ELEMENT_ARRAY_BARRIER_BIT = 0x00000002;
		public static uint GL_UNIFORM_BARRIER_BIT = 0x00000004;
		public static uint GL_TEXTURE_FETCH_BARRIER_BIT = 0x00000008;
		public static uint GL_SHADER_IMAGE_ACCESS_BARRIER_BIT = 0x00000020;
		public static uint GL_COMMAND_BARRIER_BIT = 0x00000040;
		public static uint GL_PIXEL_BUFFER_BARRIER_BIT = 0x00000080;
		public static uint GL_TEXTURE_UPDATE_BARRIER_BIT = 0x00000100;
		public static uint GL_BUFFER_UPDATE_BARRIER_BIT = 0x00000200;
		public static uint GL_FRAMEBUFFER_BARRIER_BIT = 0x00000400;
		public static uint GL_TRANSFORM_FEEDBACK_BARRIER_BIT = 0x00000800;
		public static uint GL_ATOMIC_COUNTER_BARRIER_BIT = 0x00001000;
		public static uint GL_ALL_BARRIER_BITS = 0xFFFFFFFF;
		public static uint GL_MAX_IMAGE_UNITS = 0x8F38;
		public static uint GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = 0x8F39;
		public static uint GL_IMAGE_BINDING_NAME = 0x8F3A;
		public static uint GL_IMAGE_BINDING_LEVEL = 0x8F3B;
		public static uint GL_IMAGE_BINDING_LAYERED = 0x8F3C;
		public static uint GL_IMAGE_BINDING_LAYER = 0x8F3D;
		public static uint GL_IMAGE_BINDING_ACCESS = 0x8F3E;
		public static uint GL_IMAGE_1D = 0x904C;
		public static uint GL_IMAGE_2D = 0x904D;
		public static uint GL_IMAGE_3D = 0x904E;
		public static uint GL_IMAGE_2D_RECT = 0x904F;
		public static uint GL_IMAGE_CUBE = 0x9050;
		public static uint GL_IMAGE_BUFFER = 0x9051;
		public static uint GL_IMAGE_1D_ARRAY = 0x9052;
		public static uint GL_IMAGE_2D_ARRAY = 0x9053;
		public static uint GL_IMAGE_CUBE_MAP_ARRAY = 0x9054;
		public static uint GL_IMAGE_2D_MULTISAMPLE = 0x9055;
		public static uint GL_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9056;
		public static uint GL_INT_IMAGE_1D = 0x9057;
		public static uint GL_INT_IMAGE_2D = 0x9058;
		public static uint GL_INT_IMAGE_3D = 0x9059;
		public static uint GL_INT_IMAGE_2D_RECT = 0x905A;
		public static uint GL_INT_IMAGE_CUBE = 0x905B;
		public static uint GL_INT_IMAGE_BUFFER = 0x905C;
		public static uint GL_INT_IMAGE_1D_ARRAY = 0x905D;
		public static uint GL_INT_IMAGE_2D_ARRAY = 0x905E;
		public static uint GL_INT_IMAGE_CUBE_MAP_ARRAY = 0x905F;
		public static uint GL_INT_IMAGE_2D_MULTISAMPLE = 0x9060;
		public static uint GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x9061;
		public static uint GL_UNSIGNED_INT_IMAGE_1D = 0x9062;
		public static uint GL_UNSIGNED_INT_IMAGE_2D = 0x9063;
		public static uint GL_UNSIGNED_INT_IMAGE_3D = 0x9064;
		public static uint GL_UNSIGNED_INT_IMAGE_2D_RECT = 0x9065;
		public static uint GL_UNSIGNED_INT_IMAGE_CUBE = 0x9066;
		public static uint GL_UNSIGNED_INT_IMAGE_BUFFER = 0x9067;
		public static uint GL_UNSIGNED_INT_IMAGE_1D_ARRAY = 0x9068;
		public static uint GL_UNSIGNED_INT_IMAGE_2D_ARRAY = 0x9069;
		public static uint GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = 0x906A;
		public static uint GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE = 0x906B;
		public static uint GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY = 0x906C;
		public static uint GL_MAX_IMAGE_SAMPLES = 0x906D;
		public static uint GL_IMAGE_BINDING_FORMAT = 0x906E;
		public static uint GL_IMAGE_FORMAT_COMPATIBILITY_TYPE = 0x90C7;
		public static uint GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = 0x90C8;
		public static uint GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = 0x90C9;
		public static uint GL_MAX_VERTEX_IMAGE_UNIFORMS = 0x90CA;
		public static uint GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0x90CB;
		public static uint GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0x90CC;
		public static uint GL_MAX_GEOMETRY_IMAGE_UNIFORMS = 0x90CD;
		public static uint GL_MAX_FRAGMENT_IMAGE_UNIFORMS = 0x90CE;
		public static uint GL_MAX_COMBINED_IMAGE_UNIFORMS = 0x90CF;
		public static uint GL_COMPRESSED_RGBA_BPTC_UNORM = 0x8E8C;
		public static uint GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM = 0x8E8D;
		public static uint GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT = 0x8E8E;
		public static uint GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT = 0x8E8F;
		public static uint GL_TEXTURE_IMMUTABLE_FORMAT = 0x912F;

		public static uint GL_NUM_SHADING_LANGUAGE_VERSIONS = 0x82E9;
		public static uint GL_VERTEX_ATTRIB_ARRAY_LONG = 0x874E;
		public static uint GL_COMPRESSED_RGB8_ETC2 = 0x9274;
		public static uint GL_COMPRESSED_SRGB8_ETC2 = 0x9275;
		public static uint GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9276;
		public static uint GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 0x9277;
		public static uint GL_COMPRESSED_RGBA8_ETC2_EAC = 0x9278;
		public static uint GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 0x9279;
		public static uint GL_COMPRESSED_R11_EAC = 0x9270;
		public static uint GL_COMPRESSED_SIGNED_R11_EAC = 0x9271;
		public static uint GL_COMPRESSED_RG11_EAC = 0x9272;
		public static uint GL_COMPRESSED_SIGNED_RG11_EAC = 0x9273;
		public static uint GL_PRIMITIVE_RESTART_FIXED_INDEX = 0x8D69;
		public static uint GL_ANY_SAMPLES_PASSED_CONSERVATIVE = 0x8D6A;
		public static uint GL_MAX_ELEMENT_INDEX = 0x8D6B;
		public static uint GL_COMPUTE_SHADER = 0x91B9;
		public static uint GL_MAX_COMPUTE_UNIFORM_BLOCKS = 0x91BB;
		public static uint GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0x91BC;
		public static uint GL_MAX_COMPUTE_IMAGE_UNIFORMS = 0x91BD;
		public static uint GL_MAX_COMPUTE_SHARED_MEMORY_SIZE = 0x8262;
		public static uint GL_MAX_COMPUTE_UNIFORM_COMPONENTS = 0x8263;
		public static uint GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0x8264;
		public static uint GL_MAX_COMPUTE_ATOMIC_COUNTERS = 0x8265;
		public static uint GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0x8266;
		public static uint GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0x90EB;
		public static uint GL_MAX_COMPUTE_WORK_GROUP_COUNT = 0x91BE;
		public static uint GL_MAX_COMPUTE_WORK_GROUP_SIZE = 0x91BF;
		public static uint GL_COMPUTE_WORK_GROUP_SIZE = 0x8267;
		public static uint GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER = 0x90EC;
		public static uint GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER = 0x90ED;
		public static uint GL_DISPATCH_INDIRECT_BUFFER = 0x90EE;
		public static uint GL_DISPATCH_INDIRECT_BUFFER_BINDING = 0x90EF;
		public static uint GL_COMPUTE_SHADER_BIT = 0x00000020;
		public static uint GL_DEBUG_OUTPUT_SYNCHRONOUS = 0x8242;
		public static uint GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = 0x8243;
		public static uint GL_DEBUG_CALLBACK_FUNCTION = 0x8244;
		public static uint GL_DEBUG_CALLBACK_USER_PARAM = 0x8245;
		public static uint GL_DEBUG_SOURCE_API = 0x8246;
		public static uint GL_DEBUG_SOURCE_WINDOW_SYSTEM = 0x8247;
		public static uint GL_DEBUG_SOURCE_SHADER_COMPILER = 0x8248;
		public static uint GL_DEBUG_SOURCE_THIRD_PARTY = 0x8249;
		public static uint GL_DEBUG_SOURCE_APPLICATION = 0x824A;
		public static uint GL_DEBUG_SOURCE_OTHER = 0x824B;
		public static uint GL_DEBUG_TYPE_ERROR = 0x824C;
		public static uint GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR = 0x824D;
		public static uint GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR = 0x824E;
		public static uint GL_DEBUG_TYPE_PORTABILITY = 0x824F;
		public static uint GL_DEBUG_TYPE_PERFORMANCE = 0x8250;
		public static uint GL_DEBUG_TYPE_OTHER = 0x8251;
		public static uint GL_MAX_DEBUG_MESSAGE_LENGTH = 0x9143;
		public static uint GL_MAX_DEBUG_LOGGED_MESSAGES = 0x9144;
		public static uint GL_DEBUG_LOGGED_MESSAGES = 0x9145;
		public static uint GL_DEBUG_SEVERITY_HIGH = 0x9146;
		public static uint GL_DEBUG_SEVERITY_MEDIUM = 0x9147;
		public static uint GL_DEBUG_SEVERITY_LOW = 0x9148;
		public static uint GL_DEBUG_TYPE_MARKER = 0x8268;
		public static uint GL_DEBUG_TYPE_PUSH_GROUP = 0x8269;
		public static uint GL_DEBUG_TYPE_POP_GROUP = 0x826A;
		public static uint GL_DEBUG_SEVERITY_NOTIFICATION = 0x826B;
		public static uint GL_MAX_DEBUG_GROUP_STACK_DEPTH = 0x826C;
		public static uint GL_DEBUG_GROUP_STACK_DEPTH = 0x826D;
		public static uint GL_BUFFER = 0x82E0;
		public static uint GL_SHADER = 0x82E1;
		public static uint GL_PROGRAM = 0x82E2;
		public static uint GL_QUERY = 0x82E3;
		public static uint GL_PROGRAM_PIPELINE = 0x82E4;
		public static uint GL_SAMPLER = 0x82E6;
		public static uint GL_MAX_LABEL_LENGTH = 0x82E8;
		public static uint GL_DEBUG_OUTPUT = 0x92E0;
		public static uint GL_CONTEXT_FLAG_DEBUG_BIT = 0x00000002;
		public static uint GL_MAX_UNIFORM_LOCATIONS = 0x826E;
		public static uint GL_FRAMEBUFFER_DEFAULT_WIDTH = 0x9310;
		public static uint GL_FRAMEBUFFER_DEFAULT_HEIGHT = 0x9311;
		public static uint GL_FRAMEBUFFER_DEFAULT_LAYERS = 0x9312;
		public static uint GL_FRAMEBUFFER_DEFAULT_SAMPLES = 0x9313;
		public static uint GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = 0x9314;
		public static uint GL_MAX_FRAMEBUFFER_WIDTH = 0x9315;
		public static uint GL_MAX_FRAMEBUFFER_HEIGHT = 0x9316;
		public static uint GL_MAX_FRAMEBUFFER_LAYERS = 0x9317;
		public static uint GL_MAX_FRAMEBUFFER_SAMPLES = 0x9318;
		public static uint GL_INTERNALFORMAT_SUPPORTED = 0x826F;
		public static uint GL_INTERNALFORMAT_PREFERRED = 0x8270;
		public static uint GL_INTERNALFORMAT_RED_SIZE = 0x8271;
		public static uint GL_INTERNALFORMAT_GREEN_SIZE = 0x8272;
		public static uint GL_INTERNALFORMAT_BLUE_SIZE = 0x8273;
		public static uint GL_INTERNALFORMAT_ALPHA_SIZE = 0x8274;
		public static uint GL_INTERNALFORMAT_DEPTH_SIZE = 0x8275;
		public static uint GL_INTERNALFORMAT_STENCIL_SIZE = 0x8276;
		public static uint GL_INTERNALFORMAT_SHARED_SIZE = 0x8277;
		public static uint GL_INTERNALFORMAT_RED_TYPE = 0x8278;
		public static uint GL_INTERNALFORMAT_GREEN_TYPE = 0x8279;
		public static uint GL_INTERNALFORMAT_BLUE_TYPE = 0x827A;
		public static uint GL_INTERNALFORMAT_ALPHA_TYPE = 0x827B;
		public static uint GL_INTERNALFORMAT_DEPTH_TYPE = 0x827C;
		public static uint GL_INTERNALFORMAT_STENCIL_TYPE = 0x827D;
		public static uint GL_MAX_WIDTH = 0x827E;
		public static uint GL_MAX_HEIGHT = 0x827F;
		public static uint GL_MAX_DEPTH = 0x8280;
		public static uint GL_MAX_LAYERS = 0x8281;
		public static uint GL_MAX_COMBINED_DIMENSIONS = 0x8282;
		public static uint GL_COLOR_COMPONENTS = 0x8283;
		public static uint GL_DEPTH_COMPONENTS = 0x8284;
		public static uint GL_STENCIL_COMPONENTS = 0x8285;
		public static uint GL_COLOR_RENDERABLE = 0x8286;
		public static uint GL_DEPTH_RENDERABLE = 0x8287;
		public static uint GL_STENCIL_RENDERABLE = 0x8288;
		public static uint GL_FRAMEBUFFER_RENDERABLE = 0x8289;
		public static uint GL_FRAMEBUFFER_RENDERABLE_LAYERED = 0x828A;
		public static uint GL_FRAMEBUFFER_BLEND = 0x828B;
		public static uint GL_READ_PIXELS = 0x828C;
		public static uint GL_READ_PIXELS_FORMAT = 0x828D;
		public static uint GL_READ_PIXELS_TYPE = 0x828E;
		public static uint GL_TEXTURE_IMAGE_FORMAT = 0x828F;
		public static uint GL_TEXTURE_IMAGE_TYPE = 0x8290;
		public static uint GL_GET_TEXTURE_IMAGE_FORMAT = 0x8291;
		public static uint GL_GET_TEXTURE_IMAGE_TYPE = 0x8292;
		public static uint GL_MIPMAP = 0x8293;
		public static uint GL_MANUAL_GENERATE_MIPMAP = 0x8294;
		public static uint GL_AUTO_GENERATE_MIPMAP = 0x8295;
		public static uint GL_COLOR_ENCODING = 0x8296;
		public static uint GL_SRGB_READ = 0x8297;
		public static uint GL_SRGB_WRITE = 0x8298;
		public static uint GL_FILTER = 0x829A;
		public static uint GL_VERTEX_TEXTURE = 0x829B;
		public static uint GL_TESS_CONTROL_TEXTURE = 0x829C;
		public static uint GL_TESS_EVALUATION_TEXTURE = 0x829D;
		public static uint GL_GEOMETRY_TEXTURE = 0x829E;
		public static uint GL_FRAGMENT_TEXTURE = 0x829F;
		public static uint GL_COMPUTE_TEXTURE = 0x82A0;
		public static uint GL_TEXTURE_SHADOW = 0x82A1;
		public static uint GL_TEXTURE_GATHER = 0x82A2;
		public static uint GL_TEXTURE_GATHER_SHADOW = 0x82A3;
		public static uint GL_SHADER_IMAGE_LOAD = 0x82A4;
		public static uint GL_SHADER_IMAGE_STORE = 0x82A5;
		public static uint GL_SHADER_IMAGE_ATOMIC = 0x82A6;
		public static uint GL_IMAGE_TEXEL_SIZE = 0x82A7;
		public static uint GL_IMAGE_COMPATIBILITY_CLASS = 0x82A8;
		public static uint GL_IMAGE_PIXEL_FORMAT = 0x82A9;
		public static uint GL_IMAGE_PIXEL_TYPE = 0x82AA;
		public static uint GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_TEST = 0x82AC;
		public static uint GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_TEST = 0x82AD;
		public static uint GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_WRITE = 0x82AE;
		public static uint GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_WRITE = 0x82AF;
		public static uint GL_TEXTURE_COMPRESSED_BLOCK_WIDTH = 0x82B1;
		public static uint GL_TEXTURE_COMPRESSED_BLOCK_HEIGHT = 0x82B2;
		public static uint GL_TEXTURE_COMPRESSED_BLOCK_SIZE = 0x82B3;
		public static uint GL_CLEAR_BUFFER = 0x82B4;
		public static uint GL_TEXTURE_VIEW = 0x82B5;
		public static uint GL_VIEW_COMPATIBILITY_CLASS = 0x82B6;
		public static uint GL_FULL_SUPPORT = 0x82B7;
		public static uint GL_CAVEAT_SUPPORT = 0x82B8;
		public static uint GL_IMAGE_CLASS_4_X_32 = 0x82B9;
		public static uint GL_IMAGE_CLASS_2_X_32 = 0x82BA;
		public static uint GL_IMAGE_CLASS_1_X_32 = 0x82BB;
		public static uint GL_IMAGE_CLASS_4_X_16 = 0x82BC;
		public static uint GL_IMAGE_CLASS_2_X_16 = 0x82BD;
		public static uint GL_IMAGE_CLASS_1_X_16 = 0x82BE;
		public static uint GL_IMAGE_CLASS_4_X_8 = 0x82BF;
		public static uint GL_IMAGE_CLASS_2_X_8 = 0x82C0;
		public static uint GL_IMAGE_CLASS_1_X_8 = 0x82C1;
		public static uint GL_IMAGE_CLASS_11_11_10 = 0x82C2;
		public static uint GL_IMAGE_CLASS_10_10_10_2 = 0x82C3;
		public static uint GL_VIEW_CLASS_128_BITS = 0x82C4;
		public static uint GL_VIEW_CLASS_96_BITS = 0x82C5;
		public static uint GL_VIEW_CLASS_64_BITS = 0x82C6;
		public static uint GL_VIEW_CLASS_48_BITS = 0x82C7;
		public static uint GL_VIEW_CLASS_32_BITS = 0x82C8;
		public static uint GL_VIEW_CLASS_24_BITS = 0x82C9;
		public static uint GL_VIEW_CLASS_16_BITS = 0x82CA;
		public static uint GL_VIEW_CLASS_8_BITS = 0x82CB;
		public static uint GL_VIEW_CLASS_S3TC_DXT1_RGB = 0x82CC;
		public static uint GL_VIEW_CLASS_S3TC_DXT1_RGBA = 0x82CD;
		public static uint GL_VIEW_CLASS_S3TC_DXT3_RGBA = 0x82CE;
		public static uint GL_VIEW_CLASS_S3TC_DXT5_RGBA = 0x82CF;
		public static uint GL_VIEW_CLASS_RGTC1_RED = 0x82D0;
		public static uint GL_VIEW_CLASS_RGTC2_RG = 0x82D1;
		public static uint GL_VIEW_CLASS_BPTC_UNORM = 0x82D2;
		public static uint GL_VIEW_CLASS_BPTC_FLOAT = 0x82D3;
		public static uint GL_UNIFORM = 0x92E1;
		public static uint GL_UNIFORM_BLOCK = 0x92E2;
		public static uint GL_PROGRAM_INPUT = 0x92E3;
		public static uint GL_PROGRAM_OUTPUT = 0x92E4;
		public static uint GL_BUFFER_VARIABLE = 0x92E5;
		public static uint GL_SHADER_STORAGE_BLOCK = 0x92E6;
		public static uint GL_VERTEX_SUBROUTINE = 0x92E8;
		public static uint GL_TESS_CONTROL_SUBROUTINE = 0x92E9;
		public static uint GL_TESS_EVALUATION_SUBROUTINE = 0x92EA;
		public static uint GL_GEOMETRY_SUBROUTINE = 0x92EB;
		public static uint GL_FRAGMENT_SUBROUTINE = 0x92EC;
		public static uint GL_COMPUTE_SUBROUTINE = 0x92ED;
		public static uint GL_VERTEX_SUBROUTINE_UNIFORM = 0x92EE;
		public static uint GL_TESS_CONTROL_SUBROUTINE_UNIFORM = 0x92EF;
		public static uint GL_TESS_EVALUATION_SUBROUTINE_UNIFORM = 0x92F0;
		public static uint GL_GEOMETRY_SUBROUTINE_UNIFORM = 0x92F1;
		public static uint GL_FRAGMENT_SUBROUTINE_UNIFORM = 0x92F2;
		public static uint GL_COMPUTE_SUBROUTINE_UNIFORM = 0x92F3;
		public static uint GL_TRANSFORM_FEEDBACK_VARYING = 0x92F4;
		public static uint GL_ACTIVE_RESOURCES = 0x92F5;
		public static uint GL_MAX_NAME_LENGTH = 0x92F6;
		public static uint GL_MAX_NUM_ACTIVE_VARIABLES = 0x92F7;
		public static uint GL_MAX_NUM_COMPATIBLE_SUBROUTINES = 0x92F8;
		public static uint GL_NAME_LENGTH = 0x92F9;
		public static uint GL_TYPE = 0x92FA;
		public static uint GL_ARRAY_SIZE = 0x92FB;
		public static uint GL_OFFSET = 0x92FC;
		public static uint GL_BLOCK_INDEX = 0x92FD;
		public static uint GL_ARRAY_STRIDE = 0x92FE;
		public static uint GL_MATRIX_STRIDE = 0x92FF;
		public static uint GL_IS_ROW_MAJOR = 0x9300;
		public static uint GL_ATOMIC_COUNTER_BUFFER_INDEX = 0x9301;
		public static uint GL_BUFFER_BINDING = 0x9302;
		public static uint GL_BUFFER_DATA_SIZE = 0x9303;
		public static uint GL_NUM_ACTIVE_VARIABLES = 0x9304;
		public static uint GL_ACTIVE_VARIABLES = 0x9305;
		public static uint GL_REFERENCED_BY_VERTEX_SHADER = 0x9306;
		public static uint GL_REFERENCED_BY_TESS_CONTROL_SHADER = 0x9307;
		public static uint GL_REFERENCED_BY_TESS_EVALUATION_SHADER = 0x9308;
		public static uint GL_REFERENCED_BY_GEOMETRY_SHADER = 0x9309;
		public static uint GL_REFERENCED_BY_FRAGMENT_SHADER = 0x930A;
		public static uint GL_REFERENCED_BY_COMPUTE_SHADER = 0x930B;
		public static uint GL_TOP_LEVEL_ARRAY_SIZE = 0x930C;
		public static uint GL_TOP_LEVEL_ARRAY_STRIDE = 0x930D;
		public static uint GL_LOCATION = 0x930E;
		public static uint GL_LOCATION_INDEX = 0x930F;
		public static uint GL_IS_PER_PATCH = 0x92E7;
		public static uint GL_SHADER_STORAGE_BUFFER = 0x90D2;
		public static uint GL_SHADER_STORAGE_BUFFER_BINDING = 0x90D3;
		public static uint GL_SHADER_STORAGE_BUFFER_START = 0x90D4;
		public static uint GL_SHADER_STORAGE_BUFFER_SIZE = 0x90D5;
		public static uint GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0x90D6;
		public static uint GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0x90D7;
		public static uint GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0x90D8;
		public static uint GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0x90D9;
		public static uint GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0x90DA;
		public static uint GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0x90DB;
		public static uint GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0x90DC;
		public static uint GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0x90DD;
		public static uint GL_MAX_SHADER_STORAGE_BLOCK_SIZE = 0x90DE;
		public static uint GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0x90DF;
		public static uint GL_SHADER_STORAGE_BARRIER_BIT = 0x00002000;
		public static uint GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0x8F39;
		public static uint GL_DEPTH_STENCIL_TEXTURE_MODE = 0x90EA;
		public static uint GL_TEXTURE_BUFFER_OFFSET = 0x919D;
		public static uint GL_TEXTURE_BUFFER_SIZE = 0x919E;
		public static uint GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT = 0x919F;
		public static uint GL_TEXTURE_VIEW_MIN_LEVEL = 0x82DB;
		public static uint GL_TEXTURE_VIEW_NUM_LEVELS = 0x82DC;
		public static uint GL_TEXTURE_VIEW_MIN_LAYER = 0x82DD;
		public static uint GL_TEXTURE_VIEW_NUM_LAYERS = 0x82DE;
		public static uint GL_TEXTURE_IMMUTABLE_LEVELS = 0x82DF;
		public static uint GL_VERTEX_ATTRIB_BINDING = 0x82D4;
		public static uint GL_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D5;
		public static uint GL_VERTEX_BINDING_DIVISOR = 0x82D6;
		public static uint GL_VERTEX_BINDING_OFFSET = 0x82D7;
		public static uint GL_VERTEX_BINDING_STRIDE = 0x82D8;
		public static uint GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0x82D9;
		public static uint GL_MAX_VERTEX_ATTRIB_BINDINGS = 0x82DA;
		public static uint GL_VERTEX_BINDING_BUFFER = 0x8F4F;

		public static uint GL_MAX_VERTEX_ATTRIB_STRIDE = 0x82E5;
		public static uint GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED = 0x8221;
		public static uint GL_TEXTURE_BUFFER_BINDING = 0x8C2A;
		public static uint GL_MAP_PERSISTENT_BIT = 0x0040;
		public static uint GL_MAP_COHERENT_BIT = 0x0080;
		public static uint GL_DYNAMIC_STORAGE_BIT = 0x0100;
		public static uint GL_CLIENT_STORAGE_BIT = 0x0200;
		public static uint GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT = 0x00004000;
		public static uint GL_BUFFER_IMMUTABLE_STORAGE = 0x821F;
		public static uint GL_BUFFER_STORAGE_FLAGS = 0x8220;
		public static uint GL_CLEAR_TEXTURE = 0x9365;
		public static uint GL_LOCATION_COMPONENT = 0x934A;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_INDEX = 0x934B;
		public static uint GL_TRANSFORM_FEEDBACK_BUFFER_STRIDE = 0x934C;
		public static uint GL_QUERY_BUFFER = 0x9192;
		public static uint GL_QUERY_BUFFER_BARRIER_BIT = 0x00008000;
		public static uint GL_QUERY_BUFFER_BINDING = 0x9193;
		public static uint GL_QUERY_RESULT_NO_WAIT = 0x9194;
		public static uint GL_MIRROR_CLAMP_TO_EDGE = 0x8743;

		public static uint GL_CONTEXT_LOST = 0x0507;
		public static uint GL_NEGATIVE_ONE_TO_ONE = 0x935E;
		public static uint GL_ZERO_TO_ONE = 0x935F;
		public static uint GL_CLIP_ORIGIN = 0x935C;
		public static uint GL_CLIP_DEPTH_MODE = 0x935D;
		public static uint GL_QUERY_WAIT_INVERTED = 0x8E17;
		public static uint GL_QUERY_NO_WAIT_INVERTED = 0x8E18;
		public static uint GL_QUERY_BY_REGION_WAIT_INVERTED = 0x8E19;
		public static uint GL_QUERY_BY_REGION_NO_WAIT_INVERTED = 0x8E1A;
		public static uint GL_MAX_CULL_DISTANCES = 0x82F9;
		public static uint GL_MAX_COMBINED_CLIP_AND_CULL_DISTANCES = 0x82FA;
		public static uint GL_TEXTURE_TARGET = 0x1006;
		public static uint GL_QUERY_TARGET = 0x82EA;
		public static uint GL_GUILTY_CONTEXT_RESET = 0x8253;
		public static uint GL_INNOCENT_CONTEXT_RESET = 0x8254;
		public static uint GL_UNKNOWN_CONTEXT_RESET = 0x8255;
		public static uint GL_RESET_NOTIFICATION_STRATEGY = 0x8256;
		public static uint GL_LOSE_CONTEXT_ON_RESET = 0x8252;
		public static uint GL_NO_RESET_NOTIFICATION = 0x8261;
		public static uint GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT = 0x00000004;
		public static uint GL_CONTEXT_RELEASE_BEHAVIOR = 0x82FB;
		public static uint GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = 0x82FC;

		public static uint GL_SHADER_BINARY_FORMAT_SPIR_V = 0x9551;
		public static uint GL_SPIR_V_BINARY = 0x9552;
		public static uint GL_PARAMETER_BUFFER = 0x80EE;
		public static uint GL_PARAMETER_BUFFER_BINDING = 0x80EF;
		public static uint GL_CONTEXT_FLAG_NO_ERROR_BIT = 0x00000008;
		public static uint GL_VERTICES_SUBMITTED = 0x82EE;
		public static uint GL_PRIMITIVES_SUBMITTED = 0x82EF;
		public static uint GL_VERTEX_SHADER_INVOCATIONS = 0x82F0;
		public static uint GL_TESS_CONTROL_SHADER_PATCHES = 0x82F1;
		public static uint GL_TESS_EVALUATION_SHADER_INVOCATIONS = 0x82F2;
		public static uint GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED = 0x82F3;
		public static uint GL_FRAGMENT_SHADER_INVOCATIONS = 0x82F4;
		public static uint GL_COMPUTE_SHADER_INVOCATIONS = 0x82F5;
		public static uint GL_CLIPPING_INPUT_PRIMITIVES = 0x82F6;
		public static uint GL_CLIPPING_OUTPUT_PRIMITIVES = 0x82F7;
		public static uint GL_POLYGON_OFFSET_CLAMP = 0x8E1B;
		public static uint GL_SPIR_V_EXTENSIONS = 0x9553;
		public static uint GL_NUM_SPIR_V_EXTENSIONS = 0x9554;
		public static uint GL_TEXTURE_MAX_ANISOTROPY = 0x84FE;
		public static uint GL_MAX_TEXTURE_MAX_ANISOTROPY = 0x84FF;
		public static uint GL_TRANSFORM_FEEDBACK_OVERFLOW = 0x82EC;
		public static uint GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW = 0x82ED;


		[DllImport("opengl32.dll")]
		private static extern IntPtr wglGetProcAddress(string name);

		[DllImport("kernel32.dll")]
		private static extern IntPtr LoadLibrary(string dllToLoad);

		[DllImport("kernel32.dll")]
		private static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);

		[DllImport("kernel32.dll")]
		private static extern bool FreeLibrary(IntPtr hModule);

		public static Call_glAccum glAccum;
		public static Call_glAlphaFunc glAlphaFunc;
		public static Call_glAreTexturesResident glAreTexturesResident;
		public static Call_glArrayElement glArrayElement;
		public static Call_glBegin glBegin;
		public static Call_glBindTexture glBindTexture;
		public static Call_glBitmap glBitmap;
		public static Call_glBlendFunc glBlendFunc;
		public static Call_glCallList glCallList;
		public static Call_glCallLists glCallLists;
		public static Call_glClear glClear;
		public static Call_glClearAccum glClearAccum;
		public static Call_glClearColor glClearColor;
		public static Call_glClearDepth glClearDepth;
		public static Call_glClearIndex glClearIndex;
		public static Call_glClearStencil glClearStencil;
		public static Call_glClipPlane glClipPlane;
		public static Call_glColor3b glColor3b;
		public static Call_glColor3bv glColor3bv;
		public static Call_glColor3d glColor3d;
		public static Call_glColor3dv glColor3dv;
		public static Call_glColor3f glColor3f;
		public static Call_glColor3fv glColor3fv;
		public static Call_glColor3i glColor3i;
		public static Call_glColor3iv glColor3iv;
		public static Call_glColor3s glColor3s;
		public static Call_glColor3sv glColor3sv;
		public static Call_glColor3ub glColor3ub;
		public static Call_glColor3ubv glColor3ubv;
		public static Call_glColor3ui glColor3ui;
		public static Call_glColor3uiv glColor3uiv;
		public static Call_glColor3us glColor3us;
		public static Call_glColor3usv glColor3usv;
		public static Call_glColor4b glColor4b;
		public static Call_glColor4bv glColor4bv;
		public static Call_glColor4d glColor4d;
		public static Call_glColor4dv glColor4dv;
		public static Call_glColor4f glColor4f;
		public static Call_glColor4fv glColor4fv;
		public static Call_glColor4i glColor4i;
		public static Call_glColor4iv glColor4iv;
		public static Call_glColor4s glColor4s;
		public static Call_glColor4sv glColor4sv;
		public static Call_glColor4ub glColor4ub;
		public static Call_glColor4ubv glColor4ubv;
		public static Call_glColor4ui glColor4ui;
		public static Call_glColor4uiv glColor4uiv;
		public static Call_glColor4us glColor4us;
		public static Call_glColor4usv glColor4usv;
		public static Call_glColorMask glColorMask;
		public static Call_glColorMaterial glColorMaterial;
		public static Call_glColorPointer glColorPointer;
		public static Call_glCopyPixels glCopyPixels;
		public static Call_glCopyTexImage1D glCopyTexImage1D;
		public static Call_glCopyTexImage2D glCopyTexImage2D;
		public static Call_glCopyTexSubImage1D glCopyTexSubImage1D;
		public static Call_glCopyTexSubImage2D glCopyTexSubImage2D;
		public static Call_glCullFace glCullFace;
		public static Call_glDeleteLists glDeleteLists;
		public static Call_glDeleteTextures glDeleteTextures;
		public static Call_glDepthFunc glDepthFunc;
		public static Call_glDepthMask glDepthMask;
		public static Call_glDepthRange glDepthRange;
		public static Call_glDisable glDisable;
		public static Call_glDisableClientState glDisableClientState;
		public static Call_glDrawArrays glDrawArrays;
		public static Call_glDrawBuffer glDrawBuffer;
		public static Call_glDrawElements glDrawElements;
		public static Call_glDrawPixels glDrawPixels;
		public static Call_glEdgeFlag glEdgeFlag;
		public static Call_glEdgeFlagPointer glEdgeFlagPointer;
		public static Call_glEdgeFlagv glEdgeFlagv;
		public static Call_glEnable glEnable;
		public static Call_glEnableClientState glEnableClientState;
		public static Call_glEnd glEnd;
		public static Call_glEndList glEndList;
		public static Call_glEvalCoord1d glEvalCoord1d;
		public static Call_glEvalCoord1dv glEvalCoord1dv;
		public static Call_glEvalCoord1f glEvalCoord1f;
		public static Call_glEvalCoord1fv glEvalCoord1fv;
		public static Call_glEvalCoord2d glEvalCoord2d;
		public static Call_glEvalCoord2dv glEvalCoord2dv;
		public static Call_glEvalCoord2f glEvalCoord2f;
		public static Call_glEvalCoord2fv glEvalCoord2fv;
		public static Call_glEvalMesh1 glEvalMesh1;
		public static Call_glEvalMesh2 glEvalMesh2;
		public static Call_glEvalPoint1 glEvalPoint1;
		public static Call_glEvalPoint2 glEvalPoint2;
		public static Call_glFeedbackBuffer glFeedbackBuffer;
		public static Call_glFinish glFinish;
		public static Call_glFlush glFlush;
		public static Call_glFogf glFogf;
		public static Call_glFogfv glFogfv;
		public static Call_glFogi glFogi;
		public static Call_glFogiv glFogiv;
		public static Call_glFrontFace glFrontFace;
		public static Call_glFrustum glFrustum;
		public static Call_glGenLists glGenLists;
		public static Call_glGenTextures glGenTextures;
		public static Call_glGetBooleanv glGetBooleanv;
		public static Call_glGetClipPlane glGetClipPlane;
		public static Call_glGetDoublev glGetDoublev;
		public static Call_glGetError glGetError;
		public static Call_glGetFloatv glGetFloatv;
		public static Call_glGetIntegerv glGetIntegerv;
		public static Call_glGetLightfv glGetLightfv;
		public static Call_glGetLightiv glGetLightiv;
		public static Call_glGetMapdv glGetMapdv;
		public static Call_glGetMapfv glGetMapfv;
		public static Call_glGetMapiv glGetMapiv;
		public static Call_glGetMaterialfv glGetMaterialfv;
		public static Call_glGetMaterialiv glGetMaterialiv;
		public static Call_glGetPixelMapfv glGetPixelMapfv;
		public static Call_glGetPixelMapuiv glGetPixelMapuiv;
		public static Call_glGetPixelMapusv glGetPixelMapusv;
		public static Call_glGetPointerv glGetPointerv;
		public static Call_glGetPolygonStipple glGetPolygonStipple;
		public static Call_glGetString glGetString;
		public static Call_glGetTexEnvfv glGetTexEnvfv;
		public static Call_glGetTexEnviv glGetTexEnviv;
		public static Call_glGetTexGendv glGetTexGendv;
		public static Call_glGetTexGenfv glGetTexGenfv;
		public static Call_glGetTexGeniv glGetTexGeniv;
		public static Call_glGetTexImage glGetTexImage;
		public static Call_glGetTexLevelParameterfv glGetTexLevelParameterfv;
		public static Call_glGetTexLevelParameteriv glGetTexLevelParameteriv;
		public static Call_glGetTexParameterfv glGetTexParameterfv;
		public static Call_glGetTexParameteriv glGetTexParameteriv;
		public static Call_glHint glHint;
		public static Call_glIndexMask glIndexMask;
		public static Call_glIndexPointer glIndexPointer;
		public static Call_glIndexd glIndexd;
		public static Call_glIndexdv glIndexdv;
		public static Call_glIndexf glIndexf;
		public static Call_glIndexfv glIndexfv;
		public static Call_glIndexi glIndexi;
		public static Call_glIndexiv glIndexiv;
		public static Call_glIndexs glIndexs;
		public static Call_glIndexsv glIndexsv;
		public static Call_glIndexub glIndexub;
		public static Call_glIndexubv glIndexubv;
		public static Call_glInitNames glInitNames;
		public static Call_glInterleavedArrays glInterleavedArrays;
		public static Call_glIsEnabled glIsEnabled;
		public static Call_glIsList glIsList;
		public static Call_glIsTexture glIsTexture;
		public static Call_glLightModelf glLightModelf;
		public static Call_glLightModelfv glLightModelfv;
		public static Call_glLightModeli glLightModeli;
		public static Call_glLightModeliv glLightModeliv;
		public static Call_glLightf glLightf;
		public static Call_glLightfv glLightfv;
		public static Call_glLighti glLighti;
		public static Call_glLightiv glLightiv;
		public static Call_glLineStipple glLineStipple;
		public static Call_glLineWidth glLineWidth;
		public static Call_glListBase glListBase;
		public static Call_glLoadIdentity glLoadIdentity;
		public static Call_glLoadMatrixd glLoadMatrixd;
		public static Call_glLoadMatrixf glLoadMatrixf;
		public static Call_glLoadName glLoadName;
		public static Call_glLogicOp glLogicOp;
		public static Call_glMap1d glMap1d;
		public static Call_glMap1f glMap1f;
		public static Call_glMap2d glMap2d;
		public static Call_glMap2f glMap2f;
		public static Call_glMapGrid1d glMapGrid1d;
		public static Call_glMapGrid1f glMapGrid1f;
		public static Call_glMapGrid2d glMapGrid2d;
		public static Call_glMapGrid2f glMapGrid2f;
		public static Call_glMaterialf glMaterialf;
		public static Call_glMaterialfv glMaterialfv;
		public static Call_glMateriali glMateriali;
		public static Call_glMaterialiv glMaterialiv;
		public static Call_glMatrixMode glMatrixMode;
		public static Call_glMultMatrixd glMultMatrixd;
		public static Call_glMultMatrixf glMultMatrixf;
		public static Call_glNewList glNewList;
		public static Call_glNormal3b glNormal3b;
		public static Call_glNormal3bv glNormal3bv;
		public static Call_glNormal3d glNormal3d;
		public static Call_glNormal3dv glNormal3dv;
		public static Call_glNormal3f glNormal3f;
		public static Call_glNormal3fv glNormal3fv;
		public static Call_glNormal3i glNormal3i;
		public static Call_glNormal3iv glNormal3iv;
		public static Call_glNormal3s glNormal3s;
		public static Call_glNormal3sv glNormal3sv;
		public static Call_glNormalPointer glNormalPointer;
		public static Call_glOrtho glOrtho;
		public static Call_glPassThrough glPassThrough;
		public static Call_glPixelMapfv glPixelMapfv;
		public static Call_glPixelMapuiv glPixelMapuiv;
		public static Call_glPixelMapusv glPixelMapusv;
		public static Call_glPixelStoref glPixelStoref;
		public static Call_glPixelStorei glPixelStorei;
		public static Call_glPixelTransferf glPixelTransferf;
		public static Call_glPixelTransferi glPixelTransferi;
		public static Call_glPixelZoom glPixelZoom;
		public static Call_glPointSize glPointSize;
		public static Call_glPolygonMode glPolygonMode;
		public static Call_glPolygonOffset glPolygonOffset;
		public static Call_glPolygonStipple glPolygonStipple;
		public static Call_glPopAttrib glPopAttrib;
		public static Call_glPopClientAttrib glPopClientAttrib;
		public static Call_glPopMatrix glPopMatrix;
		public static Call_glPopName glPopName;
		public static Call_glPrioritizeTextures glPrioritizeTextures;
		public static Call_glPushAttrib glPushAttrib;
		public static Call_glPushClientAttrib glPushClientAttrib;
		public static Call_glPushMatrix glPushMatrix;
		public static Call_glPushName glPushName;
		public static Call_glRasterPos2d glRasterPos2d;
		public static Call_glRasterPos2dv glRasterPos2dv;
		public static Call_glRasterPos2f glRasterPos2f;
		public static Call_glRasterPos2fv glRasterPos2fv;
		public static Call_glRasterPos2i glRasterPos2i;
		public static Call_glRasterPos2iv glRasterPos2iv;
		public static Call_glRasterPos2s glRasterPos2s;
		public static Call_glRasterPos2sv glRasterPos2sv;
		public static Call_glRasterPos3d glRasterPos3d;
		public static Call_glRasterPos3dv glRasterPos3dv;
		public static Call_glRasterPos3f glRasterPos3f;
		public static Call_glRasterPos3fv glRasterPos3fv;
		public static Call_glRasterPos3i glRasterPos3i;
		public static Call_glRasterPos3iv glRasterPos3iv;
		public static Call_glRasterPos3s glRasterPos3s;
		public static Call_glRasterPos3sv glRasterPos3sv;
		public static Call_glRasterPos4d glRasterPos4d;
		public static Call_glRasterPos4dv glRasterPos4dv;
		public static Call_glRasterPos4f glRasterPos4f;
		public static Call_glRasterPos4fv glRasterPos4fv;
		public static Call_glRasterPos4i glRasterPos4i;
		public static Call_glRasterPos4iv glRasterPos4iv;
		public static Call_glRasterPos4s glRasterPos4s;
		public static Call_glRasterPos4sv glRasterPos4sv;
		public static Call_glReadBuffer glReadBuffer;
		public static Call_glReadPixels glReadPixels;
		public static Call_glRectd glRectd;
		public static Call_glRectdv glRectdv;
		public static Call_glRectf glRectf;
		public static Call_glRectfv glRectfv;
		public static Call_glRecti glRecti;
		public static Call_glRectiv glRectiv;
		public static Call_glRects glRects;
		public static Call_glRectsv glRectsv;
		public static Call_glRenderMode glRenderMode;
		public static Call_glRotated glRotated;
		public static Call_glRotatef glRotatef;
		public static Call_glScaled glScaled;
		public static Call_glScalef glScalef;
		public static Call_glScissor glScissor;
		public static Call_glSelectBuffer glSelectBuffer;
		public static Call_glShadeModel glShadeModel;
		public static Call_glStencilFunc glStencilFunc;
		public static Call_glStencilMask glStencilMask;
		public static Call_glStencilOp glStencilOp;
		public static Call_glTexCoord1d glTexCoord1d;
		public static Call_glTexCoord1dv glTexCoord1dv;
		public static Call_glTexCoord1f glTexCoord1f;
		public static Call_glTexCoord1fv glTexCoord1fv;
		public static Call_glTexCoord1i glTexCoord1i;
		public static Call_glTexCoord1iv glTexCoord1iv;
		public static Call_glTexCoord1s glTexCoord1s;
		public static Call_glTexCoord1sv glTexCoord1sv;
		public static Call_glTexCoord2d glTexCoord2d;
		public static Call_glTexCoord2dv glTexCoord2dv;
		public static Call_glTexCoord2f glTexCoord2f;
		public static Call_glTexCoord2fv glTexCoord2fv;
		public static Call_glTexCoord2i glTexCoord2i;
		public static Call_glTexCoord2iv glTexCoord2iv;
		public static Call_glTexCoord2s glTexCoord2s;
		public static Call_glTexCoord2sv glTexCoord2sv;
		public static Call_glTexCoord3d glTexCoord3d;
		public static Call_glTexCoord3dv glTexCoord3dv;
		public static Call_glTexCoord3f glTexCoord3f;
		public static Call_glTexCoord3fv glTexCoord3fv;
		public static Call_glTexCoord3i glTexCoord3i;
		public static Call_glTexCoord3iv glTexCoord3iv;
		public static Call_glTexCoord3s glTexCoord3s;
		public static Call_glTexCoord3sv glTexCoord3sv;
		public static Call_glTexCoord4d glTexCoord4d;
		public static Call_glTexCoord4dv glTexCoord4dv;
		public static Call_glTexCoord4f glTexCoord4f;
		public static Call_glTexCoord4fv glTexCoord4fv;
		public static Call_glTexCoord4i glTexCoord4i;
		public static Call_glTexCoord4iv glTexCoord4iv;
		public static Call_glTexCoord4s glTexCoord4s;
		public static Call_glTexCoord4sv glTexCoord4sv;
		public static Call_glTexCoordPointer glTexCoordPointer;
		public static Call_glTexEnvf glTexEnvf;
		public static Call_glTexEnvfv glTexEnvfv;
		public static Call_glTexEnvi glTexEnvi;
		public static Call_glTexEnviv glTexEnviv;
		public static Call_glTexGend glTexGend;
		public static Call_glTexGendv glTexGendv;
		public static Call_glTexGenf glTexGenf;
		public static Call_glTexGenfv glTexGenfv;
		public static Call_glTexGeni glTexGeni;
		public static Call_glTexGeniv glTexGeniv;
		public static Call_glTexImage1D glTexImage1D;
		public static Call_glTexImage2D glTexImage2D;
		public static Call_glTexParameterf glTexParameterf;
		public static Call_glTexParameterfv glTexParameterfv;
		public static Call_glTexParameteri glTexParameteri;
		public static Call_glTexParameteriv glTexParameteriv;
		public static Call_glTexSubImage1D glTexSubImage1D;
		public static Call_glTexSubImage2D glTexSubImage2D;
		public static Call_glTranslated glTranslated;
		public static Call_glTranslatef glTranslatef;
		public static Call_glVertex2d glVertex2d;
		public static Call_glVertex2dv glVertex2dv;
		public static Call_glVertex2f glVertex2f;
		public static Call_glVertex2fv glVertex2fv;
		public static Call_glVertex2i glVertex2i;
		public static Call_glVertex2iv glVertex2iv;
		public static Call_glVertex2s glVertex2s;
		public static Call_glVertex2sv glVertex2sv;
		public static Call_glVertex3d glVertex3d;
		public static Call_glVertex3dv glVertex3dv;
		public static Call_glVertex3f glVertex3f;
		public static Call_glVertex3fv glVertex3fv;
		public static Call_glVertex3i glVertex3i;
		public static Call_glVertex3iv glVertex3iv;
		public static Call_glVertex3s glVertex3s;
		public static Call_glVertex3sv glVertex3sv;
		public static Call_glVertex4d glVertex4d;
		public static Call_glVertex4dv glVertex4dv;
		public static Call_glVertex4f glVertex4f;
		public static Call_glVertex4fv glVertex4fv;
		public static Call_glVertex4i glVertex4i;
		public static Call_glVertex4iv glVertex4iv;
		public static Call_glVertex4s glVertex4s;
		public static Call_glVertex4sv glVertex4sv;
		public static Call_glVertexPointer glVertexPointer;
		public static Call_glViewport glViewport;

		public static Call_glDrawRangeElements glDrawRangeElements;
		public static Call_glTexImage3D glTexImage3D;
		public static Call_glTexSubImage3D glTexSubImage3D;
		public static Call_glCopyTexSubImage3D glCopyTexSubImage3D;

		public static Call_glActiveTexture glActiveTexture;
		public static Call_glSampleCoverage glSampleCoverage;
		public static Call_glCompressedTexImage3D glCompressedTexImage3D;
		public static Call_glCompressedTexImage2D glCompressedTexImage2D;
		public static Call_glCompressedTexImage1D glCompressedTexImage1D;
		public static Call_glCompressedTexSubImage3D glCompressedTexSubImage3D;
		public static Call_glCompressedTexSubImage2D glCompressedTexSubImage2D;
		public static Call_glCompressedTexSubImage1D glCompressedTexSubImage1D;
		public static Call_glGetCompressedTexImage glGetCompressedTexImage;

		public static Call_glBlendFuncSeparate glBlendFuncSeparate;
		public static Call_glMultiDrawArrays glMultiDrawArrays;
		public static Call_glMultiDrawElements glMultiDrawElements;
		public static Call_glPointParameterf glPointParameterf;
		public static Call_glPointParameterfv glPointParameterfv;
		public static Call_glPointParameteri glPointParameteri;
		public static Call_glPointParameteriv glPointParameteriv;
		public static Call_glBlendColor glBlendColor;
		public static Call_glBlendEquation glBlendEquation;

		public static Call_glGenQueries glGenQueries;
		public static Call_glDeleteQueries glDeleteQueries;
		public static Call_glIsQuery glIsQuery;
		public static Call_glBeginQuery glBeginQuery;
		public static Call_glEndQuery glEndQuery;
		public static Call_glGetQueryiv glGetQueryiv;
		public static Call_glGetQueryObjectiv glGetQueryObjectiv;
		public static Call_glGetQueryObjectuiv glGetQueryObjectuiv;
		public static Call_glBindBuffer glBindBuffer;
		public static Call_glDeleteBuffers glDeleteBuffers;
		public static Call_glGenBuffers glGenBuffers;
		public static Call_glIsBuffer glIsBuffer;
		public static Call_glBufferData glBufferData;
		public static Call_glBufferSubData glBufferSubData;
		public static Call_glGetBufferSubData glGetBufferSubData;
		public static Call_glMapBuffer glMapBuffer;
		public static Call_glUnmapBuffer glUnmapBuffer;
		public static Call_glGetBufferParameteriv glGetBufferParameteriv;
		public static Call_glGetBufferPointerv glGetBufferPointerv;

		public static Call_glBlendEquationSeparate glBlendEquationSeparate;
		public static Call_glDrawBuffers glDrawBuffers;
		public static Call_glStencilOpSeparate glStencilOpSeparate;
		public static Call_glStencilFuncSeparate glStencilFuncSeparate;
		public static Call_glStencilMaskSeparate glStencilMaskSeparate;
		public static Call_glAttachShader glAttachShader;
		public static Call_glBindAttribLocation glBindAttribLocation;
		public static Call_glCompileShader glCompileShader;
		public static Call_glCreateProgram glCreateProgram;
		public static Call_glCreateShader glCreateShader;
		public static Call_glDeleteProgram glDeleteProgram;
		public static Call_glDeleteShader glDeleteShader;
		public static Call_glDetachShader glDetachShader;
		public static Call_glDisableVertexAttribArray glDisableVertexAttribArray;
		public static Call_glEnableVertexAttribArray glEnableVertexAttribArray;
		public static Call_glGetActiveAttrib glGetActiveAttrib;
		public static Call_glGetActiveUniform glGetActiveUniform;
		public static Call_glGetAttachedShaders glGetAttachedShaders;
		public static Call_glGetAttribLocation glGetAttribLocation;
		public static Call_glGetProgramiv glGetProgramiv;
		public static Call_glGetProgramInfoLog glGetProgramInfoLog;
		public static Call_glGetShaderiv glGetShaderiv;
		public static Call_glGetShaderInfoLog glGetShaderInfoLog;
		public static Call_glGetShaderSource glGetShaderSource;
		public static Call_glGetUniformLocation glGetUniformLocation;
		public static Call_glGetUniformfv glGetUniformfv;
		public static Call_glGetUniformiv glGetUniformiv;
		public static Call_glGetVertexAttribdv glGetVertexAttribdv;
		public static Call_glGetVertexAttribfv glGetVertexAttribfv;
		public static Call_glGetVertexAttribiv glGetVertexAttribiv;
		public static Call_glGetVertexAttribPointerv glGetVertexAttribPointerv;
		public static Call_glIsProgram glIsProgram;
		public static Call_glIsShader glIsShader;
		public static Call_glLinkProgram glLinkProgram;
		public static Call_glShaderSource glShaderSource;
		public static Call_glUseProgram glUseProgram;
		public static Call_glUniform1f glUniform1f;
		public static Call_glUniform2f glUniform2f;
		public static Call_glUniform3f glUniform3f;
		public static Call_glUniform4f glUniform4f;
		public static Call_glUniform1i glUniform1i;
		public static Call_glUniform2i glUniform2i;
		public static Call_glUniform3i glUniform3i;
		public static Call_glUniform4i glUniform4i;
		public static Call_glUniform1fv glUniform1fv;
		public static Call_glUniform2fv glUniform2fv;
		public static Call_glUniform3fv glUniform3fv;
		public static Call_glUniform4fv glUniform4fv;
		public static Call_glUniform1iv glUniform1iv;
		public static Call_glUniform2iv glUniform2iv;
		public static Call_glUniform3iv glUniform3iv;
		public static Call_glUniform4iv glUniform4iv;
		public static Call_glUniformMatrix2fv glUniformMatrix2fv;
		public static Call_glUniformMatrix3fv glUniformMatrix3fv;
		public static Call_glUniformMatrix4fv glUniformMatrix4fv;
		public static Call_glValidateProgram glValidateProgram;
		public static Call_glVertexAttrib1d glVertexAttrib1d;
		public static Call_glVertexAttrib1dv glVertexAttrib1dv;
		public static Call_glVertexAttrib1f glVertexAttrib1f;
		public static Call_glVertexAttrib1fv glVertexAttrib1fv;
		public static Call_glVertexAttrib1s glVertexAttrib1s;
		public static Call_glVertexAttrib1sv glVertexAttrib1sv;
		public static Call_glVertexAttrib2d glVertexAttrib2d;
		public static Call_glVertexAttrib2dv glVertexAttrib2dv;
		public static Call_glVertexAttrib2f glVertexAttrib2f;
		public static Call_glVertexAttrib2fv glVertexAttrib2fv;
		public static Call_glVertexAttrib2s glVertexAttrib2s;
		public static Call_glVertexAttrib2sv glVertexAttrib2sv;
		public static Call_glVertexAttrib3d glVertexAttrib3d;
		public static Call_glVertexAttrib3dv glVertexAttrib3dv;
		public static Call_glVertexAttrib3f glVertexAttrib3f;
		public static Call_glVertexAttrib3fv glVertexAttrib3fv;
		public static Call_glVertexAttrib3s glVertexAttrib3s;
		public static Call_glVertexAttrib3sv glVertexAttrib3sv;
		public static Call_glVertexAttrib4Nbv glVertexAttrib4Nbv;
		public static Call_glVertexAttrib4Niv glVertexAttrib4Niv;
		public static Call_glVertexAttrib4Nsv glVertexAttrib4Nsv;
		public static Call_glVertexAttrib4Nub glVertexAttrib4Nub;
		public static Call_glVertexAttrib4Nubv glVertexAttrib4Nubv;
		public static Call_glVertexAttrib4Nuiv glVertexAttrib4Nuiv;
		public static Call_glVertexAttrib4Nusv glVertexAttrib4Nusv;
		public static Call_glVertexAttrib4bv glVertexAttrib4bv;
		public static Call_glVertexAttrib4d glVertexAttrib4d;
		public static Call_glVertexAttrib4dv glVertexAttrib4dv;
		public static Call_glVertexAttrib4f glVertexAttrib4f;
		public static Call_glVertexAttrib4fv glVertexAttrib4fv;
		public static Call_glVertexAttrib4iv glVertexAttrib4iv;
		public static Call_glVertexAttrib4s glVertexAttrib4s;
		public static Call_glVertexAttrib4sv glVertexAttrib4sv;
		public static Call_glVertexAttrib4ubv glVertexAttrib4ubv;
		public static Call_glVertexAttrib4uiv glVertexAttrib4uiv;
		public static Call_glVertexAttrib4usv glVertexAttrib4usv;
		public static Call_glVertexAttribPointer glVertexAttribPointer;

		public static Call_glUniformMatrix2x3fv glUniformMatrix2x3fv;
		public static Call_glUniformMatrix3x2fv glUniformMatrix3x2fv;
		public static Call_glUniformMatrix2x4fv glUniformMatrix2x4fv;
		public static Call_glUniformMatrix4x2fv glUniformMatrix4x2fv;
		public static Call_glUniformMatrix3x4fv glUniformMatrix3x4fv;
		public static Call_glUniformMatrix4x3fv glUniformMatrix4x3fv;

		public static Call_glColorMaski glColorMaski;
		public static Call_glGetBooleani_v glGetBooleani_v;
		public static Call_glGetIntegeri_v glGetIntegeri_v;
		public static Call_glEnablei glEnablei;
		public static Call_glDisablei glDisablei;
		public static Call_glIsEnabledi glIsEnabledi;
		public static Call_glBeginTransformFeedback glBeginTransformFeedback;
		public static Call_glEndTransformFeedback glEndTransformFeedback;
		public static Call_glBindBufferRange glBindBufferRange;
		public static Call_glBindBufferBase glBindBufferBase;
		public static Call_glTransformFeedbackVaryings glTransformFeedbackVaryings;
		public static Call_glGetTransformFeedbackVarying glGetTransformFeedbackVarying;
		public static Call_glClampColor glClampColor;
		public static Call_glBeginConditionalRender glBeginConditionalRender;
		public static Call_glEndConditionalRender glEndConditionalRender;
		public static Call_glVertexAttribIPointer glVertexAttribIPointer;
		public static Call_glGetVertexAttribIiv glGetVertexAttribIiv;
		public static Call_glGetVertexAttribIuiv glGetVertexAttribIuiv;
		public static Call_glVertexAttribI1i glVertexAttribI1i;
		public static Call_glVertexAttribI2i glVertexAttribI2i;
		public static Call_glVertexAttribI3i glVertexAttribI3i;
		public static Call_glVertexAttribI4i glVertexAttribI4i;
		public static Call_glVertexAttribI1ui glVertexAttribI1ui;
		public static Call_glVertexAttribI2ui glVertexAttribI2ui;
		public static Call_glVertexAttribI3ui glVertexAttribI3ui;
		public static Call_glVertexAttribI4ui glVertexAttribI4ui;
		public static Call_glVertexAttribI1iv glVertexAttribI1iv;
		public static Call_glVertexAttribI2iv glVertexAttribI2iv;
		public static Call_glVertexAttribI3iv glVertexAttribI3iv;
		public static Call_glVertexAttribI4iv glVertexAttribI4iv;
		public static Call_glVertexAttribI1uiv glVertexAttribI1uiv;
		public static Call_glVertexAttribI2uiv glVertexAttribI2uiv;
		public static Call_glVertexAttribI3uiv glVertexAttribI3uiv;
		public static Call_glVertexAttribI4uiv glVertexAttribI4uiv;
		public static Call_glVertexAttribI4bv glVertexAttribI4bv;
		public static Call_glVertexAttribI4sv glVertexAttribI4sv;
		public static Call_glVertexAttribI4ubv glVertexAttribI4ubv;
		public static Call_glVertexAttribI4usv glVertexAttribI4usv;
		public static Call_glGetUniformuiv glGetUniformuiv;
		public static Call_glBindFragDataLocation glBindFragDataLocation;
		public static Call_glGetFragDataLocation glGetFragDataLocation;
		public static Call_glUniform1ui glUniform1ui;
		public static Call_glUniform2ui glUniform2ui;
		public static Call_glUniform3ui glUniform3ui;
		public static Call_glUniform4ui glUniform4ui;
		public static Call_glUniform1uiv glUniform1uiv;
		public static Call_glUniform2uiv glUniform2uiv;
		public static Call_glUniform3uiv glUniform3uiv;
		public static Call_glUniform4uiv glUniform4uiv;
		public static Call_glTexParameterIiv glTexParameterIiv;
		public static Call_glTexParameterIuiv glTexParameterIuiv;
		public static Call_glGetTexParameterIiv glGetTexParameterIiv;
		public static Call_glGetTexParameterIuiv glGetTexParameterIuiv;
		public static Call_glClearBufferiv glClearBufferiv;
		public static Call_glClearBufferuiv glClearBufferuiv;
		public static Call_glClearBufferfv glClearBufferfv;
		public static Call_glClearBufferfi glClearBufferfi;
		public static Call_glGetStringi glGetStringi;
		public static Call_glIsRenderbuffer glIsRenderbuffer;
		public static Call_glBindRenderbuffer glBindRenderbuffer;
		public static Call_glDeleteRenderbuffers glDeleteRenderbuffers;
		public static Call_glGenRenderbuffers glGenRenderbuffers;
		public static Call_glRenderbufferStorage glRenderbufferStorage;
		public static Call_glGetRenderbufferParameteriv glGetRenderbufferParameteriv;
		public static Call_glIsFramebuffer glIsFramebuffer;
		public static Call_glBindFramebuffer glBindFramebuffer;
		public static Call_glDeleteFramebuffers glDeleteFramebuffers;
		public static Call_glGenFramebuffers glGenFramebuffers;
		public static Call_glCheckFramebufferStatus glCheckFramebufferStatus;
		public static Call_glFramebufferTexture1D glFramebufferTexture1D;
		public static Call_glFramebufferTexture2D glFramebufferTexture2D;
		public static Call_glFramebufferTexture3D glFramebufferTexture3D;
		public static Call_glFramebufferRenderbuffer glFramebufferRenderbuffer;
		public static Call_glGetFramebufferAttachmentParameteriv glGetFramebufferAttachmentParameteriv;
		public static Call_glGenerateMipmap glGenerateMipmap;
		public static Call_glBlitFramebuffer glBlitFramebuffer;
		public static Call_glRenderbufferStorageMultisample glRenderbufferStorageMultisample;
		public static Call_glFramebufferTextureLayer glFramebufferTextureLayer;
		public static Call_glMapBufferRange glMapBufferRange;
		public static Call_glFlushMappedBufferRange glFlushMappedBufferRange;
		public static Call_glBindVertexArray glBindVertexArray;
		public static Call_glDeleteVertexArrays glDeleteVertexArrays;
		public static Call_glGenVertexArrays glGenVertexArrays;
		public static Call_glIsVertexArray glIsVertexArray;

		public static Call_glDrawArraysInstanced glDrawArraysInstanced;
		public static Call_glDrawElementsInstanced glDrawElementsInstanced;
		public static Call_glTexBuffer glTexBuffer;
		public static Call_glPrimitiveRestartIndex glPrimitiveRestartIndex;
		public static Call_glCopyBufferSubData glCopyBufferSubData;
		public static Call_glGetUniformIndices glGetUniformIndices;
		public static Call_glGetActiveUniformsiv glGetActiveUniformsiv;
		public static Call_glGetActiveUniformName glGetActiveUniformName;
		public static Call_glGetUniformBlockIndex glGetUniformBlockIndex;
		public static Call_glGetActiveUniformBlockiv glGetActiveUniformBlockiv;
		public static Call_glGetActiveUniformBlockName glGetActiveUniformBlockName;
		public static Call_glUniformBlockBinding glUniformBlockBinding;

		public static Call_glDrawElementsBaseVertex glDrawElementsBaseVertex;
		public static Call_glDrawRangeElementsBaseVertex glDrawRangeElementsBaseVertex;
		public static Call_glDrawElementsInstancedBaseVertex glDrawElementsInstancedBaseVertex;
		public static Call_glMultiDrawElementsBaseVertex glMultiDrawElementsBaseVertex;
		public static Call_glProvokingVertex glProvokingVertex;
		public static Call_glFenceSync glFenceSync;
		public static Call_glIsSync glIsSync;
		public static Call_glDeleteSync glDeleteSync;
		public static Call_glClientWaitSync glClientWaitSync;
		public static Call_glWaitSync glWaitSync;
		public static Call_glGetInteger64v glGetInteger64v;
		public static Call_glGetSynciv glGetSynciv;
		public static Call_glGetInteger64i_v glGetInteger64i_v;
		public static Call_glGetBufferParameteri64v glGetBufferParameteri64v;
		public static Call_glFramebufferTexture glFramebufferTexture;
		public static Call_glTexImage2DMultisample glTexImage2DMultisample;
		public static Call_glTexImage3DMultisample glTexImage3DMultisample;
		public static Call_glGetMultisamplefv glGetMultisamplefv;
		public static Call_glSampleMaski glSampleMaski;

		public static Call_glBindFragDataLocationIndexed glBindFragDataLocationIndexed;
		public static Call_glGetFragDataIndex glGetFragDataIndex;
		public static Call_glGenSamplers glGenSamplers;
		public static Call_glDeleteSamplers glDeleteSamplers;
		public static Call_glIsSampler glIsSampler;
		public static Call_glBindSampler glBindSampler;
		public static Call_glSamplerParameteri glSamplerParameteri;
		public static Call_glSamplerParameteriv glSamplerParameteriv;
		public static Call_glSamplerParameterf glSamplerParameterf;
		public static Call_glSamplerParameterfv glSamplerParameterfv;
		public static Call_glSamplerParameterIiv glSamplerParameterIiv;
		public static Call_glSamplerParameterIuiv glSamplerParameterIuiv;
		public static Call_glGetSamplerParameteriv glGetSamplerParameteriv;
		public static Call_glGetSamplerParameterIiv glGetSamplerParameterIiv;
		public static Call_glGetSamplerParameterfv glGetSamplerParameterfv;
		public static Call_glGetSamplerParameterIuiv glGetSamplerParameterIuiv;
		public static Call_glQueryCounter glQueryCounter;
		public static Call_glGetQueryObjecti64v glGetQueryObjecti64v;
		public static Call_glGetQueryObjectui64v glGetQueryObjectui64v;
		public static Call_glVertexAttribDivisor glVertexAttribDivisor;
		public static Call_glVertexAttribP1ui glVertexAttribP1ui;
		public static Call_glVertexAttribP1uiv glVertexAttribP1uiv;
		public static Call_glVertexAttribP2ui glVertexAttribP2ui;
		public static Call_glVertexAttribP2uiv glVertexAttribP2uiv;
		public static Call_glVertexAttribP3ui glVertexAttribP3ui;
		public static Call_glVertexAttribP3uiv glVertexAttribP3uiv;
		public static Call_glVertexAttribP4ui glVertexAttribP4ui;
		public static Call_glVertexAttribP4uiv glVertexAttribP4uiv;

		public static Call_glMinSampleShading glMinSampleShading;
		public static Call_glBlendEquationi glBlendEquationi;
		public static Call_glBlendEquationSeparatei glBlendEquationSeparatei;
		public static Call_glBlendFunci glBlendFunci;
		public static Call_glBlendFuncSeparatei glBlendFuncSeparatei;
		public static Call_glDrawArraysIndirect glDrawArraysIndirect;
		public static Call_glDrawElementsIndirect glDrawElementsIndirect;
		public static Call_glUniform1d glUniform1d;
		public static Call_glUniform2d glUniform2d;
		public static Call_glUniform3d glUniform3d;
		public static Call_glUniform4d glUniform4d;
		public static Call_glUniform1dv glUniform1dv;
		public static Call_glUniform2dv glUniform2dv;
		public static Call_glUniform3dv glUniform3dv;
		public static Call_glUniform4dv glUniform4dv;
		public static Call_glUniformMatrix2dv glUniformMatrix2dv;
		public static Call_glUniformMatrix3dv glUniformMatrix3dv;
		public static Call_glUniformMatrix4dv glUniformMatrix4dv;
		public static Call_glUniformMatrix2x3dv glUniformMatrix2x3dv;
		public static Call_glUniformMatrix2x4dv glUniformMatrix2x4dv;
		public static Call_glUniformMatrix3x2dv glUniformMatrix3x2dv;
		public static Call_glUniformMatrix3x4dv glUniformMatrix3x4dv;
		public static Call_glUniformMatrix4x2dv glUniformMatrix4x2dv;
		public static Call_glUniformMatrix4x3dv glUniformMatrix4x3dv;
		public static Call_glGetUniformdv glGetUniformdv;
		public static Call_glGetSubroutineUniformLocation glGetSubroutineUniformLocation;
		public static Call_glGetSubroutineIndex glGetSubroutineIndex;
		public static Call_glGetActiveSubroutineUniformiv glGetActiveSubroutineUniformiv;
		public static Call_glGetActiveSubroutineUniformName glGetActiveSubroutineUniformName;
		public static Call_glGetActiveSubroutineName glGetActiveSubroutineName;
		public static Call_glUniformSubroutinesuiv glUniformSubroutinesuiv;
		public static Call_glGetUniformSubroutineuiv glGetUniformSubroutineuiv;
		public static Call_glGetProgramStageiv glGetProgramStageiv;
		public static Call_glPatchParameteri glPatchParameteri;
		public static Call_glPatchParameterfv glPatchParameterfv;
		public static Call_glBindTransformFeedback glBindTransformFeedback;
		public static Call_glDeleteTransformFeedbacks glDeleteTransformFeedbacks;
		public static Call_glGenTransformFeedbacks glGenTransformFeedbacks;
		public static Call_glIsTransformFeedback glIsTransformFeedback;
		public static Call_glPauseTransformFeedback glPauseTransformFeedback;
		public static Call_glResumeTransformFeedback glResumeTransformFeedback;
		public static Call_glDrawTransformFeedback glDrawTransformFeedback;
		public static Call_glDrawTransformFeedbackStream glDrawTransformFeedbackStream;
		public static Call_glBeginQueryIndexed glBeginQueryIndexed;
		public static Call_glEndQueryIndexed glEndQueryIndexed;
		public static Call_glGetQueryIndexediv glGetQueryIndexediv;

		public static Call_glReleaseShaderCompiler glReleaseShaderCompiler;
		public static Call_glShaderBinary glShaderBinary;
		public static Call_glGetShaderPrecisionFormat glGetShaderPrecisionFormat;
		public static Call_glDepthRangef glDepthRangef;
		public static Call_glClearDepthf glClearDepthf;
		public static Call_glGetProgramBinary glGetProgramBinary;
		public static Call_glProgramBinary glProgramBinary;
		public static Call_glProgramParameteri glProgramParameteri;
		public static Call_glUseProgramStages glUseProgramStages;
		public static Call_glActiveShaderProgram glActiveShaderProgram;
		public static Call_glCreateShaderProgramv glCreateShaderProgramv;
		public static Call_glBindProgramPipeline glBindProgramPipeline;
		public static Call_glDeleteProgramPipelines glDeleteProgramPipelines;
		public static Call_glGenProgramPipelines glGenProgramPipelines;
		public static Call_glIsProgramPipeline glIsProgramPipeline;
		public static Call_glGetProgramPipelineiv glGetProgramPipelineiv;
		public static Call_glProgramUniform1i glProgramUniform1i;
		public static Call_glProgramUniform1iv glProgramUniform1iv;
		public static Call_glProgramUniform1f glProgramUniform1f;
		public static Call_glProgramUniform1fv glProgramUniform1fv;
		public static Call_glProgramUniform1d glProgramUniform1d;
		public static Call_glProgramUniform1dv glProgramUniform1dv;
		public static Call_glProgramUniform1ui glProgramUniform1ui;
		public static Call_glProgramUniform1uiv glProgramUniform1uiv;
		public static Call_glProgramUniform2i glProgramUniform2i;
		public static Call_glProgramUniform2iv glProgramUniform2iv;
		public static Call_glProgramUniform2f glProgramUniform2f;
		public static Call_glProgramUniform2fv glProgramUniform2fv;
		public static Call_glProgramUniform2d glProgramUniform2d;
		public static Call_glProgramUniform2dv glProgramUniform2dv;
		public static Call_glProgramUniform2ui glProgramUniform2ui;
		public static Call_glProgramUniform2uiv glProgramUniform2uiv;
		public static Call_glProgramUniform3i glProgramUniform3i;
		public static Call_glProgramUniform3iv glProgramUniform3iv;
		public static Call_glProgramUniform3f glProgramUniform3f;
		public static Call_glProgramUniform3fv glProgramUniform3fv;
		public static Call_glProgramUniform3d glProgramUniform3d;
		public static Call_glProgramUniform3dv glProgramUniform3dv;
		public static Call_glProgramUniform3ui glProgramUniform3ui;
		public static Call_glProgramUniform3uiv glProgramUniform3uiv;
		public static Call_glProgramUniform4i glProgramUniform4i;
		public static Call_glProgramUniform4iv glProgramUniform4iv;
		public static Call_glProgramUniform4f glProgramUniform4f;
		public static Call_glProgramUniform4fv glProgramUniform4fv;
		public static Call_glProgramUniform4d glProgramUniform4d;
		public static Call_glProgramUniform4dv glProgramUniform4dv;
		public static Call_glProgramUniform4ui glProgramUniform4ui;
		public static Call_glProgramUniform4uiv glProgramUniform4uiv;
		public static Call_glProgramUniformMatrix2fv glProgramUniformMatrix2fv;
		public static Call_glProgramUniformMatrix3fv glProgramUniformMatrix3fv;
		public static Call_glProgramUniformMatrix4fv glProgramUniformMatrix4fv;
		public static Call_glProgramUniformMatrix2dv glProgramUniformMatrix2dv;
		public static Call_glProgramUniformMatrix3dv glProgramUniformMatrix3dv;
		public static Call_glProgramUniformMatrix4dv glProgramUniformMatrix4dv;
		public static Call_glProgramUniformMatrix2x3fv glProgramUniformMatrix2x3fv;
		public static Call_glProgramUniformMatrix3x2fv glProgramUniformMatrix3x2fv;
		public static Call_glProgramUniformMatrix2x4fv glProgramUniformMatrix2x4fv;
		public static Call_glProgramUniformMatrix4x2fv glProgramUniformMatrix4x2fv;
		public static Call_glProgramUniformMatrix3x4fv glProgramUniformMatrix3x4fv;
		public static Call_glProgramUniformMatrix4x3fv glProgramUniformMatrix4x3fv;
		public static Call_glProgramUniformMatrix2x3dv glProgramUniformMatrix2x3dv;
		public static Call_glProgramUniformMatrix3x2dv glProgramUniformMatrix3x2dv;
		public static Call_glProgramUniformMatrix2x4dv glProgramUniformMatrix2x4dv;
		public static Call_glProgramUniformMatrix4x2dv glProgramUniformMatrix4x2dv;
		public static Call_glProgramUniformMatrix3x4dv glProgramUniformMatrix3x4dv;
		public static Call_glProgramUniformMatrix4x3dv glProgramUniformMatrix4x3dv;
		public static Call_glValidateProgramPipeline glValidateProgramPipeline;
		public static Call_glGetProgramPipelineInfoLog glGetProgramPipelineInfoLog;
		public static Call_glVertexAttribL1d glVertexAttribL1d;
		public static Call_glVertexAttribL2d glVertexAttribL2d;
		public static Call_glVertexAttribL3d glVertexAttribL3d;
		public static Call_glVertexAttribL4d glVertexAttribL4d;
		public static Call_glVertexAttribL1dv glVertexAttribL1dv;
		public static Call_glVertexAttribL2dv glVertexAttribL2dv;
		public static Call_glVertexAttribL3dv glVertexAttribL3dv;
		public static Call_glVertexAttribL4dv glVertexAttribL4dv;
		public static Call_glVertexAttribLPointer glVertexAttribLPointer;
		public static Call_glGetVertexAttribLdv glGetVertexAttribLdv;
		public static Call_glViewportArrayv glViewportArrayv;
		public static Call_glViewportIndexedf glViewportIndexedf;
		public static Call_glViewportIndexedfv glViewportIndexedfv;
		public static Call_glScissorArrayv glScissorArrayv;
		public static Call_glScissorIndexed glScissorIndexed;
		public static Call_glScissorIndexedv glScissorIndexedv;
		public static Call_glDepthRangeArrayv glDepthRangeArrayv;
		public static Call_glDepthRangeIndexed glDepthRangeIndexed;
		public static Call_glGetFloati_v glGetFloati_v;
		public static Call_glGetDoublei_v glGetDoublei_v;

		public static Call_glDrawArraysInstancedBaseInstance glDrawArraysInstancedBaseInstance;
		public static Call_glDrawElementsInstancedBaseInstance glDrawElementsInstancedBaseInstance;
		public static Call_glDrawElementsInstancedBaseVertexBaseInstance glDrawElementsInstancedBaseVertexBaseInstance;
		public static Call_glGetInternalformativ glGetInternalformativ;
		public static Call_glGetActiveAtomicCounterBufferiv glGetActiveAtomicCounterBufferiv;
		public static Call_glBindImageTexture glBindImageTexture;
		public static Call_glMemoryBarrier glMemoryBarrier;
		public static Call_glTexStorage1D glTexStorage1D;
		public static Call_glTexStorage2D glTexStorage2D;
		public static Call_glTexStorage3D glTexStorage3D;
		public static Call_glDrawTransformFeedbackInstanced glDrawTransformFeedbackInstanced;
		public static Call_glDrawTransformFeedbackStreamInstanced glDrawTransformFeedbackStreamInstanced;

		public static Call_glClearBufferData glClearBufferData;
		public static Call_glClearBufferSubData glClearBufferSubData;
		public static Call_glDispatchCompute glDispatchCompute;
		public static Call_glDispatchComputeIndirect glDispatchComputeIndirect;
		public static Call_glCopyImageSubData glCopyImageSubData;
		public static Call_glFramebufferParameteri glFramebufferParameteri;
		public static Call_glGetFramebufferParameteriv glGetFramebufferParameteriv;
		public static Call_glGetInternalformati64v glGetInternalformati64v;
		public static Call_glInvalidateTexSubImage glInvalidateTexSubImage;
		public static Call_glInvalidateTexImage glInvalidateTexImage;
		public static Call_glInvalidateBufferSubData glInvalidateBufferSubData;
		public static Call_glInvalidateBufferData glInvalidateBufferData;
		public static Call_glInvalidateFramebuffer glInvalidateFramebuffer;
		public static Call_glInvalidateSubFramebuffer glInvalidateSubFramebuffer;
		public static Call_glMultiDrawArraysIndirect glMultiDrawArraysIndirect;
		public static Call_glMultiDrawElementsIndirect glMultiDrawElementsIndirect;
		public static Call_glGetProgramInterfaceiv glGetProgramInterfaceiv;
		public static Call_glGetProgramResourceIndex glGetProgramResourceIndex;
		public static Call_glGetProgramResourceName glGetProgramResourceName;
		public static Call_glGetProgramResourceiv glGetProgramResourceiv;
		public static Call_glGetProgramResourceLocation glGetProgramResourceLocation;
		public static Call_glGetProgramResourceLocationIndex glGetProgramResourceLocationIndex;
		public static Call_glShaderStorageBlockBinding glShaderStorageBlockBinding;
		public static Call_glTexBufferRange glTexBufferRange;
		public static Call_glTexStorage2DMultisample glTexStorage2DMultisample;
		public static Call_glTexStorage3DMultisample glTexStorage3DMultisample;
		public static Call_glTextureView glTextureView;
		public static Call_glBindVertexBuffer glBindVertexBuffer;
		public static Call_glVertexAttribFormat glVertexAttribFormat;
		public static Call_glVertexAttribIFormat glVertexAttribIFormat;
		public static Call_glVertexAttribLFormat glVertexAttribLFormat;
		public static Call_glVertexAttribBinding glVertexAttribBinding;
		public static Call_glVertexBindingDivisor glVertexBindingDivisor;
		public static Call_glDebugMessageControl glDebugMessageControl;
		public static Call_glDebugMessageInsert glDebugMessageInsert;
		public static Call_glDebugMessageCallback glDebugMessageCallback;
		public static Call_glGetDebugMessageLog glGetDebugMessageLog;
		public static Call_glPushDebugGroup glPushDebugGroup;
		public static Call_glPopDebugGroup glPopDebugGroup;
		public static Call_glObjectLabel glObjectLabel;
		public static Call_glGetObjectLabel glGetObjectLabel;
		public static Call_glObjectPtrLabel glObjectPtrLabel;
		public static Call_glGetObjectPtrLabel glGetObjectPtrLabel;

		public static Call_glBufferStorage glBufferStorage;
		public static Call_glClearTexImage glClearTexImage;
		public static Call_glClearTexSubImage glClearTexSubImage;
		public static Call_glBindBuffersBase glBindBuffersBase;
		public static Call_glBindBuffersRange glBindBuffersRange;
		public static Call_glBindTextures glBindTextures;
		public static Call_glBindSamplers glBindSamplers;
		public static Call_glBindImageTextures glBindImageTextures;
		public static Call_glBindVertexBuffers glBindVertexBuffers;

		public static Call_glClipControl glClipControl;
		public static Call_glCreateTransformFeedbacks glCreateTransformFeedbacks;
		public static Call_glTransformFeedbackBufferBase glTransformFeedbackBufferBase;
		public static Call_glTransformFeedbackBufferRange glTransformFeedbackBufferRange;
		public static Call_glGetTransformFeedbackiv glGetTransformFeedbackiv;
		public static Call_glGetTransformFeedbacki_v glGetTransformFeedbacki_v;
		public static Call_glGetTransformFeedbacki64_v glGetTransformFeedbacki64_v;
		public static Call_glCreateBuffers glCreateBuffers;
		public static Call_glNamedBufferStorage glNamedBufferStorage;
		public static Call_glNamedBufferData glNamedBufferData;
		public static Call_glNamedBufferSubData glNamedBufferSubData;
		public static Call_glCopyNamedBufferSubData glCopyNamedBufferSubData;
		public static Call_glClearNamedBufferData glClearNamedBufferData;
		public static Call_glClearNamedBufferSubData glClearNamedBufferSubData;
		public static Call_glMapNamedBuffer glMapNamedBuffer;
		public static Call_glMapNamedBufferRange glMapNamedBufferRange;
		public static Call_glUnmapNamedBuffer glUnmapNamedBuffer;
		public static Call_glFlushMappedNamedBufferRange glFlushMappedNamedBufferRange;
		public static Call_glGetNamedBufferParameteriv glGetNamedBufferParameteriv;
		public static Call_glGetNamedBufferParameteri64v glGetNamedBufferParameteri64v;
		public static Call_glGetNamedBufferPointerv glGetNamedBufferPointerv;
		public static Call_glGetNamedBufferSubData glGetNamedBufferSubData;
		public static Call_glCreateFramebuffers glCreateFramebuffers;
		public static Call_glNamedFramebufferRenderbuffer glNamedFramebufferRenderbuffer;
		public static Call_glNamedFramebufferParameteri glNamedFramebufferParameteri;
		public static Call_glNamedFramebufferTexture glNamedFramebufferTexture;
		public static Call_glNamedFramebufferTextureLayer glNamedFramebufferTextureLayer;
		public static Call_glNamedFramebufferDrawBuffer glNamedFramebufferDrawBuffer;
		public static Call_glNamedFramebufferDrawBuffers glNamedFramebufferDrawBuffers;
		public static Call_glNamedFramebufferReadBuffer glNamedFramebufferReadBuffer;
		public static Call_glInvalidateNamedFramebufferData glInvalidateNamedFramebufferData;
		public static Call_glInvalidateNamedFramebufferSubData glInvalidateNamedFramebufferSubData;
		public static Call_glClearNamedFramebufferiv glClearNamedFramebufferiv;
		public static Call_glClearNamedFramebufferuiv glClearNamedFramebufferuiv;
		public static Call_glClearNamedFramebufferfv glClearNamedFramebufferfv;
		public static Call_glClearNamedFramebufferfi glClearNamedFramebufferfi;
		public static Call_glBlitNamedFramebuffer glBlitNamedFramebuffer;
		public static Call_glCheckNamedFramebufferStatus glCheckNamedFramebufferStatus;
		public static Call_glGetNamedFramebufferParameteriv glGetNamedFramebufferParameteriv;
		public static Call_glGetNamedFramebufferAttachmentParameteriv glGetNamedFramebufferAttachmentParameteriv;
		public static Call_glCreateRenderbuffers glCreateRenderbuffers;
		public static Call_glNamedRenderbufferStorage glNamedRenderbufferStorage;
		public static Call_glNamedRenderbufferStorageMultisample glNamedRenderbufferStorageMultisample;
		public static Call_glGetNamedRenderbufferParameteriv glGetNamedRenderbufferParameteriv;
		public static Call_glCreateTextures glCreateTextures;
		public static Call_glTextureBuffer glTextureBuffer;
		public static Call_glTextureBufferRange glTextureBufferRange;
		public static Call_glTextureStorage1D glTextureStorage1D;
		public static Call_glTextureStorage2D glTextureStorage2D;
		public static Call_glTextureStorage3D glTextureStorage3D;
		public static Call_glTextureStorage2DMultisample glTextureStorage2DMultisample;
		public static Call_glTextureStorage3DMultisample glTextureStorage3DMultisample;
		public static Call_glTextureSubImage1D glTextureSubImage1D;
		public static Call_glTextureSubImage2D glTextureSubImage2D;
		public static Call_glTextureSubImage3D glTextureSubImage3D;
		public static Call_glCompressedTextureSubImage1D glCompressedTextureSubImage1D;
		public static Call_glCompressedTextureSubImage2D glCompressedTextureSubImage2D;
		public static Call_glCompressedTextureSubImage3D glCompressedTextureSubImage3D;
		public static Call_glCopyTextureSubImage1D glCopyTextureSubImage1D;
		public static Call_glCopyTextureSubImage2D glCopyTextureSubImage2D;
		public static Call_glCopyTextureSubImage3D glCopyTextureSubImage3D;
		public static Call_glTextureParameterf glTextureParameterf;
		public static Call_glTextureParameterfv glTextureParameterfv;
		public static Call_glTextureParameteri glTextureParameteri;
		public static Call_glTextureParameterIiv glTextureParameterIiv;
		public static Call_glTextureParameterIuiv glTextureParameterIuiv;
		public static Call_glTextureParameteriv glTextureParameteriv;
		public static Call_glGenerateTextureMipmap glGenerateTextureMipmap;
		public static Call_glBindTextureUnit glBindTextureUnit;
		public static Call_glGetTextureImage glGetTextureImage;
		public static Call_glGetCompressedTextureImage glGetCompressedTextureImage;
		public static Call_glGetTextureLevelParameterfv glGetTextureLevelParameterfv;
		public static Call_glGetTextureLevelParameteriv glGetTextureLevelParameteriv;
		public static Call_glGetTextureParameterfv glGetTextureParameterfv;
		public static Call_glGetTextureParameterIiv glGetTextureParameterIiv;
		public static Call_glGetTextureParameterIuiv glGetTextureParameterIuiv;
		public static Call_glGetTextureParameteriv glGetTextureParameteriv;
		public static Call_glCreateVertexArrays glCreateVertexArrays;
		public static Call_glDisableVertexArrayAttrib glDisableVertexArrayAttrib;
		public static Call_glEnableVertexArrayAttrib glEnableVertexArrayAttrib;
		public static Call_glVertexArrayElementBuffer glVertexArrayElementBuffer;
		public static Call_glVertexArrayVertexBuffer glVertexArrayVertexBuffer;
		public static Call_glVertexArrayVertexBuffers glVertexArrayVertexBuffers;
		public static Call_glVertexArrayAttribBinding glVertexArrayAttribBinding;
		public static Call_glVertexArrayAttribFormat glVertexArrayAttribFormat;
		public static Call_glVertexArrayAttribIFormat glVertexArrayAttribIFormat;
		public static Call_glVertexArrayAttribLFormat glVertexArrayAttribLFormat;
		public static Call_glVertexArrayBindingDivisor glVertexArrayBindingDivisor;
		public static Call_glGetVertexArrayiv glGetVertexArrayiv;
		public static Call_glGetVertexArrayIndexediv glGetVertexArrayIndexediv;
		public static Call_glGetVertexArrayIndexed64iv glGetVertexArrayIndexed64iv;
		public static Call_glCreateSamplers glCreateSamplers;
		public static Call_glCreateProgramPipelines glCreateProgramPipelines;
		public static Call_glCreateQueries glCreateQueries;
		public static Call_glGetQueryBufferObjecti64v glGetQueryBufferObjecti64v;
		public static Call_glGetQueryBufferObjectiv glGetQueryBufferObjectiv;
		public static Call_glGetQueryBufferObjectui64v glGetQueryBufferObjectui64v;
		public static Call_glGetQueryBufferObjectuiv glGetQueryBufferObjectuiv;
		public static Call_glMemoryBarrierByRegion glMemoryBarrierByRegion;
		public static Call_glGetTextureSubImage glGetTextureSubImage;
		public static Call_glGetCompressedTextureSubImage glGetCompressedTextureSubImage;
		public static Call_glGetGraphicsResetStatus glGetGraphicsResetStatus;
		public static Call_glGetnCompressedTexImage glGetnCompressedTexImage;
		public static Call_glGetnTexImage glGetnTexImage;
		public static Call_glGetnUniformdv glGetnUniformdv;
		public static Call_glGetnUniformfv glGetnUniformfv;
		public static Call_glGetnUniformiv glGetnUniformiv;
		public static Call_glGetnUniformuiv glGetnUniformuiv;
		public static Call_glReadnPixels glReadnPixels;
		public static Call_glTextureBarrier glTextureBarrier;

		public static Call_glSpecializeShader glSpecializeShader;
		public static Call_glMultiDrawArraysIndirectCount glMultiDrawArraysIndirectCount;
		public static Call_glMultiDrawElementsIndirectCount glMultiDrawElementsIndirectCount;
		public static Call_glPolygonOffsetClamp glPolygonOffsetClamp;

		public static void initgl() {
			IntPtr glf;
			IntPtr pDll = LoadLibrary(@"OpenGL32");
			glf = wglGetProcAddress("glAccum");
			if (glf != IntPtr.Zero)
				glAccum  = (Call_glAccum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAccum));
			else {
				glf = GetProcAddress(pDll,"glAccum");
				if (glf != IntPtr.Zero)
					glAccum  = (Call_glAccum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAccum));
			}

			glf = wglGetProcAddress("glAlphaFunc");
			if (glf != IntPtr.Zero)
				glAlphaFunc  = (Call_glAlphaFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAlphaFunc));
			else {
				glf = GetProcAddress(pDll,"glAlphaFunc");
				if (glf != IntPtr.Zero)
					glAlphaFunc  = (Call_glAlphaFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAlphaFunc));
			}

			glf = wglGetProcAddress("glAreTexturesResident");
			if (glf != IntPtr.Zero)
				glAreTexturesResident  = (Call_glAreTexturesResident)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAreTexturesResident));
			else {
				glf = GetProcAddress(pDll,"glAreTexturesResident");
				if (glf != IntPtr.Zero)
					glAreTexturesResident  = (Call_glAreTexturesResident)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAreTexturesResident));
			}

			glf = wglGetProcAddress("glArrayElement");
			if (glf != IntPtr.Zero)
				glArrayElement  = (Call_glArrayElement)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glArrayElement));
			else {
				glf = GetProcAddress(pDll,"glArrayElement");
				if (glf != IntPtr.Zero)
					glArrayElement  = (Call_glArrayElement)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glArrayElement));
			}

			glf = wglGetProcAddress("glBegin");
			if (glf != IntPtr.Zero)
				glBegin  = (Call_glBegin)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBegin));
			else {
				glf = GetProcAddress(pDll,"glBegin");
				if (glf != IntPtr.Zero)
					glBegin  = (Call_glBegin)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBegin));
			}

			glf = wglGetProcAddress("glBindTexture");
			if (glf != IntPtr.Zero)
				glBindTexture  = (Call_glBindTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTexture));
			else {
				glf = GetProcAddress(pDll,"glBindTexture");
				if (glf != IntPtr.Zero)
					glBindTexture  = (Call_glBindTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTexture));
			}

			glf = wglGetProcAddress("glBitmap");
			if (glf != IntPtr.Zero)
				glBitmap  = (Call_glBitmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBitmap));
			else {
				glf = GetProcAddress(pDll,"glBitmap");
				if (glf != IntPtr.Zero)
					glBitmap  = (Call_glBitmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBitmap));
			}

			glf = wglGetProcAddress("glBlendFunc");
			if (glf != IntPtr.Zero)
				glBlendFunc  = (Call_glBlendFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFunc));
			else {
				glf = GetProcAddress(pDll,"glBlendFunc");
				if (glf != IntPtr.Zero)
					glBlendFunc  = (Call_glBlendFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFunc));
			}

			glf = wglGetProcAddress("glCallList");
			if (glf != IntPtr.Zero)
				glCallList  = (Call_glCallList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCallList));
			else {
				glf = GetProcAddress(pDll,"glCallList");
				if (glf != IntPtr.Zero)
					glCallList  = (Call_glCallList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCallList));
			}

			glf = wglGetProcAddress("glCallLists");
			if (glf != IntPtr.Zero)
				glCallLists  = (Call_glCallLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCallLists));
			else {
				glf = GetProcAddress(pDll,"glCallLists");
				if (glf != IntPtr.Zero)
					glCallLists  = (Call_glCallLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCallLists));
			}

			glf = wglGetProcAddress("glClear");
			if (glf != IntPtr.Zero)
				glClear  = (Call_glClear)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClear));
			else {
				glf = GetProcAddress(pDll,"glClear");
				if (glf != IntPtr.Zero)
					glClear  = (Call_glClear)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClear));
			}

			glf = wglGetProcAddress("glClearAccum");
			if (glf != IntPtr.Zero)
				glClearAccum  = (Call_glClearAccum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearAccum));
			else {
				glf = GetProcAddress(pDll,"glClearAccum");
				if (glf != IntPtr.Zero)
					glClearAccum  = (Call_glClearAccum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearAccum));
			}

			glf = wglGetProcAddress("glClearColor");
			if (glf != IntPtr.Zero)
				glClearColor  = (Call_glClearColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearColor));
			else {
				glf = GetProcAddress(pDll,"glClearColor");
				if (glf != IntPtr.Zero)
					glClearColor  = (Call_glClearColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearColor));
			}

			glf = wglGetProcAddress("glClearDepth");
			if (glf != IntPtr.Zero)
				glClearDepth  = (Call_glClearDepth)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearDepth));
			else {
				glf = GetProcAddress(pDll,"glClearDepth");
				if (glf != IntPtr.Zero)
					glClearDepth  = (Call_glClearDepth)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearDepth));
			}

			glf = wglGetProcAddress("glClearIndex");
			if (glf != IntPtr.Zero)
				glClearIndex  = (Call_glClearIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearIndex));
			else {
				glf = GetProcAddress(pDll,"glClearIndex");
				if (glf != IntPtr.Zero)
					glClearIndex  = (Call_glClearIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearIndex));
			}

			glf = wglGetProcAddress("glClearStencil");
			if (glf != IntPtr.Zero)
				glClearStencil  = (Call_glClearStencil)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearStencil));
			else {
				glf = GetProcAddress(pDll,"glClearStencil");
				if (glf != IntPtr.Zero)
					glClearStencil  = (Call_glClearStencil)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearStencil));
			}

			glf = wglGetProcAddress("glClipPlane");
			if (glf != IntPtr.Zero)
				glClipPlane  = (Call_glClipPlane)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClipPlane));
			else {
				glf = GetProcAddress(pDll,"glClipPlane");
				if (glf != IntPtr.Zero)
					glClipPlane  = (Call_glClipPlane)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClipPlane));
			}

			glf = wglGetProcAddress("glColor3b");
			if (glf != IntPtr.Zero)
				glColor3b  = (Call_glColor3b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3b));
			else {
				glf = GetProcAddress(pDll,"glColor3b");
				if (glf != IntPtr.Zero)
					glColor3b  = (Call_glColor3b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3b));
			}

			glf = wglGetProcAddress("glColor3bv");
			if (glf != IntPtr.Zero)
				glColor3bv  = (Call_glColor3bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3bv));
			else {
				glf = GetProcAddress(pDll,"glColor3bv");
				if (glf != IntPtr.Zero)
					glColor3bv  = (Call_glColor3bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3bv));
			}

			glf = wglGetProcAddress("glColor3d");
			if (glf != IntPtr.Zero)
				glColor3d  = (Call_glColor3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3d));
			else {
				glf = GetProcAddress(pDll,"glColor3d");
				if (glf != IntPtr.Zero)
					glColor3d  = (Call_glColor3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3d));
			}

			glf = wglGetProcAddress("glColor3dv");
			if (glf != IntPtr.Zero)
				glColor3dv  = (Call_glColor3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3dv));
			else {
				glf = GetProcAddress(pDll,"glColor3dv");
				if (glf != IntPtr.Zero)
					glColor3dv  = (Call_glColor3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3dv));
			}

			glf = wglGetProcAddress("glColor3f");
			if (glf != IntPtr.Zero)
				glColor3f  = (Call_glColor3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3f));
			else {
				glf = GetProcAddress(pDll,"glColor3f");
				if (glf != IntPtr.Zero)
					glColor3f  = (Call_glColor3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3f));
			}

			glf = wglGetProcAddress("glColor3fv");
			if (glf != IntPtr.Zero)
				glColor3fv  = (Call_glColor3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3fv));
			else {
				glf = GetProcAddress(pDll,"glColor3fv");
				if (glf != IntPtr.Zero)
					glColor3fv  = (Call_glColor3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3fv));
			}

			glf = wglGetProcAddress("glColor3i");
			if (glf != IntPtr.Zero)
				glColor3i  = (Call_glColor3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3i));
			else {
				glf = GetProcAddress(pDll,"glColor3i");
				if (glf != IntPtr.Zero)
					glColor3i  = (Call_glColor3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3i));
			}

			glf = wglGetProcAddress("glColor3iv");
			if (glf != IntPtr.Zero)
				glColor3iv  = (Call_glColor3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3iv));
			else {
				glf = GetProcAddress(pDll,"glColor3iv");
				if (glf != IntPtr.Zero)
					glColor3iv  = (Call_glColor3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3iv));
			}

			glf = wglGetProcAddress("glColor3s");
			if (glf != IntPtr.Zero)
				glColor3s  = (Call_glColor3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3s));
			else {
				glf = GetProcAddress(pDll,"glColor3s");
				if (glf != IntPtr.Zero)
					glColor3s  = (Call_glColor3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3s));
			}

			glf = wglGetProcAddress("glColor3sv");
			if (glf != IntPtr.Zero)
				glColor3sv  = (Call_glColor3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3sv));
			else {
				glf = GetProcAddress(pDll,"glColor3sv");
				if (glf != IntPtr.Zero)
					glColor3sv  = (Call_glColor3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3sv));
			}

			glf = wglGetProcAddress("glColor3ub");
			if (glf != IntPtr.Zero)
				glColor3ub  = (Call_glColor3ub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ub));
			else {
				glf = GetProcAddress(pDll,"glColor3ub");
				if (glf != IntPtr.Zero)
					glColor3ub  = (Call_glColor3ub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ub));
			}

			glf = wglGetProcAddress("glColor3ubv");
			if (glf != IntPtr.Zero)
				glColor3ubv  = (Call_glColor3ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ubv));
			else {
				glf = GetProcAddress(pDll,"glColor3ubv");
				if (glf != IntPtr.Zero)
					glColor3ubv  = (Call_glColor3ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ubv));
			}

			glf = wglGetProcAddress("glColor3ui");
			if (glf != IntPtr.Zero)
				glColor3ui  = (Call_glColor3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ui));
			else {
				glf = GetProcAddress(pDll,"glColor3ui");
				if (glf != IntPtr.Zero)
					glColor3ui  = (Call_glColor3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3ui));
			}

			glf = wglGetProcAddress("glColor3uiv");
			if (glf != IntPtr.Zero)
				glColor3uiv  = (Call_glColor3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3uiv));
			else {
				glf = GetProcAddress(pDll,"glColor3uiv");
				if (glf != IntPtr.Zero)
					glColor3uiv  = (Call_glColor3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3uiv));
			}

			glf = wglGetProcAddress("glColor3us");
			if (glf != IntPtr.Zero)
				glColor3us  = (Call_glColor3us)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3us));
			else {
				glf = GetProcAddress(pDll,"glColor3us");
				if (glf != IntPtr.Zero)
					glColor3us  = (Call_glColor3us)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3us));
			}

			glf = wglGetProcAddress("glColor3usv");
			if (glf != IntPtr.Zero)
				glColor3usv  = (Call_glColor3usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3usv));
			else {
				glf = GetProcAddress(pDll,"glColor3usv");
				if (glf != IntPtr.Zero)
					glColor3usv  = (Call_glColor3usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor3usv));
			}

			glf = wglGetProcAddress("glColor4b");
			if (glf != IntPtr.Zero)
				glColor4b  = (Call_glColor4b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4b));
			else {
				glf = GetProcAddress(pDll,"glColor4b");
				if (glf != IntPtr.Zero)
					glColor4b  = (Call_glColor4b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4b));
			}

			glf = wglGetProcAddress("glColor4bv");
			if (glf != IntPtr.Zero)
				glColor4bv  = (Call_glColor4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4bv));
			else {
				glf = GetProcAddress(pDll,"glColor4bv");
				if (glf != IntPtr.Zero)
					glColor4bv  = (Call_glColor4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4bv));
			}

			glf = wglGetProcAddress("glColor4d");
			if (glf != IntPtr.Zero)
				glColor4d  = (Call_glColor4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4d));
			else {
				glf = GetProcAddress(pDll,"glColor4d");
				if (glf != IntPtr.Zero)
					glColor4d  = (Call_glColor4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4d));
			}

			glf = wglGetProcAddress("glColor4dv");
			if (glf != IntPtr.Zero)
				glColor4dv  = (Call_glColor4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4dv));
			else {
				glf = GetProcAddress(pDll,"glColor4dv");
				if (glf != IntPtr.Zero)
					glColor4dv  = (Call_glColor4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4dv));
			}

			glf = wglGetProcAddress("glColor4f");
			if (glf != IntPtr.Zero)
				glColor4f  = (Call_glColor4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4f));
			else {
				glf = GetProcAddress(pDll,"glColor4f");
				if (glf != IntPtr.Zero)
					glColor4f  = (Call_glColor4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4f));
			}

			glf = wglGetProcAddress("glColor4fv");
			if (glf != IntPtr.Zero)
				glColor4fv  = (Call_glColor4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4fv));
			else {
				glf = GetProcAddress(pDll,"glColor4fv");
				if (glf != IntPtr.Zero)
					glColor4fv  = (Call_glColor4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4fv));
			}

			glf = wglGetProcAddress("glColor4i");
			if (glf != IntPtr.Zero)
				glColor4i  = (Call_glColor4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4i));
			else {
				glf = GetProcAddress(pDll,"glColor4i");
				if (glf != IntPtr.Zero)
					glColor4i  = (Call_glColor4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4i));
			}

			glf = wglGetProcAddress("glColor4iv");
			if (glf != IntPtr.Zero)
				glColor4iv  = (Call_glColor4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4iv));
			else {
				glf = GetProcAddress(pDll,"glColor4iv");
				if (glf != IntPtr.Zero)
					glColor4iv  = (Call_glColor4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4iv));
			}

			glf = wglGetProcAddress("glColor4s");
			if (glf != IntPtr.Zero)
				glColor4s  = (Call_glColor4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4s));
			else {
				glf = GetProcAddress(pDll,"glColor4s");
				if (glf != IntPtr.Zero)
					glColor4s  = (Call_glColor4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4s));
			}

			glf = wglGetProcAddress("glColor4sv");
			if (glf != IntPtr.Zero)
				glColor4sv  = (Call_glColor4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4sv));
			else {
				glf = GetProcAddress(pDll,"glColor4sv");
				if (glf != IntPtr.Zero)
					glColor4sv  = (Call_glColor4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4sv));
			}

			glf = wglGetProcAddress("glColor4ub");
			if (glf != IntPtr.Zero)
				glColor4ub  = (Call_glColor4ub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ub));
			else {
				glf = GetProcAddress(pDll,"glColor4ub");
				if (glf != IntPtr.Zero)
					glColor4ub  = (Call_glColor4ub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ub));
			}

			glf = wglGetProcAddress("glColor4ubv");
			if (glf != IntPtr.Zero)
				glColor4ubv  = (Call_glColor4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ubv));
			else {
				glf = GetProcAddress(pDll,"glColor4ubv");
				if (glf != IntPtr.Zero)
					glColor4ubv  = (Call_glColor4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ubv));
			}

			glf = wglGetProcAddress("glColor4ui");
			if (glf != IntPtr.Zero)
				glColor4ui  = (Call_glColor4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ui));
			else {
				glf = GetProcAddress(pDll,"glColor4ui");
				if (glf != IntPtr.Zero)
					glColor4ui  = (Call_glColor4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4ui));
			}

			glf = wglGetProcAddress("glColor4uiv");
			if (glf != IntPtr.Zero)
				glColor4uiv  = (Call_glColor4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4uiv));
			else {
				glf = GetProcAddress(pDll,"glColor4uiv");
				if (glf != IntPtr.Zero)
					glColor4uiv  = (Call_glColor4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4uiv));
			}

			glf = wglGetProcAddress("glColor4us");
			if (glf != IntPtr.Zero)
				glColor4us  = (Call_glColor4us)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4us));
			else {
				glf = GetProcAddress(pDll,"glColor4us");
				if (glf != IntPtr.Zero)
					glColor4us  = (Call_glColor4us)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4us));
			}

			glf = wglGetProcAddress("glColor4usv");
			if (glf != IntPtr.Zero)
				glColor4usv  = (Call_glColor4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4usv));
			else {
				glf = GetProcAddress(pDll,"glColor4usv");
				if (glf != IntPtr.Zero)
					glColor4usv  = (Call_glColor4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColor4usv));
			}

			glf = wglGetProcAddress("glColorMask");
			if (glf != IntPtr.Zero)
				glColorMask  = (Call_glColorMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMask));
			else {
				glf = GetProcAddress(pDll,"glColorMask");
				if (glf != IntPtr.Zero)
					glColorMask  = (Call_glColorMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMask));
			}

			glf = wglGetProcAddress("glColorMaterial");
			if (glf != IntPtr.Zero)
				glColorMaterial  = (Call_glColorMaterial)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMaterial));
			else {
				glf = GetProcAddress(pDll,"glColorMaterial");
				if (glf != IntPtr.Zero)
					glColorMaterial  = (Call_glColorMaterial)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMaterial));
			}

			glf = wglGetProcAddress("glColorPointer");
			if (glf != IntPtr.Zero)
				glColorPointer  = (Call_glColorPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorPointer));
			else {
				glf = GetProcAddress(pDll,"glColorPointer");
				if (glf != IntPtr.Zero)
					glColorPointer  = (Call_glColorPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorPointer));
			}

			glf = wglGetProcAddress("glCopyPixels");
			if (glf != IntPtr.Zero)
				glCopyPixels  = (Call_glCopyPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyPixels));
			else {
				glf = GetProcAddress(pDll,"glCopyPixels");
				if (glf != IntPtr.Zero)
					glCopyPixels  = (Call_glCopyPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyPixels));
			}

			glf = wglGetProcAddress("glCopyTexImage1D");
			if (glf != IntPtr.Zero)
				glCopyTexImage1D  = (Call_glCopyTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexImage1D));
			else {
				glf = GetProcAddress(pDll,"glCopyTexImage1D");
				if (glf != IntPtr.Zero)
					glCopyTexImage1D  = (Call_glCopyTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexImage1D));
			}

			glf = wglGetProcAddress("glCopyTexImage2D");
			if (glf != IntPtr.Zero)
				glCopyTexImage2D  = (Call_glCopyTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexImage2D));
			else {
				glf = GetProcAddress(pDll,"glCopyTexImage2D");
				if (glf != IntPtr.Zero)
					glCopyTexImage2D  = (Call_glCopyTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexImage2D));
			}

			glf = wglGetProcAddress("glCopyTexSubImage1D");
			if (glf != IntPtr.Zero)
				glCopyTexSubImage1D  = (Call_glCopyTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glCopyTexSubImage1D");
				if (glf != IntPtr.Zero)
					glCopyTexSubImage1D  = (Call_glCopyTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage1D));
			}

			glf = wglGetProcAddress("glCopyTexSubImage2D");
			if (glf != IntPtr.Zero)
				glCopyTexSubImage2D  = (Call_glCopyTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glCopyTexSubImage2D");
				if (glf != IntPtr.Zero)
					glCopyTexSubImage2D  = (Call_glCopyTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage2D));
			}

			glf = wglGetProcAddress("glCullFace");
			if (glf != IntPtr.Zero)
				glCullFace  = (Call_glCullFace)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCullFace));
			else {
				glf = GetProcAddress(pDll,"glCullFace");
				if (glf != IntPtr.Zero)
					glCullFace  = (Call_glCullFace)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCullFace));
			}

			glf = wglGetProcAddress("glDeleteLists");
			if (glf != IntPtr.Zero)
				glDeleteLists  = (Call_glDeleteLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteLists));
			else {
				glf = GetProcAddress(pDll,"glDeleteLists");
				if (glf != IntPtr.Zero)
					glDeleteLists  = (Call_glDeleteLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteLists));
			}

			glf = wglGetProcAddress("glDeleteTextures");
			if (glf != IntPtr.Zero)
				glDeleteTextures  = (Call_glDeleteTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteTextures));
			else {
				glf = GetProcAddress(pDll,"glDeleteTextures");
				if (glf != IntPtr.Zero)
					glDeleteTextures  = (Call_glDeleteTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteTextures));
			}

			glf = wglGetProcAddress("glDepthFunc");
			if (glf != IntPtr.Zero)
				glDepthFunc  = (Call_glDepthFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthFunc));
			else {
				glf = GetProcAddress(pDll,"glDepthFunc");
				if (glf != IntPtr.Zero)
					glDepthFunc  = (Call_glDepthFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthFunc));
			}

			glf = wglGetProcAddress("glDepthMask");
			if (glf != IntPtr.Zero)
				glDepthMask  = (Call_glDepthMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthMask));
			else {
				glf = GetProcAddress(pDll,"glDepthMask");
				if (glf != IntPtr.Zero)
					glDepthMask  = (Call_glDepthMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthMask));
			}

			glf = wglGetProcAddress("glDepthRange");
			if (glf != IntPtr.Zero)
				glDepthRange  = (Call_glDepthRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRange));
			else {
				glf = GetProcAddress(pDll,"glDepthRange");
				if (glf != IntPtr.Zero)
					glDepthRange  = (Call_glDepthRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRange));
			}

			glf = wglGetProcAddress("glDisable");
			if (glf != IntPtr.Zero)
				glDisable  = (Call_glDisable)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisable));
			else {
				glf = GetProcAddress(pDll,"glDisable");
				if (glf != IntPtr.Zero)
					glDisable  = (Call_glDisable)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisable));
			}

			glf = wglGetProcAddress("glDisableClientState");
			if (glf != IntPtr.Zero)
				glDisableClientState  = (Call_glDisableClientState)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableClientState));
			else {
				glf = GetProcAddress(pDll,"glDisableClientState");
				if (glf != IntPtr.Zero)
					glDisableClientState  = (Call_glDisableClientState)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableClientState));
			}

			glf = wglGetProcAddress("glDrawArrays");
			if (glf != IntPtr.Zero)
				glDrawArrays  = (Call_glDrawArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArrays));
			else {
				glf = GetProcAddress(pDll,"glDrawArrays");
				if (glf != IntPtr.Zero)
					glDrawArrays  = (Call_glDrawArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArrays));
			}

			glf = wglGetProcAddress("glDrawBuffer");
			if (glf != IntPtr.Zero)
				glDrawBuffer  = (Call_glDrawBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawBuffer));
			else {
				glf = GetProcAddress(pDll,"glDrawBuffer");
				if (glf != IntPtr.Zero)
					glDrawBuffer  = (Call_glDrawBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawBuffer));
			}

			glf = wglGetProcAddress("glDrawElements");
			if (glf != IntPtr.Zero)
				glDrawElements  = (Call_glDrawElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElements));
			else {
				glf = GetProcAddress(pDll,"glDrawElements");
				if (glf != IntPtr.Zero)
					glDrawElements  = (Call_glDrawElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElements));
			}

			glf = wglGetProcAddress("glDrawPixels");
			if (glf != IntPtr.Zero)
				glDrawPixels  = (Call_glDrawPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawPixels));
			else {
				glf = GetProcAddress(pDll,"glDrawPixels");
				if (glf != IntPtr.Zero)
					glDrawPixels  = (Call_glDrawPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawPixels));
			}

			glf = wglGetProcAddress("glEdgeFlag");
			if (glf != IntPtr.Zero)
				glEdgeFlag  = (Call_glEdgeFlag)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlag));
			else {
				glf = GetProcAddress(pDll,"glEdgeFlag");
				if (glf != IntPtr.Zero)
					glEdgeFlag  = (Call_glEdgeFlag)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlag));
			}

			glf = wglGetProcAddress("glEdgeFlagPointer");
			if (glf != IntPtr.Zero)
				glEdgeFlagPointer  = (Call_glEdgeFlagPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlagPointer));
			else {
				glf = GetProcAddress(pDll,"glEdgeFlagPointer");
				if (glf != IntPtr.Zero)
					glEdgeFlagPointer  = (Call_glEdgeFlagPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlagPointer));
			}

			glf = wglGetProcAddress("glEdgeFlagv");
			if (glf != IntPtr.Zero)
				glEdgeFlagv  = (Call_glEdgeFlagv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlagv));
			else {
				glf = GetProcAddress(pDll,"glEdgeFlagv");
				if (glf != IntPtr.Zero)
					glEdgeFlagv  = (Call_glEdgeFlagv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEdgeFlagv));
			}

			glf = wglGetProcAddress("glEnable");
			if (glf != IntPtr.Zero)
				glEnable  = (Call_glEnable)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnable));
			else {
				glf = GetProcAddress(pDll,"glEnable");
				if (glf != IntPtr.Zero)
					glEnable  = (Call_glEnable)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnable));
			}

			glf = wglGetProcAddress("glEnableClientState");
			if (glf != IntPtr.Zero)
				glEnableClientState  = (Call_glEnableClientState)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableClientState));
			else {
				glf = GetProcAddress(pDll,"glEnableClientState");
				if (glf != IntPtr.Zero)
					glEnableClientState  = (Call_glEnableClientState)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableClientState));
			}

			glf = wglGetProcAddress("glEnd");
			if (glf != IntPtr.Zero)
				glEnd  = (Call_glEnd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnd));
			else {
				glf = GetProcAddress(pDll,"glEnd");
				if (glf != IntPtr.Zero)
					glEnd  = (Call_glEnd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnd));
			}

			glf = wglGetProcAddress("glEndList");
			if (glf != IntPtr.Zero)
				glEndList  = (Call_glEndList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndList));
			else {
				glf = GetProcAddress(pDll,"glEndList");
				if (glf != IntPtr.Zero)
					glEndList  = (Call_glEndList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndList));
			}

			glf = wglGetProcAddress("glEvalCoord1d");
			if (glf != IntPtr.Zero)
				glEvalCoord1d  = (Call_glEvalCoord1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1d));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord1d");
				if (glf != IntPtr.Zero)
					glEvalCoord1d  = (Call_glEvalCoord1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1d));
			}

			glf = wglGetProcAddress("glEvalCoord1dv");
			if (glf != IntPtr.Zero)
				glEvalCoord1dv  = (Call_glEvalCoord1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1dv));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord1dv");
				if (glf != IntPtr.Zero)
					glEvalCoord1dv  = (Call_glEvalCoord1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1dv));
			}

			glf = wglGetProcAddress("glEvalCoord1f");
			if (glf != IntPtr.Zero)
				glEvalCoord1f  = (Call_glEvalCoord1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1f));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord1f");
				if (glf != IntPtr.Zero)
					glEvalCoord1f  = (Call_glEvalCoord1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1f));
			}

			glf = wglGetProcAddress("glEvalCoord1fv");
			if (glf != IntPtr.Zero)
				glEvalCoord1fv  = (Call_glEvalCoord1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1fv));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord1fv");
				if (glf != IntPtr.Zero)
					glEvalCoord1fv  = (Call_glEvalCoord1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord1fv));
			}

			glf = wglGetProcAddress("glEvalCoord2d");
			if (glf != IntPtr.Zero)
				glEvalCoord2d  = (Call_glEvalCoord2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2d));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord2d");
				if (glf != IntPtr.Zero)
					glEvalCoord2d  = (Call_glEvalCoord2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2d));
			}

			glf = wglGetProcAddress("glEvalCoord2dv");
			if (glf != IntPtr.Zero)
				glEvalCoord2dv  = (Call_glEvalCoord2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2dv));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord2dv");
				if (glf != IntPtr.Zero)
					glEvalCoord2dv  = (Call_glEvalCoord2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2dv));
			}

			glf = wglGetProcAddress("glEvalCoord2f");
			if (glf != IntPtr.Zero)
				glEvalCoord2f  = (Call_glEvalCoord2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2f));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord2f");
				if (glf != IntPtr.Zero)
					glEvalCoord2f  = (Call_glEvalCoord2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2f));
			}

			glf = wglGetProcAddress("glEvalCoord2fv");
			if (glf != IntPtr.Zero)
				glEvalCoord2fv  = (Call_glEvalCoord2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2fv));
			else {
				glf = GetProcAddress(pDll,"glEvalCoord2fv");
				if (glf != IntPtr.Zero)
					glEvalCoord2fv  = (Call_glEvalCoord2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalCoord2fv));
			}

			glf = wglGetProcAddress("glEvalMesh1");
			if (glf != IntPtr.Zero)
				glEvalMesh1  = (Call_glEvalMesh1)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalMesh1));
			else {
				glf = GetProcAddress(pDll,"glEvalMesh1");
				if (glf != IntPtr.Zero)
					glEvalMesh1  = (Call_glEvalMesh1)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalMesh1));
			}

			glf = wglGetProcAddress("glEvalMesh2");
			if (glf != IntPtr.Zero)
				glEvalMesh2  = (Call_glEvalMesh2)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalMesh2));
			else {
				glf = GetProcAddress(pDll,"glEvalMesh2");
				if (glf != IntPtr.Zero)
					glEvalMesh2  = (Call_glEvalMesh2)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalMesh2));
			}

			glf = wglGetProcAddress("glEvalPoint1");
			if (glf != IntPtr.Zero)
				glEvalPoint1  = (Call_glEvalPoint1)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalPoint1));
			else {
				glf = GetProcAddress(pDll,"glEvalPoint1");
				if (glf != IntPtr.Zero)
					glEvalPoint1  = (Call_glEvalPoint1)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalPoint1));
			}

			glf = wglGetProcAddress("glEvalPoint2");
			if (glf != IntPtr.Zero)
				glEvalPoint2  = (Call_glEvalPoint2)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalPoint2));
			else {
				glf = GetProcAddress(pDll,"glEvalPoint2");
				if (glf != IntPtr.Zero)
					glEvalPoint2  = (Call_glEvalPoint2)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEvalPoint2));
			}

			glf = wglGetProcAddress("glFeedbackBuffer");
			if (glf != IntPtr.Zero)
				glFeedbackBuffer  = (Call_glFeedbackBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFeedbackBuffer));
			else {
				glf = GetProcAddress(pDll,"glFeedbackBuffer");
				if (glf != IntPtr.Zero)
					glFeedbackBuffer  = (Call_glFeedbackBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFeedbackBuffer));
			}

			glf = wglGetProcAddress("glFinish");
			if (glf != IntPtr.Zero)
				glFinish  = (Call_glFinish)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFinish));
			else {
				glf = GetProcAddress(pDll,"glFinish");
				if (glf != IntPtr.Zero)
					glFinish  = (Call_glFinish)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFinish));
			}

			glf = wglGetProcAddress("glFlush");
			if (glf != IntPtr.Zero)
				glFlush  = (Call_glFlush)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlush));
			else {
				glf = GetProcAddress(pDll,"glFlush");
				if (glf != IntPtr.Zero)
					glFlush  = (Call_glFlush)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlush));
			}

			glf = wglGetProcAddress("glFogf");
			if (glf != IntPtr.Zero)
				glFogf  = (Call_glFogf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogf));
			else {
				glf = GetProcAddress(pDll,"glFogf");
				if (glf != IntPtr.Zero)
					glFogf  = (Call_glFogf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogf));
			}

			glf = wglGetProcAddress("glFogfv");
			if (glf != IntPtr.Zero)
				glFogfv  = (Call_glFogfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogfv));
			else {
				glf = GetProcAddress(pDll,"glFogfv");
				if (glf != IntPtr.Zero)
					glFogfv  = (Call_glFogfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogfv));
			}

			glf = wglGetProcAddress("glFogi");
			if (glf != IntPtr.Zero)
				glFogi  = (Call_glFogi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogi));
			else {
				glf = GetProcAddress(pDll,"glFogi");
				if (glf != IntPtr.Zero)
					glFogi  = (Call_glFogi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogi));
			}

			glf = wglGetProcAddress("glFogiv");
			if (glf != IntPtr.Zero)
				glFogiv  = (Call_glFogiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogiv));
			else {
				glf = GetProcAddress(pDll,"glFogiv");
				if (glf != IntPtr.Zero)
					glFogiv  = (Call_glFogiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFogiv));
			}

			glf = wglGetProcAddress("glFrontFace");
			if (glf != IntPtr.Zero)
				glFrontFace  = (Call_glFrontFace)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFrontFace));
			else {
				glf = GetProcAddress(pDll,"glFrontFace");
				if (glf != IntPtr.Zero)
					glFrontFace  = (Call_glFrontFace)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFrontFace));
			}

			glf = wglGetProcAddress("glFrustum");
			if (glf != IntPtr.Zero)
				glFrustum  = (Call_glFrustum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFrustum));
			else {
				glf = GetProcAddress(pDll,"glFrustum");
				if (glf != IntPtr.Zero)
					glFrustum  = (Call_glFrustum)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFrustum));
			}

			glf = wglGetProcAddress("glGenLists");
			if (glf != IntPtr.Zero)
				glGenLists  = (Call_glGenLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenLists));
			else {
				glf = GetProcAddress(pDll,"glGenLists");
				if (glf != IntPtr.Zero)
					glGenLists  = (Call_glGenLists)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenLists));
			}

			glf = wglGetProcAddress("glGenTextures");
			if (glf != IntPtr.Zero)
				glGenTextures  = (Call_glGenTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenTextures));
			else {
				glf = GetProcAddress(pDll,"glGenTextures");
				if (glf != IntPtr.Zero)
					glGenTextures  = (Call_glGenTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenTextures));
			}

			glf = wglGetProcAddress("glGetBooleanv");
			if (glf != IntPtr.Zero)
				glGetBooleanv  = (Call_glGetBooleanv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBooleanv));
			else {
				glf = GetProcAddress(pDll,"glGetBooleanv");
				if (glf != IntPtr.Zero)
					glGetBooleanv  = (Call_glGetBooleanv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBooleanv));
			}

			glf = wglGetProcAddress("glGetClipPlane");
			if (glf != IntPtr.Zero)
				glGetClipPlane  = (Call_glGetClipPlane)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetClipPlane));
			else {
				glf = GetProcAddress(pDll,"glGetClipPlane");
				if (glf != IntPtr.Zero)
					glGetClipPlane  = (Call_glGetClipPlane)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetClipPlane));
			}

			glf = wglGetProcAddress("glGetDoublev");
			if (glf != IntPtr.Zero)
				glGetDoublev  = (Call_glGetDoublev)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDoublev));
			else {
				glf = GetProcAddress(pDll,"glGetDoublev");
				if (glf != IntPtr.Zero)
					glGetDoublev  = (Call_glGetDoublev)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDoublev));
			}

			glf = wglGetProcAddress("glGetError");
			if (glf != IntPtr.Zero)
				glGetError  = (Call_glGetError)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetError));
			else {
				glf = GetProcAddress(pDll,"glGetError");
				if (glf != IntPtr.Zero)
					glGetError  = (Call_glGetError)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetError));
			}

			glf = wglGetProcAddress("glGetFloatv");
			if (glf != IntPtr.Zero)
				glGetFloatv  = (Call_glGetFloatv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFloatv));
			else {
				glf = GetProcAddress(pDll,"glGetFloatv");
				if (glf != IntPtr.Zero)
					glGetFloatv  = (Call_glGetFloatv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFloatv));
			}

			glf = wglGetProcAddress("glGetIntegerv");
			if (glf != IntPtr.Zero)
				glGetIntegerv  = (Call_glGetIntegerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetIntegerv));
			else {
				glf = GetProcAddress(pDll,"glGetIntegerv");
				if (glf != IntPtr.Zero)
					glGetIntegerv  = (Call_glGetIntegerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetIntegerv));
			}

			glf = wglGetProcAddress("glGetLightfv");
			if (glf != IntPtr.Zero)
				glGetLightfv  = (Call_glGetLightfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetLightfv));
			else {
				glf = GetProcAddress(pDll,"glGetLightfv");
				if (glf != IntPtr.Zero)
					glGetLightfv  = (Call_glGetLightfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetLightfv));
			}

			glf = wglGetProcAddress("glGetLightiv");
			if (glf != IntPtr.Zero)
				glGetLightiv  = (Call_glGetLightiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetLightiv));
			else {
				glf = GetProcAddress(pDll,"glGetLightiv");
				if (glf != IntPtr.Zero)
					glGetLightiv  = (Call_glGetLightiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetLightiv));
			}

			glf = wglGetProcAddress("glGetMapdv");
			if (glf != IntPtr.Zero)
				glGetMapdv  = (Call_glGetMapdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapdv));
			else {
				glf = GetProcAddress(pDll,"glGetMapdv");
				if (glf != IntPtr.Zero)
					glGetMapdv  = (Call_glGetMapdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapdv));
			}

			glf = wglGetProcAddress("glGetMapfv");
			if (glf != IntPtr.Zero)
				glGetMapfv  = (Call_glGetMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapfv));
			else {
				glf = GetProcAddress(pDll,"glGetMapfv");
				if (glf != IntPtr.Zero)
					glGetMapfv  = (Call_glGetMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapfv));
			}

			glf = wglGetProcAddress("glGetMapiv");
			if (glf != IntPtr.Zero)
				glGetMapiv  = (Call_glGetMapiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapiv));
			else {
				glf = GetProcAddress(pDll,"glGetMapiv");
				if (glf != IntPtr.Zero)
					glGetMapiv  = (Call_glGetMapiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMapiv));
			}

			glf = wglGetProcAddress("glGetMaterialfv");
			if (glf != IntPtr.Zero)
				glGetMaterialfv  = (Call_glGetMaterialfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMaterialfv));
			else {
				glf = GetProcAddress(pDll,"glGetMaterialfv");
				if (glf != IntPtr.Zero)
					glGetMaterialfv  = (Call_glGetMaterialfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMaterialfv));
			}

			glf = wglGetProcAddress("glGetMaterialiv");
			if (glf != IntPtr.Zero)
				glGetMaterialiv  = (Call_glGetMaterialiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMaterialiv));
			else {
				glf = GetProcAddress(pDll,"glGetMaterialiv");
				if (glf != IntPtr.Zero)
					glGetMaterialiv  = (Call_glGetMaterialiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMaterialiv));
			}

			glf = wglGetProcAddress("glGetPixelMapfv");
			if (glf != IntPtr.Zero)
				glGetPixelMapfv  = (Call_glGetPixelMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapfv));
			else {
				glf = GetProcAddress(pDll,"glGetPixelMapfv");
				if (glf != IntPtr.Zero)
					glGetPixelMapfv  = (Call_glGetPixelMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapfv));
			}

			glf = wglGetProcAddress("glGetPixelMapuiv");
			if (glf != IntPtr.Zero)
				glGetPixelMapuiv  = (Call_glGetPixelMapuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapuiv));
			else {
				glf = GetProcAddress(pDll,"glGetPixelMapuiv");
				if (glf != IntPtr.Zero)
					glGetPixelMapuiv  = (Call_glGetPixelMapuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapuiv));
			}

			glf = wglGetProcAddress("glGetPixelMapusv");
			if (glf != IntPtr.Zero)
				glGetPixelMapusv  = (Call_glGetPixelMapusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapusv));
			else {
				glf = GetProcAddress(pDll,"glGetPixelMapusv");
				if (glf != IntPtr.Zero)
					glGetPixelMapusv  = (Call_glGetPixelMapusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPixelMapusv));
			}

			glf = wglGetProcAddress("glGetPointerv");
			if (glf != IntPtr.Zero)
				glGetPointerv  = (Call_glGetPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPointerv));
			else {
				glf = GetProcAddress(pDll,"glGetPointerv");
				if (glf != IntPtr.Zero)
					glGetPointerv  = (Call_glGetPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPointerv));
			}

			glf = wglGetProcAddress("glGetPolygonStipple");
			if (glf != IntPtr.Zero)
				glGetPolygonStipple  = (Call_glGetPolygonStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPolygonStipple));
			else {
				glf = GetProcAddress(pDll,"glGetPolygonStipple");
				if (glf != IntPtr.Zero)
					glGetPolygonStipple  = (Call_glGetPolygonStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetPolygonStipple));
			}

			glf = wglGetProcAddress("glGetString");
			if (glf != IntPtr.Zero)
				glGetString  = (Call_glGetString)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetString));
			else {
				glf = GetProcAddress(pDll,"glGetString");
				if (glf != IntPtr.Zero)
					glGetString  = (Call_glGetString)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetString));
			}

			glf = wglGetProcAddress("glGetTexEnvfv");
			if (glf != IntPtr.Zero)
				glGetTexEnvfv  = (Call_glGetTexEnvfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexEnvfv));
			else {
				glf = GetProcAddress(pDll,"glGetTexEnvfv");
				if (glf != IntPtr.Zero)
					glGetTexEnvfv  = (Call_glGetTexEnvfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexEnvfv));
			}

			glf = wglGetProcAddress("glGetTexEnviv");
			if (glf != IntPtr.Zero)
				glGetTexEnviv  = (Call_glGetTexEnviv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexEnviv));
			else {
				glf = GetProcAddress(pDll,"glGetTexEnviv");
				if (glf != IntPtr.Zero)
					glGetTexEnviv  = (Call_glGetTexEnviv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexEnviv));
			}

			glf = wglGetProcAddress("glGetTexGendv");
			if (glf != IntPtr.Zero)
				glGetTexGendv  = (Call_glGetTexGendv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGendv));
			else {
				glf = GetProcAddress(pDll,"glGetTexGendv");
				if (glf != IntPtr.Zero)
					glGetTexGendv  = (Call_glGetTexGendv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGendv));
			}

			glf = wglGetProcAddress("glGetTexGenfv");
			if (glf != IntPtr.Zero)
				glGetTexGenfv  = (Call_glGetTexGenfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGenfv));
			else {
				glf = GetProcAddress(pDll,"glGetTexGenfv");
				if (glf != IntPtr.Zero)
					glGetTexGenfv  = (Call_glGetTexGenfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGenfv));
			}

			glf = wglGetProcAddress("glGetTexGeniv");
			if (glf != IntPtr.Zero)
				glGetTexGeniv  = (Call_glGetTexGeniv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGeniv));
			else {
				glf = GetProcAddress(pDll,"glGetTexGeniv");
				if (glf != IntPtr.Zero)
					glGetTexGeniv  = (Call_glGetTexGeniv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexGeniv));
			}

			glf = wglGetProcAddress("glGetTexImage");
			if (glf != IntPtr.Zero)
				glGetTexImage  = (Call_glGetTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexImage));
			else {
				glf = GetProcAddress(pDll,"glGetTexImage");
				if (glf != IntPtr.Zero)
					glGetTexImage  = (Call_glGetTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexImage));
			}

			glf = wglGetProcAddress("glGetTexLevelParameterfv");
			if (glf != IntPtr.Zero)
				glGetTexLevelParameterfv  = (Call_glGetTexLevelParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexLevelParameterfv));
			else {
				glf = GetProcAddress(pDll,"glGetTexLevelParameterfv");
				if (glf != IntPtr.Zero)
					glGetTexLevelParameterfv  = (Call_glGetTexLevelParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexLevelParameterfv));
			}

			glf = wglGetProcAddress("glGetTexLevelParameteriv");
			if (glf != IntPtr.Zero)
				glGetTexLevelParameteriv  = (Call_glGetTexLevelParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexLevelParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetTexLevelParameteriv");
				if (glf != IntPtr.Zero)
					glGetTexLevelParameteriv  = (Call_glGetTexLevelParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexLevelParameteriv));
			}

			glf = wglGetProcAddress("glGetTexParameterfv");
			if (glf != IntPtr.Zero)
				glGetTexParameterfv  = (Call_glGetTexParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterfv));
			else {
				glf = GetProcAddress(pDll,"glGetTexParameterfv");
				if (glf != IntPtr.Zero)
					glGetTexParameterfv  = (Call_glGetTexParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterfv));
			}

			glf = wglGetProcAddress("glGetTexParameteriv");
			if (glf != IntPtr.Zero)
				glGetTexParameteriv  = (Call_glGetTexParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetTexParameteriv");
				if (glf != IntPtr.Zero)
					glGetTexParameteriv  = (Call_glGetTexParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameteriv));
			}

			glf = wglGetProcAddress("glHint");
			if (glf != IntPtr.Zero)
				glHint  = (Call_glHint)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glHint));
			else {
				glf = GetProcAddress(pDll,"glHint");
				if (glf != IntPtr.Zero)
					glHint  = (Call_glHint)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glHint));
			}

			glf = wglGetProcAddress("glIndexMask");
			if (glf != IntPtr.Zero)
				glIndexMask  = (Call_glIndexMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexMask));
			else {
				glf = GetProcAddress(pDll,"glIndexMask");
				if (glf != IntPtr.Zero)
					glIndexMask  = (Call_glIndexMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexMask));
			}

			glf = wglGetProcAddress("glIndexPointer");
			if (glf != IntPtr.Zero)
				glIndexPointer  = (Call_glIndexPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexPointer));
			else {
				glf = GetProcAddress(pDll,"glIndexPointer");
				if (glf != IntPtr.Zero)
					glIndexPointer  = (Call_glIndexPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexPointer));
			}

			glf = wglGetProcAddress("glIndexd");
			if (glf != IntPtr.Zero)
				glIndexd  = (Call_glIndexd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexd));
			else {
				glf = GetProcAddress(pDll,"glIndexd");
				if (glf != IntPtr.Zero)
					glIndexd  = (Call_glIndexd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexd));
			}

			glf = wglGetProcAddress("glIndexdv");
			if (glf != IntPtr.Zero)
				glIndexdv  = (Call_glIndexdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexdv));
			else {
				glf = GetProcAddress(pDll,"glIndexdv");
				if (glf != IntPtr.Zero)
					glIndexdv  = (Call_glIndexdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexdv));
			}

			glf = wglGetProcAddress("glIndexf");
			if (glf != IntPtr.Zero)
				glIndexf  = (Call_glIndexf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexf));
			else {
				glf = GetProcAddress(pDll,"glIndexf");
				if (glf != IntPtr.Zero)
					glIndexf  = (Call_glIndexf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexf));
			}

			glf = wglGetProcAddress("glIndexfv");
			if (glf != IntPtr.Zero)
				glIndexfv  = (Call_glIndexfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexfv));
			else {
				glf = GetProcAddress(pDll,"glIndexfv");
				if (glf != IntPtr.Zero)
					glIndexfv  = (Call_glIndexfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexfv));
			}

			glf = wglGetProcAddress("glIndexi");
			if (glf != IntPtr.Zero)
				glIndexi  = (Call_glIndexi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexi));
			else {
				glf = GetProcAddress(pDll,"glIndexi");
				if (glf != IntPtr.Zero)
					glIndexi  = (Call_glIndexi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexi));
			}

			glf = wglGetProcAddress("glIndexiv");
			if (glf != IntPtr.Zero)
				glIndexiv  = (Call_glIndexiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexiv));
			else {
				glf = GetProcAddress(pDll,"glIndexiv");
				if (glf != IntPtr.Zero)
					glIndexiv  = (Call_glIndexiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexiv));
			}

			glf = wglGetProcAddress("glIndexs");
			if (glf != IntPtr.Zero)
				glIndexs  = (Call_glIndexs)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexs));
			else {
				glf = GetProcAddress(pDll,"glIndexs");
				if (glf != IntPtr.Zero)
					glIndexs  = (Call_glIndexs)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexs));
			}

			glf = wglGetProcAddress("glIndexsv");
			if (glf != IntPtr.Zero)
				glIndexsv  = (Call_glIndexsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexsv));
			else {
				glf = GetProcAddress(pDll,"glIndexsv");
				if (glf != IntPtr.Zero)
					glIndexsv  = (Call_glIndexsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexsv));
			}

			glf = wglGetProcAddress("glIndexub");
			if (glf != IntPtr.Zero)
				glIndexub  = (Call_glIndexub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexub));
			else {
				glf = GetProcAddress(pDll,"glIndexub");
				if (glf != IntPtr.Zero)
					glIndexub  = (Call_glIndexub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexub));
			}

			glf = wglGetProcAddress("glIndexubv");
			if (glf != IntPtr.Zero)
				glIndexubv  = (Call_glIndexubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexubv));
			else {
				glf = GetProcAddress(pDll,"glIndexubv");
				if (glf != IntPtr.Zero)
					glIndexubv  = (Call_glIndexubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIndexubv));
			}

			glf = wglGetProcAddress("glInitNames");
			if (glf != IntPtr.Zero)
				glInitNames  = (Call_glInitNames)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInitNames));
			else {
				glf = GetProcAddress(pDll,"glInitNames");
				if (glf != IntPtr.Zero)
					glInitNames  = (Call_glInitNames)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInitNames));
			}

			glf = wglGetProcAddress("glInterleavedArrays");
			if (glf != IntPtr.Zero)
				glInterleavedArrays  = (Call_glInterleavedArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInterleavedArrays));
			else {
				glf = GetProcAddress(pDll,"glInterleavedArrays");
				if (glf != IntPtr.Zero)
					glInterleavedArrays  = (Call_glInterleavedArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInterleavedArrays));
			}

			glf = wglGetProcAddress("glIsEnabled");
			if (glf != IntPtr.Zero)
				glIsEnabled  = (Call_glIsEnabled)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsEnabled));
			else {
				glf = GetProcAddress(pDll,"glIsEnabled");
				if (glf != IntPtr.Zero)
					glIsEnabled  = (Call_glIsEnabled)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsEnabled));
			}

			glf = wglGetProcAddress("glIsList");
			if (glf != IntPtr.Zero)
				glIsList  = (Call_glIsList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsList));
			else {
				glf = GetProcAddress(pDll,"glIsList");
				if (glf != IntPtr.Zero)
					glIsList  = (Call_glIsList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsList));
			}

			glf = wglGetProcAddress("glIsTexture");
			if (glf != IntPtr.Zero)
				glIsTexture  = (Call_glIsTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsTexture));
			else {
				glf = GetProcAddress(pDll,"glIsTexture");
				if (glf != IntPtr.Zero)
					glIsTexture  = (Call_glIsTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsTexture));
			}

			glf = wglGetProcAddress("glLightModelf");
			if (glf != IntPtr.Zero)
				glLightModelf  = (Call_glLightModelf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModelf));
			else {
				glf = GetProcAddress(pDll,"glLightModelf");
				if (glf != IntPtr.Zero)
					glLightModelf  = (Call_glLightModelf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModelf));
			}

			glf = wglGetProcAddress("glLightModelfv");
			if (glf != IntPtr.Zero)
				glLightModelfv  = (Call_glLightModelfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModelfv));
			else {
				glf = GetProcAddress(pDll,"glLightModelfv");
				if (glf != IntPtr.Zero)
					glLightModelfv  = (Call_glLightModelfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModelfv));
			}

			glf = wglGetProcAddress("glLightModeli");
			if (glf != IntPtr.Zero)
				glLightModeli  = (Call_glLightModeli)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModeli));
			else {
				glf = GetProcAddress(pDll,"glLightModeli");
				if (glf != IntPtr.Zero)
					glLightModeli  = (Call_glLightModeli)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModeli));
			}

			glf = wglGetProcAddress("glLightModeliv");
			if (glf != IntPtr.Zero)
				glLightModeliv  = (Call_glLightModeliv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModeliv));
			else {
				glf = GetProcAddress(pDll,"glLightModeliv");
				if (glf != IntPtr.Zero)
					glLightModeliv  = (Call_glLightModeliv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightModeliv));
			}

			glf = wglGetProcAddress("glLightf");
			if (glf != IntPtr.Zero)
				glLightf  = (Call_glLightf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightf));
			else {
				glf = GetProcAddress(pDll,"glLightf");
				if (glf != IntPtr.Zero)
					glLightf  = (Call_glLightf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightf));
			}

			glf = wglGetProcAddress("glLightfv");
			if (glf != IntPtr.Zero)
				glLightfv  = (Call_glLightfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightfv));
			else {
				glf = GetProcAddress(pDll,"glLightfv");
				if (glf != IntPtr.Zero)
					glLightfv  = (Call_glLightfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightfv));
			}

			glf = wglGetProcAddress("glLighti");
			if (glf != IntPtr.Zero)
				glLighti  = (Call_glLighti)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLighti));
			else {
				glf = GetProcAddress(pDll,"glLighti");
				if (glf != IntPtr.Zero)
					glLighti  = (Call_glLighti)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLighti));
			}

			glf = wglGetProcAddress("glLightiv");
			if (glf != IntPtr.Zero)
				glLightiv  = (Call_glLightiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightiv));
			else {
				glf = GetProcAddress(pDll,"glLightiv");
				if (glf != IntPtr.Zero)
					glLightiv  = (Call_glLightiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLightiv));
			}

			glf = wglGetProcAddress("glLineStipple");
			if (glf != IntPtr.Zero)
				glLineStipple  = (Call_glLineStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLineStipple));
			else {
				glf = GetProcAddress(pDll,"glLineStipple");
				if (glf != IntPtr.Zero)
					glLineStipple  = (Call_glLineStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLineStipple));
			}

			glf = wglGetProcAddress("glLineWidth");
			if (glf != IntPtr.Zero)
				glLineWidth  = (Call_glLineWidth)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLineWidth));
			else {
				glf = GetProcAddress(pDll,"glLineWidth");
				if (glf != IntPtr.Zero)
					glLineWidth  = (Call_glLineWidth)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLineWidth));
			}

			glf = wglGetProcAddress("glListBase");
			if (glf != IntPtr.Zero)
				glListBase  = (Call_glListBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glListBase));
			else {
				glf = GetProcAddress(pDll,"glListBase");
				if (glf != IntPtr.Zero)
					glListBase  = (Call_glListBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glListBase));
			}

			glf = wglGetProcAddress("glLoadIdentity");
			if (glf != IntPtr.Zero)
				glLoadIdentity  = (Call_glLoadIdentity)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadIdentity));
			else {
				glf = GetProcAddress(pDll,"glLoadIdentity");
				if (glf != IntPtr.Zero)
					glLoadIdentity  = (Call_glLoadIdentity)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadIdentity));
			}

			glf = wglGetProcAddress("glLoadMatrixd");
			if (glf != IntPtr.Zero)
				glLoadMatrixd  = (Call_glLoadMatrixd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadMatrixd));
			else {
				glf = GetProcAddress(pDll,"glLoadMatrixd");
				if (glf != IntPtr.Zero)
					glLoadMatrixd  = (Call_glLoadMatrixd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadMatrixd));
			}

			glf = wglGetProcAddress("glLoadMatrixf");
			if (glf != IntPtr.Zero)
				glLoadMatrixf  = (Call_glLoadMatrixf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadMatrixf));
			else {
				glf = GetProcAddress(pDll,"glLoadMatrixf");
				if (glf != IntPtr.Zero)
					glLoadMatrixf  = (Call_glLoadMatrixf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadMatrixf));
			}

			glf = wglGetProcAddress("glLoadName");
			if (glf != IntPtr.Zero)
				glLoadName  = (Call_glLoadName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadName));
			else {
				glf = GetProcAddress(pDll,"glLoadName");
				if (glf != IntPtr.Zero)
					glLoadName  = (Call_glLoadName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLoadName));
			}

			glf = wglGetProcAddress("glLogicOp");
			if (glf != IntPtr.Zero)
				glLogicOp  = (Call_glLogicOp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLogicOp));
			else {
				glf = GetProcAddress(pDll,"glLogicOp");
				if (glf != IntPtr.Zero)
					glLogicOp  = (Call_glLogicOp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLogicOp));
			}

			glf = wglGetProcAddress("glMap1d");
			if (glf != IntPtr.Zero)
				glMap1d  = (Call_glMap1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap1d));
			else {
				glf = GetProcAddress(pDll,"glMap1d");
				if (glf != IntPtr.Zero)
					glMap1d  = (Call_glMap1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap1d));
			}

			glf = wglGetProcAddress("glMap1f");
			if (glf != IntPtr.Zero)
				glMap1f  = (Call_glMap1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap1f));
			else {
				glf = GetProcAddress(pDll,"glMap1f");
				if (glf != IntPtr.Zero)
					glMap1f  = (Call_glMap1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap1f));
			}

			glf = wglGetProcAddress("glMap2d");
			if (glf != IntPtr.Zero)
				glMap2d  = (Call_glMap2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap2d));
			else {
				glf = GetProcAddress(pDll,"glMap2d");
				if (glf != IntPtr.Zero)
					glMap2d  = (Call_glMap2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap2d));
			}

			glf = wglGetProcAddress("glMap2f");
			if (glf != IntPtr.Zero)
				glMap2f  = (Call_glMap2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap2f));
			else {
				glf = GetProcAddress(pDll,"glMap2f");
				if (glf != IntPtr.Zero)
					glMap2f  = (Call_glMap2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMap2f));
			}

			glf = wglGetProcAddress("glMapGrid1d");
			if (glf != IntPtr.Zero)
				glMapGrid1d  = (Call_glMapGrid1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid1d));
			else {
				glf = GetProcAddress(pDll,"glMapGrid1d");
				if (glf != IntPtr.Zero)
					glMapGrid1d  = (Call_glMapGrid1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid1d));
			}

			glf = wglGetProcAddress("glMapGrid1f");
			if (glf != IntPtr.Zero)
				glMapGrid1f  = (Call_glMapGrid1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid1f));
			else {
				glf = GetProcAddress(pDll,"glMapGrid1f");
				if (glf != IntPtr.Zero)
					glMapGrid1f  = (Call_glMapGrid1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid1f));
			}

			glf = wglGetProcAddress("glMapGrid2d");
			if (glf != IntPtr.Zero)
				glMapGrid2d  = (Call_glMapGrid2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid2d));
			else {
				glf = GetProcAddress(pDll,"glMapGrid2d");
				if (glf != IntPtr.Zero)
					glMapGrid2d  = (Call_glMapGrid2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid2d));
			}

			glf = wglGetProcAddress("glMapGrid2f");
			if (glf != IntPtr.Zero)
				glMapGrid2f  = (Call_glMapGrid2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid2f));
			else {
				glf = GetProcAddress(pDll,"glMapGrid2f");
				if (glf != IntPtr.Zero)
					glMapGrid2f  = (Call_glMapGrid2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapGrid2f));
			}

			glf = wglGetProcAddress("glMaterialf");
			if (glf != IntPtr.Zero)
				glMaterialf  = (Call_glMaterialf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialf));
			else {
				glf = GetProcAddress(pDll,"glMaterialf");
				if (glf != IntPtr.Zero)
					glMaterialf  = (Call_glMaterialf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialf));
			}

			glf = wglGetProcAddress("glMaterialfv");
			if (glf != IntPtr.Zero)
				glMaterialfv  = (Call_glMaterialfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialfv));
			else {
				glf = GetProcAddress(pDll,"glMaterialfv");
				if (glf != IntPtr.Zero)
					glMaterialfv  = (Call_glMaterialfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialfv));
			}

			glf = wglGetProcAddress("glMateriali");
			if (glf != IntPtr.Zero)
				glMateriali  = (Call_glMateriali)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMateriali));
			else {
				glf = GetProcAddress(pDll,"glMateriali");
				if (glf != IntPtr.Zero)
					glMateriali  = (Call_glMateriali)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMateriali));
			}

			glf = wglGetProcAddress("glMaterialiv");
			if (glf != IntPtr.Zero)
				glMaterialiv  = (Call_glMaterialiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialiv));
			else {
				glf = GetProcAddress(pDll,"glMaterialiv");
				if (glf != IntPtr.Zero)
					glMaterialiv  = (Call_glMaterialiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMaterialiv));
			}

			glf = wglGetProcAddress("glMatrixMode");
			if (glf != IntPtr.Zero)
				glMatrixMode  = (Call_glMatrixMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMatrixMode));
			else {
				glf = GetProcAddress(pDll,"glMatrixMode");
				if (glf != IntPtr.Zero)
					glMatrixMode  = (Call_glMatrixMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMatrixMode));
			}

			glf = wglGetProcAddress("glMultMatrixd");
			if (glf != IntPtr.Zero)
				glMultMatrixd  = (Call_glMultMatrixd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultMatrixd));
			else {
				glf = GetProcAddress(pDll,"glMultMatrixd");
				if (glf != IntPtr.Zero)
					glMultMatrixd  = (Call_glMultMatrixd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultMatrixd));
			}

			glf = wglGetProcAddress("glMultMatrixf");
			if (glf != IntPtr.Zero)
				glMultMatrixf  = (Call_glMultMatrixf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultMatrixf));
			else {
				glf = GetProcAddress(pDll,"glMultMatrixf");
				if (glf != IntPtr.Zero)
					glMultMatrixf  = (Call_glMultMatrixf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultMatrixf));
			}

			glf = wglGetProcAddress("glNewList");
			if (glf != IntPtr.Zero)
				glNewList  = (Call_glNewList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNewList));
			else {
				glf = GetProcAddress(pDll,"glNewList");
				if (glf != IntPtr.Zero)
					glNewList  = (Call_glNewList)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNewList));
			}

			glf = wglGetProcAddress("glNormal3b");
			if (glf != IntPtr.Zero)
				glNormal3b  = (Call_glNormal3b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3b));
			else {
				glf = GetProcAddress(pDll,"glNormal3b");
				if (glf != IntPtr.Zero)
					glNormal3b  = (Call_glNormal3b)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3b));
			}

			glf = wglGetProcAddress("glNormal3bv");
			if (glf != IntPtr.Zero)
				glNormal3bv  = (Call_glNormal3bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3bv));
			else {
				glf = GetProcAddress(pDll,"glNormal3bv");
				if (glf != IntPtr.Zero)
					glNormal3bv  = (Call_glNormal3bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3bv));
			}

			glf = wglGetProcAddress("glNormal3d");
			if (glf != IntPtr.Zero)
				glNormal3d  = (Call_glNormal3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3d));
			else {
				glf = GetProcAddress(pDll,"glNormal3d");
				if (glf != IntPtr.Zero)
					glNormal3d  = (Call_glNormal3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3d));
			}

			glf = wglGetProcAddress("glNormal3dv");
			if (glf != IntPtr.Zero)
				glNormal3dv  = (Call_glNormal3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3dv));
			else {
				glf = GetProcAddress(pDll,"glNormal3dv");
				if (glf != IntPtr.Zero)
					glNormal3dv  = (Call_glNormal3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3dv));
			}

			glf = wglGetProcAddress("glNormal3f");
			if (glf != IntPtr.Zero)
				glNormal3f  = (Call_glNormal3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3f));
			else {
				glf = GetProcAddress(pDll,"glNormal3f");
				if (glf != IntPtr.Zero)
					glNormal3f  = (Call_glNormal3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3f));
			}

			glf = wglGetProcAddress("glNormal3fv");
			if (glf != IntPtr.Zero)
				glNormal3fv  = (Call_glNormal3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3fv));
			else {
				glf = GetProcAddress(pDll,"glNormal3fv");
				if (glf != IntPtr.Zero)
					glNormal3fv  = (Call_glNormal3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3fv));
			}

			glf = wglGetProcAddress("glNormal3i");
			if (glf != IntPtr.Zero)
				glNormal3i  = (Call_glNormal3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3i));
			else {
				glf = GetProcAddress(pDll,"glNormal3i");
				if (glf != IntPtr.Zero)
					glNormal3i  = (Call_glNormal3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3i));
			}

			glf = wglGetProcAddress("glNormal3iv");
			if (glf != IntPtr.Zero)
				glNormal3iv  = (Call_glNormal3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3iv));
			else {
				glf = GetProcAddress(pDll,"glNormal3iv");
				if (glf != IntPtr.Zero)
					glNormal3iv  = (Call_glNormal3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3iv));
			}

			glf = wglGetProcAddress("glNormal3s");
			if (glf != IntPtr.Zero)
				glNormal3s  = (Call_glNormal3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3s));
			else {
				glf = GetProcAddress(pDll,"glNormal3s");
				if (glf != IntPtr.Zero)
					glNormal3s  = (Call_glNormal3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3s));
			}

			glf = wglGetProcAddress("glNormal3sv");
			if (glf != IntPtr.Zero)
				glNormal3sv  = (Call_glNormal3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3sv));
			else {
				glf = GetProcAddress(pDll,"glNormal3sv");
				if (glf != IntPtr.Zero)
					glNormal3sv  = (Call_glNormal3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormal3sv));
			}

			glf = wglGetProcAddress("glNormalPointer");
			if (glf != IntPtr.Zero)
				glNormalPointer  = (Call_glNormalPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormalPointer));
			else {
				glf = GetProcAddress(pDll,"glNormalPointer");
				if (glf != IntPtr.Zero)
					glNormalPointer  = (Call_glNormalPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNormalPointer));
			}

			glf = wglGetProcAddress("glOrtho");
			if (glf != IntPtr.Zero)
				glOrtho  = (Call_glOrtho)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glOrtho));
			else {
				glf = GetProcAddress(pDll,"glOrtho");
				if (glf != IntPtr.Zero)
					glOrtho  = (Call_glOrtho)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glOrtho));
			}

			glf = wglGetProcAddress("glPassThrough");
			if (glf != IntPtr.Zero)
				glPassThrough  = (Call_glPassThrough)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPassThrough));
			else {
				glf = GetProcAddress(pDll,"glPassThrough");
				if (glf != IntPtr.Zero)
					glPassThrough  = (Call_glPassThrough)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPassThrough));
			}

			glf = wglGetProcAddress("glPixelMapfv");
			if (glf != IntPtr.Zero)
				glPixelMapfv  = (Call_glPixelMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapfv));
			else {
				glf = GetProcAddress(pDll,"glPixelMapfv");
				if (glf != IntPtr.Zero)
					glPixelMapfv  = (Call_glPixelMapfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapfv));
			}

			glf = wglGetProcAddress("glPixelMapuiv");
			if (glf != IntPtr.Zero)
				glPixelMapuiv  = (Call_glPixelMapuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapuiv));
			else {
				glf = GetProcAddress(pDll,"glPixelMapuiv");
				if (glf != IntPtr.Zero)
					glPixelMapuiv  = (Call_glPixelMapuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapuiv));
			}

			glf = wglGetProcAddress("glPixelMapusv");
			if (glf != IntPtr.Zero)
				glPixelMapusv  = (Call_glPixelMapusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapusv));
			else {
				glf = GetProcAddress(pDll,"glPixelMapusv");
				if (glf != IntPtr.Zero)
					glPixelMapusv  = (Call_glPixelMapusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelMapusv));
			}

			glf = wglGetProcAddress("glPixelStoref");
			if (glf != IntPtr.Zero)
				glPixelStoref  = (Call_glPixelStoref)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelStoref));
			else {
				glf = GetProcAddress(pDll,"glPixelStoref");
				if (glf != IntPtr.Zero)
					glPixelStoref  = (Call_glPixelStoref)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelStoref));
			}

			glf = wglGetProcAddress("glPixelStorei");
			if (glf != IntPtr.Zero)
				glPixelStorei  = (Call_glPixelStorei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelStorei));
			else {
				glf = GetProcAddress(pDll,"glPixelStorei");
				if (glf != IntPtr.Zero)
					glPixelStorei  = (Call_glPixelStorei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelStorei));
			}

			glf = wglGetProcAddress("glPixelTransferf");
			if (glf != IntPtr.Zero)
				glPixelTransferf  = (Call_glPixelTransferf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelTransferf));
			else {
				glf = GetProcAddress(pDll,"glPixelTransferf");
				if (glf != IntPtr.Zero)
					glPixelTransferf  = (Call_glPixelTransferf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelTransferf));
			}

			glf = wglGetProcAddress("glPixelTransferi");
			if (glf != IntPtr.Zero)
				glPixelTransferi  = (Call_glPixelTransferi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelTransferi));
			else {
				glf = GetProcAddress(pDll,"glPixelTransferi");
				if (glf != IntPtr.Zero)
					glPixelTransferi  = (Call_glPixelTransferi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelTransferi));
			}

			glf = wglGetProcAddress("glPixelZoom");
			if (glf != IntPtr.Zero)
				glPixelZoom  = (Call_glPixelZoom)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelZoom));
			else {
				glf = GetProcAddress(pDll,"glPixelZoom");
				if (glf != IntPtr.Zero)
					glPixelZoom  = (Call_glPixelZoom)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPixelZoom));
			}

			glf = wglGetProcAddress("glPointSize");
			if (glf != IntPtr.Zero)
				glPointSize  = (Call_glPointSize)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointSize));
			else {
				glf = GetProcAddress(pDll,"glPointSize");
				if (glf != IntPtr.Zero)
					glPointSize  = (Call_glPointSize)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointSize));
			}

			glf = wglGetProcAddress("glPolygonMode");
			if (glf != IntPtr.Zero)
				glPolygonMode  = (Call_glPolygonMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonMode));
			else {
				glf = GetProcAddress(pDll,"glPolygonMode");
				if (glf != IntPtr.Zero)
					glPolygonMode  = (Call_glPolygonMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonMode));
			}

			glf = wglGetProcAddress("glPolygonOffset");
			if (glf != IntPtr.Zero)
				glPolygonOffset  = (Call_glPolygonOffset)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonOffset));
			else {
				glf = GetProcAddress(pDll,"glPolygonOffset");
				if (glf != IntPtr.Zero)
					glPolygonOffset  = (Call_glPolygonOffset)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonOffset));
			}

			glf = wglGetProcAddress("glPolygonStipple");
			if (glf != IntPtr.Zero)
				glPolygonStipple  = (Call_glPolygonStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonStipple));
			else {
				glf = GetProcAddress(pDll,"glPolygonStipple");
				if (glf != IntPtr.Zero)
					glPolygonStipple  = (Call_glPolygonStipple)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonStipple));
			}

			glf = wglGetProcAddress("glPopAttrib");
			if (glf != IntPtr.Zero)
				glPopAttrib  = (Call_glPopAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopAttrib));
			else {
				glf = GetProcAddress(pDll,"glPopAttrib");
				if (glf != IntPtr.Zero)
					glPopAttrib  = (Call_glPopAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopAttrib));
			}

			glf = wglGetProcAddress("glPopClientAttrib");
			if (glf != IntPtr.Zero)
				glPopClientAttrib  = (Call_glPopClientAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopClientAttrib));
			else {
				glf = GetProcAddress(pDll,"glPopClientAttrib");
				if (glf != IntPtr.Zero)
					glPopClientAttrib  = (Call_glPopClientAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopClientAttrib));
			}

			glf = wglGetProcAddress("glPopMatrix");
			if (glf != IntPtr.Zero)
				glPopMatrix  = (Call_glPopMatrix)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopMatrix));
			else {
				glf = GetProcAddress(pDll,"glPopMatrix");
				if (glf != IntPtr.Zero)
					glPopMatrix  = (Call_glPopMatrix)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopMatrix));
			}

			glf = wglGetProcAddress("glPopName");
			if (glf != IntPtr.Zero)
				glPopName  = (Call_glPopName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopName));
			else {
				glf = GetProcAddress(pDll,"glPopName");
				if (glf != IntPtr.Zero)
					glPopName  = (Call_glPopName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopName));
			}

			glf = wglGetProcAddress("glPrioritizeTextures");
			if (glf != IntPtr.Zero)
				glPrioritizeTextures  = (Call_glPrioritizeTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPrioritizeTextures));
			else {
				glf = GetProcAddress(pDll,"glPrioritizeTextures");
				if (glf != IntPtr.Zero)
					glPrioritizeTextures  = (Call_glPrioritizeTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPrioritizeTextures));
			}

			glf = wglGetProcAddress("glPushAttrib");
			if (glf != IntPtr.Zero)
				glPushAttrib  = (Call_glPushAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushAttrib));
			else {
				glf = GetProcAddress(pDll,"glPushAttrib");
				if (glf != IntPtr.Zero)
					glPushAttrib  = (Call_glPushAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushAttrib));
			}

			glf = wglGetProcAddress("glPushClientAttrib");
			if (glf != IntPtr.Zero)
				glPushClientAttrib  = (Call_glPushClientAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushClientAttrib));
			else {
				glf = GetProcAddress(pDll,"glPushClientAttrib");
				if (glf != IntPtr.Zero)
					glPushClientAttrib  = (Call_glPushClientAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushClientAttrib));
			}

			glf = wglGetProcAddress("glPushMatrix");
			if (glf != IntPtr.Zero)
				glPushMatrix  = (Call_glPushMatrix)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushMatrix));
			else {
				glf = GetProcAddress(pDll,"glPushMatrix");
				if (glf != IntPtr.Zero)
					glPushMatrix  = (Call_glPushMatrix)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushMatrix));
			}

			glf = wglGetProcAddress("glPushName");
			if (glf != IntPtr.Zero)
				glPushName  = (Call_glPushName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushName));
			else {
				glf = GetProcAddress(pDll,"glPushName");
				if (glf != IntPtr.Zero)
					glPushName  = (Call_glPushName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushName));
			}

			glf = wglGetProcAddress("glRasterPos2d");
			if (glf != IntPtr.Zero)
				glRasterPos2d  = (Call_glRasterPos2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2d));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2d");
				if (glf != IntPtr.Zero)
					glRasterPos2d  = (Call_glRasterPos2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2d));
			}

			glf = wglGetProcAddress("glRasterPos2dv");
			if (glf != IntPtr.Zero)
				glRasterPos2dv  = (Call_glRasterPos2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2dv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2dv");
				if (glf != IntPtr.Zero)
					glRasterPos2dv  = (Call_glRasterPos2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2dv));
			}

			glf = wglGetProcAddress("glRasterPos2f");
			if (glf != IntPtr.Zero)
				glRasterPos2f  = (Call_glRasterPos2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2f));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2f");
				if (glf != IntPtr.Zero)
					glRasterPos2f  = (Call_glRasterPos2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2f));
			}

			glf = wglGetProcAddress("glRasterPos2fv");
			if (glf != IntPtr.Zero)
				glRasterPos2fv  = (Call_glRasterPos2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2fv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2fv");
				if (glf != IntPtr.Zero)
					glRasterPos2fv  = (Call_glRasterPos2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2fv));
			}

			glf = wglGetProcAddress("glRasterPos2i");
			if (glf != IntPtr.Zero)
				glRasterPos2i  = (Call_glRasterPos2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2i));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2i");
				if (glf != IntPtr.Zero)
					glRasterPos2i  = (Call_glRasterPos2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2i));
			}

			glf = wglGetProcAddress("glRasterPos2iv");
			if (glf != IntPtr.Zero)
				glRasterPos2iv  = (Call_glRasterPos2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2iv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2iv");
				if (glf != IntPtr.Zero)
					glRasterPos2iv  = (Call_glRasterPos2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2iv));
			}

			glf = wglGetProcAddress("glRasterPos2s");
			if (glf != IntPtr.Zero)
				glRasterPos2s  = (Call_glRasterPos2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2s));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2s");
				if (glf != IntPtr.Zero)
					glRasterPos2s  = (Call_glRasterPos2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2s));
			}

			glf = wglGetProcAddress("glRasterPos2sv");
			if (glf != IntPtr.Zero)
				glRasterPos2sv  = (Call_glRasterPos2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2sv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos2sv");
				if (glf != IntPtr.Zero)
					glRasterPos2sv  = (Call_glRasterPos2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos2sv));
			}

			glf = wglGetProcAddress("glRasterPos3d");
			if (glf != IntPtr.Zero)
				glRasterPos3d  = (Call_glRasterPos3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3d));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3d");
				if (glf != IntPtr.Zero)
					glRasterPos3d  = (Call_glRasterPos3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3d));
			}

			glf = wglGetProcAddress("glRasterPos3dv");
			if (glf != IntPtr.Zero)
				glRasterPos3dv  = (Call_glRasterPos3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3dv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3dv");
				if (glf != IntPtr.Zero)
					glRasterPos3dv  = (Call_glRasterPos3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3dv));
			}

			glf = wglGetProcAddress("glRasterPos3f");
			if (glf != IntPtr.Zero)
				glRasterPos3f  = (Call_glRasterPos3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3f));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3f");
				if (glf != IntPtr.Zero)
					glRasterPos3f  = (Call_glRasterPos3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3f));
			}

			glf = wglGetProcAddress("glRasterPos3fv");
			if (glf != IntPtr.Zero)
				glRasterPos3fv  = (Call_glRasterPos3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3fv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3fv");
				if (glf != IntPtr.Zero)
					glRasterPos3fv  = (Call_glRasterPos3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3fv));
			}

			glf = wglGetProcAddress("glRasterPos3i");
			if (glf != IntPtr.Zero)
				glRasterPos3i  = (Call_glRasterPos3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3i));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3i");
				if (glf != IntPtr.Zero)
					glRasterPos3i  = (Call_glRasterPos3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3i));
			}

			glf = wglGetProcAddress("glRasterPos3iv");
			if (glf != IntPtr.Zero)
				glRasterPos3iv  = (Call_glRasterPos3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3iv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3iv");
				if (glf != IntPtr.Zero)
					glRasterPos3iv  = (Call_glRasterPos3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3iv));
			}

			glf = wglGetProcAddress("glRasterPos3s");
			if (glf != IntPtr.Zero)
				glRasterPos3s  = (Call_glRasterPos3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3s));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3s");
				if (glf != IntPtr.Zero)
					glRasterPos3s  = (Call_glRasterPos3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3s));
			}

			glf = wglGetProcAddress("glRasterPos3sv");
			if (glf != IntPtr.Zero)
				glRasterPos3sv  = (Call_glRasterPos3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3sv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos3sv");
				if (glf != IntPtr.Zero)
					glRasterPos3sv  = (Call_glRasterPos3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos3sv));
			}

			glf = wglGetProcAddress("glRasterPos4d");
			if (glf != IntPtr.Zero)
				glRasterPos4d  = (Call_glRasterPos4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4d));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4d");
				if (glf != IntPtr.Zero)
					glRasterPos4d  = (Call_glRasterPos4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4d));
			}

			glf = wglGetProcAddress("glRasterPos4dv");
			if (glf != IntPtr.Zero)
				glRasterPos4dv  = (Call_glRasterPos4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4dv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4dv");
				if (glf != IntPtr.Zero)
					glRasterPos4dv  = (Call_glRasterPos4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4dv));
			}

			glf = wglGetProcAddress("glRasterPos4f");
			if (glf != IntPtr.Zero)
				glRasterPos4f  = (Call_glRasterPos4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4f));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4f");
				if (glf != IntPtr.Zero)
					glRasterPos4f  = (Call_glRasterPos4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4f));
			}

			glf = wglGetProcAddress("glRasterPos4fv");
			if (glf != IntPtr.Zero)
				glRasterPos4fv  = (Call_glRasterPos4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4fv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4fv");
				if (glf != IntPtr.Zero)
					glRasterPos4fv  = (Call_glRasterPos4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4fv));
			}

			glf = wglGetProcAddress("glRasterPos4i");
			if (glf != IntPtr.Zero)
				glRasterPos4i  = (Call_glRasterPos4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4i));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4i");
				if (glf != IntPtr.Zero)
					glRasterPos4i  = (Call_glRasterPos4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4i));
			}

			glf = wglGetProcAddress("glRasterPos4iv");
			if (glf != IntPtr.Zero)
				glRasterPos4iv  = (Call_glRasterPos4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4iv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4iv");
				if (glf != IntPtr.Zero)
					glRasterPos4iv  = (Call_glRasterPos4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4iv));
			}

			glf = wglGetProcAddress("glRasterPos4s");
			if (glf != IntPtr.Zero)
				glRasterPos4s  = (Call_glRasterPos4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4s));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4s");
				if (glf != IntPtr.Zero)
					glRasterPos4s  = (Call_glRasterPos4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4s));
			}

			glf = wglGetProcAddress("glRasterPos4sv");
			if (glf != IntPtr.Zero)
				glRasterPos4sv  = (Call_glRasterPos4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4sv));
			else {
				glf = GetProcAddress(pDll,"glRasterPos4sv");
				if (glf != IntPtr.Zero)
					glRasterPos4sv  = (Call_glRasterPos4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRasterPos4sv));
			}

			glf = wglGetProcAddress("glReadBuffer");
			if (glf != IntPtr.Zero)
				glReadBuffer  = (Call_glReadBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadBuffer));
			else {
				glf = GetProcAddress(pDll,"glReadBuffer");
				if (glf != IntPtr.Zero)
					glReadBuffer  = (Call_glReadBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadBuffer));
			}

			glf = wglGetProcAddress("glReadPixels");
			if (glf != IntPtr.Zero)
				glReadPixels  = (Call_glReadPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadPixels));
			else {
				glf = GetProcAddress(pDll,"glReadPixels");
				if (glf != IntPtr.Zero)
					glReadPixels  = (Call_glReadPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadPixels));
			}

			glf = wglGetProcAddress("glRectd");
			if (glf != IntPtr.Zero)
				glRectd  = (Call_glRectd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectd));
			else {
				glf = GetProcAddress(pDll,"glRectd");
				if (glf != IntPtr.Zero)
					glRectd  = (Call_glRectd)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectd));
			}

			glf = wglGetProcAddress("glRectdv");
			if (glf != IntPtr.Zero)
				glRectdv  = (Call_glRectdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectdv));
			else {
				glf = GetProcAddress(pDll,"glRectdv");
				if (glf != IntPtr.Zero)
					glRectdv  = (Call_glRectdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectdv));
			}

			glf = wglGetProcAddress("glRectf");
			if (glf != IntPtr.Zero)
				glRectf  = (Call_glRectf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectf));
			else {
				glf = GetProcAddress(pDll,"glRectf");
				if (glf != IntPtr.Zero)
					glRectf  = (Call_glRectf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectf));
			}

			glf = wglGetProcAddress("glRectfv");
			if (glf != IntPtr.Zero)
				glRectfv  = (Call_glRectfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectfv));
			else {
				glf = GetProcAddress(pDll,"glRectfv");
				if (glf != IntPtr.Zero)
					glRectfv  = (Call_glRectfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectfv));
			}

			glf = wglGetProcAddress("glRecti");
			if (glf != IntPtr.Zero)
				glRecti  = (Call_glRecti)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRecti));
			else {
				glf = GetProcAddress(pDll,"glRecti");
				if (glf != IntPtr.Zero)
					glRecti  = (Call_glRecti)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRecti));
			}

			glf = wglGetProcAddress("glRectiv");
			if (glf != IntPtr.Zero)
				glRectiv  = (Call_glRectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectiv));
			else {
				glf = GetProcAddress(pDll,"glRectiv");
				if (glf != IntPtr.Zero)
					glRectiv  = (Call_glRectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectiv));
			}

			glf = wglGetProcAddress("glRects");
			if (glf != IntPtr.Zero)
				glRects  = (Call_glRects)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRects));
			else {
				glf = GetProcAddress(pDll,"glRects");
				if (glf != IntPtr.Zero)
					glRects  = (Call_glRects)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRects));
			}

			glf = wglGetProcAddress("glRectsv");
			if (glf != IntPtr.Zero)
				glRectsv  = (Call_glRectsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectsv));
			else {
				glf = GetProcAddress(pDll,"glRectsv");
				if (glf != IntPtr.Zero)
					glRectsv  = (Call_glRectsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRectsv));
			}

			glf = wglGetProcAddress("glRenderMode");
			if (glf != IntPtr.Zero)
				glRenderMode  = (Call_glRenderMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderMode));
			else {
				glf = GetProcAddress(pDll,"glRenderMode");
				if (glf != IntPtr.Zero)
					glRenderMode  = (Call_glRenderMode)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderMode));
			}

			glf = wglGetProcAddress("glRotated");
			if (glf != IntPtr.Zero)
				glRotated  = (Call_glRotated)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRotated));
			else {
				glf = GetProcAddress(pDll,"glRotated");
				if (glf != IntPtr.Zero)
					glRotated  = (Call_glRotated)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRotated));
			}

			glf = wglGetProcAddress("glRotatef");
			if (glf != IntPtr.Zero)
				glRotatef  = (Call_glRotatef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRotatef));
			else {
				glf = GetProcAddress(pDll,"glRotatef");
				if (glf != IntPtr.Zero)
					glRotatef  = (Call_glRotatef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRotatef));
			}

			glf = wglGetProcAddress("glScaled");
			if (glf != IntPtr.Zero)
				glScaled  = (Call_glScaled)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScaled));
			else {
				glf = GetProcAddress(pDll,"glScaled");
				if (glf != IntPtr.Zero)
					glScaled  = (Call_glScaled)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScaled));
			}

			glf = wglGetProcAddress("glScalef");
			if (glf != IntPtr.Zero)
				glScalef  = (Call_glScalef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScalef));
			else {
				glf = GetProcAddress(pDll,"glScalef");
				if (glf != IntPtr.Zero)
					glScalef  = (Call_glScalef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScalef));
			}

			glf = wglGetProcAddress("glScissor");
			if (glf != IntPtr.Zero)
				glScissor  = (Call_glScissor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissor));
			else {
				glf = GetProcAddress(pDll,"glScissor");
				if (glf != IntPtr.Zero)
					glScissor  = (Call_glScissor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissor));
			}

			glf = wglGetProcAddress("glSelectBuffer");
			if (glf != IntPtr.Zero)
				glSelectBuffer  = (Call_glSelectBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSelectBuffer));
			else {
				glf = GetProcAddress(pDll,"glSelectBuffer");
				if (glf != IntPtr.Zero)
					glSelectBuffer  = (Call_glSelectBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSelectBuffer));
			}

			glf = wglGetProcAddress("glShadeModel");
			if (glf != IntPtr.Zero)
				glShadeModel  = (Call_glShadeModel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShadeModel));
			else {
				glf = GetProcAddress(pDll,"glShadeModel");
				if (glf != IntPtr.Zero)
					glShadeModel  = (Call_glShadeModel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShadeModel));
			}

			glf = wglGetProcAddress("glStencilFunc");
			if (glf != IntPtr.Zero)
				glStencilFunc  = (Call_glStencilFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilFunc));
			else {
				glf = GetProcAddress(pDll,"glStencilFunc");
				if (glf != IntPtr.Zero)
					glStencilFunc  = (Call_glStencilFunc)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilFunc));
			}

			glf = wglGetProcAddress("glStencilMask");
			if (glf != IntPtr.Zero)
				glStencilMask  = (Call_glStencilMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilMask));
			else {
				glf = GetProcAddress(pDll,"glStencilMask");
				if (glf != IntPtr.Zero)
					glStencilMask  = (Call_glStencilMask)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilMask));
			}

			glf = wglGetProcAddress("glStencilOp");
			if (glf != IntPtr.Zero)
				glStencilOp  = (Call_glStencilOp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilOp));
			else {
				glf = GetProcAddress(pDll,"glStencilOp");
				if (glf != IntPtr.Zero)
					glStencilOp  = (Call_glStencilOp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilOp));
			}

			glf = wglGetProcAddress("glTexCoord1d");
			if (glf != IntPtr.Zero)
				glTexCoord1d  = (Call_glTexCoord1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1d));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1d");
				if (glf != IntPtr.Zero)
					glTexCoord1d  = (Call_glTexCoord1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1d));
			}

			glf = wglGetProcAddress("glTexCoord1dv");
			if (glf != IntPtr.Zero)
				glTexCoord1dv  = (Call_glTexCoord1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1dv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1dv");
				if (glf != IntPtr.Zero)
					glTexCoord1dv  = (Call_glTexCoord1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1dv));
			}

			glf = wglGetProcAddress("glTexCoord1f");
			if (glf != IntPtr.Zero)
				glTexCoord1f  = (Call_glTexCoord1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1f));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1f");
				if (glf != IntPtr.Zero)
					glTexCoord1f  = (Call_glTexCoord1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1f));
			}

			glf = wglGetProcAddress("glTexCoord1fv");
			if (glf != IntPtr.Zero)
				glTexCoord1fv  = (Call_glTexCoord1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1fv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1fv");
				if (glf != IntPtr.Zero)
					glTexCoord1fv  = (Call_glTexCoord1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1fv));
			}

			glf = wglGetProcAddress("glTexCoord1i");
			if (glf != IntPtr.Zero)
				glTexCoord1i  = (Call_glTexCoord1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1i));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1i");
				if (glf != IntPtr.Zero)
					glTexCoord1i  = (Call_glTexCoord1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1i));
			}

			glf = wglGetProcAddress("glTexCoord1iv");
			if (glf != IntPtr.Zero)
				glTexCoord1iv  = (Call_glTexCoord1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1iv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1iv");
				if (glf != IntPtr.Zero)
					glTexCoord1iv  = (Call_glTexCoord1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1iv));
			}

			glf = wglGetProcAddress("glTexCoord1s");
			if (glf != IntPtr.Zero)
				glTexCoord1s  = (Call_glTexCoord1s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1s));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1s");
				if (glf != IntPtr.Zero)
					glTexCoord1s  = (Call_glTexCoord1s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1s));
			}

			glf = wglGetProcAddress("glTexCoord1sv");
			if (glf != IntPtr.Zero)
				glTexCoord1sv  = (Call_glTexCoord1sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1sv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord1sv");
				if (glf != IntPtr.Zero)
					glTexCoord1sv  = (Call_glTexCoord1sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord1sv));
			}

			glf = wglGetProcAddress("glTexCoord2d");
			if (glf != IntPtr.Zero)
				glTexCoord2d  = (Call_glTexCoord2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2d));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2d");
				if (glf != IntPtr.Zero)
					glTexCoord2d  = (Call_glTexCoord2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2d));
			}

			glf = wglGetProcAddress("glTexCoord2dv");
			if (glf != IntPtr.Zero)
				glTexCoord2dv  = (Call_glTexCoord2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2dv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2dv");
				if (glf != IntPtr.Zero)
					glTexCoord2dv  = (Call_glTexCoord2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2dv));
			}

			glf = wglGetProcAddress("glTexCoord2f");
			if (glf != IntPtr.Zero)
				glTexCoord2f  = (Call_glTexCoord2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2f));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2f");
				if (glf != IntPtr.Zero)
					glTexCoord2f  = (Call_glTexCoord2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2f));
			}

			glf = wglGetProcAddress("glTexCoord2fv");
			if (glf != IntPtr.Zero)
				glTexCoord2fv  = (Call_glTexCoord2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2fv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2fv");
				if (glf != IntPtr.Zero)
					glTexCoord2fv  = (Call_glTexCoord2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2fv));
			}

			glf = wglGetProcAddress("glTexCoord2i");
			if (glf != IntPtr.Zero)
				glTexCoord2i  = (Call_glTexCoord2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2i));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2i");
				if (glf != IntPtr.Zero)
					glTexCoord2i  = (Call_glTexCoord2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2i));
			}

			glf = wglGetProcAddress("glTexCoord2iv");
			if (glf != IntPtr.Zero)
				glTexCoord2iv  = (Call_glTexCoord2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2iv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2iv");
				if (glf != IntPtr.Zero)
					glTexCoord2iv  = (Call_glTexCoord2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2iv));
			}

			glf = wglGetProcAddress("glTexCoord2s");
			if (glf != IntPtr.Zero)
				glTexCoord2s  = (Call_glTexCoord2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2s));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2s");
				if (glf != IntPtr.Zero)
					glTexCoord2s  = (Call_glTexCoord2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2s));
			}

			glf = wglGetProcAddress("glTexCoord2sv");
			if (glf != IntPtr.Zero)
				glTexCoord2sv  = (Call_glTexCoord2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2sv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord2sv");
				if (glf != IntPtr.Zero)
					glTexCoord2sv  = (Call_glTexCoord2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord2sv));
			}

			glf = wglGetProcAddress("glTexCoord3d");
			if (glf != IntPtr.Zero)
				glTexCoord3d  = (Call_glTexCoord3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3d));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3d");
				if (glf != IntPtr.Zero)
					glTexCoord3d  = (Call_glTexCoord3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3d));
			}

			glf = wglGetProcAddress("glTexCoord3dv");
			if (glf != IntPtr.Zero)
				glTexCoord3dv  = (Call_glTexCoord3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3dv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3dv");
				if (glf != IntPtr.Zero)
					glTexCoord3dv  = (Call_glTexCoord3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3dv));
			}

			glf = wglGetProcAddress("glTexCoord3f");
			if (glf != IntPtr.Zero)
				glTexCoord3f  = (Call_glTexCoord3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3f));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3f");
				if (glf != IntPtr.Zero)
					glTexCoord3f  = (Call_glTexCoord3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3f));
			}

			glf = wglGetProcAddress("glTexCoord3fv");
			if (glf != IntPtr.Zero)
				glTexCoord3fv  = (Call_glTexCoord3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3fv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3fv");
				if (glf != IntPtr.Zero)
					glTexCoord3fv  = (Call_glTexCoord3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3fv));
			}

			glf = wglGetProcAddress("glTexCoord3i");
			if (glf != IntPtr.Zero)
				glTexCoord3i  = (Call_glTexCoord3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3i));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3i");
				if (glf != IntPtr.Zero)
					glTexCoord3i  = (Call_glTexCoord3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3i));
			}

			glf = wglGetProcAddress("glTexCoord3iv");
			if (glf != IntPtr.Zero)
				glTexCoord3iv  = (Call_glTexCoord3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3iv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3iv");
				if (glf != IntPtr.Zero)
					glTexCoord3iv  = (Call_glTexCoord3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3iv));
			}

			glf = wglGetProcAddress("glTexCoord3s");
			if (glf != IntPtr.Zero)
				glTexCoord3s  = (Call_glTexCoord3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3s));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3s");
				if (glf != IntPtr.Zero)
					glTexCoord3s  = (Call_glTexCoord3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3s));
			}

			glf = wglGetProcAddress("glTexCoord3sv");
			if (glf != IntPtr.Zero)
				glTexCoord3sv  = (Call_glTexCoord3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3sv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord3sv");
				if (glf != IntPtr.Zero)
					glTexCoord3sv  = (Call_glTexCoord3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord3sv));
			}

			glf = wglGetProcAddress("glTexCoord4d");
			if (glf != IntPtr.Zero)
				glTexCoord4d  = (Call_glTexCoord4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4d));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4d");
				if (glf != IntPtr.Zero)
					glTexCoord4d  = (Call_glTexCoord4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4d));
			}

			glf = wglGetProcAddress("glTexCoord4dv");
			if (glf != IntPtr.Zero)
				glTexCoord4dv  = (Call_glTexCoord4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4dv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4dv");
				if (glf != IntPtr.Zero)
					glTexCoord4dv  = (Call_glTexCoord4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4dv));
			}

			glf = wglGetProcAddress("glTexCoord4f");
			if (glf != IntPtr.Zero)
				glTexCoord4f  = (Call_glTexCoord4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4f));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4f");
				if (glf != IntPtr.Zero)
					glTexCoord4f  = (Call_glTexCoord4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4f));
			}

			glf = wglGetProcAddress("glTexCoord4fv");
			if (glf != IntPtr.Zero)
				glTexCoord4fv  = (Call_glTexCoord4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4fv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4fv");
				if (glf != IntPtr.Zero)
					glTexCoord4fv  = (Call_glTexCoord4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4fv));
			}

			glf = wglGetProcAddress("glTexCoord4i");
			if (glf != IntPtr.Zero)
				glTexCoord4i  = (Call_glTexCoord4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4i));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4i");
				if (glf != IntPtr.Zero)
					glTexCoord4i  = (Call_glTexCoord4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4i));
			}

			glf = wglGetProcAddress("glTexCoord4iv");
			if (glf != IntPtr.Zero)
				glTexCoord4iv  = (Call_glTexCoord4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4iv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4iv");
				if (glf != IntPtr.Zero)
					glTexCoord4iv  = (Call_glTexCoord4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4iv));
			}

			glf = wglGetProcAddress("glTexCoord4s");
			if (glf != IntPtr.Zero)
				glTexCoord4s  = (Call_glTexCoord4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4s));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4s");
				if (glf != IntPtr.Zero)
					glTexCoord4s  = (Call_glTexCoord4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4s));
			}

			glf = wglGetProcAddress("glTexCoord4sv");
			if (glf != IntPtr.Zero)
				glTexCoord4sv  = (Call_glTexCoord4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4sv));
			else {
				glf = GetProcAddress(pDll,"glTexCoord4sv");
				if (glf != IntPtr.Zero)
					glTexCoord4sv  = (Call_glTexCoord4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoord4sv));
			}

			glf = wglGetProcAddress("glTexCoordPointer");
			if (glf != IntPtr.Zero)
				glTexCoordPointer  = (Call_glTexCoordPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoordPointer));
			else {
				glf = GetProcAddress(pDll,"glTexCoordPointer");
				if (glf != IntPtr.Zero)
					glTexCoordPointer  = (Call_glTexCoordPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexCoordPointer));
			}

			glf = wglGetProcAddress("glTexEnvf");
			if (glf != IntPtr.Zero)
				glTexEnvf  = (Call_glTexEnvf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvf));
			else {
				glf = GetProcAddress(pDll,"glTexEnvf");
				if (glf != IntPtr.Zero)
					glTexEnvf  = (Call_glTexEnvf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvf));
			}

			glf = wglGetProcAddress("glTexEnvfv");
			if (glf != IntPtr.Zero)
				glTexEnvfv  = (Call_glTexEnvfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvfv));
			else {
				glf = GetProcAddress(pDll,"glTexEnvfv");
				if (glf != IntPtr.Zero)
					glTexEnvfv  = (Call_glTexEnvfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvfv));
			}

			glf = wglGetProcAddress("glTexEnvi");
			if (glf != IntPtr.Zero)
				glTexEnvi  = (Call_glTexEnvi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvi));
			else {
				glf = GetProcAddress(pDll,"glTexEnvi");
				if (glf != IntPtr.Zero)
					glTexEnvi  = (Call_glTexEnvi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnvi));
			}

			glf = wglGetProcAddress("glTexEnviv");
			if (glf != IntPtr.Zero)
				glTexEnviv  = (Call_glTexEnviv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnviv));
			else {
				glf = GetProcAddress(pDll,"glTexEnviv");
				if (glf != IntPtr.Zero)
					glTexEnviv  = (Call_glTexEnviv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexEnviv));
			}

			glf = wglGetProcAddress("glTexGend");
			if (glf != IntPtr.Zero)
				glTexGend  = (Call_glTexGend)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGend));
			else {
				glf = GetProcAddress(pDll,"glTexGend");
				if (glf != IntPtr.Zero)
					glTexGend  = (Call_glTexGend)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGend));
			}

			glf = wglGetProcAddress("glTexGendv");
			if (glf != IntPtr.Zero)
				glTexGendv  = (Call_glTexGendv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGendv));
			else {
				glf = GetProcAddress(pDll,"glTexGendv");
				if (glf != IntPtr.Zero)
					glTexGendv  = (Call_glTexGendv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGendv));
			}

			glf = wglGetProcAddress("glTexGenf");
			if (glf != IntPtr.Zero)
				glTexGenf  = (Call_glTexGenf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGenf));
			else {
				glf = GetProcAddress(pDll,"glTexGenf");
				if (glf != IntPtr.Zero)
					glTexGenf  = (Call_glTexGenf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGenf));
			}

			glf = wglGetProcAddress("glTexGenfv");
			if (glf != IntPtr.Zero)
				glTexGenfv  = (Call_glTexGenfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGenfv));
			else {
				glf = GetProcAddress(pDll,"glTexGenfv");
				if (glf != IntPtr.Zero)
					glTexGenfv  = (Call_glTexGenfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGenfv));
			}

			glf = wglGetProcAddress("glTexGeni");
			if (glf != IntPtr.Zero)
				glTexGeni  = (Call_glTexGeni)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGeni));
			else {
				glf = GetProcAddress(pDll,"glTexGeni");
				if (glf != IntPtr.Zero)
					glTexGeni  = (Call_glTexGeni)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGeni));
			}

			glf = wglGetProcAddress("glTexGeniv");
			if (glf != IntPtr.Zero)
				glTexGeniv  = (Call_glTexGeniv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGeniv));
			else {
				glf = GetProcAddress(pDll,"glTexGeniv");
				if (glf != IntPtr.Zero)
					glTexGeniv  = (Call_glTexGeniv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexGeniv));
			}

			glf = wglGetProcAddress("glTexImage1D");
			if (glf != IntPtr.Zero)
				glTexImage1D  = (Call_glTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage1D));
			else {
				glf = GetProcAddress(pDll,"glTexImage1D");
				if (glf != IntPtr.Zero)
					glTexImage1D  = (Call_glTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage1D));
			}

			glf = wglGetProcAddress("glTexImage2D");
			if (glf != IntPtr.Zero)
				glTexImage2D  = (Call_glTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage2D));
			else {
				glf = GetProcAddress(pDll,"glTexImage2D");
				if (glf != IntPtr.Zero)
					glTexImage2D  = (Call_glTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage2D));
			}

			glf = wglGetProcAddress("glTexParameterf");
			if (glf != IntPtr.Zero)
				glTexParameterf  = (Call_glTexParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterf));
			else {
				glf = GetProcAddress(pDll,"glTexParameterf");
				if (glf != IntPtr.Zero)
					glTexParameterf  = (Call_glTexParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterf));
			}

			glf = wglGetProcAddress("glTexParameterfv");
			if (glf != IntPtr.Zero)
				glTexParameterfv  = (Call_glTexParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterfv));
			else {
				glf = GetProcAddress(pDll,"glTexParameterfv");
				if (glf != IntPtr.Zero)
					glTexParameterfv  = (Call_glTexParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterfv));
			}

			glf = wglGetProcAddress("glTexParameteri");
			if (glf != IntPtr.Zero)
				glTexParameteri  = (Call_glTexParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameteri));
			else {
				glf = GetProcAddress(pDll,"glTexParameteri");
				if (glf != IntPtr.Zero)
					glTexParameteri  = (Call_glTexParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameteri));
			}

			glf = wglGetProcAddress("glTexParameteriv");
			if (glf != IntPtr.Zero)
				glTexParameteriv  = (Call_glTexParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameteriv));
			else {
				glf = GetProcAddress(pDll,"glTexParameteriv");
				if (glf != IntPtr.Zero)
					glTexParameteriv  = (Call_glTexParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameteriv));
			}

			glf = wglGetProcAddress("glTexSubImage1D");
			if (glf != IntPtr.Zero)
				glTexSubImage1D  = (Call_glTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glTexSubImage1D");
				if (glf != IntPtr.Zero)
					glTexSubImage1D  = (Call_glTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage1D));
			}

			glf = wglGetProcAddress("glTexSubImage2D");
			if (glf != IntPtr.Zero)
				glTexSubImage2D  = (Call_glTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glTexSubImage2D");
				if (glf != IntPtr.Zero)
					glTexSubImage2D  = (Call_glTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage2D));
			}

			glf = wglGetProcAddress("glTranslated");
			if (glf != IntPtr.Zero)
				glTranslated  = (Call_glTranslated)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTranslated));
			else {
				glf = GetProcAddress(pDll,"glTranslated");
				if (glf != IntPtr.Zero)
					glTranslated  = (Call_glTranslated)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTranslated));
			}

			glf = wglGetProcAddress("glTranslatef");
			if (glf != IntPtr.Zero)
				glTranslatef  = (Call_glTranslatef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTranslatef));
			else {
				glf = GetProcAddress(pDll,"glTranslatef");
				if (glf != IntPtr.Zero)
					glTranslatef  = (Call_glTranslatef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTranslatef));
			}

			glf = wglGetProcAddress("glVertex2d");
			if (glf != IntPtr.Zero)
				glVertex2d  = (Call_glVertex2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2d));
			else {
				glf = GetProcAddress(pDll,"glVertex2d");
				if (glf != IntPtr.Zero)
					glVertex2d  = (Call_glVertex2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2d));
			}

			glf = wglGetProcAddress("glVertex2dv");
			if (glf != IntPtr.Zero)
				glVertex2dv  = (Call_glVertex2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2dv));
			else {
				glf = GetProcAddress(pDll,"glVertex2dv");
				if (glf != IntPtr.Zero)
					glVertex2dv  = (Call_glVertex2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2dv));
			}

			glf = wglGetProcAddress("glVertex2f");
			if (glf != IntPtr.Zero)
				glVertex2f  = (Call_glVertex2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2f));
			else {
				glf = GetProcAddress(pDll,"glVertex2f");
				if (glf != IntPtr.Zero)
					glVertex2f  = (Call_glVertex2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2f));
			}

			glf = wglGetProcAddress("glVertex2fv");
			if (glf != IntPtr.Zero)
				glVertex2fv  = (Call_glVertex2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2fv));
			else {
				glf = GetProcAddress(pDll,"glVertex2fv");
				if (glf != IntPtr.Zero)
					glVertex2fv  = (Call_glVertex2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2fv));
			}

			glf = wglGetProcAddress("glVertex2i");
			if (glf != IntPtr.Zero)
				glVertex2i  = (Call_glVertex2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2i));
			else {
				glf = GetProcAddress(pDll,"glVertex2i");
				if (glf != IntPtr.Zero)
					glVertex2i  = (Call_glVertex2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2i));
			}

			glf = wglGetProcAddress("glVertex2iv");
			if (glf != IntPtr.Zero)
				glVertex2iv  = (Call_glVertex2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2iv));
			else {
				glf = GetProcAddress(pDll,"glVertex2iv");
				if (glf != IntPtr.Zero)
					glVertex2iv  = (Call_glVertex2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2iv));
			}

			glf = wglGetProcAddress("glVertex2s");
			if (glf != IntPtr.Zero)
				glVertex2s  = (Call_glVertex2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2s));
			else {
				glf = GetProcAddress(pDll,"glVertex2s");
				if (glf != IntPtr.Zero)
					glVertex2s  = (Call_glVertex2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2s));
			}

			glf = wglGetProcAddress("glVertex2sv");
			if (glf != IntPtr.Zero)
				glVertex2sv  = (Call_glVertex2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2sv));
			else {
				glf = GetProcAddress(pDll,"glVertex2sv");
				if (glf != IntPtr.Zero)
					glVertex2sv  = (Call_glVertex2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex2sv));
			}

			glf = wglGetProcAddress("glVertex3d");
			if (glf != IntPtr.Zero)
				glVertex3d  = (Call_glVertex3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3d));
			else {
				glf = GetProcAddress(pDll,"glVertex3d");
				if (glf != IntPtr.Zero)
					glVertex3d  = (Call_glVertex3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3d));
			}

			glf = wglGetProcAddress("glVertex3dv");
			if (glf != IntPtr.Zero)
				glVertex3dv  = (Call_glVertex3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3dv));
			else {
				glf = GetProcAddress(pDll,"glVertex3dv");
				if (glf != IntPtr.Zero)
					glVertex3dv  = (Call_glVertex3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3dv));
			}

			glf = wglGetProcAddress("glVertex3f");
			if (glf != IntPtr.Zero)
				glVertex3f  = (Call_glVertex3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3f));
			else {
				glf = GetProcAddress(pDll,"glVertex3f");
				if (glf != IntPtr.Zero)
					glVertex3f  = (Call_glVertex3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3f));
			}

			glf = wglGetProcAddress("glVertex3fv");
			if (glf != IntPtr.Zero)
				glVertex3fv  = (Call_glVertex3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3fv));
			else {
				glf = GetProcAddress(pDll,"glVertex3fv");
				if (glf != IntPtr.Zero)
					glVertex3fv  = (Call_glVertex3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3fv));
			}

			glf = wglGetProcAddress("glVertex3i");
			if (glf != IntPtr.Zero)
				glVertex3i  = (Call_glVertex3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3i));
			else {
				glf = GetProcAddress(pDll,"glVertex3i");
				if (glf != IntPtr.Zero)
					glVertex3i  = (Call_glVertex3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3i));
			}

			glf = wglGetProcAddress("glVertex3iv");
			if (glf != IntPtr.Zero)
				glVertex3iv  = (Call_glVertex3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3iv));
			else {
				glf = GetProcAddress(pDll,"glVertex3iv");
				if (glf != IntPtr.Zero)
					glVertex3iv  = (Call_glVertex3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3iv));
			}

			glf = wglGetProcAddress("glVertex3s");
			if (glf != IntPtr.Zero)
				glVertex3s  = (Call_glVertex3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3s));
			else {
				glf = GetProcAddress(pDll,"glVertex3s");
				if (glf != IntPtr.Zero)
					glVertex3s  = (Call_glVertex3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3s));
			}

			glf = wglGetProcAddress("glVertex3sv");
			if (glf != IntPtr.Zero)
				glVertex3sv  = (Call_glVertex3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3sv));
			else {
				glf = GetProcAddress(pDll,"glVertex3sv");
				if (glf != IntPtr.Zero)
					glVertex3sv  = (Call_glVertex3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex3sv));
			}

			glf = wglGetProcAddress("glVertex4d");
			if (glf != IntPtr.Zero)
				glVertex4d  = (Call_glVertex4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4d));
			else {
				glf = GetProcAddress(pDll,"glVertex4d");
				if (glf != IntPtr.Zero)
					glVertex4d  = (Call_glVertex4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4d));
			}

			glf = wglGetProcAddress("glVertex4dv");
			if (glf != IntPtr.Zero)
				glVertex4dv  = (Call_glVertex4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4dv));
			else {
				glf = GetProcAddress(pDll,"glVertex4dv");
				if (glf != IntPtr.Zero)
					glVertex4dv  = (Call_glVertex4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4dv));
			}

			glf = wglGetProcAddress("glVertex4f");
			if (glf != IntPtr.Zero)
				glVertex4f  = (Call_glVertex4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4f));
			else {
				glf = GetProcAddress(pDll,"glVertex4f");
				if (glf != IntPtr.Zero)
					glVertex4f  = (Call_glVertex4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4f));
			}

			glf = wglGetProcAddress("glVertex4fv");
			if (glf != IntPtr.Zero)
				glVertex4fv  = (Call_glVertex4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4fv));
			else {
				glf = GetProcAddress(pDll,"glVertex4fv");
				if (glf != IntPtr.Zero)
					glVertex4fv  = (Call_glVertex4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4fv));
			}

			glf = wglGetProcAddress("glVertex4i");
			if (glf != IntPtr.Zero)
				glVertex4i  = (Call_glVertex4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4i));
			else {
				glf = GetProcAddress(pDll,"glVertex4i");
				if (glf != IntPtr.Zero)
					glVertex4i  = (Call_glVertex4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4i));
			}

			glf = wglGetProcAddress("glVertex4iv");
			if (glf != IntPtr.Zero)
				glVertex4iv  = (Call_glVertex4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4iv));
			else {
				glf = GetProcAddress(pDll,"glVertex4iv");
				if (glf != IntPtr.Zero)
					glVertex4iv  = (Call_glVertex4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4iv));
			}

			glf = wglGetProcAddress("glVertex4s");
			if (glf != IntPtr.Zero)
				glVertex4s  = (Call_glVertex4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4s));
			else {
				glf = GetProcAddress(pDll,"glVertex4s");
				if (glf != IntPtr.Zero)
					glVertex4s  = (Call_glVertex4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4s));
			}

			glf = wglGetProcAddress("glVertex4sv");
			if (glf != IntPtr.Zero)
				glVertex4sv  = (Call_glVertex4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4sv));
			else {
				glf = GetProcAddress(pDll,"glVertex4sv");
				if (glf != IntPtr.Zero)
					glVertex4sv  = (Call_glVertex4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertex4sv));
			}

			glf = wglGetProcAddress("glVertexPointer");
			if (glf != IntPtr.Zero)
				glVertexPointer  = (Call_glVertexPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexPointer));
			else {
				glf = GetProcAddress(pDll,"glVertexPointer");
				if (glf != IntPtr.Zero)
					glVertexPointer  = (Call_glVertexPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexPointer));
			}

			glf = wglGetProcAddress("glViewport");
			if (glf != IntPtr.Zero)
				glViewport  = (Call_glViewport)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewport));
			else {
				glf = GetProcAddress(pDll,"glViewport");
				if (glf != IntPtr.Zero)
					glViewport  = (Call_glViewport)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewport));
			}

			glf = wglGetProcAddress("glDrawRangeElements");
			if (glf != IntPtr.Zero)
				glDrawRangeElements  = (Call_glDrawRangeElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawRangeElements));
			else {
				glf = GetProcAddress(pDll,"glDrawRangeElements");
				if (glf != IntPtr.Zero)
					glDrawRangeElements  = (Call_glDrawRangeElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawRangeElements));
			}

			glf = wglGetProcAddress("glTexImage3D");
			if (glf != IntPtr.Zero)
				glTexImage3D  = (Call_glTexImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage3D));
			else {
				glf = GetProcAddress(pDll,"glTexImage3D");
				if (glf != IntPtr.Zero)
					glTexImage3D  = (Call_glTexImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage3D));
			}

			glf = wglGetProcAddress("glTexSubImage3D");
			if (glf != IntPtr.Zero)
				glTexSubImage3D  = (Call_glTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glTexSubImage3D");
				if (glf != IntPtr.Zero)
					glTexSubImage3D  = (Call_glTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexSubImage3D));
			}

			glf = wglGetProcAddress("glCopyTexSubImage3D");
			if (glf != IntPtr.Zero)
				glCopyTexSubImage3D  = (Call_glCopyTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glCopyTexSubImage3D");
				if (glf != IntPtr.Zero)
					glCopyTexSubImage3D  = (Call_glCopyTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTexSubImage3D));
			}

			glf = wglGetProcAddress("glActiveTexture");
			if (glf != IntPtr.Zero)
				glActiveTexture  = (Call_glActiveTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glActiveTexture));
			else {
				glf = GetProcAddress(pDll,"glActiveTexture");
				if (glf != IntPtr.Zero)
					glActiveTexture  = (Call_glActiveTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glActiveTexture));
			}

			glf = wglGetProcAddress("glSampleCoverage");
			if (glf != IntPtr.Zero)
				glSampleCoverage  = (Call_glSampleCoverage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSampleCoverage));
			else {
				glf = GetProcAddress(pDll,"glSampleCoverage");
				if (glf != IntPtr.Zero)
					glSampleCoverage  = (Call_glSampleCoverage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSampleCoverage));
			}

			glf = wglGetProcAddress("glCompressedTexImage3D");
			if (glf != IntPtr.Zero)
				glCompressedTexImage3D  = (Call_glCompressedTexImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage3D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexImage3D");
				if (glf != IntPtr.Zero)
					glCompressedTexImage3D  = (Call_glCompressedTexImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage3D));
			}

			glf = wglGetProcAddress("glCompressedTexImage2D");
			if (glf != IntPtr.Zero)
				glCompressedTexImage2D  = (Call_glCompressedTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage2D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexImage2D");
				if (glf != IntPtr.Zero)
					glCompressedTexImage2D  = (Call_glCompressedTexImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage2D));
			}

			glf = wglGetProcAddress("glCompressedTexImage1D");
			if (glf != IntPtr.Zero)
				glCompressedTexImage1D  = (Call_glCompressedTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage1D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexImage1D");
				if (glf != IntPtr.Zero)
					glCompressedTexImage1D  = (Call_glCompressedTexImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexImage1D));
			}

			glf = wglGetProcAddress("glCompressedTexSubImage3D");
			if (glf != IntPtr.Zero)
				glCompressedTexSubImage3D  = (Call_glCompressedTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexSubImage3D");
				if (glf != IntPtr.Zero)
					glCompressedTexSubImage3D  = (Call_glCompressedTexSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage3D));
			}

			glf = wglGetProcAddress("glCompressedTexSubImage2D");
			if (glf != IntPtr.Zero)
				glCompressedTexSubImage2D  = (Call_glCompressedTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexSubImage2D");
				if (glf != IntPtr.Zero)
					glCompressedTexSubImage2D  = (Call_glCompressedTexSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage2D));
			}

			glf = wglGetProcAddress("glCompressedTexSubImage1D");
			if (glf != IntPtr.Zero)
				glCompressedTexSubImage1D  = (Call_glCompressedTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTexSubImage1D");
				if (glf != IntPtr.Zero)
					glCompressedTexSubImage1D  = (Call_glCompressedTexSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTexSubImage1D));
			}

			glf = wglGetProcAddress("glGetCompressedTexImage");
			if (glf != IntPtr.Zero)
				glGetCompressedTexImage  = (Call_glGetCompressedTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTexImage));
			else {
				glf = GetProcAddress(pDll,"glGetCompressedTexImage");
				if (glf != IntPtr.Zero)
					glGetCompressedTexImage  = (Call_glGetCompressedTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTexImage));
			}

			glf = wglGetProcAddress("glBlendFuncSeparate");
			if (glf != IntPtr.Zero)
				glBlendFuncSeparate  = (Call_glBlendFuncSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFuncSeparate));
			else {
				glf = GetProcAddress(pDll,"glBlendFuncSeparate");
				if (glf != IntPtr.Zero)
					glBlendFuncSeparate  = (Call_glBlendFuncSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFuncSeparate));
			}

			glf = wglGetProcAddress("glMultiDrawArrays");
			if (glf != IntPtr.Zero)
				glMultiDrawArrays  = (Call_glMultiDrawArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArrays));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawArrays");
				if (glf != IntPtr.Zero)
					glMultiDrawArrays  = (Call_glMultiDrawArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArrays));
			}

			glf = wglGetProcAddress("glMultiDrawElements");
			if (glf != IntPtr.Zero)
				glMultiDrawElements  = (Call_glMultiDrawElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElements));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawElements");
				if (glf != IntPtr.Zero)
					glMultiDrawElements  = (Call_glMultiDrawElements)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElements));
			}

			glf = wglGetProcAddress("glPointParameterf");
			if (glf != IntPtr.Zero)
				glPointParameterf  = (Call_glPointParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameterf));
			else {
				glf = GetProcAddress(pDll,"glPointParameterf");
				if (glf != IntPtr.Zero)
					glPointParameterf  = (Call_glPointParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameterf));
			}

			glf = wglGetProcAddress("glPointParameterfv");
			if (glf != IntPtr.Zero)
				glPointParameterfv  = (Call_glPointParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameterfv));
			else {
				glf = GetProcAddress(pDll,"glPointParameterfv");
				if (glf != IntPtr.Zero)
					glPointParameterfv  = (Call_glPointParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameterfv));
			}

			glf = wglGetProcAddress("glPointParameteri");
			if (glf != IntPtr.Zero)
				glPointParameteri  = (Call_glPointParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameteri));
			else {
				glf = GetProcAddress(pDll,"glPointParameteri");
				if (glf != IntPtr.Zero)
					glPointParameteri  = (Call_glPointParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameteri));
			}

			glf = wglGetProcAddress("glPointParameteriv");
			if (glf != IntPtr.Zero)
				glPointParameteriv  = (Call_glPointParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameteriv));
			else {
				glf = GetProcAddress(pDll,"glPointParameteriv");
				if (glf != IntPtr.Zero)
					glPointParameteriv  = (Call_glPointParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPointParameteriv));
			}

			glf = wglGetProcAddress("glBlendColor");
			if (glf != IntPtr.Zero)
				glBlendColor  = (Call_glBlendColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendColor));
			else {
				glf = GetProcAddress(pDll,"glBlendColor");
				if (glf != IntPtr.Zero)
					glBlendColor  = (Call_glBlendColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendColor));
			}

			glf = wglGetProcAddress("glBlendEquation");
			if (glf != IntPtr.Zero)
				glBlendEquation  = (Call_glBlendEquation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquation));
			else {
				glf = GetProcAddress(pDll,"glBlendEquation");
				if (glf != IntPtr.Zero)
					glBlendEquation  = (Call_glBlendEquation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquation));
			}

			glf = wglGetProcAddress("glGenQueries");
			if (glf != IntPtr.Zero)
				glGenQueries  = (Call_glGenQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenQueries));
			else {
				glf = GetProcAddress(pDll,"glGenQueries");
				if (glf != IntPtr.Zero)
					glGenQueries  = (Call_glGenQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenQueries));
			}

			glf = wglGetProcAddress("glDeleteQueries");
			if (glf != IntPtr.Zero)
				glDeleteQueries  = (Call_glDeleteQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteQueries));
			else {
				glf = GetProcAddress(pDll,"glDeleteQueries");
				if (glf != IntPtr.Zero)
					glDeleteQueries  = (Call_glDeleteQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteQueries));
			}

			glf = wglGetProcAddress("glIsQuery");
			if (glf != IntPtr.Zero)
				glIsQuery  = (Call_glIsQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsQuery));
			else {
				glf = GetProcAddress(pDll,"glIsQuery");
				if (glf != IntPtr.Zero)
					glIsQuery  = (Call_glIsQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsQuery));
			}

			glf = wglGetProcAddress("glBeginQuery");
			if (glf != IntPtr.Zero)
				glBeginQuery  = (Call_glBeginQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginQuery));
			else {
				glf = GetProcAddress(pDll,"glBeginQuery");
				if (glf != IntPtr.Zero)
					glBeginQuery  = (Call_glBeginQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginQuery));
			}

			glf = wglGetProcAddress("glEndQuery");
			if (glf != IntPtr.Zero)
				glEndQuery  = (Call_glEndQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndQuery));
			else {
				glf = GetProcAddress(pDll,"glEndQuery");
				if (glf != IntPtr.Zero)
					glEndQuery  = (Call_glEndQuery)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndQuery));
			}

			glf = wglGetProcAddress("glGetQueryiv");
			if (glf != IntPtr.Zero)
				glGetQueryiv  = (Call_glGetQueryiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryiv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryiv");
				if (glf != IntPtr.Zero)
					glGetQueryiv  = (Call_glGetQueryiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryiv));
			}

			glf = wglGetProcAddress("glGetQueryObjectiv");
			if (glf != IntPtr.Zero)
				glGetQueryObjectiv  = (Call_glGetQueryObjectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectiv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryObjectiv");
				if (glf != IntPtr.Zero)
					glGetQueryObjectiv  = (Call_glGetQueryObjectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectiv));
			}

			glf = wglGetProcAddress("glGetQueryObjectuiv");
			if (glf != IntPtr.Zero)
				glGetQueryObjectuiv  = (Call_glGetQueryObjectuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectuiv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryObjectuiv");
				if (glf != IntPtr.Zero)
					glGetQueryObjectuiv  = (Call_glGetQueryObjectuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectuiv));
			}

			glf = wglGetProcAddress("glBindBuffer");
			if (glf != IntPtr.Zero)
				glBindBuffer  = (Call_glBindBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffer));
			else {
				glf = GetProcAddress(pDll,"glBindBuffer");
				if (glf != IntPtr.Zero)
					glBindBuffer  = (Call_glBindBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffer));
			}

			glf = wglGetProcAddress("glDeleteBuffers");
			if (glf != IntPtr.Zero)
				glDeleteBuffers  = (Call_glDeleteBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteBuffers));
			else {
				glf = GetProcAddress(pDll,"glDeleteBuffers");
				if (glf != IntPtr.Zero)
					glDeleteBuffers  = (Call_glDeleteBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteBuffers));
			}

			glf = wglGetProcAddress("glGenBuffers");
			if (glf != IntPtr.Zero)
				glGenBuffers  = (Call_glGenBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenBuffers));
			else {
				glf = GetProcAddress(pDll,"glGenBuffers");
				if (glf != IntPtr.Zero)
					glGenBuffers  = (Call_glGenBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenBuffers));
			}

			glf = wglGetProcAddress("glIsBuffer");
			if (glf != IntPtr.Zero)
				glIsBuffer  = (Call_glIsBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsBuffer));
			else {
				glf = GetProcAddress(pDll,"glIsBuffer");
				if (glf != IntPtr.Zero)
					glIsBuffer  = (Call_glIsBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsBuffer));
			}

			glf = wglGetProcAddress("glBufferData");
			if (glf != IntPtr.Zero)
				glBufferData  = (Call_glBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferData));
			else {
				glf = GetProcAddress(pDll,"glBufferData");
				if (glf != IntPtr.Zero)
					glBufferData  = (Call_glBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferData));
			}

			glf = wglGetProcAddress("glBufferSubData");
			if (glf != IntPtr.Zero)
				glBufferSubData  = (Call_glBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glBufferSubData");
				if (glf != IntPtr.Zero)
					glBufferSubData  = (Call_glBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferSubData));
			}

			glf = wglGetProcAddress("glGetBufferSubData");
			if (glf != IntPtr.Zero)
				glGetBufferSubData  = (Call_glGetBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glGetBufferSubData");
				if (glf != IntPtr.Zero)
					glGetBufferSubData  = (Call_glGetBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferSubData));
			}

			glf = wglGetProcAddress("glMapBuffer");
			if (glf != IntPtr.Zero)
				glMapBuffer  = (Call_glMapBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapBuffer));
			else {
				glf = GetProcAddress(pDll,"glMapBuffer");
				if (glf != IntPtr.Zero)
					glMapBuffer  = (Call_glMapBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapBuffer));
			}

			glf = wglGetProcAddress("glUnmapBuffer");
			if (glf != IntPtr.Zero)
				glUnmapBuffer  = (Call_glUnmapBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUnmapBuffer));
			else {
				glf = GetProcAddress(pDll,"glUnmapBuffer");
				if (glf != IntPtr.Zero)
					glUnmapBuffer  = (Call_glUnmapBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUnmapBuffer));
			}

			glf = wglGetProcAddress("glGetBufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetBufferParameteriv  = (Call_glGetBufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetBufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetBufferParameteriv  = (Call_glGetBufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferParameteriv));
			}

			glf = wglGetProcAddress("glGetBufferPointerv");
			if (glf != IntPtr.Zero)
				glGetBufferPointerv  = (Call_glGetBufferPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferPointerv));
			else {
				glf = GetProcAddress(pDll,"glGetBufferPointerv");
				if (glf != IntPtr.Zero)
					glGetBufferPointerv  = (Call_glGetBufferPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferPointerv));
			}

			glf = wglGetProcAddress("glBlendEquationSeparate");
			if (glf != IntPtr.Zero)
				glBlendEquationSeparate  = (Call_glBlendEquationSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationSeparate));
			else {
				glf = GetProcAddress(pDll,"glBlendEquationSeparate");
				if (glf != IntPtr.Zero)
					glBlendEquationSeparate  = (Call_glBlendEquationSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationSeparate));
			}

			glf = wglGetProcAddress("glDrawBuffers");
			if (glf != IntPtr.Zero)
				glDrawBuffers  = (Call_glDrawBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawBuffers));
			else {
				glf = GetProcAddress(pDll,"glDrawBuffers");
				if (glf != IntPtr.Zero)
					glDrawBuffers  = (Call_glDrawBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawBuffers));
			}

			glf = wglGetProcAddress("glStencilOpSeparate");
			if (glf != IntPtr.Zero)
				glStencilOpSeparate  = (Call_glStencilOpSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilOpSeparate));
			else {
				glf = GetProcAddress(pDll,"glStencilOpSeparate");
				if (glf != IntPtr.Zero)
					glStencilOpSeparate  = (Call_glStencilOpSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilOpSeparate));
			}

			glf = wglGetProcAddress("glStencilFuncSeparate");
			if (glf != IntPtr.Zero)
				glStencilFuncSeparate  = (Call_glStencilFuncSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilFuncSeparate));
			else {
				glf = GetProcAddress(pDll,"glStencilFuncSeparate");
				if (glf != IntPtr.Zero)
					glStencilFuncSeparate  = (Call_glStencilFuncSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilFuncSeparate));
			}

			glf = wglGetProcAddress("glStencilMaskSeparate");
			if (glf != IntPtr.Zero)
				glStencilMaskSeparate  = (Call_glStencilMaskSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilMaskSeparate));
			else {
				glf = GetProcAddress(pDll,"glStencilMaskSeparate");
				if (glf != IntPtr.Zero)
					glStencilMaskSeparate  = (Call_glStencilMaskSeparate)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glStencilMaskSeparate));
			}

			glf = wglGetProcAddress("glAttachShader");
			if (glf != IntPtr.Zero)
				glAttachShader  = (Call_glAttachShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAttachShader));
			else {
				glf = GetProcAddress(pDll,"glAttachShader");
				if (glf != IntPtr.Zero)
					glAttachShader  = (Call_glAttachShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glAttachShader));
			}

			glf = wglGetProcAddress("glBindAttribLocation");
			if (glf != IntPtr.Zero)
				glBindAttribLocation  = (Call_glBindAttribLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindAttribLocation));
			else {
				glf = GetProcAddress(pDll,"glBindAttribLocation");
				if (glf != IntPtr.Zero)
					glBindAttribLocation  = (Call_glBindAttribLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindAttribLocation));
			}

			glf = wglGetProcAddress("glCompileShader");
			if (glf != IntPtr.Zero)
				glCompileShader  = (Call_glCompileShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompileShader));
			else {
				glf = GetProcAddress(pDll,"glCompileShader");
				if (glf != IntPtr.Zero)
					glCompileShader  = (Call_glCompileShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompileShader));
			}

			glf = wglGetProcAddress("glCreateProgram");
			if (glf != IntPtr.Zero)
				glCreateProgram  = (Call_glCreateProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateProgram));
			else {
				glf = GetProcAddress(pDll,"glCreateProgram");
				if (glf != IntPtr.Zero)
					glCreateProgram  = (Call_glCreateProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateProgram));
			}

			glf = wglGetProcAddress("glCreateShader");
			if (glf != IntPtr.Zero)
				glCreateShader  = (Call_glCreateShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateShader));
			else {
				glf = GetProcAddress(pDll,"glCreateShader");
				if (glf != IntPtr.Zero)
					glCreateShader  = (Call_glCreateShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateShader));
			}

			glf = wglGetProcAddress("glDeleteProgram");
			if (glf != IntPtr.Zero)
				glDeleteProgram  = (Call_glDeleteProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteProgram));
			else {
				glf = GetProcAddress(pDll,"glDeleteProgram");
				if (glf != IntPtr.Zero)
					glDeleteProgram  = (Call_glDeleteProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteProgram));
			}

			glf = wglGetProcAddress("glDeleteShader");
			if (glf != IntPtr.Zero)
				glDeleteShader  = (Call_glDeleteShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteShader));
			else {
				glf = GetProcAddress(pDll,"glDeleteShader");
				if (glf != IntPtr.Zero)
					glDeleteShader  = (Call_glDeleteShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteShader));
			}

			glf = wglGetProcAddress("glDetachShader");
			if (glf != IntPtr.Zero)
				glDetachShader  = (Call_glDetachShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDetachShader));
			else {
				glf = GetProcAddress(pDll,"glDetachShader");
				if (glf != IntPtr.Zero)
					glDetachShader  = (Call_glDetachShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDetachShader));
			}

			glf = wglGetProcAddress("glDisableVertexAttribArray");
			if (glf != IntPtr.Zero)
				glDisableVertexAttribArray  = (Call_glDisableVertexAttribArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableVertexAttribArray));
			else {
				glf = GetProcAddress(pDll,"glDisableVertexAttribArray");
				if (glf != IntPtr.Zero)
					glDisableVertexAttribArray  = (Call_glDisableVertexAttribArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableVertexAttribArray));
			}

			glf = wglGetProcAddress("glEnableVertexAttribArray");
			if (glf != IntPtr.Zero)
				glEnableVertexAttribArray  = (Call_glEnableVertexAttribArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableVertexAttribArray));
			else {
				glf = GetProcAddress(pDll,"glEnableVertexAttribArray");
				if (glf != IntPtr.Zero)
					glEnableVertexAttribArray  = (Call_glEnableVertexAttribArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableVertexAttribArray));
			}

			glf = wglGetProcAddress("glGetActiveAttrib");
			if (glf != IntPtr.Zero)
				glGetActiveAttrib  = (Call_glGetActiveAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveAttrib));
			else {
				glf = GetProcAddress(pDll,"glGetActiveAttrib");
				if (glf != IntPtr.Zero)
					glGetActiveAttrib  = (Call_glGetActiveAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveAttrib));
			}

			glf = wglGetProcAddress("glGetActiveUniform");
			if (glf != IntPtr.Zero)
				glGetActiveUniform  = (Call_glGetActiveUniform)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniform));
			else {
				glf = GetProcAddress(pDll,"glGetActiveUniform");
				if (glf != IntPtr.Zero)
					glGetActiveUniform  = (Call_glGetActiveUniform)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniform));
			}

			glf = wglGetProcAddress("glGetAttachedShaders");
			if (glf != IntPtr.Zero)
				glGetAttachedShaders  = (Call_glGetAttachedShaders)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetAttachedShaders));
			else {
				glf = GetProcAddress(pDll,"glGetAttachedShaders");
				if (glf != IntPtr.Zero)
					glGetAttachedShaders  = (Call_glGetAttachedShaders)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetAttachedShaders));
			}

			glf = wglGetProcAddress("glGetAttribLocation");
			if (glf != IntPtr.Zero)
				glGetAttribLocation  = (Call_glGetAttribLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetAttribLocation));
			else {
				glf = GetProcAddress(pDll,"glGetAttribLocation");
				if (glf != IntPtr.Zero)
					glGetAttribLocation  = (Call_glGetAttribLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetAttribLocation));
			}

			glf = wglGetProcAddress("glGetProgramiv");
			if (glf != IntPtr.Zero)
				glGetProgramiv  = (Call_glGetProgramiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramiv));
			else {
				glf = GetProcAddress(pDll,"glGetProgramiv");
				if (glf != IntPtr.Zero)
					glGetProgramiv  = (Call_glGetProgramiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramiv));
			}

			glf = wglGetProcAddress("glGetProgramInfoLog");
			if (glf != IntPtr.Zero)
				glGetProgramInfoLog  = (Call_glGetProgramInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramInfoLog));
			else {
				glf = GetProcAddress(pDll,"glGetProgramInfoLog");
				if (glf != IntPtr.Zero)
					glGetProgramInfoLog  = (Call_glGetProgramInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramInfoLog));
			}

			glf = wglGetProcAddress("glGetShaderiv");
			if (glf != IntPtr.Zero)
				glGetShaderiv  = (Call_glGetShaderiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderiv));
			else {
				glf = GetProcAddress(pDll,"glGetShaderiv");
				if (glf != IntPtr.Zero)
					glGetShaderiv  = (Call_glGetShaderiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderiv));
			}

			glf = wglGetProcAddress("glGetShaderInfoLog");
			if (glf != IntPtr.Zero)
				glGetShaderInfoLog  = (Call_glGetShaderInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderInfoLog));
			else {
				glf = GetProcAddress(pDll,"glGetShaderInfoLog");
				if (glf != IntPtr.Zero)
					glGetShaderInfoLog  = (Call_glGetShaderInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderInfoLog));
			}

			glf = wglGetProcAddress("glGetShaderSource");
			if (glf != IntPtr.Zero)
				glGetShaderSource  = (Call_glGetShaderSource)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderSource));
			else {
				glf = GetProcAddress(pDll,"glGetShaderSource");
				if (glf != IntPtr.Zero)
					glGetShaderSource  = (Call_glGetShaderSource)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderSource));
			}

			glf = wglGetProcAddress("glGetUniformLocation");
			if (glf != IntPtr.Zero)
				glGetUniformLocation  = (Call_glGetUniformLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformLocation));
			else {
				glf = GetProcAddress(pDll,"glGetUniformLocation");
				if (glf != IntPtr.Zero)
					glGetUniformLocation  = (Call_glGetUniformLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformLocation));
			}

			glf = wglGetProcAddress("glGetUniformfv");
			if (glf != IntPtr.Zero)
				glGetUniformfv  = (Call_glGetUniformfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformfv));
			else {
				glf = GetProcAddress(pDll,"glGetUniformfv");
				if (glf != IntPtr.Zero)
					glGetUniformfv  = (Call_glGetUniformfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformfv));
			}

			glf = wglGetProcAddress("glGetUniformiv");
			if (glf != IntPtr.Zero)
				glGetUniformiv  = (Call_glGetUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformiv));
			else {
				glf = GetProcAddress(pDll,"glGetUniformiv");
				if (glf != IntPtr.Zero)
					glGetUniformiv  = (Call_glGetUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformiv));
			}

			glf = wglGetProcAddress("glGetVertexAttribdv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribdv  = (Call_glGetVertexAttribdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribdv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribdv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribdv  = (Call_glGetVertexAttribdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribdv));
			}

			glf = wglGetProcAddress("glGetVertexAttribfv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribfv  = (Call_glGetVertexAttribfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribfv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribfv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribfv  = (Call_glGetVertexAttribfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribfv));
			}

			glf = wglGetProcAddress("glGetVertexAttribiv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribiv  = (Call_glGetVertexAttribiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribiv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribiv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribiv  = (Call_glGetVertexAttribiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribiv));
			}

			glf = wglGetProcAddress("glGetVertexAttribPointerv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribPointerv  = (Call_glGetVertexAttribPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribPointerv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribPointerv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribPointerv  = (Call_glGetVertexAttribPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribPointerv));
			}

			glf = wglGetProcAddress("glIsProgram");
			if (glf != IntPtr.Zero)
				glIsProgram  = (Call_glIsProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsProgram));
			else {
				glf = GetProcAddress(pDll,"glIsProgram");
				if (glf != IntPtr.Zero)
					glIsProgram  = (Call_glIsProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsProgram));
			}

			glf = wglGetProcAddress("glIsShader");
			if (glf != IntPtr.Zero)
				glIsShader  = (Call_glIsShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsShader));
			else {
				glf = GetProcAddress(pDll,"glIsShader");
				if (glf != IntPtr.Zero)
					glIsShader  = (Call_glIsShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsShader));
			}

			glf = wglGetProcAddress("glLinkProgram");
			if (glf != IntPtr.Zero)
				glLinkProgram  = (Call_glLinkProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLinkProgram));
			else {
				glf = GetProcAddress(pDll,"glLinkProgram");
				if (glf != IntPtr.Zero)
					glLinkProgram  = (Call_glLinkProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glLinkProgram));
			}

			glf = wglGetProcAddress("glShaderSource");
			if (glf != IntPtr.Zero)
				glShaderSource  = (Call_glShaderSource)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderSource));
			else {
				glf = GetProcAddress(pDll,"glShaderSource");
				if (glf != IntPtr.Zero)
					glShaderSource  = (Call_glShaderSource)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderSource));
			}

			glf = wglGetProcAddress("glUseProgram");
			if (glf != IntPtr.Zero)
				glUseProgram  = (Call_glUseProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUseProgram));
			else {
				glf = GetProcAddress(pDll,"glUseProgram");
				if (glf != IntPtr.Zero)
					glUseProgram  = (Call_glUseProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUseProgram));
			}

			glf = wglGetProcAddress("glUniform1f");
			if (glf != IntPtr.Zero)
				glUniform1f  = (Call_glUniform1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1f));
			else {
				glf = GetProcAddress(pDll,"glUniform1f");
				if (glf != IntPtr.Zero)
					glUniform1f  = (Call_glUniform1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1f));
			}

			glf = wglGetProcAddress("glUniform2f");
			if (glf != IntPtr.Zero)
				glUniform2f  = (Call_glUniform2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2f));
			else {
				glf = GetProcAddress(pDll,"glUniform2f");
				if (glf != IntPtr.Zero)
					glUniform2f  = (Call_glUniform2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2f));
			}

			glf = wglGetProcAddress("glUniform3f");
			if (glf != IntPtr.Zero)
				glUniform3f  = (Call_glUniform3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3f));
			else {
				glf = GetProcAddress(pDll,"glUniform3f");
				if (glf != IntPtr.Zero)
					glUniform3f  = (Call_glUniform3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3f));
			}

			glf = wglGetProcAddress("glUniform4f");
			if (glf != IntPtr.Zero)
				glUniform4f  = (Call_glUniform4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4f));
			else {
				glf = GetProcAddress(pDll,"glUniform4f");
				if (glf != IntPtr.Zero)
					glUniform4f  = (Call_glUniform4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4f));
			}

			glf = wglGetProcAddress("glUniform1i");
			if (glf != IntPtr.Zero)
				glUniform1i  = (Call_glUniform1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1i));
			else {
				glf = GetProcAddress(pDll,"glUniform1i");
				if (glf != IntPtr.Zero)
					glUniform1i  = (Call_glUniform1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1i));
			}

			glf = wglGetProcAddress("glUniform2i");
			if (glf != IntPtr.Zero)
				glUniform2i  = (Call_glUniform2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2i));
			else {
				glf = GetProcAddress(pDll,"glUniform2i");
				if (glf != IntPtr.Zero)
					glUniform2i  = (Call_glUniform2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2i));
			}

			glf = wglGetProcAddress("glUniform3i");
			if (glf != IntPtr.Zero)
				glUniform3i  = (Call_glUniform3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3i));
			else {
				glf = GetProcAddress(pDll,"glUniform3i");
				if (glf != IntPtr.Zero)
					glUniform3i  = (Call_glUniform3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3i));
			}

			glf = wglGetProcAddress("glUniform4i");
			if (glf != IntPtr.Zero)
				glUniform4i  = (Call_glUniform4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4i));
			else {
				glf = GetProcAddress(pDll,"glUniform4i");
				if (glf != IntPtr.Zero)
					glUniform4i  = (Call_glUniform4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4i));
			}

			glf = wglGetProcAddress("glUniform1fv");
			if (glf != IntPtr.Zero)
				glUniform1fv  = (Call_glUniform1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1fv));
			else {
				glf = GetProcAddress(pDll,"glUniform1fv");
				if (glf != IntPtr.Zero)
					glUniform1fv  = (Call_glUniform1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1fv));
			}

			glf = wglGetProcAddress("glUniform2fv");
			if (glf != IntPtr.Zero)
				glUniform2fv  = (Call_glUniform2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2fv));
			else {
				glf = GetProcAddress(pDll,"glUniform2fv");
				if (glf != IntPtr.Zero)
					glUniform2fv  = (Call_glUniform2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2fv));
			}

			glf = wglGetProcAddress("glUniform3fv");
			if (glf != IntPtr.Zero)
				glUniform3fv  = (Call_glUniform3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3fv));
			else {
				glf = GetProcAddress(pDll,"glUniform3fv");
				if (glf != IntPtr.Zero)
					glUniform3fv  = (Call_glUniform3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3fv));
			}

			glf = wglGetProcAddress("glUniform4fv");
			if (glf != IntPtr.Zero)
				glUniform4fv  = (Call_glUniform4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4fv));
			else {
				glf = GetProcAddress(pDll,"glUniform4fv");
				if (glf != IntPtr.Zero)
					glUniform4fv  = (Call_glUniform4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4fv));
			}

			glf = wglGetProcAddress("glUniform1iv");
			if (glf != IntPtr.Zero)
				glUniform1iv  = (Call_glUniform1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1iv));
			else {
				glf = GetProcAddress(pDll,"glUniform1iv");
				if (glf != IntPtr.Zero)
					glUniform1iv  = (Call_glUniform1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1iv));
			}

			glf = wglGetProcAddress("glUniform2iv");
			if (glf != IntPtr.Zero)
				glUniform2iv  = (Call_glUniform2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2iv));
			else {
				glf = GetProcAddress(pDll,"glUniform2iv");
				if (glf != IntPtr.Zero)
					glUniform2iv  = (Call_glUniform2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2iv));
			}

			glf = wglGetProcAddress("glUniform3iv");
			if (glf != IntPtr.Zero)
				glUniform3iv  = (Call_glUniform3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3iv));
			else {
				glf = GetProcAddress(pDll,"glUniform3iv");
				if (glf != IntPtr.Zero)
					glUniform3iv  = (Call_glUniform3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3iv));
			}

			glf = wglGetProcAddress("glUniform4iv");
			if (glf != IntPtr.Zero)
				glUniform4iv  = (Call_glUniform4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4iv));
			else {
				glf = GetProcAddress(pDll,"glUniform4iv");
				if (glf != IntPtr.Zero)
					glUniform4iv  = (Call_glUniform4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4iv));
			}

			glf = wglGetProcAddress("glUniformMatrix2fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2fv  = (Call_glUniformMatrix2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2fv  = (Call_glUniformMatrix2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2fv));
			}

			glf = wglGetProcAddress("glUniformMatrix3fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3fv  = (Call_glUniformMatrix3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3fv  = (Call_glUniformMatrix3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3fv));
			}

			glf = wglGetProcAddress("glUniformMatrix4fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4fv  = (Call_glUniformMatrix4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4fv  = (Call_glUniformMatrix4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4fv));
			}

			glf = wglGetProcAddress("glValidateProgram");
			if (glf != IntPtr.Zero)
				glValidateProgram  = (Call_glValidateProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glValidateProgram));
			else {
				glf = GetProcAddress(pDll,"glValidateProgram");
				if (glf != IntPtr.Zero)
					glValidateProgram  = (Call_glValidateProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glValidateProgram));
			}

			glf = wglGetProcAddress("glVertexAttrib1d");
			if (glf != IntPtr.Zero)
				glVertexAttrib1d  = (Call_glVertexAttrib1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1d");
				if (glf != IntPtr.Zero)
					glVertexAttrib1d  = (Call_glVertexAttrib1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1d));
			}

			glf = wglGetProcAddress("glVertexAttrib1dv");
			if (glf != IntPtr.Zero)
				glVertexAttrib1dv  = (Call_glVertexAttrib1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1dv");
				if (glf != IntPtr.Zero)
					glVertexAttrib1dv  = (Call_glVertexAttrib1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1dv));
			}

			glf = wglGetProcAddress("glVertexAttrib1f");
			if (glf != IntPtr.Zero)
				glVertexAttrib1f  = (Call_glVertexAttrib1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1f));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1f");
				if (glf != IntPtr.Zero)
					glVertexAttrib1f  = (Call_glVertexAttrib1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1f));
			}

			glf = wglGetProcAddress("glVertexAttrib1fv");
			if (glf != IntPtr.Zero)
				glVertexAttrib1fv  = (Call_glVertexAttrib1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1fv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1fv");
				if (glf != IntPtr.Zero)
					glVertexAttrib1fv  = (Call_glVertexAttrib1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1fv));
			}

			glf = wglGetProcAddress("glVertexAttrib1s");
			if (glf != IntPtr.Zero)
				glVertexAttrib1s  = (Call_glVertexAttrib1s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1s));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1s");
				if (glf != IntPtr.Zero)
					glVertexAttrib1s  = (Call_glVertexAttrib1s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1s));
			}

			glf = wglGetProcAddress("glVertexAttrib1sv");
			if (glf != IntPtr.Zero)
				glVertexAttrib1sv  = (Call_glVertexAttrib1sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1sv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib1sv");
				if (glf != IntPtr.Zero)
					glVertexAttrib1sv  = (Call_glVertexAttrib1sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib1sv));
			}

			glf = wglGetProcAddress("glVertexAttrib2d");
			if (glf != IntPtr.Zero)
				glVertexAttrib2d  = (Call_glVertexAttrib2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2d");
				if (glf != IntPtr.Zero)
					glVertexAttrib2d  = (Call_glVertexAttrib2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2d));
			}

			glf = wglGetProcAddress("glVertexAttrib2dv");
			if (glf != IntPtr.Zero)
				glVertexAttrib2dv  = (Call_glVertexAttrib2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2dv");
				if (glf != IntPtr.Zero)
					glVertexAttrib2dv  = (Call_glVertexAttrib2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2dv));
			}

			glf = wglGetProcAddress("glVertexAttrib2f");
			if (glf != IntPtr.Zero)
				glVertexAttrib2f  = (Call_glVertexAttrib2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2f));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2f");
				if (glf != IntPtr.Zero)
					glVertexAttrib2f  = (Call_glVertexAttrib2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2f));
			}

			glf = wglGetProcAddress("glVertexAttrib2fv");
			if (glf != IntPtr.Zero)
				glVertexAttrib2fv  = (Call_glVertexAttrib2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2fv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2fv");
				if (glf != IntPtr.Zero)
					glVertexAttrib2fv  = (Call_glVertexAttrib2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2fv));
			}

			glf = wglGetProcAddress("glVertexAttrib2s");
			if (glf != IntPtr.Zero)
				glVertexAttrib2s  = (Call_glVertexAttrib2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2s));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2s");
				if (glf != IntPtr.Zero)
					glVertexAttrib2s  = (Call_glVertexAttrib2s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2s));
			}

			glf = wglGetProcAddress("glVertexAttrib2sv");
			if (glf != IntPtr.Zero)
				glVertexAttrib2sv  = (Call_glVertexAttrib2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2sv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib2sv");
				if (glf != IntPtr.Zero)
					glVertexAttrib2sv  = (Call_glVertexAttrib2sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib2sv));
			}

			glf = wglGetProcAddress("glVertexAttrib3d");
			if (glf != IntPtr.Zero)
				glVertexAttrib3d  = (Call_glVertexAttrib3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3d");
				if (glf != IntPtr.Zero)
					glVertexAttrib3d  = (Call_glVertexAttrib3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3d));
			}

			glf = wglGetProcAddress("glVertexAttrib3dv");
			if (glf != IntPtr.Zero)
				glVertexAttrib3dv  = (Call_glVertexAttrib3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3dv");
				if (glf != IntPtr.Zero)
					glVertexAttrib3dv  = (Call_glVertexAttrib3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3dv));
			}

			glf = wglGetProcAddress("glVertexAttrib3f");
			if (glf != IntPtr.Zero)
				glVertexAttrib3f  = (Call_glVertexAttrib3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3f));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3f");
				if (glf != IntPtr.Zero)
					glVertexAttrib3f  = (Call_glVertexAttrib3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3f));
			}

			glf = wglGetProcAddress("glVertexAttrib3fv");
			if (glf != IntPtr.Zero)
				glVertexAttrib3fv  = (Call_glVertexAttrib3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3fv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3fv");
				if (glf != IntPtr.Zero)
					glVertexAttrib3fv  = (Call_glVertexAttrib3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3fv));
			}

			glf = wglGetProcAddress("glVertexAttrib3s");
			if (glf != IntPtr.Zero)
				glVertexAttrib3s  = (Call_glVertexAttrib3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3s));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3s");
				if (glf != IntPtr.Zero)
					glVertexAttrib3s  = (Call_glVertexAttrib3s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3s));
			}

			glf = wglGetProcAddress("glVertexAttrib3sv");
			if (glf != IntPtr.Zero)
				glVertexAttrib3sv  = (Call_glVertexAttrib3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3sv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib3sv");
				if (glf != IntPtr.Zero)
					glVertexAttrib3sv  = (Call_glVertexAttrib3sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib3sv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nbv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nbv  = (Call_glVertexAttrib4Nbv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nbv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nbv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nbv  = (Call_glVertexAttrib4Nbv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nbv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Niv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Niv  = (Call_glVertexAttrib4Niv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Niv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Niv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Niv  = (Call_glVertexAttrib4Niv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Niv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nsv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nsv  = (Call_glVertexAttrib4Nsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nsv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nsv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nsv  = (Call_glVertexAttrib4Nsv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nsv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nub");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nub  = (Call_glVertexAttrib4Nub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nub));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nub");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nub  = (Call_glVertexAttrib4Nub)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nub));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nubv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nubv  = (Call_glVertexAttrib4Nubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nubv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nubv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nubv  = (Call_glVertexAttrib4Nubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nubv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nuiv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nuiv  = (Call_glVertexAttrib4Nuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nuiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nuiv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nuiv  = (Call_glVertexAttrib4Nuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nuiv));
			}

			glf = wglGetProcAddress("glVertexAttrib4Nusv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4Nusv  = (Call_glVertexAttrib4Nusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nusv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4Nusv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4Nusv  = (Call_glVertexAttrib4Nusv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4Nusv));
			}

			glf = wglGetProcAddress("glVertexAttrib4bv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4bv  = (Call_glVertexAttrib4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4bv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4bv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4bv  = (Call_glVertexAttrib4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4bv));
			}

			glf = wglGetProcAddress("glVertexAttrib4d");
			if (glf != IntPtr.Zero)
				glVertexAttrib4d  = (Call_glVertexAttrib4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4d");
				if (glf != IntPtr.Zero)
					glVertexAttrib4d  = (Call_glVertexAttrib4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4d));
			}

			glf = wglGetProcAddress("glVertexAttrib4dv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4dv  = (Call_glVertexAttrib4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4dv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4dv  = (Call_glVertexAttrib4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4dv));
			}

			glf = wglGetProcAddress("glVertexAttrib4f");
			if (glf != IntPtr.Zero)
				glVertexAttrib4f  = (Call_glVertexAttrib4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4f));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4f");
				if (glf != IntPtr.Zero)
					glVertexAttrib4f  = (Call_glVertexAttrib4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4f));
			}

			glf = wglGetProcAddress("glVertexAttrib4fv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4fv  = (Call_glVertexAttrib4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4fv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4fv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4fv  = (Call_glVertexAttrib4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4fv));
			}

			glf = wglGetProcAddress("glVertexAttrib4iv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4iv  = (Call_glVertexAttrib4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4iv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4iv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4iv  = (Call_glVertexAttrib4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4iv));
			}

			glf = wglGetProcAddress("glVertexAttrib4s");
			if (glf != IntPtr.Zero)
				glVertexAttrib4s  = (Call_glVertexAttrib4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4s));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4s");
				if (glf != IntPtr.Zero)
					glVertexAttrib4s  = (Call_glVertexAttrib4s)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4s));
			}

			glf = wglGetProcAddress("glVertexAttrib4sv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4sv  = (Call_glVertexAttrib4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4sv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4sv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4sv  = (Call_glVertexAttrib4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4sv));
			}

			glf = wglGetProcAddress("glVertexAttrib4ubv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4ubv  = (Call_glVertexAttrib4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4ubv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4ubv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4ubv  = (Call_glVertexAttrib4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4ubv));
			}

			glf = wglGetProcAddress("glVertexAttrib4uiv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4uiv  = (Call_glVertexAttrib4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4uiv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4uiv  = (Call_glVertexAttrib4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4uiv));
			}

			glf = wglGetProcAddress("glVertexAttrib4usv");
			if (glf != IntPtr.Zero)
				glVertexAttrib4usv  = (Call_glVertexAttrib4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4usv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttrib4usv");
				if (glf != IntPtr.Zero)
					glVertexAttrib4usv  = (Call_glVertexAttrib4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttrib4usv));
			}

			glf = wglGetProcAddress("glVertexAttribPointer");
			if (glf != IntPtr.Zero)
				glVertexAttribPointer  = (Call_glVertexAttribPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribPointer));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribPointer");
				if (glf != IntPtr.Zero)
					glVertexAttribPointer  = (Call_glVertexAttribPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribPointer));
			}

			glf = wglGetProcAddress("glUniformMatrix2x3fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2x3fv  = (Call_glUniformMatrix2x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x3fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2x3fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2x3fv  = (Call_glUniformMatrix2x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x3fv));
			}

			glf = wglGetProcAddress("glUniformMatrix3x2fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3x2fv  = (Call_glUniformMatrix3x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x2fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3x2fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3x2fv  = (Call_glUniformMatrix3x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x2fv));
			}

			glf = wglGetProcAddress("glUniformMatrix2x4fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2x4fv  = (Call_glUniformMatrix2x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x4fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2x4fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2x4fv  = (Call_glUniformMatrix2x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x4fv));
			}

			glf = wglGetProcAddress("glUniformMatrix4x2fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4x2fv  = (Call_glUniformMatrix4x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x2fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4x2fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4x2fv  = (Call_glUniformMatrix4x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x2fv));
			}

			glf = wglGetProcAddress("glUniformMatrix3x4fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3x4fv  = (Call_glUniformMatrix3x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x4fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3x4fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3x4fv  = (Call_glUniformMatrix3x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x4fv));
			}

			glf = wglGetProcAddress("glUniformMatrix4x3fv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4x3fv  = (Call_glUniformMatrix4x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x3fv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4x3fv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4x3fv  = (Call_glUniformMatrix4x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x3fv));
			}

			glf = wglGetProcAddress("glColorMaski");
			if (glf != IntPtr.Zero)
				glColorMaski  = (Call_glColorMaski)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMaski));
			else {
				glf = GetProcAddress(pDll,"glColorMaski");
				if (glf != IntPtr.Zero)
					glColorMaski  = (Call_glColorMaski)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glColorMaski));
			}

			glf = wglGetProcAddress("glGetBooleani_v");
			if (glf != IntPtr.Zero)
				glGetBooleani_v  = (Call_glGetBooleani_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBooleani_v));
			else {
				glf = GetProcAddress(pDll,"glGetBooleani_v");
				if (glf != IntPtr.Zero)
					glGetBooleani_v  = (Call_glGetBooleani_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBooleani_v));
			}

			glf = wglGetProcAddress("glGetIntegeri_v");
			if (glf != IntPtr.Zero)
				glGetIntegeri_v  = (Call_glGetIntegeri_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetIntegeri_v));
			else {
				glf = GetProcAddress(pDll,"glGetIntegeri_v");
				if (glf != IntPtr.Zero)
					glGetIntegeri_v  = (Call_glGetIntegeri_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetIntegeri_v));
			}

			glf = wglGetProcAddress("glEnablei");
			if (glf != IntPtr.Zero)
				glEnablei  = (Call_glEnablei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnablei));
			else {
				glf = GetProcAddress(pDll,"glEnablei");
				if (glf != IntPtr.Zero)
					glEnablei  = (Call_glEnablei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnablei));
			}

			glf = wglGetProcAddress("glDisablei");
			if (glf != IntPtr.Zero)
				glDisablei  = (Call_glDisablei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisablei));
			else {
				glf = GetProcAddress(pDll,"glDisablei");
				if (glf != IntPtr.Zero)
					glDisablei  = (Call_glDisablei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisablei));
			}

			glf = wglGetProcAddress("glIsEnabledi");
			if (glf != IntPtr.Zero)
				glIsEnabledi  = (Call_glIsEnabledi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsEnabledi));
			else {
				glf = GetProcAddress(pDll,"glIsEnabledi");
				if (glf != IntPtr.Zero)
					glIsEnabledi  = (Call_glIsEnabledi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsEnabledi));
			}

			glf = wglGetProcAddress("glBeginTransformFeedback");
			if (glf != IntPtr.Zero)
				glBeginTransformFeedback  = (Call_glBeginTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glBeginTransformFeedback");
				if (glf != IntPtr.Zero)
					glBeginTransformFeedback  = (Call_glBeginTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginTransformFeedback));
			}

			glf = wglGetProcAddress("glEndTransformFeedback");
			if (glf != IntPtr.Zero)
				glEndTransformFeedback  = (Call_glEndTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glEndTransformFeedback");
				if (glf != IntPtr.Zero)
					glEndTransformFeedback  = (Call_glEndTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndTransformFeedback));
			}

			glf = wglGetProcAddress("glBindBufferRange");
			if (glf != IntPtr.Zero)
				glBindBufferRange  = (Call_glBindBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBufferRange));
			else {
				glf = GetProcAddress(pDll,"glBindBufferRange");
				if (glf != IntPtr.Zero)
					glBindBufferRange  = (Call_glBindBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBufferRange));
			}

			glf = wglGetProcAddress("glBindBufferBase");
			if (glf != IntPtr.Zero)
				glBindBufferBase  = (Call_glBindBufferBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBufferBase));
			else {
				glf = GetProcAddress(pDll,"glBindBufferBase");
				if (glf != IntPtr.Zero)
					glBindBufferBase  = (Call_glBindBufferBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBufferBase));
			}

			glf = wglGetProcAddress("glTransformFeedbackVaryings");
			if (glf != IntPtr.Zero)
				glTransformFeedbackVaryings  = (Call_glTransformFeedbackVaryings)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackVaryings));
			else {
				glf = GetProcAddress(pDll,"glTransformFeedbackVaryings");
				if (glf != IntPtr.Zero)
					glTransformFeedbackVaryings  = (Call_glTransformFeedbackVaryings)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackVaryings));
			}

			glf = wglGetProcAddress("glGetTransformFeedbackVarying");
			if (glf != IntPtr.Zero)
				glGetTransformFeedbackVarying  = (Call_glGetTransformFeedbackVarying)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbackVarying));
			else {
				glf = GetProcAddress(pDll,"glGetTransformFeedbackVarying");
				if (glf != IntPtr.Zero)
					glGetTransformFeedbackVarying  = (Call_glGetTransformFeedbackVarying)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbackVarying));
			}

			glf = wglGetProcAddress("glClampColor");
			if (glf != IntPtr.Zero)
				glClampColor  = (Call_glClampColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClampColor));
			else {
				glf = GetProcAddress(pDll,"glClampColor");
				if (glf != IntPtr.Zero)
					glClampColor  = (Call_glClampColor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClampColor));
			}

			glf = wglGetProcAddress("glBeginConditionalRender");
			if (glf != IntPtr.Zero)
				glBeginConditionalRender  = (Call_glBeginConditionalRender)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginConditionalRender));
			else {
				glf = GetProcAddress(pDll,"glBeginConditionalRender");
				if (glf != IntPtr.Zero)
					glBeginConditionalRender  = (Call_glBeginConditionalRender)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginConditionalRender));
			}

			glf = wglGetProcAddress("glEndConditionalRender");
			if (glf != IntPtr.Zero)
				glEndConditionalRender  = (Call_glEndConditionalRender)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndConditionalRender));
			else {
				glf = GetProcAddress(pDll,"glEndConditionalRender");
				if (glf != IntPtr.Zero)
					glEndConditionalRender  = (Call_glEndConditionalRender)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndConditionalRender));
			}

			glf = wglGetProcAddress("glVertexAttribIPointer");
			if (glf != IntPtr.Zero)
				glVertexAttribIPointer  = (Call_glVertexAttribIPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribIPointer));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribIPointer");
				if (glf != IntPtr.Zero)
					glVertexAttribIPointer  = (Call_glVertexAttribIPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribIPointer));
			}

			glf = wglGetProcAddress("glGetVertexAttribIiv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribIiv  = (Call_glGetVertexAttribIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribIiv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribIiv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribIiv  = (Call_glGetVertexAttribIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribIiv));
			}

			glf = wglGetProcAddress("glGetVertexAttribIuiv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribIuiv  = (Call_glGetVertexAttribIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribIuiv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribIuiv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribIuiv  = (Call_glGetVertexAttribIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribIuiv));
			}

			glf = wglGetProcAddress("glVertexAttribI1i");
			if (glf != IntPtr.Zero)
				glVertexAttribI1i  = (Call_glVertexAttribI1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1i));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI1i");
				if (glf != IntPtr.Zero)
					glVertexAttribI1i  = (Call_glVertexAttribI1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1i));
			}

			glf = wglGetProcAddress("glVertexAttribI2i");
			if (glf != IntPtr.Zero)
				glVertexAttribI2i  = (Call_glVertexAttribI2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2i));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI2i");
				if (glf != IntPtr.Zero)
					glVertexAttribI2i  = (Call_glVertexAttribI2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2i));
			}

			glf = wglGetProcAddress("glVertexAttribI3i");
			if (glf != IntPtr.Zero)
				glVertexAttribI3i  = (Call_glVertexAttribI3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3i));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI3i");
				if (glf != IntPtr.Zero)
					glVertexAttribI3i  = (Call_glVertexAttribI3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3i));
			}

			glf = wglGetProcAddress("glVertexAttribI4i");
			if (glf != IntPtr.Zero)
				glVertexAttribI4i  = (Call_glVertexAttribI4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4i));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4i");
				if (glf != IntPtr.Zero)
					glVertexAttribI4i  = (Call_glVertexAttribI4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4i));
			}

			glf = wglGetProcAddress("glVertexAttribI1ui");
			if (glf != IntPtr.Zero)
				glVertexAttribI1ui  = (Call_glVertexAttribI1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI1ui");
				if (glf != IntPtr.Zero)
					glVertexAttribI1ui  = (Call_glVertexAttribI1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1ui));
			}

			glf = wglGetProcAddress("glVertexAttribI2ui");
			if (glf != IntPtr.Zero)
				glVertexAttribI2ui  = (Call_glVertexAttribI2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI2ui");
				if (glf != IntPtr.Zero)
					glVertexAttribI2ui  = (Call_glVertexAttribI2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2ui));
			}

			glf = wglGetProcAddress("glVertexAttribI3ui");
			if (glf != IntPtr.Zero)
				glVertexAttribI3ui  = (Call_glVertexAttribI3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI3ui");
				if (glf != IntPtr.Zero)
					glVertexAttribI3ui  = (Call_glVertexAttribI3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3ui));
			}

			glf = wglGetProcAddress("glVertexAttribI4ui");
			if (glf != IntPtr.Zero)
				glVertexAttribI4ui  = (Call_glVertexAttribI4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4ui");
				if (glf != IntPtr.Zero)
					glVertexAttribI4ui  = (Call_glVertexAttribI4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4ui));
			}

			glf = wglGetProcAddress("glVertexAttribI1iv");
			if (glf != IntPtr.Zero)
				glVertexAttribI1iv  = (Call_glVertexAttribI1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1iv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI1iv");
				if (glf != IntPtr.Zero)
					glVertexAttribI1iv  = (Call_glVertexAttribI1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1iv));
			}

			glf = wglGetProcAddress("glVertexAttribI2iv");
			if (glf != IntPtr.Zero)
				glVertexAttribI2iv  = (Call_glVertexAttribI2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2iv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI2iv");
				if (glf != IntPtr.Zero)
					glVertexAttribI2iv  = (Call_glVertexAttribI2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2iv));
			}

			glf = wglGetProcAddress("glVertexAttribI3iv");
			if (glf != IntPtr.Zero)
				glVertexAttribI3iv  = (Call_glVertexAttribI3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3iv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI3iv");
				if (glf != IntPtr.Zero)
					glVertexAttribI3iv  = (Call_glVertexAttribI3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3iv));
			}

			glf = wglGetProcAddress("glVertexAttribI4iv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4iv  = (Call_glVertexAttribI4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4iv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4iv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4iv  = (Call_glVertexAttribI4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4iv));
			}

			glf = wglGetProcAddress("glVertexAttribI1uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribI1uiv  = (Call_glVertexAttribI1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI1uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribI1uiv  = (Call_glVertexAttribI1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI1uiv));
			}

			glf = wglGetProcAddress("glVertexAttribI2uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribI2uiv  = (Call_glVertexAttribI2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI2uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribI2uiv  = (Call_glVertexAttribI2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI2uiv));
			}

			glf = wglGetProcAddress("glVertexAttribI3uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribI3uiv  = (Call_glVertexAttribI3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI3uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribI3uiv  = (Call_glVertexAttribI3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI3uiv));
			}

			glf = wglGetProcAddress("glVertexAttribI4uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4uiv  = (Call_glVertexAttribI4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4uiv  = (Call_glVertexAttribI4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4uiv));
			}

			glf = wglGetProcAddress("glVertexAttribI4bv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4bv  = (Call_glVertexAttribI4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4bv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4bv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4bv  = (Call_glVertexAttribI4bv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4bv));
			}

			glf = wglGetProcAddress("glVertexAttribI4sv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4sv  = (Call_glVertexAttribI4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4sv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4sv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4sv  = (Call_glVertexAttribI4sv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4sv));
			}

			glf = wglGetProcAddress("glVertexAttribI4ubv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4ubv  = (Call_glVertexAttribI4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4ubv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4ubv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4ubv  = (Call_glVertexAttribI4ubv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4ubv));
			}

			glf = wglGetProcAddress("glVertexAttribI4usv");
			if (glf != IntPtr.Zero)
				glVertexAttribI4usv  = (Call_glVertexAttribI4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4usv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribI4usv");
				if (glf != IntPtr.Zero)
					glVertexAttribI4usv  = (Call_glVertexAttribI4usv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribI4usv));
			}

			glf = wglGetProcAddress("glGetUniformuiv");
			if (glf != IntPtr.Zero)
				glGetUniformuiv  = (Call_glGetUniformuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformuiv));
			else {
				glf = GetProcAddress(pDll,"glGetUniformuiv");
				if (glf != IntPtr.Zero)
					glGetUniformuiv  = (Call_glGetUniformuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformuiv));
			}

			glf = wglGetProcAddress("glBindFragDataLocation");
			if (glf != IntPtr.Zero)
				glBindFragDataLocation  = (Call_glBindFragDataLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFragDataLocation));
			else {
				glf = GetProcAddress(pDll,"glBindFragDataLocation");
				if (glf != IntPtr.Zero)
					glBindFragDataLocation  = (Call_glBindFragDataLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFragDataLocation));
			}

			glf = wglGetProcAddress("glGetFragDataLocation");
			if (glf != IntPtr.Zero)
				glGetFragDataLocation  = (Call_glGetFragDataLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFragDataLocation));
			else {
				glf = GetProcAddress(pDll,"glGetFragDataLocation");
				if (glf != IntPtr.Zero)
					glGetFragDataLocation  = (Call_glGetFragDataLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFragDataLocation));
			}

			glf = wglGetProcAddress("glUniform1ui");
			if (glf != IntPtr.Zero)
				glUniform1ui  = (Call_glUniform1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1ui));
			else {
				glf = GetProcAddress(pDll,"glUniform1ui");
				if (glf != IntPtr.Zero)
					glUniform1ui  = (Call_glUniform1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1ui));
			}

			glf = wglGetProcAddress("glUniform2ui");
			if (glf != IntPtr.Zero)
				glUniform2ui  = (Call_glUniform2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2ui));
			else {
				glf = GetProcAddress(pDll,"glUniform2ui");
				if (glf != IntPtr.Zero)
					glUniform2ui  = (Call_glUniform2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2ui));
			}

			glf = wglGetProcAddress("glUniform3ui");
			if (glf != IntPtr.Zero)
				glUniform3ui  = (Call_glUniform3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3ui));
			else {
				glf = GetProcAddress(pDll,"glUniform3ui");
				if (glf != IntPtr.Zero)
					glUniform3ui  = (Call_glUniform3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3ui));
			}

			glf = wglGetProcAddress("glUniform4ui");
			if (glf != IntPtr.Zero)
				glUniform4ui  = (Call_glUniform4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4ui));
			else {
				glf = GetProcAddress(pDll,"glUniform4ui");
				if (glf != IntPtr.Zero)
					glUniform4ui  = (Call_glUniform4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4ui));
			}

			glf = wglGetProcAddress("glUniform1uiv");
			if (glf != IntPtr.Zero)
				glUniform1uiv  = (Call_glUniform1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1uiv));
			else {
				glf = GetProcAddress(pDll,"glUniform1uiv");
				if (glf != IntPtr.Zero)
					glUniform1uiv  = (Call_glUniform1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1uiv));
			}

			glf = wglGetProcAddress("glUniform2uiv");
			if (glf != IntPtr.Zero)
				glUniform2uiv  = (Call_glUniform2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2uiv));
			else {
				glf = GetProcAddress(pDll,"glUniform2uiv");
				if (glf != IntPtr.Zero)
					glUniform2uiv  = (Call_glUniform2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2uiv));
			}

			glf = wglGetProcAddress("glUniform3uiv");
			if (glf != IntPtr.Zero)
				glUniform3uiv  = (Call_glUniform3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3uiv));
			else {
				glf = GetProcAddress(pDll,"glUniform3uiv");
				if (glf != IntPtr.Zero)
					glUniform3uiv  = (Call_glUniform3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3uiv));
			}

			glf = wglGetProcAddress("glUniform4uiv");
			if (glf != IntPtr.Zero)
				glUniform4uiv  = (Call_glUniform4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4uiv));
			else {
				glf = GetProcAddress(pDll,"glUniform4uiv");
				if (glf != IntPtr.Zero)
					glUniform4uiv  = (Call_glUniform4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4uiv));
			}

			glf = wglGetProcAddress("glTexParameterIiv");
			if (glf != IntPtr.Zero)
				glTexParameterIiv  = (Call_glTexParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glTexParameterIiv");
				if (glf != IntPtr.Zero)
					glTexParameterIiv  = (Call_glTexParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterIiv));
			}

			glf = wglGetProcAddress("glTexParameterIuiv");
			if (glf != IntPtr.Zero)
				glTexParameterIuiv  = (Call_glTexParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glTexParameterIuiv");
				if (glf != IntPtr.Zero)
					glTexParameterIuiv  = (Call_glTexParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexParameterIuiv));
			}

			glf = wglGetProcAddress("glGetTexParameterIiv");
			if (glf != IntPtr.Zero)
				glGetTexParameterIiv  = (Call_glGetTexParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glGetTexParameterIiv");
				if (glf != IntPtr.Zero)
					glGetTexParameterIiv  = (Call_glGetTexParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterIiv));
			}

			glf = wglGetProcAddress("glGetTexParameterIuiv");
			if (glf != IntPtr.Zero)
				glGetTexParameterIuiv  = (Call_glGetTexParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glGetTexParameterIuiv");
				if (glf != IntPtr.Zero)
					glGetTexParameterIuiv  = (Call_glGetTexParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTexParameterIuiv));
			}

			glf = wglGetProcAddress("glClearBufferiv");
			if (glf != IntPtr.Zero)
				glClearBufferiv  = (Call_glClearBufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferiv));
			else {
				glf = GetProcAddress(pDll,"glClearBufferiv");
				if (glf != IntPtr.Zero)
					glClearBufferiv  = (Call_glClearBufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferiv));
			}

			glf = wglGetProcAddress("glClearBufferuiv");
			if (glf != IntPtr.Zero)
				glClearBufferuiv  = (Call_glClearBufferuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferuiv));
			else {
				glf = GetProcAddress(pDll,"glClearBufferuiv");
				if (glf != IntPtr.Zero)
					glClearBufferuiv  = (Call_glClearBufferuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferuiv));
			}

			glf = wglGetProcAddress("glClearBufferfv");
			if (glf != IntPtr.Zero)
				glClearBufferfv  = (Call_glClearBufferfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferfv));
			else {
				glf = GetProcAddress(pDll,"glClearBufferfv");
				if (glf != IntPtr.Zero)
					glClearBufferfv  = (Call_glClearBufferfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferfv));
			}

			glf = wglGetProcAddress("glClearBufferfi");
			if (glf != IntPtr.Zero)
				glClearBufferfi  = (Call_glClearBufferfi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferfi));
			else {
				glf = GetProcAddress(pDll,"glClearBufferfi");
				if (glf != IntPtr.Zero)
					glClearBufferfi  = (Call_glClearBufferfi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferfi));
			}

			glf = wglGetProcAddress("glGetStringi");
			if (glf != IntPtr.Zero)
				glGetStringi  = (Call_glGetStringi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetStringi));
			else {
				glf = GetProcAddress(pDll,"glGetStringi");
				if (glf != IntPtr.Zero)
					glGetStringi  = (Call_glGetStringi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetStringi));
			}

			glf = wglGetProcAddress("glIsRenderbuffer");
			if (glf != IntPtr.Zero)
				glIsRenderbuffer  = (Call_glIsRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsRenderbuffer));
			else {
				glf = GetProcAddress(pDll,"glIsRenderbuffer");
				if (glf != IntPtr.Zero)
					glIsRenderbuffer  = (Call_glIsRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsRenderbuffer));
			}

			glf = wglGetProcAddress("glBindRenderbuffer");
			if (glf != IntPtr.Zero)
				glBindRenderbuffer  = (Call_glBindRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindRenderbuffer));
			else {
				glf = GetProcAddress(pDll,"glBindRenderbuffer");
				if (glf != IntPtr.Zero)
					glBindRenderbuffer  = (Call_glBindRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindRenderbuffer));
			}

			glf = wglGetProcAddress("glDeleteRenderbuffers");
			if (glf != IntPtr.Zero)
				glDeleteRenderbuffers  = (Call_glDeleteRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteRenderbuffers));
			else {
				glf = GetProcAddress(pDll,"glDeleteRenderbuffers");
				if (glf != IntPtr.Zero)
					glDeleteRenderbuffers  = (Call_glDeleteRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteRenderbuffers));
			}

			glf = wglGetProcAddress("glGenRenderbuffers");
			if (glf != IntPtr.Zero)
				glGenRenderbuffers  = (Call_glGenRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenRenderbuffers));
			else {
				glf = GetProcAddress(pDll,"glGenRenderbuffers");
				if (glf != IntPtr.Zero)
					glGenRenderbuffers  = (Call_glGenRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenRenderbuffers));
			}

			glf = wglGetProcAddress("glRenderbufferStorage");
			if (glf != IntPtr.Zero)
				glRenderbufferStorage  = (Call_glRenderbufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderbufferStorage));
			else {
				glf = GetProcAddress(pDll,"glRenderbufferStorage");
				if (glf != IntPtr.Zero)
					glRenderbufferStorage  = (Call_glRenderbufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderbufferStorage));
			}

			glf = wglGetProcAddress("glGetRenderbufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetRenderbufferParameteriv  = (Call_glGetRenderbufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetRenderbufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetRenderbufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetRenderbufferParameteriv  = (Call_glGetRenderbufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetRenderbufferParameteriv));
			}

			glf = wglGetProcAddress("glIsFramebuffer");
			if (glf != IntPtr.Zero)
				glIsFramebuffer  = (Call_glIsFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glIsFramebuffer");
				if (glf != IntPtr.Zero)
					glIsFramebuffer  = (Call_glIsFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsFramebuffer));
			}

			glf = wglGetProcAddress("glBindFramebuffer");
			if (glf != IntPtr.Zero)
				glBindFramebuffer  = (Call_glBindFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glBindFramebuffer");
				if (glf != IntPtr.Zero)
					glBindFramebuffer  = (Call_glBindFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFramebuffer));
			}

			glf = wglGetProcAddress("glDeleteFramebuffers");
			if (glf != IntPtr.Zero)
				glDeleteFramebuffers  = (Call_glDeleteFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteFramebuffers));
			else {
				glf = GetProcAddress(pDll,"glDeleteFramebuffers");
				if (glf != IntPtr.Zero)
					glDeleteFramebuffers  = (Call_glDeleteFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteFramebuffers));
			}

			glf = wglGetProcAddress("glGenFramebuffers");
			if (glf != IntPtr.Zero)
				glGenFramebuffers  = (Call_glGenFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenFramebuffers));
			else {
				glf = GetProcAddress(pDll,"glGenFramebuffers");
				if (glf != IntPtr.Zero)
					glGenFramebuffers  = (Call_glGenFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenFramebuffers));
			}

			glf = wglGetProcAddress("glCheckFramebufferStatus");
			if (glf != IntPtr.Zero)
				glCheckFramebufferStatus  = (Call_glCheckFramebufferStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCheckFramebufferStatus));
			else {
				glf = GetProcAddress(pDll,"glCheckFramebufferStatus");
				if (glf != IntPtr.Zero)
					glCheckFramebufferStatus  = (Call_glCheckFramebufferStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCheckFramebufferStatus));
			}

			glf = wglGetProcAddress("glFramebufferTexture1D");
			if (glf != IntPtr.Zero)
				glFramebufferTexture1D  = (Call_glFramebufferTexture1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture1D));
			else {
				glf = GetProcAddress(pDll,"glFramebufferTexture1D");
				if (glf != IntPtr.Zero)
					glFramebufferTexture1D  = (Call_glFramebufferTexture1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture1D));
			}

			glf = wglGetProcAddress("glFramebufferTexture2D");
			if (glf != IntPtr.Zero)
				glFramebufferTexture2D  = (Call_glFramebufferTexture2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture2D));
			else {
				glf = GetProcAddress(pDll,"glFramebufferTexture2D");
				if (glf != IntPtr.Zero)
					glFramebufferTexture2D  = (Call_glFramebufferTexture2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture2D));
			}

			glf = wglGetProcAddress("glFramebufferTexture3D");
			if (glf != IntPtr.Zero)
				glFramebufferTexture3D  = (Call_glFramebufferTexture3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture3D));
			else {
				glf = GetProcAddress(pDll,"glFramebufferTexture3D");
				if (glf != IntPtr.Zero)
					glFramebufferTexture3D  = (Call_glFramebufferTexture3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture3D));
			}

			glf = wglGetProcAddress("glFramebufferRenderbuffer");
			if (glf != IntPtr.Zero)
				glFramebufferRenderbuffer  = (Call_glFramebufferRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferRenderbuffer));
			else {
				glf = GetProcAddress(pDll,"glFramebufferRenderbuffer");
				if (glf != IntPtr.Zero)
					glFramebufferRenderbuffer  = (Call_glFramebufferRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferRenderbuffer));
			}

			glf = wglGetProcAddress("glGetFramebufferAttachmentParameteriv");
			if (glf != IntPtr.Zero)
				glGetFramebufferAttachmentParameteriv  = (Call_glGetFramebufferAttachmentParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFramebufferAttachmentParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetFramebufferAttachmentParameteriv");
				if (glf != IntPtr.Zero)
					glGetFramebufferAttachmentParameteriv  = (Call_glGetFramebufferAttachmentParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFramebufferAttachmentParameteriv));
			}

			glf = wglGetProcAddress("glGenerateMipmap");
			if (glf != IntPtr.Zero)
				glGenerateMipmap  = (Call_glGenerateMipmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenerateMipmap));
			else {
				glf = GetProcAddress(pDll,"glGenerateMipmap");
				if (glf != IntPtr.Zero)
					glGenerateMipmap  = (Call_glGenerateMipmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenerateMipmap));
			}

			glf = wglGetProcAddress("glBlitFramebuffer");
			if (glf != IntPtr.Zero)
				glBlitFramebuffer  = (Call_glBlitFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlitFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glBlitFramebuffer");
				if (glf != IntPtr.Zero)
					glBlitFramebuffer  = (Call_glBlitFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlitFramebuffer));
			}

			glf = wglGetProcAddress("glRenderbufferStorageMultisample");
			if (glf != IntPtr.Zero)
				glRenderbufferStorageMultisample  = (Call_glRenderbufferStorageMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderbufferStorageMultisample));
			else {
				glf = GetProcAddress(pDll,"glRenderbufferStorageMultisample");
				if (glf != IntPtr.Zero)
					glRenderbufferStorageMultisample  = (Call_glRenderbufferStorageMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glRenderbufferStorageMultisample));
			}

			glf = wglGetProcAddress("glFramebufferTextureLayer");
			if (glf != IntPtr.Zero)
				glFramebufferTextureLayer  = (Call_glFramebufferTextureLayer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTextureLayer));
			else {
				glf = GetProcAddress(pDll,"glFramebufferTextureLayer");
				if (glf != IntPtr.Zero)
					glFramebufferTextureLayer  = (Call_glFramebufferTextureLayer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTextureLayer));
			}

			glf = wglGetProcAddress("glMapBufferRange");
			if (glf != IntPtr.Zero)
				glMapBufferRange  = (Call_glMapBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapBufferRange));
			else {
				glf = GetProcAddress(pDll,"glMapBufferRange");
				if (glf != IntPtr.Zero)
					glMapBufferRange  = (Call_glMapBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapBufferRange));
			}

			glf = wglGetProcAddress("glFlushMappedBufferRange");
			if (glf != IntPtr.Zero)
				glFlushMappedBufferRange  = (Call_glFlushMappedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlushMappedBufferRange));
			else {
				glf = GetProcAddress(pDll,"glFlushMappedBufferRange");
				if (glf != IntPtr.Zero)
					glFlushMappedBufferRange  = (Call_glFlushMappedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlushMappedBufferRange));
			}

			glf = wglGetProcAddress("glBindVertexArray");
			if (glf != IntPtr.Zero)
				glBindVertexArray  = (Call_glBindVertexArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexArray));
			else {
				glf = GetProcAddress(pDll,"glBindVertexArray");
				if (glf != IntPtr.Zero)
					glBindVertexArray  = (Call_glBindVertexArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexArray));
			}

			glf = wglGetProcAddress("glDeleteVertexArrays");
			if (glf != IntPtr.Zero)
				glDeleteVertexArrays  = (Call_glDeleteVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteVertexArrays));
			else {
				glf = GetProcAddress(pDll,"glDeleteVertexArrays");
				if (glf != IntPtr.Zero)
					glDeleteVertexArrays  = (Call_glDeleteVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteVertexArrays));
			}

			glf = wglGetProcAddress("glGenVertexArrays");
			if (glf != IntPtr.Zero)
				glGenVertexArrays  = (Call_glGenVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenVertexArrays));
			else {
				glf = GetProcAddress(pDll,"glGenVertexArrays");
				if (glf != IntPtr.Zero)
					glGenVertexArrays  = (Call_glGenVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenVertexArrays));
			}

			glf = wglGetProcAddress("glIsVertexArray");
			if (glf != IntPtr.Zero)
				glIsVertexArray  = (Call_glIsVertexArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsVertexArray));
			else {
				glf = GetProcAddress(pDll,"glIsVertexArray");
				if (glf != IntPtr.Zero)
					glIsVertexArray  = (Call_glIsVertexArray)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsVertexArray));
			}

			glf = wglGetProcAddress("glDrawArraysInstanced");
			if (glf != IntPtr.Zero)
				glDrawArraysInstanced  = (Call_glDrawArraysInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysInstanced));
			else {
				glf = GetProcAddress(pDll,"glDrawArraysInstanced");
				if (glf != IntPtr.Zero)
					glDrawArraysInstanced  = (Call_glDrawArraysInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysInstanced));
			}

			glf = wglGetProcAddress("glDrawElementsInstanced");
			if (glf != IntPtr.Zero)
				glDrawElementsInstanced  = (Call_glDrawElementsInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstanced));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsInstanced");
				if (glf != IntPtr.Zero)
					glDrawElementsInstanced  = (Call_glDrawElementsInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstanced));
			}

			glf = wglGetProcAddress("glTexBuffer");
			if (glf != IntPtr.Zero)
				glTexBuffer  = (Call_glTexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexBuffer));
			else {
				glf = GetProcAddress(pDll,"glTexBuffer");
				if (glf != IntPtr.Zero)
					glTexBuffer  = (Call_glTexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexBuffer));
			}

			glf = wglGetProcAddress("glPrimitiveRestartIndex");
			if (glf != IntPtr.Zero)
				glPrimitiveRestartIndex  = (Call_glPrimitiveRestartIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPrimitiveRestartIndex));
			else {
				glf = GetProcAddress(pDll,"glPrimitiveRestartIndex");
				if (glf != IntPtr.Zero)
					glPrimitiveRestartIndex  = (Call_glPrimitiveRestartIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPrimitiveRestartIndex));
			}

			glf = wglGetProcAddress("glCopyBufferSubData");
			if (glf != IntPtr.Zero)
				glCopyBufferSubData  = (Call_glCopyBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glCopyBufferSubData");
				if (glf != IntPtr.Zero)
					glCopyBufferSubData  = (Call_glCopyBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyBufferSubData));
			}

			glf = wglGetProcAddress("glGetUniformIndices");
			if (glf != IntPtr.Zero)
				glGetUniformIndices  = (Call_glGetUniformIndices)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformIndices));
			else {
				glf = GetProcAddress(pDll,"glGetUniformIndices");
				if (glf != IntPtr.Zero)
					glGetUniformIndices  = (Call_glGetUniformIndices)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformIndices));
			}

			glf = wglGetProcAddress("glGetActiveUniformsiv");
			if (glf != IntPtr.Zero)
				glGetActiveUniformsiv  = (Call_glGetActiveUniformsiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformsiv));
			else {
				glf = GetProcAddress(pDll,"glGetActiveUniformsiv");
				if (glf != IntPtr.Zero)
					glGetActiveUniformsiv  = (Call_glGetActiveUniformsiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformsiv));
			}

			glf = wglGetProcAddress("glGetActiveUniformName");
			if (glf != IntPtr.Zero)
				glGetActiveUniformName  = (Call_glGetActiveUniformName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformName));
			else {
				glf = GetProcAddress(pDll,"glGetActiveUniformName");
				if (glf != IntPtr.Zero)
					glGetActiveUniformName  = (Call_glGetActiveUniformName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformName));
			}

			glf = wglGetProcAddress("glGetUniformBlockIndex");
			if (glf != IntPtr.Zero)
				glGetUniformBlockIndex  = (Call_glGetUniformBlockIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformBlockIndex));
			else {
				glf = GetProcAddress(pDll,"glGetUniformBlockIndex");
				if (glf != IntPtr.Zero)
					glGetUniformBlockIndex  = (Call_glGetUniformBlockIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformBlockIndex));
			}

			glf = wglGetProcAddress("glGetActiveUniformBlockiv");
			if (glf != IntPtr.Zero)
				glGetActiveUniformBlockiv  = (Call_glGetActiveUniformBlockiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformBlockiv));
			else {
				glf = GetProcAddress(pDll,"glGetActiveUniformBlockiv");
				if (glf != IntPtr.Zero)
					glGetActiveUniformBlockiv  = (Call_glGetActiveUniformBlockiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformBlockiv));
			}

			glf = wglGetProcAddress("glGetActiveUniformBlockName");
			if (glf != IntPtr.Zero)
				glGetActiveUniformBlockName  = (Call_glGetActiveUniformBlockName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformBlockName));
			else {
				glf = GetProcAddress(pDll,"glGetActiveUniformBlockName");
				if (glf != IntPtr.Zero)
					glGetActiveUniformBlockName  = (Call_glGetActiveUniformBlockName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveUniformBlockName));
			}

			glf = wglGetProcAddress("glUniformBlockBinding");
			if (glf != IntPtr.Zero)
				glUniformBlockBinding  = (Call_glUniformBlockBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformBlockBinding));
			else {
				glf = GetProcAddress(pDll,"glUniformBlockBinding");
				if (glf != IntPtr.Zero)
					glUniformBlockBinding  = (Call_glUniformBlockBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformBlockBinding));
			}

			glf = wglGetProcAddress("glDrawElementsBaseVertex");
			if (glf != IntPtr.Zero)
				glDrawElementsBaseVertex  = (Call_glDrawElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsBaseVertex));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsBaseVertex");
				if (glf != IntPtr.Zero)
					glDrawElementsBaseVertex  = (Call_glDrawElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsBaseVertex));
			}

			glf = wglGetProcAddress("glDrawRangeElementsBaseVertex");
			if (glf != IntPtr.Zero)
				glDrawRangeElementsBaseVertex  = (Call_glDrawRangeElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawRangeElementsBaseVertex));
			else {
				glf = GetProcAddress(pDll,"glDrawRangeElementsBaseVertex");
				if (glf != IntPtr.Zero)
					glDrawRangeElementsBaseVertex  = (Call_glDrawRangeElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawRangeElementsBaseVertex));
			}

			glf = wglGetProcAddress("glDrawElementsInstancedBaseVertex");
			if (glf != IntPtr.Zero)
				glDrawElementsInstancedBaseVertex  = (Call_glDrawElementsInstancedBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseVertex));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsInstancedBaseVertex");
				if (glf != IntPtr.Zero)
					glDrawElementsInstancedBaseVertex  = (Call_glDrawElementsInstancedBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseVertex));
			}

			glf = wglGetProcAddress("glMultiDrawElementsBaseVertex");
			if (glf != IntPtr.Zero)
				glMultiDrawElementsBaseVertex  = (Call_glMultiDrawElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsBaseVertex));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawElementsBaseVertex");
				if (glf != IntPtr.Zero)
					glMultiDrawElementsBaseVertex  = (Call_glMultiDrawElementsBaseVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsBaseVertex));
			}

			glf = wglGetProcAddress("glProvokingVertex");
			if (glf != IntPtr.Zero)
				glProvokingVertex  = (Call_glProvokingVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProvokingVertex));
			else {
				glf = GetProcAddress(pDll,"glProvokingVertex");
				if (glf != IntPtr.Zero)
					glProvokingVertex  = (Call_glProvokingVertex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProvokingVertex));
			}

			glf = wglGetProcAddress("glFenceSync");
			if (glf != IntPtr.Zero)
				glFenceSync  = (Call_glFenceSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFenceSync));
			else {
				glf = GetProcAddress(pDll,"glFenceSync");
				if (glf != IntPtr.Zero)
					glFenceSync  = (Call_glFenceSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFenceSync));
			}

			glf = wglGetProcAddress("glIsSync");
			if (glf != IntPtr.Zero)
				glIsSync  = (Call_glIsSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsSync));
			else {
				glf = GetProcAddress(pDll,"glIsSync");
				if (glf != IntPtr.Zero)
					glIsSync  = (Call_glIsSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsSync));
			}

			glf = wglGetProcAddress("glDeleteSync");
			if (glf != IntPtr.Zero)
				glDeleteSync  = (Call_glDeleteSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteSync));
			else {
				glf = GetProcAddress(pDll,"glDeleteSync");
				if (glf != IntPtr.Zero)
					glDeleteSync  = (Call_glDeleteSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteSync));
			}

			glf = wglGetProcAddress("glClientWaitSync");
			if (glf != IntPtr.Zero)
				glClientWaitSync  = (Call_glClientWaitSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClientWaitSync));
			else {
				glf = GetProcAddress(pDll,"glClientWaitSync");
				if (glf != IntPtr.Zero)
					glClientWaitSync  = (Call_glClientWaitSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClientWaitSync));
			}

			glf = wglGetProcAddress("glWaitSync");
			if (glf != IntPtr.Zero)
				glWaitSync  = (Call_glWaitSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glWaitSync));
			else {
				glf = GetProcAddress(pDll,"glWaitSync");
				if (glf != IntPtr.Zero)
					glWaitSync  = (Call_glWaitSync)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glWaitSync));
			}

			glf = wglGetProcAddress("glGetInteger64v");
			if (glf != IntPtr.Zero)
				glGetInteger64v  = (Call_glGetInteger64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInteger64v));
			else {
				glf = GetProcAddress(pDll,"glGetInteger64v");
				if (glf != IntPtr.Zero)
					glGetInteger64v  = (Call_glGetInteger64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInteger64v));
			}

			glf = wglGetProcAddress("glGetSynciv");
			if (glf != IntPtr.Zero)
				glGetSynciv  = (Call_glGetSynciv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSynciv));
			else {
				glf = GetProcAddress(pDll,"glGetSynciv");
				if (glf != IntPtr.Zero)
					glGetSynciv  = (Call_glGetSynciv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSynciv));
			}

			glf = wglGetProcAddress("glGetInteger64i_v");
			if (glf != IntPtr.Zero)
				glGetInteger64i_v  = (Call_glGetInteger64i_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInteger64i_v));
			else {
				glf = GetProcAddress(pDll,"glGetInteger64i_v");
				if (glf != IntPtr.Zero)
					glGetInteger64i_v  = (Call_glGetInteger64i_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInteger64i_v));
			}

			glf = wglGetProcAddress("glGetBufferParameteri64v");
			if (glf != IntPtr.Zero)
				glGetBufferParameteri64v  = (Call_glGetBufferParameteri64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferParameteri64v));
			else {
				glf = GetProcAddress(pDll,"glGetBufferParameteri64v");
				if (glf != IntPtr.Zero)
					glGetBufferParameteri64v  = (Call_glGetBufferParameteri64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetBufferParameteri64v));
			}

			glf = wglGetProcAddress("glFramebufferTexture");
			if (glf != IntPtr.Zero)
				glFramebufferTexture  = (Call_glFramebufferTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture));
			else {
				glf = GetProcAddress(pDll,"glFramebufferTexture");
				if (glf != IntPtr.Zero)
					glFramebufferTexture  = (Call_glFramebufferTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferTexture));
			}

			glf = wglGetProcAddress("glTexImage2DMultisample");
			if (glf != IntPtr.Zero)
				glTexImage2DMultisample  = (Call_glTexImage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage2DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTexImage2DMultisample");
				if (glf != IntPtr.Zero)
					glTexImage2DMultisample  = (Call_glTexImage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage2DMultisample));
			}

			glf = wglGetProcAddress("glTexImage3DMultisample");
			if (glf != IntPtr.Zero)
				glTexImage3DMultisample  = (Call_glTexImage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage3DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTexImage3DMultisample");
				if (glf != IntPtr.Zero)
					glTexImage3DMultisample  = (Call_glTexImage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexImage3DMultisample));
			}

			glf = wglGetProcAddress("glGetMultisamplefv");
			if (glf != IntPtr.Zero)
				glGetMultisamplefv  = (Call_glGetMultisamplefv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMultisamplefv));
			else {
				glf = GetProcAddress(pDll,"glGetMultisamplefv");
				if (glf != IntPtr.Zero)
					glGetMultisamplefv  = (Call_glGetMultisamplefv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetMultisamplefv));
			}

			glf = wglGetProcAddress("glSampleMaski");
			if (glf != IntPtr.Zero)
				glSampleMaski  = (Call_glSampleMaski)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSampleMaski));
			else {
				glf = GetProcAddress(pDll,"glSampleMaski");
				if (glf != IntPtr.Zero)
					glSampleMaski  = (Call_glSampleMaski)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSampleMaski));
			}

			glf = wglGetProcAddress("glBindFragDataLocationIndexed");
			if (glf != IntPtr.Zero)
				glBindFragDataLocationIndexed  = (Call_glBindFragDataLocationIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFragDataLocationIndexed));
			else {
				glf = GetProcAddress(pDll,"glBindFragDataLocationIndexed");
				if (glf != IntPtr.Zero)
					glBindFragDataLocationIndexed  = (Call_glBindFragDataLocationIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindFragDataLocationIndexed));
			}

			glf = wglGetProcAddress("glGetFragDataIndex");
			if (glf != IntPtr.Zero)
				glGetFragDataIndex  = (Call_glGetFragDataIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFragDataIndex));
			else {
				glf = GetProcAddress(pDll,"glGetFragDataIndex");
				if (glf != IntPtr.Zero)
					glGetFragDataIndex  = (Call_glGetFragDataIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFragDataIndex));
			}

			glf = wglGetProcAddress("glGenSamplers");
			if (glf != IntPtr.Zero)
				glGenSamplers  = (Call_glGenSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenSamplers));
			else {
				glf = GetProcAddress(pDll,"glGenSamplers");
				if (glf != IntPtr.Zero)
					glGenSamplers  = (Call_glGenSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenSamplers));
			}

			glf = wglGetProcAddress("glDeleteSamplers");
			if (glf != IntPtr.Zero)
				glDeleteSamplers  = (Call_glDeleteSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteSamplers));
			else {
				glf = GetProcAddress(pDll,"glDeleteSamplers");
				if (glf != IntPtr.Zero)
					glDeleteSamplers  = (Call_glDeleteSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteSamplers));
			}

			glf = wglGetProcAddress("glIsSampler");
			if (glf != IntPtr.Zero)
				glIsSampler  = (Call_glIsSampler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsSampler));
			else {
				glf = GetProcAddress(pDll,"glIsSampler");
				if (glf != IntPtr.Zero)
					glIsSampler  = (Call_glIsSampler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsSampler));
			}

			glf = wglGetProcAddress("glBindSampler");
			if (glf != IntPtr.Zero)
				glBindSampler  = (Call_glBindSampler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindSampler));
			else {
				glf = GetProcAddress(pDll,"glBindSampler");
				if (glf != IntPtr.Zero)
					glBindSampler  = (Call_glBindSampler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindSampler));
			}

			glf = wglGetProcAddress("glSamplerParameteri");
			if (glf != IntPtr.Zero)
				glSamplerParameteri  = (Call_glSamplerParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameteri));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameteri");
				if (glf != IntPtr.Zero)
					glSamplerParameteri  = (Call_glSamplerParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameteri));
			}

			glf = wglGetProcAddress("glSamplerParameteriv");
			if (glf != IntPtr.Zero)
				glSamplerParameteriv  = (Call_glSamplerParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameteriv));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameteriv");
				if (glf != IntPtr.Zero)
					glSamplerParameteriv  = (Call_glSamplerParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameteriv));
			}

			glf = wglGetProcAddress("glSamplerParameterf");
			if (glf != IntPtr.Zero)
				glSamplerParameterf  = (Call_glSamplerParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterf));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameterf");
				if (glf != IntPtr.Zero)
					glSamplerParameterf  = (Call_glSamplerParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterf));
			}

			glf = wglGetProcAddress("glSamplerParameterfv");
			if (glf != IntPtr.Zero)
				glSamplerParameterfv  = (Call_glSamplerParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterfv));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameterfv");
				if (glf != IntPtr.Zero)
					glSamplerParameterfv  = (Call_glSamplerParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterfv));
			}

			glf = wglGetProcAddress("glSamplerParameterIiv");
			if (glf != IntPtr.Zero)
				glSamplerParameterIiv  = (Call_glSamplerParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameterIiv");
				if (glf != IntPtr.Zero)
					glSamplerParameterIiv  = (Call_glSamplerParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterIiv));
			}

			glf = wglGetProcAddress("glSamplerParameterIuiv");
			if (glf != IntPtr.Zero)
				glSamplerParameterIuiv  = (Call_glSamplerParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glSamplerParameterIuiv");
				if (glf != IntPtr.Zero)
					glSamplerParameterIuiv  = (Call_glSamplerParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSamplerParameterIuiv));
			}

			glf = wglGetProcAddress("glGetSamplerParameteriv");
			if (glf != IntPtr.Zero)
				glGetSamplerParameteriv  = (Call_glGetSamplerParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetSamplerParameteriv");
				if (glf != IntPtr.Zero)
					glGetSamplerParameteriv  = (Call_glGetSamplerParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameteriv));
			}

			glf = wglGetProcAddress("glGetSamplerParameterIiv");
			if (glf != IntPtr.Zero)
				glGetSamplerParameterIiv  = (Call_glGetSamplerParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glGetSamplerParameterIiv");
				if (glf != IntPtr.Zero)
					glGetSamplerParameterIiv  = (Call_glGetSamplerParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterIiv));
			}

			glf = wglGetProcAddress("glGetSamplerParameterfv");
			if (glf != IntPtr.Zero)
				glGetSamplerParameterfv  = (Call_glGetSamplerParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterfv));
			else {
				glf = GetProcAddress(pDll,"glGetSamplerParameterfv");
				if (glf != IntPtr.Zero)
					glGetSamplerParameterfv  = (Call_glGetSamplerParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterfv));
			}

			glf = wglGetProcAddress("glGetSamplerParameterIuiv");
			if (glf != IntPtr.Zero)
				glGetSamplerParameterIuiv  = (Call_glGetSamplerParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glGetSamplerParameterIuiv");
				if (glf != IntPtr.Zero)
					glGetSamplerParameterIuiv  = (Call_glGetSamplerParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSamplerParameterIuiv));
			}

			glf = wglGetProcAddress("glQueryCounter");
			if (glf != IntPtr.Zero)
				glQueryCounter  = (Call_glQueryCounter)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glQueryCounter));
			else {
				glf = GetProcAddress(pDll,"glQueryCounter");
				if (glf != IntPtr.Zero)
					glQueryCounter  = (Call_glQueryCounter)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glQueryCounter));
			}

			glf = wglGetProcAddress("glGetQueryObjecti64v");
			if (glf != IntPtr.Zero)
				glGetQueryObjecti64v  = (Call_glGetQueryObjecti64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjecti64v));
			else {
				glf = GetProcAddress(pDll,"glGetQueryObjecti64v");
				if (glf != IntPtr.Zero)
					glGetQueryObjecti64v  = (Call_glGetQueryObjecti64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjecti64v));
			}

			glf = wglGetProcAddress("glGetQueryObjectui64v");
			if (glf != IntPtr.Zero)
				glGetQueryObjectui64v  = (Call_glGetQueryObjectui64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectui64v));
			else {
				glf = GetProcAddress(pDll,"glGetQueryObjectui64v");
				if (glf != IntPtr.Zero)
					glGetQueryObjectui64v  = (Call_glGetQueryObjectui64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryObjectui64v));
			}

			glf = wglGetProcAddress("glVertexAttribDivisor");
			if (glf != IntPtr.Zero)
				glVertexAttribDivisor  = (Call_glVertexAttribDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribDivisor));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribDivisor");
				if (glf != IntPtr.Zero)
					glVertexAttribDivisor  = (Call_glVertexAttribDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribDivisor));
			}

			glf = wglGetProcAddress("glVertexAttribP1ui");
			if (glf != IntPtr.Zero)
				glVertexAttribP1ui  = (Call_glVertexAttribP1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP1ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP1ui");
				if (glf != IntPtr.Zero)
					glVertexAttribP1ui  = (Call_glVertexAttribP1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP1ui));
			}

			glf = wglGetProcAddress("glVertexAttribP1uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribP1uiv  = (Call_glVertexAttribP1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP1uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP1uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribP1uiv  = (Call_glVertexAttribP1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP1uiv));
			}

			glf = wglGetProcAddress("glVertexAttribP2ui");
			if (glf != IntPtr.Zero)
				glVertexAttribP2ui  = (Call_glVertexAttribP2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP2ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP2ui");
				if (glf != IntPtr.Zero)
					glVertexAttribP2ui  = (Call_glVertexAttribP2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP2ui));
			}

			glf = wglGetProcAddress("glVertexAttribP2uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribP2uiv  = (Call_glVertexAttribP2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP2uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP2uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribP2uiv  = (Call_glVertexAttribP2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP2uiv));
			}

			glf = wglGetProcAddress("glVertexAttribP3ui");
			if (glf != IntPtr.Zero)
				glVertexAttribP3ui  = (Call_glVertexAttribP3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP3ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP3ui");
				if (glf != IntPtr.Zero)
					glVertexAttribP3ui  = (Call_glVertexAttribP3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP3ui));
			}

			glf = wglGetProcAddress("glVertexAttribP3uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribP3uiv  = (Call_glVertexAttribP3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP3uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP3uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribP3uiv  = (Call_glVertexAttribP3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP3uiv));
			}

			glf = wglGetProcAddress("glVertexAttribP4ui");
			if (glf != IntPtr.Zero)
				glVertexAttribP4ui  = (Call_glVertexAttribP4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP4ui));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP4ui");
				if (glf != IntPtr.Zero)
					glVertexAttribP4ui  = (Call_glVertexAttribP4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP4ui));
			}

			glf = wglGetProcAddress("glVertexAttribP4uiv");
			if (glf != IntPtr.Zero)
				glVertexAttribP4uiv  = (Call_glVertexAttribP4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP4uiv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribP4uiv");
				if (glf != IntPtr.Zero)
					glVertexAttribP4uiv  = (Call_glVertexAttribP4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribP4uiv));
			}

			glf = wglGetProcAddress("glMinSampleShading");
			if (glf != IntPtr.Zero)
				glMinSampleShading  = (Call_glMinSampleShading)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMinSampleShading));
			else {
				glf = GetProcAddress(pDll,"glMinSampleShading");
				if (glf != IntPtr.Zero)
					glMinSampleShading  = (Call_glMinSampleShading)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMinSampleShading));
			}

			glf = wglGetProcAddress("glBlendEquationi");
			if (glf != IntPtr.Zero)
				glBlendEquationi  = (Call_glBlendEquationi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationi));
			else {
				glf = GetProcAddress(pDll,"glBlendEquationi");
				if (glf != IntPtr.Zero)
					glBlendEquationi  = (Call_glBlendEquationi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationi));
			}

			glf = wglGetProcAddress("glBlendEquationSeparatei");
			if (glf != IntPtr.Zero)
				glBlendEquationSeparatei  = (Call_glBlendEquationSeparatei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationSeparatei));
			else {
				glf = GetProcAddress(pDll,"glBlendEquationSeparatei");
				if (glf != IntPtr.Zero)
					glBlendEquationSeparatei  = (Call_glBlendEquationSeparatei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendEquationSeparatei));
			}

			glf = wglGetProcAddress("glBlendFunci");
			if (glf != IntPtr.Zero)
				glBlendFunci  = (Call_glBlendFunci)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFunci));
			else {
				glf = GetProcAddress(pDll,"glBlendFunci");
				if (glf != IntPtr.Zero)
					glBlendFunci  = (Call_glBlendFunci)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFunci));
			}

			glf = wglGetProcAddress("glBlendFuncSeparatei");
			if (glf != IntPtr.Zero)
				glBlendFuncSeparatei  = (Call_glBlendFuncSeparatei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFuncSeparatei));
			else {
				glf = GetProcAddress(pDll,"glBlendFuncSeparatei");
				if (glf != IntPtr.Zero)
					glBlendFuncSeparatei  = (Call_glBlendFuncSeparatei)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlendFuncSeparatei));
			}

			glf = wglGetProcAddress("glDrawArraysIndirect");
			if (glf != IntPtr.Zero)
				glDrawArraysIndirect  = (Call_glDrawArraysIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysIndirect));
			else {
				glf = GetProcAddress(pDll,"glDrawArraysIndirect");
				if (glf != IntPtr.Zero)
					glDrawArraysIndirect  = (Call_glDrawArraysIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysIndirect));
			}

			glf = wglGetProcAddress("glDrawElementsIndirect");
			if (glf != IntPtr.Zero)
				glDrawElementsIndirect  = (Call_glDrawElementsIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsIndirect));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsIndirect");
				if (glf != IntPtr.Zero)
					glDrawElementsIndirect  = (Call_glDrawElementsIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsIndirect));
			}

			glf = wglGetProcAddress("glUniform1d");
			if (glf != IntPtr.Zero)
				glUniform1d  = (Call_glUniform1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1d));
			else {
				glf = GetProcAddress(pDll,"glUniform1d");
				if (glf != IntPtr.Zero)
					glUniform1d  = (Call_glUniform1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1d));
			}

			glf = wglGetProcAddress("glUniform2d");
			if (glf != IntPtr.Zero)
				glUniform2d  = (Call_glUniform2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2d));
			else {
				glf = GetProcAddress(pDll,"glUniform2d");
				if (glf != IntPtr.Zero)
					glUniform2d  = (Call_glUniform2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2d));
			}

			glf = wglGetProcAddress("glUniform3d");
			if (glf != IntPtr.Zero)
				glUniform3d  = (Call_glUniform3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3d));
			else {
				glf = GetProcAddress(pDll,"glUniform3d");
				if (glf != IntPtr.Zero)
					glUniform3d  = (Call_glUniform3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3d));
			}

			glf = wglGetProcAddress("glUniform4d");
			if (glf != IntPtr.Zero)
				glUniform4d  = (Call_glUniform4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4d));
			else {
				glf = GetProcAddress(pDll,"glUniform4d");
				if (glf != IntPtr.Zero)
					glUniform4d  = (Call_glUniform4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4d));
			}

			glf = wglGetProcAddress("glUniform1dv");
			if (glf != IntPtr.Zero)
				glUniform1dv  = (Call_glUniform1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1dv));
			else {
				glf = GetProcAddress(pDll,"glUniform1dv");
				if (glf != IntPtr.Zero)
					glUniform1dv  = (Call_glUniform1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform1dv));
			}

			glf = wglGetProcAddress("glUniform2dv");
			if (glf != IntPtr.Zero)
				glUniform2dv  = (Call_glUniform2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2dv));
			else {
				glf = GetProcAddress(pDll,"glUniform2dv");
				if (glf != IntPtr.Zero)
					glUniform2dv  = (Call_glUniform2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform2dv));
			}

			glf = wglGetProcAddress("glUniform3dv");
			if (glf != IntPtr.Zero)
				glUniform3dv  = (Call_glUniform3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3dv));
			else {
				glf = GetProcAddress(pDll,"glUniform3dv");
				if (glf != IntPtr.Zero)
					glUniform3dv  = (Call_glUniform3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform3dv));
			}

			glf = wglGetProcAddress("glUniform4dv");
			if (glf != IntPtr.Zero)
				glUniform4dv  = (Call_glUniform4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4dv));
			else {
				glf = GetProcAddress(pDll,"glUniform4dv");
				if (glf != IntPtr.Zero)
					glUniform4dv  = (Call_glUniform4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniform4dv));
			}

			glf = wglGetProcAddress("glUniformMatrix2dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2dv  = (Call_glUniformMatrix2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2dv  = (Call_glUniformMatrix2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2dv));
			}

			glf = wglGetProcAddress("glUniformMatrix3dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3dv  = (Call_glUniformMatrix3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3dv  = (Call_glUniformMatrix3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3dv));
			}

			glf = wglGetProcAddress("glUniformMatrix4dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4dv  = (Call_glUniformMatrix4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4dv  = (Call_glUniformMatrix4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4dv));
			}

			glf = wglGetProcAddress("glUniformMatrix2x3dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2x3dv  = (Call_glUniformMatrix2x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x3dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2x3dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2x3dv  = (Call_glUniformMatrix2x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x3dv));
			}

			glf = wglGetProcAddress("glUniformMatrix2x4dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix2x4dv  = (Call_glUniformMatrix2x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x4dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix2x4dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix2x4dv  = (Call_glUniformMatrix2x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix2x4dv));
			}

			glf = wglGetProcAddress("glUniformMatrix3x2dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3x2dv  = (Call_glUniformMatrix3x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x2dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3x2dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3x2dv  = (Call_glUniformMatrix3x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x2dv));
			}

			glf = wglGetProcAddress("glUniformMatrix3x4dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix3x4dv  = (Call_glUniformMatrix3x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x4dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix3x4dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix3x4dv  = (Call_glUniformMatrix3x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix3x4dv));
			}

			glf = wglGetProcAddress("glUniformMatrix4x2dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4x2dv  = (Call_glUniformMatrix4x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x2dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4x2dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4x2dv  = (Call_glUniformMatrix4x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x2dv));
			}

			glf = wglGetProcAddress("glUniformMatrix4x3dv");
			if (glf != IntPtr.Zero)
				glUniformMatrix4x3dv  = (Call_glUniformMatrix4x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x3dv));
			else {
				glf = GetProcAddress(pDll,"glUniformMatrix4x3dv");
				if (glf != IntPtr.Zero)
					glUniformMatrix4x3dv  = (Call_glUniformMatrix4x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformMatrix4x3dv));
			}

			glf = wglGetProcAddress("glGetUniformdv");
			if (glf != IntPtr.Zero)
				glGetUniformdv  = (Call_glGetUniformdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformdv));
			else {
				glf = GetProcAddress(pDll,"glGetUniformdv");
				if (glf != IntPtr.Zero)
					glGetUniformdv  = (Call_glGetUniformdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformdv));
			}

			glf = wglGetProcAddress("glGetSubroutineUniformLocation");
			if (glf != IntPtr.Zero)
				glGetSubroutineUniformLocation  = (Call_glGetSubroutineUniformLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSubroutineUniformLocation));
			else {
				glf = GetProcAddress(pDll,"glGetSubroutineUniformLocation");
				if (glf != IntPtr.Zero)
					glGetSubroutineUniformLocation  = (Call_glGetSubroutineUniformLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSubroutineUniformLocation));
			}

			glf = wglGetProcAddress("glGetSubroutineIndex");
			if (glf != IntPtr.Zero)
				glGetSubroutineIndex  = (Call_glGetSubroutineIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSubroutineIndex));
			else {
				glf = GetProcAddress(pDll,"glGetSubroutineIndex");
				if (glf != IntPtr.Zero)
					glGetSubroutineIndex  = (Call_glGetSubroutineIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetSubroutineIndex));
			}

			glf = wglGetProcAddress("glGetActiveSubroutineUniformiv");
			if (glf != IntPtr.Zero)
				glGetActiveSubroutineUniformiv  = (Call_glGetActiveSubroutineUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineUniformiv));
			else {
				glf = GetProcAddress(pDll,"glGetActiveSubroutineUniformiv");
				if (glf != IntPtr.Zero)
					glGetActiveSubroutineUniformiv  = (Call_glGetActiveSubroutineUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineUniformiv));
			}

			glf = wglGetProcAddress("glGetActiveSubroutineUniformName");
			if (glf != IntPtr.Zero)
				glGetActiveSubroutineUniformName  = (Call_glGetActiveSubroutineUniformName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineUniformName));
			else {
				glf = GetProcAddress(pDll,"glGetActiveSubroutineUniformName");
				if (glf != IntPtr.Zero)
					glGetActiveSubroutineUniformName  = (Call_glGetActiveSubroutineUniformName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineUniformName));
			}

			glf = wglGetProcAddress("glGetActiveSubroutineName");
			if (glf != IntPtr.Zero)
				glGetActiveSubroutineName  = (Call_glGetActiveSubroutineName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineName));
			else {
				glf = GetProcAddress(pDll,"glGetActiveSubroutineName");
				if (glf != IntPtr.Zero)
					glGetActiveSubroutineName  = (Call_glGetActiveSubroutineName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveSubroutineName));
			}

			glf = wglGetProcAddress("glUniformSubroutinesuiv");
			if (glf != IntPtr.Zero)
				glUniformSubroutinesuiv  = (Call_glUniformSubroutinesuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformSubroutinesuiv));
			else {
				glf = GetProcAddress(pDll,"glUniformSubroutinesuiv");
				if (glf != IntPtr.Zero)
					glUniformSubroutinesuiv  = (Call_glUniformSubroutinesuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUniformSubroutinesuiv));
			}

			glf = wglGetProcAddress("glGetUniformSubroutineuiv");
			if (glf != IntPtr.Zero)
				glGetUniformSubroutineuiv  = (Call_glGetUniformSubroutineuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformSubroutineuiv));
			else {
				glf = GetProcAddress(pDll,"glGetUniformSubroutineuiv");
				if (glf != IntPtr.Zero)
					glGetUniformSubroutineuiv  = (Call_glGetUniformSubroutineuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetUniformSubroutineuiv));
			}

			glf = wglGetProcAddress("glGetProgramStageiv");
			if (glf != IntPtr.Zero)
				glGetProgramStageiv  = (Call_glGetProgramStageiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramStageiv));
			else {
				glf = GetProcAddress(pDll,"glGetProgramStageiv");
				if (glf != IntPtr.Zero)
					glGetProgramStageiv  = (Call_glGetProgramStageiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramStageiv));
			}

			glf = wglGetProcAddress("glPatchParameteri");
			if (glf != IntPtr.Zero)
				glPatchParameteri  = (Call_glPatchParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPatchParameteri));
			else {
				glf = GetProcAddress(pDll,"glPatchParameteri");
				if (glf != IntPtr.Zero)
					glPatchParameteri  = (Call_glPatchParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPatchParameteri));
			}

			glf = wglGetProcAddress("glPatchParameterfv");
			if (glf != IntPtr.Zero)
				glPatchParameterfv  = (Call_glPatchParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPatchParameterfv));
			else {
				glf = GetProcAddress(pDll,"glPatchParameterfv");
				if (glf != IntPtr.Zero)
					glPatchParameterfv  = (Call_glPatchParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPatchParameterfv));
			}

			glf = wglGetProcAddress("glBindTransformFeedback");
			if (glf != IntPtr.Zero)
				glBindTransformFeedback  = (Call_glBindTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glBindTransformFeedback");
				if (glf != IntPtr.Zero)
					glBindTransformFeedback  = (Call_glBindTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTransformFeedback));
			}

			glf = wglGetProcAddress("glDeleteTransformFeedbacks");
			if (glf != IntPtr.Zero)
				glDeleteTransformFeedbacks  = (Call_glDeleteTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteTransformFeedbacks));
			else {
				glf = GetProcAddress(pDll,"glDeleteTransformFeedbacks");
				if (glf != IntPtr.Zero)
					glDeleteTransformFeedbacks  = (Call_glDeleteTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteTransformFeedbacks));
			}

			glf = wglGetProcAddress("glGenTransformFeedbacks");
			if (glf != IntPtr.Zero)
				glGenTransformFeedbacks  = (Call_glGenTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenTransformFeedbacks));
			else {
				glf = GetProcAddress(pDll,"glGenTransformFeedbacks");
				if (glf != IntPtr.Zero)
					glGenTransformFeedbacks  = (Call_glGenTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenTransformFeedbacks));
			}

			glf = wglGetProcAddress("glIsTransformFeedback");
			if (glf != IntPtr.Zero)
				glIsTransformFeedback  = (Call_glIsTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glIsTransformFeedback");
				if (glf != IntPtr.Zero)
					glIsTransformFeedback  = (Call_glIsTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsTransformFeedback));
			}

			glf = wglGetProcAddress("glPauseTransformFeedback");
			if (glf != IntPtr.Zero)
				glPauseTransformFeedback  = (Call_glPauseTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPauseTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glPauseTransformFeedback");
				if (glf != IntPtr.Zero)
					glPauseTransformFeedback  = (Call_glPauseTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPauseTransformFeedback));
			}

			glf = wglGetProcAddress("glResumeTransformFeedback");
			if (glf != IntPtr.Zero)
				glResumeTransformFeedback  = (Call_glResumeTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glResumeTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glResumeTransformFeedback");
				if (glf != IntPtr.Zero)
					glResumeTransformFeedback  = (Call_glResumeTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glResumeTransformFeedback));
			}

			glf = wglGetProcAddress("glDrawTransformFeedback");
			if (glf != IntPtr.Zero)
				glDrawTransformFeedback  = (Call_glDrawTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedback));
			else {
				glf = GetProcAddress(pDll,"glDrawTransformFeedback");
				if (glf != IntPtr.Zero)
					glDrawTransformFeedback  = (Call_glDrawTransformFeedback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedback));
			}

			glf = wglGetProcAddress("glDrawTransformFeedbackStream");
			if (glf != IntPtr.Zero)
				glDrawTransformFeedbackStream  = (Call_glDrawTransformFeedbackStream)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackStream));
			else {
				glf = GetProcAddress(pDll,"glDrawTransformFeedbackStream");
				if (glf != IntPtr.Zero)
					glDrawTransformFeedbackStream  = (Call_glDrawTransformFeedbackStream)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackStream));
			}

			glf = wglGetProcAddress("glBeginQueryIndexed");
			if (glf != IntPtr.Zero)
				glBeginQueryIndexed  = (Call_glBeginQueryIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginQueryIndexed));
			else {
				glf = GetProcAddress(pDll,"glBeginQueryIndexed");
				if (glf != IntPtr.Zero)
					glBeginQueryIndexed  = (Call_glBeginQueryIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBeginQueryIndexed));
			}

			glf = wglGetProcAddress("glEndQueryIndexed");
			if (glf != IntPtr.Zero)
				glEndQueryIndexed  = (Call_glEndQueryIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndQueryIndexed));
			else {
				glf = GetProcAddress(pDll,"glEndQueryIndexed");
				if (glf != IntPtr.Zero)
					glEndQueryIndexed  = (Call_glEndQueryIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEndQueryIndexed));
			}

			glf = wglGetProcAddress("glGetQueryIndexediv");
			if (glf != IntPtr.Zero)
				glGetQueryIndexediv  = (Call_glGetQueryIndexediv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryIndexediv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryIndexediv");
				if (glf != IntPtr.Zero)
					glGetQueryIndexediv  = (Call_glGetQueryIndexediv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryIndexediv));
			}

			glf = wglGetProcAddress("glReleaseShaderCompiler");
			if (glf != IntPtr.Zero)
				glReleaseShaderCompiler  = (Call_glReleaseShaderCompiler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReleaseShaderCompiler));
			else {
				glf = GetProcAddress(pDll,"glReleaseShaderCompiler");
				if (glf != IntPtr.Zero)
					glReleaseShaderCompiler  = (Call_glReleaseShaderCompiler)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReleaseShaderCompiler));
			}

			glf = wglGetProcAddress("glShaderBinary");
			if (glf != IntPtr.Zero)
				glShaderBinary  = (Call_glShaderBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderBinary));
			else {
				glf = GetProcAddress(pDll,"glShaderBinary");
				if (glf != IntPtr.Zero)
					glShaderBinary  = (Call_glShaderBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderBinary));
			}

			glf = wglGetProcAddress("glGetShaderPrecisionFormat");
			if (glf != IntPtr.Zero)
				glGetShaderPrecisionFormat  = (Call_glGetShaderPrecisionFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderPrecisionFormat));
			else {
				glf = GetProcAddress(pDll,"glGetShaderPrecisionFormat");
				if (glf != IntPtr.Zero)
					glGetShaderPrecisionFormat  = (Call_glGetShaderPrecisionFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetShaderPrecisionFormat));
			}

			glf = wglGetProcAddress("glDepthRangef");
			if (glf != IntPtr.Zero)
				glDepthRangef  = (Call_glDepthRangef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangef));
			else {
				glf = GetProcAddress(pDll,"glDepthRangef");
				if (glf != IntPtr.Zero)
					glDepthRangef  = (Call_glDepthRangef)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangef));
			}

			glf = wglGetProcAddress("glClearDepthf");
			if (glf != IntPtr.Zero)
				glClearDepthf  = (Call_glClearDepthf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearDepthf));
			else {
				glf = GetProcAddress(pDll,"glClearDepthf");
				if (glf != IntPtr.Zero)
					glClearDepthf  = (Call_glClearDepthf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearDepthf));
			}

			glf = wglGetProcAddress("glGetProgramBinary");
			if (glf != IntPtr.Zero)
				glGetProgramBinary  = (Call_glGetProgramBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramBinary));
			else {
				glf = GetProcAddress(pDll,"glGetProgramBinary");
				if (glf != IntPtr.Zero)
					glGetProgramBinary  = (Call_glGetProgramBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramBinary));
			}

			glf = wglGetProcAddress("glProgramBinary");
			if (glf != IntPtr.Zero)
				glProgramBinary  = (Call_glProgramBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramBinary));
			else {
				glf = GetProcAddress(pDll,"glProgramBinary");
				if (glf != IntPtr.Zero)
					glProgramBinary  = (Call_glProgramBinary)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramBinary));
			}

			glf = wglGetProcAddress("glProgramParameteri");
			if (glf != IntPtr.Zero)
				glProgramParameteri  = (Call_glProgramParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramParameteri));
			else {
				glf = GetProcAddress(pDll,"glProgramParameteri");
				if (glf != IntPtr.Zero)
					glProgramParameteri  = (Call_glProgramParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramParameteri));
			}

			glf = wglGetProcAddress("glUseProgramStages");
			if (glf != IntPtr.Zero)
				glUseProgramStages  = (Call_glUseProgramStages)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUseProgramStages));
			else {
				glf = GetProcAddress(pDll,"glUseProgramStages");
				if (glf != IntPtr.Zero)
					glUseProgramStages  = (Call_glUseProgramStages)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUseProgramStages));
			}

			glf = wglGetProcAddress("glActiveShaderProgram");
			if (glf != IntPtr.Zero)
				glActiveShaderProgram  = (Call_glActiveShaderProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glActiveShaderProgram));
			else {
				glf = GetProcAddress(pDll,"glActiveShaderProgram");
				if (glf != IntPtr.Zero)
					glActiveShaderProgram  = (Call_glActiveShaderProgram)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glActiveShaderProgram));
			}

			glf = wglGetProcAddress("glCreateShaderProgramv");
			if (glf != IntPtr.Zero)
				glCreateShaderProgramv  = (Call_glCreateShaderProgramv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateShaderProgramv));
			else {
				glf = GetProcAddress(pDll,"glCreateShaderProgramv");
				if (glf != IntPtr.Zero)
					glCreateShaderProgramv  = (Call_glCreateShaderProgramv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateShaderProgramv));
			}

			glf = wglGetProcAddress("glBindProgramPipeline");
			if (glf != IntPtr.Zero)
				glBindProgramPipeline  = (Call_glBindProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindProgramPipeline));
			else {
				glf = GetProcAddress(pDll,"glBindProgramPipeline");
				if (glf != IntPtr.Zero)
					glBindProgramPipeline  = (Call_glBindProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindProgramPipeline));
			}

			glf = wglGetProcAddress("glDeleteProgramPipelines");
			if (glf != IntPtr.Zero)
				glDeleteProgramPipelines  = (Call_glDeleteProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteProgramPipelines));
			else {
				glf = GetProcAddress(pDll,"glDeleteProgramPipelines");
				if (glf != IntPtr.Zero)
					glDeleteProgramPipelines  = (Call_glDeleteProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDeleteProgramPipelines));
			}

			glf = wglGetProcAddress("glGenProgramPipelines");
			if (glf != IntPtr.Zero)
				glGenProgramPipelines  = (Call_glGenProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenProgramPipelines));
			else {
				glf = GetProcAddress(pDll,"glGenProgramPipelines");
				if (glf != IntPtr.Zero)
					glGenProgramPipelines  = (Call_glGenProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenProgramPipelines));
			}

			glf = wglGetProcAddress("glIsProgramPipeline");
			if (glf != IntPtr.Zero)
				glIsProgramPipeline  = (Call_glIsProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsProgramPipeline));
			else {
				glf = GetProcAddress(pDll,"glIsProgramPipeline");
				if (glf != IntPtr.Zero)
					glIsProgramPipeline  = (Call_glIsProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glIsProgramPipeline));
			}

			glf = wglGetProcAddress("glGetProgramPipelineiv");
			if (glf != IntPtr.Zero)
				glGetProgramPipelineiv  = (Call_glGetProgramPipelineiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramPipelineiv));
			else {
				glf = GetProcAddress(pDll,"glGetProgramPipelineiv");
				if (glf != IntPtr.Zero)
					glGetProgramPipelineiv  = (Call_glGetProgramPipelineiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramPipelineiv));
			}

			glf = wglGetProcAddress("glProgramUniform1i");
			if (glf != IntPtr.Zero)
				glProgramUniform1i  = (Call_glProgramUniform1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1i));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1i");
				if (glf != IntPtr.Zero)
					glProgramUniform1i  = (Call_glProgramUniform1i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1i));
			}

			glf = wglGetProcAddress("glProgramUniform1iv");
			if (glf != IntPtr.Zero)
				glProgramUniform1iv  = (Call_glProgramUniform1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1iv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1iv");
				if (glf != IntPtr.Zero)
					glProgramUniform1iv  = (Call_glProgramUniform1iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1iv));
			}

			glf = wglGetProcAddress("glProgramUniform1f");
			if (glf != IntPtr.Zero)
				glProgramUniform1f  = (Call_glProgramUniform1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1f));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1f");
				if (glf != IntPtr.Zero)
					glProgramUniform1f  = (Call_glProgramUniform1f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1f));
			}

			glf = wglGetProcAddress("glProgramUniform1fv");
			if (glf != IntPtr.Zero)
				glProgramUniform1fv  = (Call_glProgramUniform1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1fv");
				if (glf != IntPtr.Zero)
					glProgramUniform1fv  = (Call_glProgramUniform1fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1fv));
			}

			glf = wglGetProcAddress("glProgramUniform1d");
			if (glf != IntPtr.Zero)
				glProgramUniform1d  = (Call_glProgramUniform1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1d));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1d");
				if (glf != IntPtr.Zero)
					glProgramUniform1d  = (Call_glProgramUniform1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1d));
			}

			glf = wglGetProcAddress("glProgramUniform1dv");
			if (glf != IntPtr.Zero)
				glProgramUniform1dv  = (Call_glProgramUniform1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1dv");
				if (glf != IntPtr.Zero)
					glProgramUniform1dv  = (Call_glProgramUniform1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1dv));
			}

			glf = wglGetProcAddress("glProgramUniform1ui");
			if (glf != IntPtr.Zero)
				glProgramUniform1ui  = (Call_glProgramUniform1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1ui));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1ui");
				if (glf != IntPtr.Zero)
					glProgramUniform1ui  = (Call_glProgramUniform1ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1ui));
			}

			glf = wglGetProcAddress("glProgramUniform1uiv");
			if (glf != IntPtr.Zero)
				glProgramUniform1uiv  = (Call_glProgramUniform1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1uiv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform1uiv");
				if (glf != IntPtr.Zero)
					glProgramUniform1uiv  = (Call_glProgramUniform1uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform1uiv));
			}

			glf = wglGetProcAddress("glProgramUniform2i");
			if (glf != IntPtr.Zero)
				glProgramUniform2i  = (Call_glProgramUniform2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2i));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2i");
				if (glf != IntPtr.Zero)
					glProgramUniform2i  = (Call_glProgramUniform2i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2i));
			}

			glf = wglGetProcAddress("glProgramUniform2iv");
			if (glf != IntPtr.Zero)
				glProgramUniform2iv  = (Call_glProgramUniform2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2iv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2iv");
				if (glf != IntPtr.Zero)
					glProgramUniform2iv  = (Call_glProgramUniform2iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2iv));
			}

			glf = wglGetProcAddress("glProgramUniform2f");
			if (glf != IntPtr.Zero)
				glProgramUniform2f  = (Call_glProgramUniform2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2f));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2f");
				if (glf != IntPtr.Zero)
					glProgramUniform2f  = (Call_glProgramUniform2f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2f));
			}

			glf = wglGetProcAddress("glProgramUniform2fv");
			if (glf != IntPtr.Zero)
				glProgramUniform2fv  = (Call_glProgramUniform2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2fv");
				if (glf != IntPtr.Zero)
					glProgramUniform2fv  = (Call_glProgramUniform2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2fv));
			}

			glf = wglGetProcAddress("glProgramUniform2d");
			if (glf != IntPtr.Zero)
				glProgramUniform2d  = (Call_glProgramUniform2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2d));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2d");
				if (glf != IntPtr.Zero)
					glProgramUniform2d  = (Call_glProgramUniform2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2d));
			}

			glf = wglGetProcAddress("glProgramUniform2dv");
			if (glf != IntPtr.Zero)
				glProgramUniform2dv  = (Call_glProgramUniform2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2dv");
				if (glf != IntPtr.Zero)
					glProgramUniform2dv  = (Call_glProgramUniform2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2dv));
			}

			glf = wglGetProcAddress("glProgramUniform2ui");
			if (glf != IntPtr.Zero)
				glProgramUniform2ui  = (Call_glProgramUniform2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2ui));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2ui");
				if (glf != IntPtr.Zero)
					glProgramUniform2ui  = (Call_glProgramUniform2ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2ui));
			}

			glf = wglGetProcAddress("glProgramUniform2uiv");
			if (glf != IntPtr.Zero)
				glProgramUniform2uiv  = (Call_glProgramUniform2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2uiv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform2uiv");
				if (glf != IntPtr.Zero)
					glProgramUniform2uiv  = (Call_glProgramUniform2uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform2uiv));
			}

			glf = wglGetProcAddress("glProgramUniform3i");
			if (glf != IntPtr.Zero)
				glProgramUniform3i  = (Call_glProgramUniform3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3i));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3i");
				if (glf != IntPtr.Zero)
					glProgramUniform3i  = (Call_glProgramUniform3i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3i));
			}

			glf = wglGetProcAddress("glProgramUniform3iv");
			if (glf != IntPtr.Zero)
				glProgramUniform3iv  = (Call_glProgramUniform3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3iv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3iv");
				if (glf != IntPtr.Zero)
					glProgramUniform3iv  = (Call_glProgramUniform3iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3iv));
			}

			glf = wglGetProcAddress("glProgramUniform3f");
			if (glf != IntPtr.Zero)
				glProgramUniform3f  = (Call_glProgramUniform3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3f));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3f");
				if (glf != IntPtr.Zero)
					glProgramUniform3f  = (Call_glProgramUniform3f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3f));
			}

			glf = wglGetProcAddress("glProgramUniform3fv");
			if (glf != IntPtr.Zero)
				glProgramUniform3fv  = (Call_glProgramUniform3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3fv");
				if (glf != IntPtr.Zero)
					glProgramUniform3fv  = (Call_glProgramUniform3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3fv));
			}

			glf = wglGetProcAddress("glProgramUniform3d");
			if (glf != IntPtr.Zero)
				glProgramUniform3d  = (Call_glProgramUniform3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3d));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3d");
				if (glf != IntPtr.Zero)
					glProgramUniform3d  = (Call_glProgramUniform3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3d));
			}

			glf = wglGetProcAddress("glProgramUniform3dv");
			if (glf != IntPtr.Zero)
				glProgramUniform3dv  = (Call_glProgramUniform3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3dv");
				if (glf != IntPtr.Zero)
					glProgramUniform3dv  = (Call_glProgramUniform3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3dv));
			}

			glf = wglGetProcAddress("glProgramUniform3ui");
			if (glf != IntPtr.Zero)
				glProgramUniform3ui  = (Call_glProgramUniform3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3ui));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3ui");
				if (glf != IntPtr.Zero)
					glProgramUniform3ui  = (Call_glProgramUniform3ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3ui));
			}

			glf = wglGetProcAddress("glProgramUniform3uiv");
			if (glf != IntPtr.Zero)
				glProgramUniform3uiv  = (Call_glProgramUniform3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3uiv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform3uiv");
				if (glf != IntPtr.Zero)
					glProgramUniform3uiv  = (Call_glProgramUniform3uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform3uiv));
			}

			glf = wglGetProcAddress("glProgramUniform4i");
			if (glf != IntPtr.Zero)
				glProgramUniform4i  = (Call_glProgramUniform4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4i));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4i");
				if (glf != IntPtr.Zero)
					glProgramUniform4i  = (Call_glProgramUniform4i)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4i));
			}

			glf = wglGetProcAddress("glProgramUniform4iv");
			if (glf != IntPtr.Zero)
				glProgramUniform4iv  = (Call_glProgramUniform4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4iv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4iv");
				if (glf != IntPtr.Zero)
					glProgramUniform4iv  = (Call_glProgramUniform4iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4iv));
			}

			glf = wglGetProcAddress("glProgramUniform4f");
			if (glf != IntPtr.Zero)
				glProgramUniform4f  = (Call_glProgramUniform4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4f));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4f");
				if (glf != IntPtr.Zero)
					glProgramUniform4f  = (Call_glProgramUniform4f)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4f));
			}

			glf = wglGetProcAddress("glProgramUniform4fv");
			if (glf != IntPtr.Zero)
				glProgramUniform4fv  = (Call_glProgramUniform4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4fv");
				if (glf != IntPtr.Zero)
					glProgramUniform4fv  = (Call_glProgramUniform4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4fv));
			}

			glf = wglGetProcAddress("glProgramUniform4d");
			if (glf != IntPtr.Zero)
				glProgramUniform4d  = (Call_glProgramUniform4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4d));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4d");
				if (glf != IntPtr.Zero)
					glProgramUniform4d  = (Call_glProgramUniform4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4d));
			}

			glf = wglGetProcAddress("glProgramUniform4dv");
			if (glf != IntPtr.Zero)
				glProgramUniform4dv  = (Call_glProgramUniform4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4dv");
				if (glf != IntPtr.Zero)
					glProgramUniform4dv  = (Call_glProgramUniform4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4dv));
			}

			glf = wglGetProcAddress("glProgramUniform4ui");
			if (glf != IntPtr.Zero)
				glProgramUniform4ui  = (Call_glProgramUniform4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4ui));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4ui");
				if (glf != IntPtr.Zero)
					glProgramUniform4ui  = (Call_glProgramUniform4ui)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4ui));
			}

			glf = wglGetProcAddress("glProgramUniform4uiv");
			if (glf != IntPtr.Zero)
				glProgramUniform4uiv  = (Call_glProgramUniform4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4uiv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniform4uiv");
				if (glf != IntPtr.Zero)
					glProgramUniform4uiv  = (Call_glProgramUniform4uiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniform4uiv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2fv  = (Call_glProgramUniformMatrix2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2fv  = (Call_glProgramUniformMatrix2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3fv  = (Call_glProgramUniformMatrix3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3fv  = (Call_glProgramUniformMatrix3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4fv  = (Call_glProgramUniformMatrix4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4fv  = (Call_glProgramUniformMatrix4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2dv  = (Call_glProgramUniformMatrix2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2dv  = (Call_glProgramUniformMatrix2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3dv  = (Call_glProgramUniformMatrix3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3dv  = (Call_glProgramUniformMatrix3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4dv  = (Call_glProgramUniformMatrix4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4dv  = (Call_glProgramUniformMatrix4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2x3fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2x3fv  = (Call_glProgramUniformMatrix2x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x3fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2x3fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2x3fv  = (Call_glProgramUniformMatrix2x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x3fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3x2fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3x2fv  = (Call_glProgramUniformMatrix3x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x2fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3x2fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3x2fv  = (Call_glProgramUniformMatrix3x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x2fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2x4fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2x4fv  = (Call_glProgramUniformMatrix2x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x4fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2x4fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2x4fv  = (Call_glProgramUniformMatrix2x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x4fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4x2fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4x2fv  = (Call_glProgramUniformMatrix4x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x2fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4x2fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4x2fv  = (Call_glProgramUniformMatrix4x2fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x2fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3x4fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3x4fv  = (Call_glProgramUniformMatrix3x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x4fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3x4fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3x4fv  = (Call_glProgramUniformMatrix3x4fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x4fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4x3fv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4x3fv  = (Call_glProgramUniformMatrix4x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x3fv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4x3fv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4x3fv  = (Call_glProgramUniformMatrix4x3fv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x3fv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2x3dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2x3dv  = (Call_glProgramUniformMatrix2x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x3dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2x3dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2x3dv  = (Call_glProgramUniformMatrix2x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x3dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3x2dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3x2dv  = (Call_glProgramUniformMatrix3x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x2dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3x2dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3x2dv  = (Call_glProgramUniformMatrix3x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x2dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix2x4dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix2x4dv  = (Call_glProgramUniformMatrix2x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x4dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix2x4dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix2x4dv  = (Call_glProgramUniformMatrix2x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix2x4dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4x2dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4x2dv  = (Call_glProgramUniformMatrix4x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x2dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4x2dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4x2dv  = (Call_glProgramUniformMatrix4x2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x2dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix3x4dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix3x4dv  = (Call_glProgramUniformMatrix3x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x4dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix3x4dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix3x4dv  = (Call_glProgramUniformMatrix3x4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix3x4dv));
			}

			glf = wglGetProcAddress("glProgramUniformMatrix4x3dv");
			if (glf != IntPtr.Zero)
				glProgramUniformMatrix4x3dv  = (Call_glProgramUniformMatrix4x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x3dv));
			else {
				glf = GetProcAddress(pDll,"glProgramUniformMatrix4x3dv");
				if (glf != IntPtr.Zero)
					glProgramUniformMatrix4x3dv  = (Call_glProgramUniformMatrix4x3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glProgramUniformMatrix4x3dv));
			}

			glf = wglGetProcAddress("glValidateProgramPipeline");
			if (glf != IntPtr.Zero)
				glValidateProgramPipeline  = (Call_glValidateProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glValidateProgramPipeline));
			else {
				glf = GetProcAddress(pDll,"glValidateProgramPipeline");
				if (glf != IntPtr.Zero)
					glValidateProgramPipeline  = (Call_glValidateProgramPipeline)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glValidateProgramPipeline));
			}

			glf = wglGetProcAddress("glGetProgramPipelineInfoLog");
			if (glf != IntPtr.Zero)
				glGetProgramPipelineInfoLog  = (Call_glGetProgramPipelineInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramPipelineInfoLog));
			else {
				glf = GetProcAddress(pDll,"glGetProgramPipelineInfoLog");
				if (glf != IntPtr.Zero)
					glGetProgramPipelineInfoLog  = (Call_glGetProgramPipelineInfoLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramPipelineInfoLog));
			}

			glf = wglGetProcAddress("glVertexAttribL1d");
			if (glf != IntPtr.Zero)
				glVertexAttribL1d  = (Call_glVertexAttribL1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL1d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL1d");
				if (glf != IntPtr.Zero)
					glVertexAttribL1d  = (Call_glVertexAttribL1d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL1d));
			}

			glf = wglGetProcAddress("glVertexAttribL2d");
			if (glf != IntPtr.Zero)
				glVertexAttribL2d  = (Call_glVertexAttribL2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL2d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL2d");
				if (glf != IntPtr.Zero)
					glVertexAttribL2d  = (Call_glVertexAttribL2d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL2d));
			}

			glf = wglGetProcAddress("glVertexAttribL3d");
			if (glf != IntPtr.Zero)
				glVertexAttribL3d  = (Call_glVertexAttribL3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL3d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL3d");
				if (glf != IntPtr.Zero)
					glVertexAttribL3d  = (Call_glVertexAttribL3d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL3d));
			}

			glf = wglGetProcAddress("glVertexAttribL4d");
			if (glf != IntPtr.Zero)
				glVertexAttribL4d  = (Call_glVertexAttribL4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL4d));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL4d");
				if (glf != IntPtr.Zero)
					glVertexAttribL4d  = (Call_glVertexAttribL4d)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL4d));
			}

			glf = wglGetProcAddress("glVertexAttribL1dv");
			if (glf != IntPtr.Zero)
				glVertexAttribL1dv  = (Call_glVertexAttribL1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL1dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL1dv");
				if (glf != IntPtr.Zero)
					glVertexAttribL1dv  = (Call_glVertexAttribL1dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL1dv));
			}

			glf = wglGetProcAddress("glVertexAttribL2dv");
			if (glf != IntPtr.Zero)
				glVertexAttribL2dv  = (Call_glVertexAttribL2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL2dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL2dv");
				if (glf != IntPtr.Zero)
					glVertexAttribL2dv  = (Call_glVertexAttribL2dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL2dv));
			}

			glf = wglGetProcAddress("glVertexAttribL3dv");
			if (glf != IntPtr.Zero)
				glVertexAttribL3dv  = (Call_glVertexAttribL3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL3dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL3dv");
				if (glf != IntPtr.Zero)
					glVertexAttribL3dv  = (Call_glVertexAttribL3dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL3dv));
			}

			glf = wglGetProcAddress("glVertexAttribL4dv");
			if (glf != IntPtr.Zero)
				glVertexAttribL4dv  = (Call_glVertexAttribL4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL4dv));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribL4dv");
				if (glf != IntPtr.Zero)
					glVertexAttribL4dv  = (Call_glVertexAttribL4dv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribL4dv));
			}

			glf = wglGetProcAddress("glVertexAttribLPointer");
			if (glf != IntPtr.Zero)
				glVertexAttribLPointer  = (Call_glVertexAttribLPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribLPointer));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribLPointer");
				if (glf != IntPtr.Zero)
					glVertexAttribLPointer  = (Call_glVertexAttribLPointer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribLPointer));
			}

			glf = wglGetProcAddress("glGetVertexAttribLdv");
			if (glf != IntPtr.Zero)
				glGetVertexAttribLdv  = (Call_glGetVertexAttribLdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribLdv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexAttribLdv");
				if (glf != IntPtr.Zero)
					glGetVertexAttribLdv  = (Call_glGetVertexAttribLdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexAttribLdv));
			}

			glf = wglGetProcAddress("glViewportArrayv");
			if (glf != IntPtr.Zero)
				glViewportArrayv  = (Call_glViewportArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportArrayv));
			else {
				glf = GetProcAddress(pDll,"glViewportArrayv");
				if (glf != IntPtr.Zero)
					glViewportArrayv  = (Call_glViewportArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportArrayv));
			}

			glf = wglGetProcAddress("glViewportIndexedf");
			if (glf != IntPtr.Zero)
				glViewportIndexedf  = (Call_glViewportIndexedf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportIndexedf));
			else {
				glf = GetProcAddress(pDll,"glViewportIndexedf");
				if (glf != IntPtr.Zero)
					glViewportIndexedf  = (Call_glViewportIndexedf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportIndexedf));
			}

			glf = wglGetProcAddress("glViewportIndexedfv");
			if (glf != IntPtr.Zero)
				glViewportIndexedfv  = (Call_glViewportIndexedfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportIndexedfv));
			else {
				glf = GetProcAddress(pDll,"glViewportIndexedfv");
				if (glf != IntPtr.Zero)
					glViewportIndexedfv  = (Call_glViewportIndexedfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glViewportIndexedfv));
			}

			glf = wglGetProcAddress("glScissorArrayv");
			if (glf != IntPtr.Zero)
				glScissorArrayv  = (Call_glScissorArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorArrayv));
			else {
				glf = GetProcAddress(pDll,"glScissorArrayv");
				if (glf != IntPtr.Zero)
					glScissorArrayv  = (Call_glScissorArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorArrayv));
			}

			glf = wglGetProcAddress("glScissorIndexed");
			if (glf != IntPtr.Zero)
				glScissorIndexed  = (Call_glScissorIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorIndexed));
			else {
				glf = GetProcAddress(pDll,"glScissorIndexed");
				if (glf != IntPtr.Zero)
					glScissorIndexed  = (Call_glScissorIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorIndexed));
			}

			glf = wglGetProcAddress("glScissorIndexedv");
			if (glf != IntPtr.Zero)
				glScissorIndexedv  = (Call_glScissorIndexedv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorIndexedv));
			else {
				glf = GetProcAddress(pDll,"glScissorIndexedv");
				if (glf != IntPtr.Zero)
					glScissorIndexedv  = (Call_glScissorIndexedv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glScissorIndexedv));
			}

			glf = wglGetProcAddress("glDepthRangeArrayv");
			if (glf != IntPtr.Zero)
				glDepthRangeArrayv  = (Call_glDepthRangeArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangeArrayv));
			else {
				glf = GetProcAddress(pDll,"glDepthRangeArrayv");
				if (glf != IntPtr.Zero)
					glDepthRangeArrayv  = (Call_glDepthRangeArrayv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangeArrayv));
			}

			glf = wglGetProcAddress("glDepthRangeIndexed");
			if (glf != IntPtr.Zero)
				glDepthRangeIndexed  = (Call_glDepthRangeIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangeIndexed));
			else {
				glf = GetProcAddress(pDll,"glDepthRangeIndexed");
				if (glf != IntPtr.Zero)
					glDepthRangeIndexed  = (Call_glDepthRangeIndexed)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDepthRangeIndexed));
			}

			glf = wglGetProcAddress("glGetFloati_v");
			if (glf != IntPtr.Zero)
				glGetFloati_v  = (Call_glGetFloati_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFloati_v));
			else {
				glf = GetProcAddress(pDll,"glGetFloati_v");
				if (glf != IntPtr.Zero)
					glGetFloati_v  = (Call_glGetFloati_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFloati_v));
			}

			glf = wglGetProcAddress("glGetDoublei_v");
			if (glf != IntPtr.Zero)
				glGetDoublei_v  = (Call_glGetDoublei_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDoublei_v));
			else {
				glf = GetProcAddress(pDll,"glGetDoublei_v");
				if (glf != IntPtr.Zero)
					glGetDoublei_v  = (Call_glGetDoublei_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDoublei_v));
			}

			glf = wglGetProcAddress("glDrawArraysInstancedBaseInstance");
			if (glf != IntPtr.Zero)
				glDrawArraysInstancedBaseInstance  = (Call_glDrawArraysInstancedBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysInstancedBaseInstance));
			else {
				glf = GetProcAddress(pDll,"glDrawArraysInstancedBaseInstance");
				if (glf != IntPtr.Zero)
					glDrawArraysInstancedBaseInstance  = (Call_glDrawArraysInstancedBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawArraysInstancedBaseInstance));
			}

			glf = wglGetProcAddress("glDrawElementsInstancedBaseInstance");
			if (glf != IntPtr.Zero)
				glDrawElementsInstancedBaseInstance  = (Call_glDrawElementsInstancedBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseInstance));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsInstancedBaseInstance");
				if (glf != IntPtr.Zero)
					glDrawElementsInstancedBaseInstance  = (Call_glDrawElementsInstancedBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseInstance));
			}

			glf = wglGetProcAddress("glDrawElementsInstancedBaseVertexBaseInstance");
			if (glf != IntPtr.Zero)
				glDrawElementsInstancedBaseVertexBaseInstance  = (Call_glDrawElementsInstancedBaseVertexBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseVertexBaseInstance));
			else {
				glf = GetProcAddress(pDll,"glDrawElementsInstancedBaseVertexBaseInstance");
				if (glf != IntPtr.Zero)
					glDrawElementsInstancedBaseVertexBaseInstance  = (Call_glDrawElementsInstancedBaseVertexBaseInstance)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawElementsInstancedBaseVertexBaseInstance));
			}

			glf = wglGetProcAddress("glGetInternalformativ");
			if (glf != IntPtr.Zero)
				glGetInternalformativ  = (Call_glGetInternalformativ)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInternalformativ));
			else {
				glf = GetProcAddress(pDll,"glGetInternalformativ");
				if (glf != IntPtr.Zero)
					glGetInternalformativ  = (Call_glGetInternalformativ)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInternalformativ));
			}

			glf = wglGetProcAddress("glGetActiveAtomicCounterBufferiv");
			if (glf != IntPtr.Zero)
				glGetActiveAtomicCounterBufferiv  = (Call_glGetActiveAtomicCounterBufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveAtomicCounterBufferiv));
			else {
				glf = GetProcAddress(pDll,"glGetActiveAtomicCounterBufferiv");
				if (glf != IntPtr.Zero)
					glGetActiveAtomicCounterBufferiv  = (Call_glGetActiveAtomicCounterBufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetActiveAtomicCounterBufferiv));
			}

			glf = wglGetProcAddress("glBindImageTexture");
			if (glf != IntPtr.Zero)
				glBindImageTexture  = (Call_glBindImageTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindImageTexture));
			else {
				glf = GetProcAddress(pDll,"glBindImageTexture");
				if (glf != IntPtr.Zero)
					glBindImageTexture  = (Call_glBindImageTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindImageTexture));
			}

			glf = wglGetProcAddress("glMemoryBarrier");
			if (glf != IntPtr.Zero)
				glMemoryBarrier  = (Call_glMemoryBarrier)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMemoryBarrier));
			else {
				glf = GetProcAddress(pDll,"glMemoryBarrier");
				if (glf != IntPtr.Zero)
					glMemoryBarrier  = (Call_glMemoryBarrier)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMemoryBarrier));
			}

			glf = wglGetProcAddress("glTexStorage1D");
			if (glf != IntPtr.Zero)
				glTexStorage1D  = (Call_glTexStorage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage1D));
			else {
				glf = GetProcAddress(pDll,"glTexStorage1D");
				if (glf != IntPtr.Zero)
					glTexStorage1D  = (Call_glTexStorage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage1D));
			}

			glf = wglGetProcAddress("glTexStorage2D");
			if (glf != IntPtr.Zero)
				glTexStorage2D  = (Call_glTexStorage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage2D));
			else {
				glf = GetProcAddress(pDll,"glTexStorage2D");
				if (glf != IntPtr.Zero)
					glTexStorage2D  = (Call_glTexStorage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage2D));
			}

			glf = wglGetProcAddress("glTexStorage3D");
			if (glf != IntPtr.Zero)
				glTexStorage3D  = (Call_glTexStorage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage3D));
			else {
				glf = GetProcAddress(pDll,"glTexStorage3D");
				if (glf != IntPtr.Zero)
					glTexStorage3D  = (Call_glTexStorage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage3D));
			}

			glf = wglGetProcAddress("glDrawTransformFeedbackInstanced");
			if (glf != IntPtr.Zero)
				glDrawTransformFeedbackInstanced  = (Call_glDrawTransformFeedbackInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackInstanced));
			else {
				glf = GetProcAddress(pDll,"glDrawTransformFeedbackInstanced");
				if (glf != IntPtr.Zero)
					glDrawTransformFeedbackInstanced  = (Call_glDrawTransformFeedbackInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackInstanced));
			}

			glf = wglGetProcAddress("glDrawTransformFeedbackStreamInstanced");
			if (glf != IntPtr.Zero)
				glDrawTransformFeedbackStreamInstanced  = (Call_glDrawTransformFeedbackStreamInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackStreamInstanced));
			else {
				glf = GetProcAddress(pDll,"glDrawTransformFeedbackStreamInstanced");
				if (glf != IntPtr.Zero)
					glDrawTransformFeedbackStreamInstanced  = (Call_glDrawTransformFeedbackStreamInstanced)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDrawTransformFeedbackStreamInstanced));
			}

			glf = wglGetProcAddress("glClearBufferData");
			if (glf != IntPtr.Zero)
				glClearBufferData  = (Call_glClearBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferData));
			else {
				glf = GetProcAddress(pDll,"glClearBufferData");
				if (glf != IntPtr.Zero)
					glClearBufferData  = (Call_glClearBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferData));
			}

			glf = wglGetProcAddress("glClearBufferSubData");
			if (glf != IntPtr.Zero)
				glClearBufferSubData  = (Call_glClearBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glClearBufferSubData");
				if (glf != IntPtr.Zero)
					glClearBufferSubData  = (Call_glClearBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearBufferSubData));
			}

			glf = wglGetProcAddress("glDispatchCompute");
			if (glf != IntPtr.Zero)
				glDispatchCompute  = (Call_glDispatchCompute)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDispatchCompute));
			else {
				glf = GetProcAddress(pDll,"glDispatchCompute");
				if (glf != IntPtr.Zero)
					glDispatchCompute  = (Call_glDispatchCompute)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDispatchCompute));
			}

			glf = wglGetProcAddress("glDispatchComputeIndirect");
			if (glf != IntPtr.Zero)
				glDispatchComputeIndirect  = (Call_glDispatchComputeIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDispatchComputeIndirect));
			else {
				glf = GetProcAddress(pDll,"glDispatchComputeIndirect");
				if (glf != IntPtr.Zero)
					glDispatchComputeIndirect  = (Call_glDispatchComputeIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDispatchComputeIndirect));
			}

			glf = wglGetProcAddress("glCopyImageSubData");
			if (glf != IntPtr.Zero)
				glCopyImageSubData  = (Call_glCopyImageSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyImageSubData));
			else {
				glf = GetProcAddress(pDll,"glCopyImageSubData");
				if (glf != IntPtr.Zero)
					glCopyImageSubData  = (Call_glCopyImageSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyImageSubData));
			}

			glf = wglGetProcAddress("glFramebufferParameteri");
			if (glf != IntPtr.Zero)
				glFramebufferParameteri  = (Call_glFramebufferParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferParameteri));
			else {
				glf = GetProcAddress(pDll,"glFramebufferParameteri");
				if (glf != IntPtr.Zero)
					glFramebufferParameteri  = (Call_glFramebufferParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFramebufferParameteri));
			}

			glf = wglGetProcAddress("glGetFramebufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetFramebufferParameteriv  = (Call_glGetFramebufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFramebufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetFramebufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetFramebufferParameteriv  = (Call_glGetFramebufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetFramebufferParameteriv));
			}

			glf = wglGetProcAddress("glGetInternalformati64v");
			if (glf != IntPtr.Zero)
				glGetInternalformati64v  = (Call_glGetInternalformati64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInternalformati64v));
			else {
				glf = GetProcAddress(pDll,"glGetInternalformati64v");
				if (glf != IntPtr.Zero)
					glGetInternalformati64v  = (Call_glGetInternalformati64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetInternalformati64v));
			}

			glf = wglGetProcAddress("glInvalidateTexSubImage");
			if (glf != IntPtr.Zero)
				glInvalidateTexSubImage  = (Call_glInvalidateTexSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateTexSubImage));
			else {
				glf = GetProcAddress(pDll,"glInvalidateTexSubImage");
				if (glf != IntPtr.Zero)
					glInvalidateTexSubImage  = (Call_glInvalidateTexSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateTexSubImage));
			}

			glf = wglGetProcAddress("glInvalidateTexImage");
			if (glf != IntPtr.Zero)
				glInvalidateTexImage  = (Call_glInvalidateTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateTexImage));
			else {
				glf = GetProcAddress(pDll,"glInvalidateTexImage");
				if (glf != IntPtr.Zero)
					glInvalidateTexImage  = (Call_glInvalidateTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateTexImage));
			}

			glf = wglGetProcAddress("glInvalidateBufferSubData");
			if (glf != IntPtr.Zero)
				glInvalidateBufferSubData  = (Call_glInvalidateBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glInvalidateBufferSubData");
				if (glf != IntPtr.Zero)
					glInvalidateBufferSubData  = (Call_glInvalidateBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateBufferSubData));
			}

			glf = wglGetProcAddress("glInvalidateBufferData");
			if (glf != IntPtr.Zero)
				glInvalidateBufferData  = (Call_glInvalidateBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateBufferData));
			else {
				glf = GetProcAddress(pDll,"glInvalidateBufferData");
				if (glf != IntPtr.Zero)
					glInvalidateBufferData  = (Call_glInvalidateBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateBufferData));
			}

			glf = wglGetProcAddress("glInvalidateFramebuffer");
			if (glf != IntPtr.Zero)
				glInvalidateFramebuffer  = (Call_glInvalidateFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glInvalidateFramebuffer");
				if (glf != IntPtr.Zero)
					glInvalidateFramebuffer  = (Call_glInvalidateFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateFramebuffer));
			}

			glf = wglGetProcAddress("glInvalidateSubFramebuffer");
			if (glf != IntPtr.Zero)
				glInvalidateSubFramebuffer  = (Call_glInvalidateSubFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateSubFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glInvalidateSubFramebuffer");
				if (glf != IntPtr.Zero)
					glInvalidateSubFramebuffer  = (Call_glInvalidateSubFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateSubFramebuffer));
			}

			glf = wglGetProcAddress("glMultiDrawArraysIndirect");
			if (glf != IntPtr.Zero)
				glMultiDrawArraysIndirect  = (Call_glMultiDrawArraysIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArraysIndirect));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawArraysIndirect");
				if (glf != IntPtr.Zero)
					glMultiDrawArraysIndirect  = (Call_glMultiDrawArraysIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArraysIndirect));
			}

			glf = wglGetProcAddress("glMultiDrawElementsIndirect");
			if (glf != IntPtr.Zero)
				glMultiDrawElementsIndirect  = (Call_glMultiDrawElementsIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsIndirect));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawElementsIndirect");
				if (glf != IntPtr.Zero)
					glMultiDrawElementsIndirect  = (Call_glMultiDrawElementsIndirect)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsIndirect));
			}

			glf = wglGetProcAddress("glGetProgramInterfaceiv");
			if (glf != IntPtr.Zero)
				glGetProgramInterfaceiv  = (Call_glGetProgramInterfaceiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramInterfaceiv));
			else {
				glf = GetProcAddress(pDll,"glGetProgramInterfaceiv");
				if (glf != IntPtr.Zero)
					glGetProgramInterfaceiv  = (Call_glGetProgramInterfaceiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramInterfaceiv));
			}

			glf = wglGetProcAddress("glGetProgramResourceIndex");
			if (glf != IntPtr.Zero)
				glGetProgramResourceIndex  = (Call_glGetProgramResourceIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceIndex));
			else {
				glf = GetProcAddress(pDll,"glGetProgramResourceIndex");
				if (glf != IntPtr.Zero)
					glGetProgramResourceIndex  = (Call_glGetProgramResourceIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceIndex));
			}

			glf = wglGetProcAddress("glGetProgramResourceName");
			if (glf != IntPtr.Zero)
				glGetProgramResourceName  = (Call_glGetProgramResourceName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceName));
			else {
				glf = GetProcAddress(pDll,"glGetProgramResourceName");
				if (glf != IntPtr.Zero)
					glGetProgramResourceName  = (Call_glGetProgramResourceName)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceName));
			}

			glf = wglGetProcAddress("glGetProgramResourceiv");
			if (glf != IntPtr.Zero)
				glGetProgramResourceiv  = (Call_glGetProgramResourceiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceiv));
			else {
				glf = GetProcAddress(pDll,"glGetProgramResourceiv");
				if (glf != IntPtr.Zero)
					glGetProgramResourceiv  = (Call_glGetProgramResourceiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceiv));
			}

			glf = wglGetProcAddress("glGetProgramResourceLocation");
			if (glf != IntPtr.Zero)
				glGetProgramResourceLocation  = (Call_glGetProgramResourceLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceLocation));
			else {
				glf = GetProcAddress(pDll,"glGetProgramResourceLocation");
				if (glf != IntPtr.Zero)
					glGetProgramResourceLocation  = (Call_glGetProgramResourceLocation)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceLocation));
			}

			glf = wglGetProcAddress("glGetProgramResourceLocationIndex");
			if (glf != IntPtr.Zero)
				glGetProgramResourceLocationIndex  = (Call_glGetProgramResourceLocationIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceLocationIndex));
			else {
				glf = GetProcAddress(pDll,"glGetProgramResourceLocationIndex");
				if (glf != IntPtr.Zero)
					glGetProgramResourceLocationIndex  = (Call_glGetProgramResourceLocationIndex)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetProgramResourceLocationIndex));
			}

			glf = wglGetProcAddress("glShaderStorageBlockBinding");
			if (glf != IntPtr.Zero)
				glShaderStorageBlockBinding  = (Call_glShaderStorageBlockBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderStorageBlockBinding));
			else {
				glf = GetProcAddress(pDll,"glShaderStorageBlockBinding");
				if (glf != IntPtr.Zero)
					glShaderStorageBlockBinding  = (Call_glShaderStorageBlockBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glShaderStorageBlockBinding));
			}

			glf = wglGetProcAddress("glTexBufferRange");
			if (glf != IntPtr.Zero)
				glTexBufferRange  = (Call_glTexBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexBufferRange));
			else {
				glf = GetProcAddress(pDll,"glTexBufferRange");
				if (glf != IntPtr.Zero)
					glTexBufferRange  = (Call_glTexBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexBufferRange));
			}

			glf = wglGetProcAddress("glTexStorage2DMultisample");
			if (glf != IntPtr.Zero)
				glTexStorage2DMultisample  = (Call_glTexStorage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage2DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTexStorage2DMultisample");
				if (glf != IntPtr.Zero)
					glTexStorage2DMultisample  = (Call_glTexStorage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage2DMultisample));
			}

			glf = wglGetProcAddress("glTexStorage3DMultisample");
			if (glf != IntPtr.Zero)
				glTexStorage3DMultisample  = (Call_glTexStorage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage3DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTexStorage3DMultisample");
				if (glf != IntPtr.Zero)
					glTexStorage3DMultisample  = (Call_glTexStorage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTexStorage3DMultisample));
			}

			glf = wglGetProcAddress("glTextureView");
			if (glf != IntPtr.Zero)
				glTextureView  = (Call_glTextureView)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureView));
			else {
				glf = GetProcAddress(pDll,"glTextureView");
				if (glf != IntPtr.Zero)
					glTextureView  = (Call_glTextureView)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureView));
			}

			glf = wglGetProcAddress("glBindVertexBuffer");
			if (glf != IntPtr.Zero)
				glBindVertexBuffer  = (Call_glBindVertexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexBuffer));
			else {
				glf = GetProcAddress(pDll,"glBindVertexBuffer");
				if (glf != IntPtr.Zero)
					glBindVertexBuffer  = (Call_glBindVertexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexBuffer));
			}

			glf = wglGetProcAddress("glVertexAttribFormat");
			if (glf != IntPtr.Zero)
				glVertexAttribFormat  = (Call_glVertexAttribFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribFormat");
				if (glf != IntPtr.Zero)
					glVertexAttribFormat  = (Call_glVertexAttribFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribFormat));
			}

			glf = wglGetProcAddress("glVertexAttribIFormat");
			if (glf != IntPtr.Zero)
				glVertexAttribIFormat  = (Call_glVertexAttribIFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribIFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribIFormat");
				if (glf != IntPtr.Zero)
					glVertexAttribIFormat  = (Call_glVertexAttribIFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribIFormat));
			}

			glf = wglGetProcAddress("glVertexAttribLFormat");
			if (glf != IntPtr.Zero)
				glVertexAttribLFormat  = (Call_glVertexAttribLFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribLFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribLFormat");
				if (glf != IntPtr.Zero)
					glVertexAttribLFormat  = (Call_glVertexAttribLFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribLFormat));
			}

			glf = wglGetProcAddress("glVertexAttribBinding");
			if (glf != IntPtr.Zero)
				glVertexAttribBinding  = (Call_glVertexAttribBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribBinding));
			else {
				glf = GetProcAddress(pDll,"glVertexAttribBinding");
				if (glf != IntPtr.Zero)
					glVertexAttribBinding  = (Call_glVertexAttribBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexAttribBinding));
			}

			glf = wglGetProcAddress("glVertexBindingDivisor");
			if (glf != IntPtr.Zero)
				glVertexBindingDivisor  = (Call_glVertexBindingDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexBindingDivisor));
			else {
				glf = GetProcAddress(pDll,"glVertexBindingDivisor");
				if (glf != IntPtr.Zero)
					glVertexBindingDivisor  = (Call_glVertexBindingDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexBindingDivisor));
			}

			glf = wglGetProcAddress("glDebugMessageControl");
			if (glf != IntPtr.Zero)
				glDebugMessageControl  = (Call_glDebugMessageControl)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageControl));
			else {
				glf = GetProcAddress(pDll,"glDebugMessageControl");
				if (glf != IntPtr.Zero)
					glDebugMessageControl  = (Call_glDebugMessageControl)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageControl));
			}

			glf = wglGetProcAddress("glDebugMessageInsert");
			if (glf != IntPtr.Zero)
				glDebugMessageInsert  = (Call_glDebugMessageInsert)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageInsert));
			else {
				glf = GetProcAddress(pDll,"glDebugMessageInsert");
				if (glf != IntPtr.Zero)
					glDebugMessageInsert  = (Call_glDebugMessageInsert)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageInsert));
			}

			glf = wglGetProcAddress("glDebugMessageCallback");
			if (glf != IntPtr.Zero)
				glDebugMessageCallback  = (Call_glDebugMessageCallback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageCallback));
			else {
				glf = GetProcAddress(pDll,"glDebugMessageCallback");
				if (glf != IntPtr.Zero)
					glDebugMessageCallback  = (Call_glDebugMessageCallback)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDebugMessageCallback));
			}

			glf = wglGetProcAddress("glGetDebugMessageLog");
			if (glf != IntPtr.Zero)
				glGetDebugMessageLog  = (Call_glGetDebugMessageLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDebugMessageLog));
			else {
				glf = GetProcAddress(pDll,"glGetDebugMessageLog");
				if (glf != IntPtr.Zero)
					glGetDebugMessageLog  = (Call_glGetDebugMessageLog)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetDebugMessageLog));
			}

			glf = wglGetProcAddress("glPushDebugGroup");
			if (glf != IntPtr.Zero)
				glPushDebugGroup  = (Call_glPushDebugGroup)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushDebugGroup));
			else {
				glf = GetProcAddress(pDll,"glPushDebugGroup");
				if (glf != IntPtr.Zero)
					glPushDebugGroup  = (Call_glPushDebugGroup)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPushDebugGroup));
			}

			glf = wglGetProcAddress("glPopDebugGroup");
			if (glf != IntPtr.Zero)
				glPopDebugGroup  = (Call_glPopDebugGroup)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopDebugGroup));
			else {
				glf = GetProcAddress(pDll,"glPopDebugGroup");
				if (glf != IntPtr.Zero)
					glPopDebugGroup  = (Call_glPopDebugGroup)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPopDebugGroup));
			}

			glf = wglGetProcAddress("glObjectLabel");
			if (glf != IntPtr.Zero)
				glObjectLabel  = (Call_glObjectLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glObjectLabel));
			else {
				glf = GetProcAddress(pDll,"glObjectLabel");
				if (glf != IntPtr.Zero)
					glObjectLabel  = (Call_glObjectLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glObjectLabel));
			}

			glf = wglGetProcAddress("glGetObjectLabel");
			if (glf != IntPtr.Zero)
				glGetObjectLabel  = (Call_glGetObjectLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetObjectLabel));
			else {
				glf = GetProcAddress(pDll,"glGetObjectLabel");
				if (glf != IntPtr.Zero)
					glGetObjectLabel  = (Call_glGetObjectLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetObjectLabel));
			}

			glf = wglGetProcAddress("glObjectPtrLabel");
			if (glf != IntPtr.Zero)
				glObjectPtrLabel  = (Call_glObjectPtrLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glObjectPtrLabel));
			else {
				glf = GetProcAddress(pDll,"glObjectPtrLabel");
				if (glf != IntPtr.Zero)
					glObjectPtrLabel  = (Call_glObjectPtrLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glObjectPtrLabel));
			}

			glf = wglGetProcAddress("glGetObjectPtrLabel");
			if (glf != IntPtr.Zero)
				glGetObjectPtrLabel  = (Call_glGetObjectPtrLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetObjectPtrLabel));
			else {
				glf = GetProcAddress(pDll,"glGetObjectPtrLabel");
				if (glf != IntPtr.Zero)
					glGetObjectPtrLabel  = (Call_glGetObjectPtrLabel)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetObjectPtrLabel));
			}

			glf = wglGetProcAddress("glBufferStorage");
			if (glf != IntPtr.Zero)
				glBufferStorage  = (Call_glBufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferStorage));
			else {
				glf = GetProcAddress(pDll,"glBufferStorage");
				if (glf != IntPtr.Zero)
					glBufferStorage  = (Call_glBufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBufferStorage));
			}

			glf = wglGetProcAddress("glClearTexImage");
			if (glf != IntPtr.Zero)
				glClearTexImage  = (Call_glClearTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearTexImage));
			else {
				glf = GetProcAddress(pDll,"glClearTexImage");
				if (glf != IntPtr.Zero)
					glClearTexImage  = (Call_glClearTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearTexImage));
			}

			glf = wglGetProcAddress("glClearTexSubImage");
			if (glf != IntPtr.Zero)
				glClearTexSubImage  = (Call_glClearTexSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearTexSubImage));
			else {
				glf = GetProcAddress(pDll,"glClearTexSubImage");
				if (glf != IntPtr.Zero)
					glClearTexSubImage  = (Call_glClearTexSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearTexSubImage));
			}

			glf = wglGetProcAddress("glBindBuffersBase");
			if (glf != IntPtr.Zero)
				glBindBuffersBase  = (Call_glBindBuffersBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffersBase));
			else {
				glf = GetProcAddress(pDll,"glBindBuffersBase");
				if (glf != IntPtr.Zero)
					glBindBuffersBase  = (Call_glBindBuffersBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffersBase));
			}

			glf = wglGetProcAddress("glBindBuffersRange");
			if (glf != IntPtr.Zero)
				glBindBuffersRange  = (Call_glBindBuffersRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffersRange));
			else {
				glf = GetProcAddress(pDll,"glBindBuffersRange");
				if (glf != IntPtr.Zero)
					glBindBuffersRange  = (Call_glBindBuffersRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindBuffersRange));
			}

			glf = wglGetProcAddress("glBindTextures");
			if (glf != IntPtr.Zero)
				glBindTextures  = (Call_glBindTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTextures));
			else {
				glf = GetProcAddress(pDll,"glBindTextures");
				if (glf != IntPtr.Zero)
					glBindTextures  = (Call_glBindTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTextures));
			}

			glf = wglGetProcAddress("glBindSamplers");
			if (glf != IntPtr.Zero)
				glBindSamplers  = (Call_glBindSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindSamplers));
			else {
				glf = GetProcAddress(pDll,"glBindSamplers");
				if (glf != IntPtr.Zero)
					glBindSamplers  = (Call_glBindSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindSamplers));
			}

			glf = wglGetProcAddress("glBindImageTextures");
			if (glf != IntPtr.Zero)
				glBindImageTextures  = (Call_glBindImageTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindImageTextures));
			else {
				glf = GetProcAddress(pDll,"glBindImageTextures");
				if (glf != IntPtr.Zero)
					glBindImageTextures  = (Call_glBindImageTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindImageTextures));
			}

			glf = wglGetProcAddress("glBindVertexBuffers");
			if (glf != IntPtr.Zero)
				glBindVertexBuffers  = (Call_glBindVertexBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexBuffers));
			else {
				glf = GetProcAddress(pDll,"glBindVertexBuffers");
				if (glf != IntPtr.Zero)
					glBindVertexBuffers  = (Call_glBindVertexBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindVertexBuffers));
			}

			glf = wglGetProcAddress("glClipControl");
			if (glf != IntPtr.Zero)
				glClipControl  = (Call_glClipControl)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClipControl));
			else {
				glf = GetProcAddress(pDll,"glClipControl");
				if (glf != IntPtr.Zero)
					glClipControl  = (Call_glClipControl)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClipControl));
			}

			glf = wglGetProcAddress("glCreateTransformFeedbacks");
			if (glf != IntPtr.Zero)
				glCreateTransformFeedbacks  = (Call_glCreateTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateTransformFeedbacks));
			else {
				glf = GetProcAddress(pDll,"glCreateTransformFeedbacks");
				if (glf != IntPtr.Zero)
					glCreateTransformFeedbacks  = (Call_glCreateTransformFeedbacks)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateTransformFeedbacks));
			}

			glf = wglGetProcAddress("glTransformFeedbackBufferBase");
			if (glf != IntPtr.Zero)
				glTransformFeedbackBufferBase  = (Call_glTransformFeedbackBufferBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackBufferBase));
			else {
				glf = GetProcAddress(pDll,"glTransformFeedbackBufferBase");
				if (glf != IntPtr.Zero)
					glTransformFeedbackBufferBase  = (Call_glTransformFeedbackBufferBase)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackBufferBase));
			}

			glf = wglGetProcAddress("glTransformFeedbackBufferRange");
			if (glf != IntPtr.Zero)
				glTransformFeedbackBufferRange  = (Call_glTransformFeedbackBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackBufferRange));
			else {
				glf = GetProcAddress(pDll,"glTransformFeedbackBufferRange");
				if (glf != IntPtr.Zero)
					glTransformFeedbackBufferRange  = (Call_glTransformFeedbackBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTransformFeedbackBufferRange));
			}

			glf = wglGetProcAddress("glGetTransformFeedbackiv");
			if (glf != IntPtr.Zero)
				glGetTransformFeedbackiv  = (Call_glGetTransformFeedbackiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbackiv));
			else {
				glf = GetProcAddress(pDll,"glGetTransformFeedbackiv");
				if (glf != IntPtr.Zero)
					glGetTransformFeedbackiv  = (Call_glGetTransformFeedbackiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbackiv));
			}

			glf = wglGetProcAddress("glGetTransformFeedbacki_v");
			if (glf != IntPtr.Zero)
				glGetTransformFeedbacki_v  = (Call_glGetTransformFeedbacki_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbacki_v));
			else {
				glf = GetProcAddress(pDll,"glGetTransformFeedbacki_v");
				if (glf != IntPtr.Zero)
					glGetTransformFeedbacki_v  = (Call_glGetTransformFeedbacki_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbacki_v));
			}

			glf = wglGetProcAddress("glGetTransformFeedbacki64_v");
			if (glf != IntPtr.Zero)
				glGetTransformFeedbacki64_v  = (Call_glGetTransformFeedbacki64_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbacki64_v));
			else {
				glf = GetProcAddress(pDll,"glGetTransformFeedbacki64_v");
				if (glf != IntPtr.Zero)
					glGetTransformFeedbacki64_v  = (Call_glGetTransformFeedbacki64_v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTransformFeedbacki64_v));
			}

			glf = wglGetProcAddress("glCreateBuffers");
			if (glf != IntPtr.Zero)
				glCreateBuffers  = (Call_glCreateBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateBuffers));
			else {
				glf = GetProcAddress(pDll,"glCreateBuffers");
				if (glf != IntPtr.Zero)
					glCreateBuffers  = (Call_glCreateBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateBuffers));
			}

			glf = wglGetProcAddress("glNamedBufferStorage");
			if (glf != IntPtr.Zero)
				glNamedBufferStorage  = (Call_glNamedBufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferStorage));
			else {
				glf = GetProcAddress(pDll,"glNamedBufferStorage");
				if (glf != IntPtr.Zero)
					glNamedBufferStorage  = (Call_glNamedBufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferStorage));
			}

			glf = wglGetProcAddress("glNamedBufferData");
			if (glf != IntPtr.Zero)
				glNamedBufferData  = (Call_glNamedBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferData));
			else {
				glf = GetProcAddress(pDll,"glNamedBufferData");
				if (glf != IntPtr.Zero)
					glNamedBufferData  = (Call_glNamedBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferData));
			}

			glf = wglGetProcAddress("glNamedBufferSubData");
			if (glf != IntPtr.Zero)
				glNamedBufferSubData  = (Call_glNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glNamedBufferSubData");
				if (glf != IntPtr.Zero)
					glNamedBufferSubData  = (Call_glNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedBufferSubData));
			}

			glf = wglGetProcAddress("glCopyNamedBufferSubData");
			if (glf != IntPtr.Zero)
				glCopyNamedBufferSubData  = (Call_glCopyNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyNamedBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glCopyNamedBufferSubData");
				if (glf != IntPtr.Zero)
					glCopyNamedBufferSubData  = (Call_glCopyNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyNamedBufferSubData));
			}

			glf = wglGetProcAddress("glClearNamedBufferData");
			if (glf != IntPtr.Zero)
				glClearNamedBufferData  = (Call_glClearNamedBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedBufferData));
			else {
				glf = GetProcAddress(pDll,"glClearNamedBufferData");
				if (glf != IntPtr.Zero)
					glClearNamedBufferData  = (Call_glClearNamedBufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedBufferData));
			}

			glf = wglGetProcAddress("glClearNamedBufferSubData");
			if (glf != IntPtr.Zero)
				glClearNamedBufferSubData  = (Call_glClearNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glClearNamedBufferSubData");
				if (glf != IntPtr.Zero)
					glClearNamedBufferSubData  = (Call_glClearNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedBufferSubData));
			}

			glf = wglGetProcAddress("glMapNamedBuffer");
			if (glf != IntPtr.Zero)
				glMapNamedBuffer  = (Call_glMapNamedBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapNamedBuffer));
			else {
				glf = GetProcAddress(pDll,"glMapNamedBuffer");
				if (glf != IntPtr.Zero)
					glMapNamedBuffer  = (Call_glMapNamedBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapNamedBuffer));
			}

			glf = wglGetProcAddress("glMapNamedBufferRange");
			if (glf != IntPtr.Zero)
				glMapNamedBufferRange  = (Call_glMapNamedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapNamedBufferRange));
			else {
				glf = GetProcAddress(pDll,"glMapNamedBufferRange");
				if (glf != IntPtr.Zero)
					glMapNamedBufferRange  = (Call_glMapNamedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMapNamedBufferRange));
			}

			glf = wglGetProcAddress("glUnmapNamedBuffer");
			if (glf != IntPtr.Zero)
				glUnmapNamedBuffer  = (Call_glUnmapNamedBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUnmapNamedBuffer));
			else {
				glf = GetProcAddress(pDll,"glUnmapNamedBuffer");
				if (glf != IntPtr.Zero)
					glUnmapNamedBuffer  = (Call_glUnmapNamedBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glUnmapNamedBuffer));
			}

			glf = wglGetProcAddress("glFlushMappedNamedBufferRange");
			if (glf != IntPtr.Zero)
				glFlushMappedNamedBufferRange  = (Call_glFlushMappedNamedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlushMappedNamedBufferRange));
			else {
				glf = GetProcAddress(pDll,"glFlushMappedNamedBufferRange");
				if (glf != IntPtr.Zero)
					glFlushMappedNamedBufferRange  = (Call_glFlushMappedNamedBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glFlushMappedNamedBufferRange));
			}

			glf = wglGetProcAddress("glGetNamedBufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetNamedBufferParameteriv  = (Call_glGetNamedBufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetNamedBufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetNamedBufferParameteriv  = (Call_glGetNamedBufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferParameteriv));
			}

			glf = wglGetProcAddress("glGetNamedBufferParameteri64v");
			if (glf != IntPtr.Zero)
				glGetNamedBufferParameteri64v  = (Call_glGetNamedBufferParameteri64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferParameteri64v));
			else {
				glf = GetProcAddress(pDll,"glGetNamedBufferParameteri64v");
				if (glf != IntPtr.Zero)
					glGetNamedBufferParameteri64v  = (Call_glGetNamedBufferParameteri64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferParameteri64v));
			}

			glf = wglGetProcAddress("glGetNamedBufferPointerv");
			if (glf != IntPtr.Zero)
				glGetNamedBufferPointerv  = (Call_glGetNamedBufferPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferPointerv));
			else {
				glf = GetProcAddress(pDll,"glGetNamedBufferPointerv");
				if (glf != IntPtr.Zero)
					glGetNamedBufferPointerv  = (Call_glGetNamedBufferPointerv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferPointerv));
			}

			glf = wglGetProcAddress("glGetNamedBufferSubData");
			if (glf != IntPtr.Zero)
				glGetNamedBufferSubData  = (Call_glGetNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferSubData));
			else {
				glf = GetProcAddress(pDll,"glGetNamedBufferSubData");
				if (glf != IntPtr.Zero)
					glGetNamedBufferSubData  = (Call_glGetNamedBufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedBufferSubData));
			}

			glf = wglGetProcAddress("glCreateFramebuffers");
			if (glf != IntPtr.Zero)
				glCreateFramebuffers  = (Call_glCreateFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateFramebuffers));
			else {
				glf = GetProcAddress(pDll,"glCreateFramebuffers");
				if (glf != IntPtr.Zero)
					glCreateFramebuffers  = (Call_glCreateFramebuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateFramebuffers));
			}

			glf = wglGetProcAddress("glNamedFramebufferRenderbuffer");
			if (glf != IntPtr.Zero)
				glNamedFramebufferRenderbuffer  = (Call_glNamedFramebufferRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferRenderbuffer));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferRenderbuffer");
				if (glf != IntPtr.Zero)
					glNamedFramebufferRenderbuffer  = (Call_glNamedFramebufferRenderbuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferRenderbuffer));
			}

			glf = wglGetProcAddress("glNamedFramebufferParameteri");
			if (glf != IntPtr.Zero)
				glNamedFramebufferParameteri  = (Call_glNamedFramebufferParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferParameteri));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferParameteri");
				if (glf != IntPtr.Zero)
					glNamedFramebufferParameteri  = (Call_glNamedFramebufferParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferParameteri));
			}

			glf = wglGetProcAddress("glNamedFramebufferTexture");
			if (glf != IntPtr.Zero)
				glNamedFramebufferTexture  = (Call_glNamedFramebufferTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferTexture));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferTexture");
				if (glf != IntPtr.Zero)
					glNamedFramebufferTexture  = (Call_glNamedFramebufferTexture)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferTexture));
			}

			glf = wglGetProcAddress("glNamedFramebufferTextureLayer");
			if (glf != IntPtr.Zero)
				glNamedFramebufferTextureLayer  = (Call_glNamedFramebufferTextureLayer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferTextureLayer));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferTextureLayer");
				if (glf != IntPtr.Zero)
					glNamedFramebufferTextureLayer  = (Call_glNamedFramebufferTextureLayer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferTextureLayer));
			}

			glf = wglGetProcAddress("glNamedFramebufferDrawBuffer");
			if (glf != IntPtr.Zero)
				glNamedFramebufferDrawBuffer  = (Call_glNamedFramebufferDrawBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferDrawBuffer));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferDrawBuffer");
				if (glf != IntPtr.Zero)
					glNamedFramebufferDrawBuffer  = (Call_glNamedFramebufferDrawBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferDrawBuffer));
			}

			glf = wglGetProcAddress("glNamedFramebufferDrawBuffers");
			if (glf != IntPtr.Zero)
				glNamedFramebufferDrawBuffers  = (Call_glNamedFramebufferDrawBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferDrawBuffers));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferDrawBuffers");
				if (glf != IntPtr.Zero)
					glNamedFramebufferDrawBuffers  = (Call_glNamedFramebufferDrawBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferDrawBuffers));
			}

			glf = wglGetProcAddress("glNamedFramebufferReadBuffer");
			if (glf != IntPtr.Zero)
				glNamedFramebufferReadBuffer  = (Call_glNamedFramebufferReadBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferReadBuffer));
			else {
				glf = GetProcAddress(pDll,"glNamedFramebufferReadBuffer");
				if (glf != IntPtr.Zero)
					glNamedFramebufferReadBuffer  = (Call_glNamedFramebufferReadBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedFramebufferReadBuffer));
			}

			glf = wglGetProcAddress("glInvalidateNamedFramebufferData");
			if (glf != IntPtr.Zero)
				glInvalidateNamedFramebufferData  = (Call_glInvalidateNamedFramebufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateNamedFramebufferData));
			else {
				glf = GetProcAddress(pDll,"glInvalidateNamedFramebufferData");
				if (glf != IntPtr.Zero)
					glInvalidateNamedFramebufferData  = (Call_glInvalidateNamedFramebufferData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateNamedFramebufferData));
			}

			glf = wglGetProcAddress("glInvalidateNamedFramebufferSubData");
			if (glf != IntPtr.Zero)
				glInvalidateNamedFramebufferSubData  = (Call_glInvalidateNamedFramebufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateNamedFramebufferSubData));
			else {
				glf = GetProcAddress(pDll,"glInvalidateNamedFramebufferSubData");
				if (glf != IntPtr.Zero)
					glInvalidateNamedFramebufferSubData  = (Call_glInvalidateNamedFramebufferSubData)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glInvalidateNamedFramebufferSubData));
			}

			glf = wglGetProcAddress("glClearNamedFramebufferiv");
			if (glf != IntPtr.Zero)
				glClearNamedFramebufferiv  = (Call_glClearNamedFramebufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferiv));
			else {
				glf = GetProcAddress(pDll,"glClearNamedFramebufferiv");
				if (glf != IntPtr.Zero)
					glClearNamedFramebufferiv  = (Call_glClearNamedFramebufferiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferiv));
			}

			glf = wglGetProcAddress("glClearNamedFramebufferuiv");
			if (glf != IntPtr.Zero)
				glClearNamedFramebufferuiv  = (Call_glClearNamedFramebufferuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferuiv));
			else {
				glf = GetProcAddress(pDll,"glClearNamedFramebufferuiv");
				if (glf != IntPtr.Zero)
					glClearNamedFramebufferuiv  = (Call_glClearNamedFramebufferuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferuiv));
			}

			glf = wglGetProcAddress("glClearNamedFramebufferfv");
			if (glf != IntPtr.Zero)
				glClearNamedFramebufferfv  = (Call_glClearNamedFramebufferfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferfv));
			else {
				glf = GetProcAddress(pDll,"glClearNamedFramebufferfv");
				if (glf != IntPtr.Zero)
					glClearNamedFramebufferfv  = (Call_glClearNamedFramebufferfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferfv));
			}

			glf = wglGetProcAddress("glClearNamedFramebufferfi");
			if (glf != IntPtr.Zero)
				glClearNamedFramebufferfi  = (Call_glClearNamedFramebufferfi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferfi));
			else {
				glf = GetProcAddress(pDll,"glClearNamedFramebufferfi");
				if (glf != IntPtr.Zero)
					glClearNamedFramebufferfi  = (Call_glClearNamedFramebufferfi)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glClearNamedFramebufferfi));
			}

			glf = wglGetProcAddress("glBlitNamedFramebuffer");
			if (glf != IntPtr.Zero)
				glBlitNamedFramebuffer  = (Call_glBlitNamedFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlitNamedFramebuffer));
			else {
				glf = GetProcAddress(pDll,"glBlitNamedFramebuffer");
				if (glf != IntPtr.Zero)
					glBlitNamedFramebuffer  = (Call_glBlitNamedFramebuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBlitNamedFramebuffer));
			}

			glf = wglGetProcAddress("glCheckNamedFramebufferStatus");
			if (glf != IntPtr.Zero)
				glCheckNamedFramebufferStatus  = (Call_glCheckNamedFramebufferStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCheckNamedFramebufferStatus));
			else {
				glf = GetProcAddress(pDll,"glCheckNamedFramebufferStatus");
				if (glf != IntPtr.Zero)
					glCheckNamedFramebufferStatus  = (Call_glCheckNamedFramebufferStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCheckNamedFramebufferStatus));
			}

			glf = wglGetProcAddress("glGetNamedFramebufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetNamedFramebufferParameteriv  = (Call_glGetNamedFramebufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedFramebufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetNamedFramebufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetNamedFramebufferParameteriv  = (Call_glGetNamedFramebufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedFramebufferParameteriv));
			}

			glf = wglGetProcAddress("glGetNamedFramebufferAttachmentParameteriv");
			if (glf != IntPtr.Zero)
				glGetNamedFramebufferAttachmentParameteriv  = (Call_glGetNamedFramebufferAttachmentParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedFramebufferAttachmentParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetNamedFramebufferAttachmentParameteriv");
				if (glf != IntPtr.Zero)
					glGetNamedFramebufferAttachmentParameteriv  = (Call_glGetNamedFramebufferAttachmentParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedFramebufferAttachmentParameteriv));
			}

			glf = wglGetProcAddress("glCreateRenderbuffers");
			if (glf != IntPtr.Zero)
				glCreateRenderbuffers  = (Call_glCreateRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateRenderbuffers));
			else {
				glf = GetProcAddress(pDll,"glCreateRenderbuffers");
				if (glf != IntPtr.Zero)
					glCreateRenderbuffers  = (Call_glCreateRenderbuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateRenderbuffers));
			}

			glf = wglGetProcAddress("glNamedRenderbufferStorage");
			if (glf != IntPtr.Zero)
				glNamedRenderbufferStorage  = (Call_glNamedRenderbufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedRenderbufferStorage));
			else {
				glf = GetProcAddress(pDll,"glNamedRenderbufferStorage");
				if (glf != IntPtr.Zero)
					glNamedRenderbufferStorage  = (Call_glNamedRenderbufferStorage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedRenderbufferStorage));
			}

			glf = wglGetProcAddress("glNamedRenderbufferStorageMultisample");
			if (glf != IntPtr.Zero)
				glNamedRenderbufferStorageMultisample  = (Call_glNamedRenderbufferStorageMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedRenderbufferStorageMultisample));
			else {
				glf = GetProcAddress(pDll,"glNamedRenderbufferStorageMultisample");
				if (glf != IntPtr.Zero)
					glNamedRenderbufferStorageMultisample  = (Call_glNamedRenderbufferStorageMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glNamedRenderbufferStorageMultisample));
			}

			glf = wglGetProcAddress("glGetNamedRenderbufferParameteriv");
			if (glf != IntPtr.Zero)
				glGetNamedRenderbufferParameteriv  = (Call_glGetNamedRenderbufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedRenderbufferParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetNamedRenderbufferParameteriv");
				if (glf != IntPtr.Zero)
					glGetNamedRenderbufferParameteriv  = (Call_glGetNamedRenderbufferParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetNamedRenderbufferParameteriv));
			}

			glf = wglGetProcAddress("glCreateTextures");
			if (glf != IntPtr.Zero)
				glCreateTextures  = (Call_glCreateTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateTextures));
			else {
				glf = GetProcAddress(pDll,"glCreateTextures");
				if (glf != IntPtr.Zero)
					glCreateTextures  = (Call_glCreateTextures)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateTextures));
			}

			glf = wglGetProcAddress("glTextureBuffer");
			if (glf != IntPtr.Zero)
				glTextureBuffer  = (Call_glTextureBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBuffer));
			else {
				glf = GetProcAddress(pDll,"glTextureBuffer");
				if (glf != IntPtr.Zero)
					glTextureBuffer  = (Call_glTextureBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBuffer));
			}

			glf = wglGetProcAddress("glTextureBufferRange");
			if (glf != IntPtr.Zero)
				glTextureBufferRange  = (Call_glTextureBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBufferRange));
			else {
				glf = GetProcAddress(pDll,"glTextureBufferRange");
				if (glf != IntPtr.Zero)
					glTextureBufferRange  = (Call_glTextureBufferRange)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBufferRange));
			}

			glf = wglGetProcAddress("glTextureStorage1D");
			if (glf != IntPtr.Zero)
				glTextureStorage1D  = (Call_glTextureStorage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage1D));
			else {
				glf = GetProcAddress(pDll,"glTextureStorage1D");
				if (glf != IntPtr.Zero)
					glTextureStorage1D  = (Call_glTextureStorage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage1D));
			}

			glf = wglGetProcAddress("glTextureStorage2D");
			if (glf != IntPtr.Zero)
				glTextureStorage2D  = (Call_glTextureStorage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage2D));
			else {
				glf = GetProcAddress(pDll,"glTextureStorage2D");
				if (glf != IntPtr.Zero)
					glTextureStorage2D  = (Call_glTextureStorage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage2D));
			}

			glf = wglGetProcAddress("glTextureStorage3D");
			if (glf != IntPtr.Zero)
				glTextureStorage3D  = (Call_glTextureStorage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage3D));
			else {
				glf = GetProcAddress(pDll,"glTextureStorage3D");
				if (glf != IntPtr.Zero)
					glTextureStorage3D  = (Call_glTextureStorage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage3D));
			}

			glf = wglGetProcAddress("glTextureStorage2DMultisample");
			if (glf != IntPtr.Zero)
				glTextureStorage2DMultisample  = (Call_glTextureStorage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage2DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTextureStorage2DMultisample");
				if (glf != IntPtr.Zero)
					glTextureStorage2DMultisample  = (Call_glTextureStorage2DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage2DMultisample));
			}

			glf = wglGetProcAddress("glTextureStorage3DMultisample");
			if (glf != IntPtr.Zero)
				glTextureStorage3DMultisample  = (Call_glTextureStorage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage3DMultisample));
			else {
				glf = GetProcAddress(pDll,"glTextureStorage3DMultisample");
				if (glf != IntPtr.Zero)
					glTextureStorage3DMultisample  = (Call_glTextureStorage3DMultisample)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureStorage3DMultisample));
			}

			glf = wglGetProcAddress("glTextureSubImage1D");
			if (glf != IntPtr.Zero)
				glTextureSubImage1D  = (Call_glTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glTextureSubImage1D");
				if (glf != IntPtr.Zero)
					glTextureSubImage1D  = (Call_glTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage1D));
			}

			glf = wglGetProcAddress("glTextureSubImage2D");
			if (glf != IntPtr.Zero)
				glTextureSubImage2D  = (Call_glTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glTextureSubImage2D");
				if (glf != IntPtr.Zero)
					glTextureSubImage2D  = (Call_glTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage2D));
			}

			glf = wglGetProcAddress("glTextureSubImage3D");
			if (glf != IntPtr.Zero)
				glTextureSubImage3D  = (Call_glTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glTextureSubImage3D");
				if (glf != IntPtr.Zero)
					glTextureSubImage3D  = (Call_glTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureSubImage3D));
			}

			glf = wglGetProcAddress("glCompressedTextureSubImage1D");
			if (glf != IntPtr.Zero)
				glCompressedTextureSubImage1D  = (Call_glCompressedTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTextureSubImage1D");
				if (glf != IntPtr.Zero)
					glCompressedTextureSubImage1D  = (Call_glCompressedTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage1D));
			}

			glf = wglGetProcAddress("glCompressedTextureSubImage2D");
			if (glf != IntPtr.Zero)
				glCompressedTextureSubImage2D  = (Call_glCompressedTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTextureSubImage2D");
				if (glf != IntPtr.Zero)
					glCompressedTextureSubImage2D  = (Call_glCompressedTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage2D));
			}

			glf = wglGetProcAddress("glCompressedTextureSubImage3D");
			if (glf != IntPtr.Zero)
				glCompressedTextureSubImage3D  = (Call_glCompressedTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glCompressedTextureSubImage3D");
				if (glf != IntPtr.Zero)
					glCompressedTextureSubImage3D  = (Call_glCompressedTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCompressedTextureSubImage3D));
			}

			glf = wglGetProcAddress("glCopyTextureSubImage1D");
			if (glf != IntPtr.Zero)
				glCopyTextureSubImage1D  = (Call_glCopyTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage1D));
			else {
				glf = GetProcAddress(pDll,"glCopyTextureSubImage1D");
				if (glf != IntPtr.Zero)
					glCopyTextureSubImage1D  = (Call_glCopyTextureSubImage1D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage1D));
			}

			glf = wglGetProcAddress("glCopyTextureSubImage2D");
			if (glf != IntPtr.Zero)
				glCopyTextureSubImage2D  = (Call_glCopyTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage2D));
			else {
				glf = GetProcAddress(pDll,"glCopyTextureSubImage2D");
				if (glf != IntPtr.Zero)
					glCopyTextureSubImage2D  = (Call_glCopyTextureSubImage2D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage2D));
			}

			glf = wglGetProcAddress("glCopyTextureSubImage3D");
			if (glf != IntPtr.Zero)
				glCopyTextureSubImage3D  = (Call_glCopyTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage3D));
			else {
				glf = GetProcAddress(pDll,"glCopyTextureSubImage3D");
				if (glf != IntPtr.Zero)
					glCopyTextureSubImage3D  = (Call_glCopyTextureSubImage3D)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCopyTextureSubImage3D));
			}

			glf = wglGetProcAddress("glTextureParameterf");
			if (glf != IntPtr.Zero)
				glTextureParameterf  = (Call_glTextureParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterf));
			else {
				glf = GetProcAddress(pDll,"glTextureParameterf");
				if (glf != IntPtr.Zero)
					glTextureParameterf  = (Call_glTextureParameterf)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterf));
			}

			glf = wglGetProcAddress("glTextureParameterfv");
			if (glf != IntPtr.Zero)
				glTextureParameterfv  = (Call_glTextureParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterfv));
			else {
				glf = GetProcAddress(pDll,"glTextureParameterfv");
				if (glf != IntPtr.Zero)
					glTextureParameterfv  = (Call_glTextureParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterfv));
			}

			glf = wglGetProcAddress("glTextureParameteri");
			if (glf != IntPtr.Zero)
				glTextureParameteri  = (Call_glTextureParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameteri));
			else {
				glf = GetProcAddress(pDll,"glTextureParameteri");
				if (glf != IntPtr.Zero)
					glTextureParameteri  = (Call_glTextureParameteri)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameteri));
			}

			glf = wglGetProcAddress("glTextureParameterIiv");
			if (glf != IntPtr.Zero)
				glTextureParameterIiv  = (Call_glTextureParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glTextureParameterIiv");
				if (glf != IntPtr.Zero)
					glTextureParameterIiv  = (Call_glTextureParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterIiv));
			}

			glf = wglGetProcAddress("glTextureParameterIuiv");
			if (glf != IntPtr.Zero)
				glTextureParameterIuiv  = (Call_glTextureParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glTextureParameterIuiv");
				if (glf != IntPtr.Zero)
					glTextureParameterIuiv  = (Call_glTextureParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameterIuiv));
			}

			glf = wglGetProcAddress("glTextureParameteriv");
			if (glf != IntPtr.Zero)
				glTextureParameteriv  = (Call_glTextureParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameteriv));
			else {
				glf = GetProcAddress(pDll,"glTextureParameteriv");
				if (glf != IntPtr.Zero)
					glTextureParameteriv  = (Call_glTextureParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureParameteriv));
			}

			glf = wglGetProcAddress("glGenerateTextureMipmap");
			if (glf != IntPtr.Zero)
				glGenerateTextureMipmap  = (Call_glGenerateTextureMipmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenerateTextureMipmap));
			else {
				glf = GetProcAddress(pDll,"glGenerateTextureMipmap");
				if (glf != IntPtr.Zero)
					glGenerateTextureMipmap  = (Call_glGenerateTextureMipmap)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGenerateTextureMipmap));
			}

			glf = wglGetProcAddress("glBindTextureUnit");
			if (glf != IntPtr.Zero)
				glBindTextureUnit  = (Call_glBindTextureUnit)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTextureUnit));
			else {
				glf = GetProcAddress(pDll,"glBindTextureUnit");
				if (glf != IntPtr.Zero)
					glBindTextureUnit  = (Call_glBindTextureUnit)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glBindTextureUnit));
			}

			glf = wglGetProcAddress("glGetTextureImage");
			if (glf != IntPtr.Zero)
				glGetTextureImage  = (Call_glGetTextureImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureImage));
			else {
				glf = GetProcAddress(pDll,"glGetTextureImage");
				if (glf != IntPtr.Zero)
					glGetTextureImage  = (Call_glGetTextureImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureImage));
			}

			glf = wglGetProcAddress("glGetCompressedTextureImage");
			if (glf != IntPtr.Zero)
				glGetCompressedTextureImage  = (Call_glGetCompressedTextureImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTextureImage));
			else {
				glf = GetProcAddress(pDll,"glGetCompressedTextureImage");
				if (glf != IntPtr.Zero)
					glGetCompressedTextureImage  = (Call_glGetCompressedTextureImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTextureImage));
			}

			glf = wglGetProcAddress("glGetTextureLevelParameterfv");
			if (glf != IntPtr.Zero)
				glGetTextureLevelParameterfv  = (Call_glGetTextureLevelParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureLevelParameterfv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureLevelParameterfv");
				if (glf != IntPtr.Zero)
					glGetTextureLevelParameterfv  = (Call_glGetTextureLevelParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureLevelParameterfv));
			}

			glf = wglGetProcAddress("glGetTextureLevelParameteriv");
			if (glf != IntPtr.Zero)
				glGetTextureLevelParameteriv  = (Call_glGetTextureLevelParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureLevelParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureLevelParameteriv");
				if (glf != IntPtr.Zero)
					glGetTextureLevelParameteriv  = (Call_glGetTextureLevelParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureLevelParameteriv));
			}

			glf = wglGetProcAddress("glGetTextureParameterfv");
			if (glf != IntPtr.Zero)
				glGetTextureParameterfv  = (Call_glGetTextureParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterfv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureParameterfv");
				if (glf != IntPtr.Zero)
					glGetTextureParameterfv  = (Call_glGetTextureParameterfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterfv));
			}

			glf = wglGetProcAddress("glGetTextureParameterIiv");
			if (glf != IntPtr.Zero)
				glGetTextureParameterIiv  = (Call_glGetTextureParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterIiv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureParameterIiv");
				if (glf != IntPtr.Zero)
					glGetTextureParameterIiv  = (Call_glGetTextureParameterIiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterIiv));
			}

			glf = wglGetProcAddress("glGetTextureParameterIuiv");
			if (glf != IntPtr.Zero)
				glGetTextureParameterIuiv  = (Call_glGetTextureParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterIuiv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureParameterIuiv");
				if (glf != IntPtr.Zero)
					glGetTextureParameterIuiv  = (Call_glGetTextureParameterIuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameterIuiv));
			}

			glf = wglGetProcAddress("glGetTextureParameteriv");
			if (glf != IntPtr.Zero)
				glGetTextureParameteriv  = (Call_glGetTextureParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameteriv));
			else {
				glf = GetProcAddress(pDll,"glGetTextureParameteriv");
				if (glf != IntPtr.Zero)
					glGetTextureParameteriv  = (Call_glGetTextureParameteriv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureParameteriv));
			}

			glf = wglGetProcAddress("glCreateVertexArrays");
			if (glf != IntPtr.Zero)
				glCreateVertexArrays  = (Call_glCreateVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateVertexArrays));
			else {
				glf = GetProcAddress(pDll,"glCreateVertexArrays");
				if (glf != IntPtr.Zero)
					glCreateVertexArrays  = (Call_glCreateVertexArrays)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateVertexArrays));
			}

			glf = wglGetProcAddress("glDisableVertexArrayAttrib");
			if (glf != IntPtr.Zero)
				glDisableVertexArrayAttrib  = (Call_glDisableVertexArrayAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableVertexArrayAttrib));
			else {
				glf = GetProcAddress(pDll,"glDisableVertexArrayAttrib");
				if (glf != IntPtr.Zero)
					glDisableVertexArrayAttrib  = (Call_glDisableVertexArrayAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glDisableVertexArrayAttrib));
			}

			glf = wglGetProcAddress("glEnableVertexArrayAttrib");
			if (glf != IntPtr.Zero)
				glEnableVertexArrayAttrib  = (Call_glEnableVertexArrayAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableVertexArrayAttrib));
			else {
				glf = GetProcAddress(pDll,"glEnableVertexArrayAttrib");
				if (glf != IntPtr.Zero)
					glEnableVertexArrayAttrib  = (Call_glEnableVertexArrayAttrib)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glEnableVertexArrayAttrib));
			}

			glf = wglGetProcAddress("glVertexArrayElementBuffer");
			if (glf != IntPtr.Zero)
				glVertexArrayElementBuffer  = (Call_glVertexArrayElementBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayElementBuffer));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayElementBuffer");
				if (glf != IntPtr.Zero)
					glVertexArrayElementBuffer  = (Call_glVertexArrayElementBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayElementBuffer));
			}

			glf = wglGetProcAddress("glVertexArrayVertexBuffer");
			if (glf != IntPtr.Zero)
				glVertexArrayVertexBuffer  = (Call_glVertexArrayVertexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayVertexBuffer));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayVertexBuffer");
				if (glf != IntPtr.Zero)
					glVertexArrayVertexBuffer  = (Call_glVertexArrayVertexBuffer)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayVertexBuffer));
			}

			glf = wglGetProcAddress("glVertexArrayVertexBuffers");
			if (glf != IntPtr.Zero)
				glVertexArrayVertexBuffers  = (Call_glVertexArrayVertexBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayVertexBuffers));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayVertexBuffers");
				if (glf != IntPtr.Zero)
					glVertexArrayVertexBuffers  = (Call_glVertexArrayVertexBuffers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayVertexBuffers));
			}

			glf = wglGetProcAddress("glVertexArrayAttribBinding");
			if (glf != IntPtr.Zero)
				glVertexArrayAttribBinding  = (Call_glVertexArrayAttribBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribBinding));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayAttribBinding");
				if (glf != IntPtr.Zero)
					glVertexArrayAttribBinding  = (Call_glVertexArrayAttribBinding)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribBinding));
			}

			glf = wglGetProcAddress("glVertexArrayAttribFormat");
			if (glf != IntPtr.Zero)
				glVertexArrayAttribFormat  = (Call_glVertexArrayAttribFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayAttribFormat");
				if (glf != IntPtr.Zero)
					glVertexArrayAttribFormat  = (Call_glVertexArrayAttribFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribFormat));
			}

			glf = wglGetProcAddress("glVertexArrayAttribIFormat");
			if (glf != IntPtr.Zero)
				glVertexArrayAttribIFormat  = (Call_glVertexArrayAttribIFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribIFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayAttribIFormat");
				if (glf != IntPtr.Zero)
					glVertexArrayAttribIFormat  = (Call_glVertexArrayAttribIFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribIFormat));
			}

			glf = wglGetProcAddress("glVertexArrayAttribLFormat");
			if (glf != IntPtr.Zero)
				glVertexArrayAttribLFormat  = (Call_glVertexArrayAttribLFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribLFormat));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayAttribLFormat");
				if (glf != IntPtr.Zero)
					glVertexArrayAttribLFormat  = (Call_glVertexArrayAttribLFormat)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayAttribLFormat));
			}

			glf = wglGetProcAddress("glVertexArrayBindingDivisor");
			if (glf != IntPtr.Zero)
				glVertexArrayBindingDivisor  = (Call_glVertexArrayBindingDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayBindingDivisor));
			else {
				glf = GetProcAddress(pDll,"glVertexArrayBindingDivisor");
				if (glf != IntPtr.Zero)
					glVertexArrayBindingDivisor  = (Call_glVertexArrayBindingDivisor)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glVertexArrayBindingDivisor));
			}

			glf = wglGetProcAddress("glGetVertexArrayiv");
			if (glf != IntPtr.Zero)
				glGetVertexArrayiv  = (Call_glGetVertexArrayiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayiv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexArrayiv");
				if (glf != IntPtr.Zero)
					glGetVertexArrayiv  = (Call_glGetVertexArrayiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayiv));
			}

			glf = wglGetProcAddress("glGetVertexArrayIndexediv");
			if (glf != IntPtr.Zero)
				glGetVertexArrayIndexediv  = (Call_glGetVertexArrayIndexediv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayIndexediv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexArrayIndexediv");
				if (glf != IntPtr.Zero)
					glGetVertexArrayIndexediv  = (Call_glGetVertexArrayIndexediv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayIndexediv));
			}

			glf = wglGetProcAddress("glGetVertexArrayIndexed64iv");
			if (glf != IntPtr.Zero)
				glGetVertexArrayIndexed64iv  = (Call_glGetVertexArrayIndexed64iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayIndexed64iv));
			else {
				glf = GetProcAddress(pDll,"glGetVertexArrayIndexed64iv");
				if (glf != IntPtr.Zero)
					glGetVertexArrayIndexed64iv  = (Call_glGetVertexArrayIndexed64iv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetVertexArrayIndexed64iv));
			}

			glf = wglGetProcAddress("glCreateSamplers");
			if (glf != IntPtr.Zero)
				glCreateSamplers  = (Call_glCreateSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateSamplers));
			else {
				glf = GetProcAddress(pDll,"glCreateSamplers");
				if (glf != IntPtr.Zero)
					glCreateSamplers  = (Call_glCreateSamplers)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateSamplers));
			}

			glf = wglGetProcAddress("glCreateProgramPipelines");
			if (glf != IntPtr.Zero)
				glCreateProgramPipelines  = (Call_glCreateProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateProgramPipelines));
			else {
				glf = GetProcAddress(pDll,"glCreateProgramPipelines");
				if (glf != IntPtr.Zero)
					glCreateProgramPipelines  = (Call_glCreateProgramPipelines)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateProgramPipelines));
			}

			glf = wglGetProcAddress("glCreateQueries");
			if (glf != IntPtr.Zero)
				glCreateQueries  = (Call_glCreateQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateQueries));
			else {
				glf = GetProcAddress(pDll,"glCreateQueries");
				if (glf != IntPtr.Zero)
					glCreateQueries  = (Call_glCreateQueries)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glCreateQueries));
			}

			glf = wglGetProcAddress("glGetQueryBufferObjecti64v");
			if (glf != IntPtr.Zero)
				glGetQueryBufferObjecti64v  = (Call_glGetQueryBufferObjecti64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjecti64v));
			else {
				glf = GetProcAddress(pDll,"glGetQueryBufferObjecti64v");
				if (glf != IntPtr.Zero)
					glGetQueryBufferObjecti64v  = (Call_glGetQueryBufferObjecti64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjecti64v));
			}

			glf = wglGetProcAddress("glGetQueryBufferObjectiv");
			if (glf != IntPtr.Zero)
				glGetQueryBufferObjectiv  = (Call_glGetQueryBufferObjectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectiv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryBufferObjectiv");
				if (glf != IntPtr.Zero)
					glGetQueryBufferObjectiv  = (Call_glGetQueryBufferObjectiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectiv));
			}

			glf = wglGetProcAddress("glGetQueryBufferObjectui64v");
			if (glf != IntPtr.Zero)
				glGetQueryBufferObjectui64v  = (Call_glGetQueryBufferObjectui64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectui64v));
			else {
				glf = GetProcAddress(pDll,"glGetQueryBufferObjectui64v");
				if (glf != IntPtr.Zero)
					glGetQueryBufferObjectui64v  = (Call_glGetQueryBufferObjectui64v)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectui64v));
			}

			glf = wglGetProcAddress("glGetQueryBufferObjectuiv");
			if (glf != IntPtr.Zero)
				glGetQueryBufferObjectuiv  = (Call_glGetQueryBufferObjectuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectuiv));
			else {
				glf = GetProcAddress(pDll,"glGetQueryBufferObjectuiv");
				if (glf != IntPtr.Zero)
					glGetQueryBufferObjectuiv  = (Call_glGetQueryBufferObjectuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetQueryBufferObjectuiv));
			}

			glf = wglGetProcAddress("glMemoryBarrierByRegion");
			if (glf != IntPtr.Zero)
				glMemoryBarrierByRegion  = (Call_glMemoryBarrierByRegion)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMemoryBarrierByRegion));
			else {
				glf = GetProcAddress(pDll,"glMemoryBarrierByRegion");
				if (glf != IntPtr.Zero)
					glMemoryBarrierByRegion  = (Call_glMemoryBarrierByRegion)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMemoryBarrierByRegion));
			}

			glf = wglGetProcAddress("glGetTextureSubImage");
			if (glf != IntPtr.Zero)
				glGetTextureSubImage  = (Call_glGetTextureSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureSubImage));
			else {
				glf = GetProcAddress(pDll,"glGetTextureSubImage");
				if (glf != IntPtr.Zero)
					glGetTextureSubImage  = (Call_glGetTextureSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetTextureSubImage));
			}

			glf = wglGetProcAddress("glGetCompressedTextureSubImage");
			if (glf != IntPtr.Zero)
				glGetCompressedTextureSubImage  = (Call_glGetCompressedTextureSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTextureSubImage));
			else {
				glf = GetProcAddress(pDll,"glGetCompressedTextureSubImage");
				if (glf != IntPtr.Zero)
					glGetCompressedTextureSubImage  = (Call_glGetCompressedTextureSubImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetCompressedTextureSubImage));
			}

			glf = wglGetProcAddress("glGetGraphicsResetStatus");
			if (glf != IntPtr.Zero)
				glGetGraphicsResetStatus  = (Call_glGetGraphicsResetStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetGraphicsResetStatus));
			else {
				glf = GetProcAddress(pDll,"glGetGraphicsResetStatus");
				if (glf != IntPtr.Zero)
					glGetGraphicsResetStatus  = (Call_glGetGraphicsResetStatus)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetGraphicsResetStatus));
			}

			glf = wglGetProcAddress("glGetnCompressedTexImage");
			if (glf != IntPtr.Zero)
				glGetnCompressedTexImage  = (Call_glGetnCompressedTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnCompressedTexImage));
			else {
				glf = GetProcAddress(pDll,"glGetnCompressedTexImage");
				if (glf != IntPtr.Zero)
					glGetnCompressedTexImage  = (Call_glGetnCompressedTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnCompressedTexImage));
			}

			glf = wglGetProcAddress("glGetnTexImage");
			if (glf != IntPtr.Zero)
				glGetnTexImage  = (Call_glGetnTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnTexImage));
			else {
				glf = GetProcAddress(pDll,"glGetnTexImage");
				if (glf != IntPtr.Zero)
					glGetnTexImage  = (Call_glGetnTexImage)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnTexImage));
			}

			glf = wglGetProcAddress("glGetnUniformdv");
			if (glf != IntPtr.Zero)
				glGetnUniformdv  = (Call_glGetnUniformdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformdv));
			else {
				glf = GetProcAddress(pDll,"glGetnUniformdv");
				if (glf != IntPtr.Zero)
					glGetnUniformdv  = (Call_glGetnUniformdv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformdv));
			}

			glf = wglGetProcAddress("glGetnUniformfv");
			if (glf != IntPtr.Zero)
				glGetnUniformfv  = (Call_glGetnUniformfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformfv));
			else {
				glf = GetProcAddress(pDll,"glGetnUniformfv");
				if (glf != IntPtr.Zero)
					glGetnUniformfv  = (Call_glGetnUniformfv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformfv));
			}

			glf = wglGetProcAddress("glGetnUniformiv");
			if (glf != IntPtr.Zero)
				glGetnUniformiv  = (Call_glGetnUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformiv));
			else {
				glf = GetProcAddress(pDll,"glGetnUniformiv");
				if (glf != IntPtr.Zero)
					glGetnUniformiv  = (Call_glGetnUniformiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformiv));
			}

			glf = wglGetProcAddress("glGetnUniformuiv");
			if (glf != IntPtr.Zero)
				glGetnUniformuiv  = (Call_glGetnUniformuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformuiv));
			else {
				glf = GetProcAddress(pDll,"glGetnUniformuiv");
				if (glf != IntPtr.Zero)
					glGetnUniformuiv  = (Call_glGetnUniformuiv)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glGetnUniformuiv));
			}

			glf = wglGetProcAddress("glReadnPixels");
			if (glf != IntPtr.Zero)
				glReadnPixels  = (Call_glReadnPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadnPixels));
			else {
				glf = GetProcAddress(pDll,"glReadnPixels");
				if (glf != IntPtr.Zero)
					glReadnPixels  = (Call_glReadnPixels)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glReadnPixels));
			}

			glf = wglGetProcAddress("glTextureBarrier");
			if (glf != IntPtr.Zero)
				glTextureBarrier  = (Call_glTextureBarrier)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBarrier));
			else {
				glf = GetProcAddress(pDll,"glTextureBarrier");
				if (glf != IntPtr.Zero)
					glTextureBarrier  = (Call_glTextureBarrier)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glTextureBarrier));
			}

			glf = wglGetProcAddress("glSpecializeShader");
			if (glf != IntPtr.Zero)
				glSpecializeShader  = (Call_glSpecializeShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSpecializeShader));
			else {
				glf = GetProcAddress(pDll,"glSpecializeShader");
				if (glf != IntPtr.Zero)
					glSpecializeShader  = (Call_glSpecializeShader)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glSpecializeShader));
			}

			glf = wglGetProcAddress("glMultiDrawArraysIndirectCount");
			if (glf != IntPtr.Zero)
				glMultiDrawArraysIndirectCount  = (Call_glMultiDrawArraysIndirectCount)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArraysIndirectCount));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawArraysIndirectCount");
				if (glf != IntPtr.Zero)
					glMultiDrawArraysIndirectCount  = (Call_glMultiDrawArraysIndirectCount)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawArraysIndirectCount));
			}

			glf = wglGetProcAddress("glMultiDrawElementsIndirectCount");
			if (glf != IntPtr.Zero)
				glMultiDrawElementsIndirectCount  = (Call_glMultiDrawElementsIndirectCount)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsIndirectCount));
			else {
				glf = GetProcAddress(pDll,"glMultiDrawElementsIndirectCount");
				if (glf != IntPtr.Zero)
					glMultiDrawElementsIndirectCount  = (Call_glMultiDrawElementsIndirectCount)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glMultiDrawElementsIndirectCount));
			}

			glf = wglGetProcAddress("glPolygonOffsetClamp");
			if (glf != IntPtr.Zero)
				glPolygonOffsetClamp  = (Call_glPolygonOffsetClamp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonOffsetClamp));
			else {
				glf = GetProcAddress(pDll,"glPolygonOffsetClamp");
				if (glf != IntPtr.Zero)
					glPolygonOffsetClamp  = (Call_glPolygonOffsetClamp)Marshal.GetDelegateForFunctionPointer(glf, typeof(Call_glPolygonOffsetClamp));
			}

		bool result = FreeLibrary(pDll);
		}
	}
}
