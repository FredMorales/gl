﻿// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Windows.Forms;

namespace gl.Winforms
{
    public partial class OpenGLPanel : UserControl
    {
        public delegate void RenderEventHandler(UserControl sender, Context Context);

        public event RenderEventHandler Render;

        private Context Context { get; set; } = new Context();

        public OpenGLPanel()
        {
            InitializeComponent();
        }

        private void DoRender()
        {
            if (Render != null)
                Render(this, Context);
        }

        private void OpenGLPanel_Load(object sender, EventArgs e)
        {
            if (Handle != null)
            {
                Context?.Init(Handle);
                Context?.BeginRender(0, 0, Width, Height);
            }
        }

        private void OpenGLPanel_Paint(object sender, PaintEventArgs e)
        {
            if (Handle != null)
            {
                Context?.BeginRender(0, 0, Width, Height);
                DoRender();
                Context?.EndRender();
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {

        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseClick(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseDoubleClick(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseDown(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseEnter(e);
        }

        protected override void OnMouseHover(EventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseHover(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseLeave(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            Context?.BeginRender(0, 0, Width, Height);
            base.OnMouseWheel(e);
        }
    }
}
