﻿// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using static gl.gl;

namespace gl.Winforms
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PIXELFORMATDESCRIPTOR
    {
        public void Init()
        {
            nSize = (ushort)Marshal.SizeOf(typeof(PIXELFORMATDESCRIPTOR));
            nVersion = 1;
            dwFlags = PFD_FLAGS.PFD_DRAW_TO_WINDOW | PFD_FLAGS.PFD_SUPPORT_OPENGL | PFD_FLAGS.PFD_DOUBLEBUFFER | PFD_FLAGS.PFD_SUPPORT_COMPOSITION;
            iPixelType = PFD_PIXEL_TYPE.PFD_TYPE_RGBA;
            cColorBits = 24;
            cRedBits = cRedShift = cGreenBits = cGreenShift = cBlueBits = cBlueShift = 0;
            cAlphaBits = cAlphaShift = 0;
            cAccumBits = cAccumRedBits = cAccumGreenBits = cAccumBlueBits = cAccumAlphaBits = 0;
            cDepthBits = 32;
            cStencilBits = cAuxBuffers = 0;
            iLayerType = PFD_LAYER_TYPES.PFD_MAIN_PLANE;
            bReserved = 0;
            dwLayerMask = dwVisibleMask = dwDamageMask = 0;
        }
        ushort nSize;
        ushort nVersion;
        PFD_FLAGS dwFlags;
        PFD_PIXEL_TYPE iPixelType;
        byte cColorBits;
        byte cRedBits;
        byte cRedShift;
        byte cGreenBits;
        byte cGreenShift;
        byte cBlueBits;
        byte cBlueShift;
        byte cAlphaBits;
        byte cAlphaShift;
        byte cAccumBits;
        byte cAccumRedBits;
        byte cAccumGreenBits;
        byte cAccumBlueBits;
        byte cAccumAlphaBits;
        byte cDepthBits;
        byte cStencilBits;
        byte cAuxBuffers;
        PFD_LAYER_TYPES iLayerType;
        byte bReserved;
        uint dwLayerMask;
        uint dwVisibleMask;
        uint dwDamageMask;
    }

    [Flags]
    public enum PFD_FLAGS : uint
    {
        PFD_DOUBLEBUFFER = 0x00000001,
        PFD_STEREO = 0x00000002,
        PFD_DRAW_TO_WINDOW = 0x00000004,
        PFD_DRAW_TO_BITMAP = 0x00000008,
        PFD_SUPPORT_GDI = 0x00000010,
        PFD_SUPPORT_OPENGL = 0x00000020,
        PFD_GENERIC_FORMAT = 0x00000040,
        PFD_NEED_PALETTE = 0x00000080,
        PFD_NEED_SYSTEM_PALETTE = 0x00000100,
        PFD_SWAP_EXCHANGE = 0x00000200,
        PFD_SWAP_COPY = 0x00000400,
        PFD_SWAP_LAYER_BUFFERS = 0x00000800,
        PFD_GENERIC_ACCELERATED = 0x00001000,
        PFD_SUPPORT_DIRECTDRAW = 0x00002000,
        PFD_DIRECT3D_ACCELERATED = 0x00004000,
        PFD_SUPPORT_COMPOSITION = 0x00008000,
        PFD_DEPTH_DONTCARE = 0x20000000,
        PFD_DOUBLEBUFFER_DONTCARE = 0x40000000,
        PFD_STEREO_DONTCARE = 0x80000000
    }

    public enum PFD_LAYER_TYPES : byte
    {
        PFD_MAIN_PLANE = 0,
        PFD_OVERLAY_PLANE = 1,
        PFD_UNDERLAY_PLANE = 255
    }

    public enum PFD_PIXEL_TYPE : byte
    {
        PFD_TYPE_RGBA = 0,
        PFD_TYPE_COLORINDEX = 1
    }

    public class Context
    {
        [DllImport("opengl32.dll")]
        private static extern IntPtr wglCreateContext(IntPtr hDC);

        [DllImport("opengl32.dll")]
        private static extern bool wglMakeCurrent(IntPtr hdc, IntPtr hrc);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ValidateRect(IntPtr hWnd, IntPtr lpRect);

        [DllImport("gdi32.dll")]
        private static extern int ChoosePixelFormat(IntPtr hdc, [In] ref PIXELFORMATDESCRIPTOR ppfd);

        [DllImport("gdi32.dll")]
        private static extern bool SetPixelFormat(IntPtr hdc, int iPixelFormat, ref PIXELFORMATDESCRIPTOR ppfd);

        [DllImport("gdi32.dll")]
        private static extern bool SwapBuffers(IntPtr hdc);


        private IntPtr HWND;
        private IntPtr HDC;
        private IntPtr HGLRC;

        public void BeginRender(int x, int y, int width, int height)
        {
            wglMakeCurrent(HDC, HGLRC);
            glViewport(x, y, width, height);
        }

        public void Deinit()
        {
        }

        public void EndRender()
        {
            glFlush();
            SwapBuffers(HDC);
            ValidateRect(HWND, IntPtr.Zero);
        }
        public void Init(IntPtr Hwnd)
        {
            HWND = Hwnd;
            HDC = GetDC(HWND);

            var pixelformatdescriptor = new PIXELFORMATDESCRIPTOR();
            pixelformatdescriptor.Init();

            var pixelFormat = ChoosePixelFormat(HDC, ref pixelformatdescriptor);
            if (!SetPixelFormat(HDC, pixelFormat, ref pixelformatdescriptor))
                throw new Win32Exception(Marshal.GetLastWin32Error());
            if ((HGLRC = wglCreateContext(HDC)) == IntPtr.Zero)
                throw new Win32Exception(Marshal.GetLastWin32Error());

            wglMakeCurrent(HDC, HGLRC);
            initgl();
            wglMakeCurrent(IntPtr.Zero, IntPtr.Zero);
        }
    }
}
