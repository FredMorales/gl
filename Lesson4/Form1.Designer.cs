﻿namespace Lesson4
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openGLPanel1 = new gl.Winforms.OpenGLPanel();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openGLPanel1
            // 
            this.openGLPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openGLPanel1.Location = new System.Drawing.Point(0, 0);
            this.openGLPanel1.Name = "openGLPanel1";
            this.openGLPanel1.Size = new System.Drawing.Size(284, 261);
            this.openGLPanel1.TabIndex = 0;
            this.openGLPanel1.Render += new gl.Winforms.OpenGLPanel.RenderEventHandler(this.openGLPanel1_Render);
            this.openGLPanel1.Load += new System.EventHandler(this.openGLPanel1_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.openGLPanel1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Lesson 4";
            this.ResumeLayout(false);

        }

        #endregion

        private gl.Winforms.OpenGLPanel openGLPanel1;
        private System.Windows.Forms.Timer timer1;
    }
}

