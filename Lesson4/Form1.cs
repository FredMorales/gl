﻿// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Windows.Forms;
using static gl.gl;
using static gl.glu;

namespace Lesson4
{
    public partial class Form1 : Form
    {
        private float rtri = 0.0f;                     // Increase The Rotation Variable For The Triangle ( NEW )
        private float rquad = 0.0f;

        public Form1()
        {
            InitializeComponent();
        }

        private void openGLPanel1_Render(UserControl sender, gl.Winforms.Context Context)
        {
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer

            glMatrixMode(GL_PROJECTION);                        // Select The Projection Matrix
            glLoadIdentity();                           // Reset The Projection Matrix

            // Calculate The Aspect Ratio Of The Window
            gluPerspective(45.0f, (float)Width / (float)Height, 0.1f, 100.0f);

            glMatrixMode(GL_MODELVIEW);                     // Select The Modelview Matrix
            glLoadIdentity();                   // Reset The View
            glTranslatef(-1.5f, 0.0f, -6.0f);             // Move Into The Screen And Left

            glRotatef(rtri, 0.0f, 1.0f, 0.0f);

            glBegin(GL_TRIANGLES);                  // Start Drawing A Triangle
                glColor3f(1.0f, 0.0f, 0.0f);          // Set Top Point Of Triangle To Red
                glVertex3f(0.0f, 1.0f, 0.0f);          // First Point Of The Triangle
                glColor3f(0.0f, 1.0f, 0.0f);          // Set Left Point Of Triangle To Green
                glVertex3f(-1.0f, -1.0f, 0.0f);          // Second Point Of The Triangle
                glColor3f(0.0f, 0.0f, 1.0f);          // Set Right Point Of Triangle To Blue
                glVertex3f(1.0f, -1.0f, 0.0f);          // Third Point Of The Triangle
            glEnd();

            glLoadIdentity();                   // Reset The Current Modelview Matrix
            glTranslatef(1.5f, 0.0f, -6.0f);              // Move Right 1.5 Units And Into The Screen 6.0
            glRotatef(rquad, 1.0f, 0.0f, 0.0f);

            glColor3f(0.5f, 0.5f, 1.0f);              // Set The Color To A Nice Blue Shade
            glBegin(GL_QUADS);                  // Start Drawing A Quad
                glVertex3f(-1.0f, 1.0f, 0.0f);          // Top Left Of The Quad
                glVertex3f(1.0f, 1.0f, 0.0f);          // Top Right Of The Quad
                glVertex3f(1.0f, -1.0f, 0.0f);          // Bottom Right Of The Quad
                glVertex3f(-1.0f, -1.0f, 0.0f);          // Bottom Left Of The Quad
            glEnd();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            rtri += 0.4f;
            rquad -= 0.35f;
            openGLPanel1.Invalidate();
        }

        private void openGLPanel1_Load(object sender, EventArgs e)
        {
            glDisable(GL_LIGHTING);
            glShadeModel(GL_SMOOTH);

            glClearDepth(1.0f);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);
        }
    }
}
